Imports System.data.SqlClient
Imports System.Data


Public Module Principal_PVE
    Dim usua As Usuario_Logeado
    Public GetSettingConexion As String
    Dim Usuario As New Usuario_Logeado
    Public Sucursal As Integer = 0
    Public ArticuloGenerico As String = ""

    Sub Main()
        'VerificacionRegistrosSistema()
        Dim login As New Frm_login
        Try
            If Environment.GetCommandLineArgs.Length > 1 Then
                AutoLoggin_Usuario(Environment.GetCommandLineArgs(1))
                VerificandoAcceso_a_Modulos("Abono_Apartados", "Abonos de Apartados", usua.Cedula, "Operaciones Ventas")
                VerificandoAcceso_a_Modulos("Apartados", "Apartados de Clienes", usua.Cedula, "Operaciones Ventas")
                VerificandoAcceso_a_Modulos("ReciboMontoDinero", "Recibo Monto Dinero", usua.Cedula, "Operaciones Cuentas por Cobrar")
                If Not Seguridad.VerificandoAcceso_a_Modulos("FacturacionPVE", "Facturaci�n PVE", Usuario.Cedula, "Operaciones Ventas") Then
                    MsgBox("No tiene permiso ejecutar el m�dulo de Facturaci�n en Punto de Venta.", MsgBoxStyle.Information, "Atenci�n...")
                    Exit Sub
                End If
                Application.Run(New FacturacionPVE(Usuario))

            Else
                login.ShowDialog()
                If login.conectado Then
                    usua = login.Usuario
                    VerificandoAcceso_a_Modulos("Abono_Apartados", "Abonos de Apartados", usua.Cedula, "Operaciones Ventas")
                    VerificandoAcceso_a_Modulos("Apartados", "Apartados de Clienes", usua.Cedula, "Operaciones Ventas")
                    VerificandoAcceso_a_Modulos("ReciboMontoDinero", "Recibo Monto Dinero", usua.Cedula, "Operaciones Cuentas por Cobrar")
                    If Not VerificandoAcceso_a_Modulos("FacturacionPVE", "Facturaci�n PVE", usua.Cedula, "Operaciones Ventas") Then
                        MsgBox("No tiene permiso ejecutar el m�dulo de Facturaci�n en Punto de Venta.", MsgBoxStyle.Information, "Atenci�n...")
                        Exit Sub
                    End If
                    Application.Run(New FacturacionPVE(login.Usuario))
                End If
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    'diego
    Function AutoLoggin_Usuario(Optional ByVal UsuarioConectado As String = "") As Boolean
        Dim Tabla As New DataTable
        Try
            cFunciones.Llenar_Tabla_Generico("Select * from usuarios", Tabla)
            Dim Usuario_autorizadores() As System.Data.DataRow
            Dim Usua As System.Data.DataRow
            Usuario_autorizadores = Tabla.Select("Cedula = '" & UsuarioConectado & "'")
            Usua = Usuario_autorizadores(0)

            Usuario.Cedula = Usua("Cedula")
            Usuario.Nombre = Usua("Nombre")
            Usuario.Clave_Entrada = Usua("Clave_Entrada")
            Usuario.Clave_Interna = Usua("Clave_Interna")
            Usuario.CambiarPrecio = Usua("CambiarPrecio")
            Usuario.Aplicar_Desc = Usua("Aplicar_Desc")
            Usuario.Exist_Negativa = Usua("Exist_Negativa")
            Usuario.Porc_Desc = Usua("Porc_Desc")
            Usuario.Porc_Precio = Usua("Porc_Precio")
        Catch ex As SystemException
            MsgBox(ex.Message)
        End Try

    End Function

    Public Sub VerificacionRegistrosSistema()  'JSA 20072006
        GetSettingConexion = GetSetting("SeeSOFT", "SeePOS", "CONEXION")
        If GetSettingConexion = "" Then
            Dim FormConexion As New EnlaceServidor.FormConexion
            FormConexion.Left = (Screen.PrimaryScreen.WorkingArea.Width - FormConexion.Width) \ 2
            FormConexion.Top = (Screen.PrimaryScreen.WorkingArea.Height - FormConexion.Height) \ 2
            FormConexion.ShowDialog()
        End If
    End Sub
    Public Sub EstadoSevidor_Enlace()  'JSA 20072006
        Dim Conexiones As New Conexion
        Dim ReintestoFallidos As Int16
Reintentos:
        Try
            '            Espera.Caption = "Establecer Conexi�n con el servidor..."
            ReintestoFallidos += 1
            Conexiones.Conectar()
            '          Espera.Caption = "Conexion establecida con [" & Conexiones.sQlconexion.DataSource & "]"
        Catch ex As Exception
            '           Espera.Caption = "Conexi�n NO establecida con el servidor..."
            If ReintestoFallidos < 3 Then
                If MsgBox("Conexi�n no estabecida con el Servidor..." & vbCrLf & "Desea intentar establecer conexion nuevamente...", MsgBoxStyle.YesNo + MsgBoxStyle.Critical, "") = MsgBoxResult.Yes Then GoTo Reintentos
            Else
                Dim FormConexion As New EnlaceServidor.FormConexion
                FormConexion.Left = (Screen.PrimaryScreen.WorkingArea.Width - FormConexion.Width) \ 2
                FormConexion.Top = (Screen.PrimaryScreen.WorkingArea.Height - FormConexion.Height) \ 2
                FormConexion.ShowDialog()
            End If

        End Try
    End Sub
End Module
