﻿Imports System.Resources

Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' La información general sobre un ensamblado se controla mediante el siguiente 
' conjunto de atributos. Cambie estos atributos para modificar la información
' asociada con un ensamblado.

' Revisar los valores de los atributos del ensamblado

<Assembly: AssemblyTitle("SeePos PVE 5.3")> 
<Assembly: AssemblyDescription("Para la pequeña y mediana empresa")> 
<Assembly: AssemblyCompany("SeeSoft")> 
<Assembly: AssemblyProduct("SeePOS PVE 5.3")> 
<Assembly: AssemblyCopyright("© SeeSoft 2011")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)> 

'El siguiente GUID sirve como identificador de la biblioteca de tipos si este proyecto se expone a COM
<Assembly: Guid("0bb18da6-18cd-41df-a977-53fcd47d343a")> 

' La información de versión de un ensamblado consta de los cuatro valores siguientes:
'
'      Versión principal
'      Versión secundaria 
'      Número de versión de compilación
'      Revisión
'
' Puede especificar todos los valores o establecer como predeterminados los números de versión de compilación y de revisión 
' mediante el asterisco ('*'), como se muestra a continuación:

<Assembly: AssemblyVersion("5.3.1.0")> 
<Assembly: AssemblyFileVersion("5.3.1.0")> 

<Assembly: NeutralResourcesLanguageAttribute("es-CR")> 