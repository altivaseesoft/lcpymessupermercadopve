﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPromociones
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.Label1 = New System.Windows.Forms.Label
        Me.ComboBox1 = New System.Windows.Forms.ComboBox
        Me.bsPromociones = New System.Windows.Forms.BindingSource(Me.components)
        Me.DtsPremiosLealtad = New SeePOS_PVE.dtsPremiosLealtad
        Me.btAceptar = New System.Windows.Forms.Button
        Me.btCancelar = New System.Windows.Forms.Button
        Me.Tb_S_PromocionesTableAdapter = New SeePOS_PVE.dtsPremiosLealtadTableAdapters.tb_S_PromocionesTableAdapter
        CType(Me.bsPromociones, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DtsPremiosLealtad, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(13, 29)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(324, 18)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Estas son la promociones disponibles para el cliente : "
        '
        'ComboBox1
        '
        Me.ComboBox1.DataSource = Me.bsPromociones
        Me.ComboBox1.DisplayMember = "Nombre"
        Me.ComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(26, 72)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(295, 21)
        Me.ComboBox1.TabIndex = 1
        Me.ComboBox1.ValueMember = "Id"
        '
        'bsPromociones
        '
        Me.bsPromociones.DataMember = "tb_S_Promociones"
        Me.bsPromociones.DataSource = Me.DtsPremiosLealtad
        '
        'DtsPremiosLealtad
        '
        Me.DtsPremiosLealtad.DataSetName = "dtsPremiosLealtad"
        Me.DtsPremiosLealtad.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'btAceptar
        '
        Me.btAceptar.Location = New System.Drawing.Point(43, 159)
        Me.btAceptar.Name = "btAceptar"
        Me.btAceptar.Size = New System.Drawing.Size(75, 23)
        Me.btAceptar.TabIndex = 2
        Me.btAceptar.Text = "Aceptar"
        Me.btAceptar.UseVisualStyleBackColor = True
        '
        'btCancelar
        '
        Me.btCancelar.Location = New System.Drawing.Point(225, 159)
        Me.btCancelar.Name = "btCancelar"
        Me.btCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btCancelar.TabIndex = 3
        Me.btCancelar.Text = "Cancelar"
        Me.btCancelar.UseVisualStyleBackColor = True
        '
        'Tb_S_PromocionesTableAdapter
        '
        Me.Tb_S_PromocionesTableAdapter.ClearBeforeFill = True
        '
        'frmPromociones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(349, 212)
        Me.Controls.Add(Me.btCancelar)
        Me.Controls.Add(Me.btAceptar)
        Me.Controls.Add(Me.ComboBox1)
        Me.Controls.Add(Me.Label1)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(365, 250)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(365, 250)
        Me.Name = "frmPromociones"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Eliga Promoción:"
        CType(Me.bsPromociones, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DtsPremiosLealtad, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents btAceptar As System.Windows.Forms.Button
    Friend WithEvents btCancelar As System.Windows.Forms.Button
    Friend WithEvents bsPromociones As System.Windows.Forms.BindingSource
    Friend WithEvents DtsPremiosLealtad As SeePOS_PVE.dtsPremiosLealtad
    Friend WithEvents Tb_S_PromocionesTableAdapter As SeePOS_PVE.dtsPremiosLealtadTableAdapters.tb_S_PromocionesTableAdapter
End Class
