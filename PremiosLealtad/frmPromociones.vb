﻿Public Class frmPromociones

    Public pubAcumuladoVentas As Double
    Public pubIdPromocion As Integer
    Private Sub frmPromociones_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        spIniciarForm()
    End Sub
   
    Private Sub spIniciarForm()

        Dim sql As New SqlClient.SqlCommand
        Try
            sql.CommandText = "select * from [tb_S_Promociones] where (MontoAcumulado <= @Acumulado) AND (Estado = 0)"
            sql.Parameters.AddWithValue("@Acumulado", pubAcumuladoVentas)
            clsEnlace.spCargarDatos(sql, DtsPremiosLealtad.tb_S_Promociones)

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
      
    End Sub

    Private Sub btAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btAceptar.Click
        pubIdPromocion = bsPromociones.Current("Id")
        DialogResult = Windows.Forms.DialogResult.Yes
    End Sub

    Private Sub btCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btCancelar.Click
        DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub
End Class