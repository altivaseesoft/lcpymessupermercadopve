Imports System.Drawing.Printing
Imports System.Data.SqlClient

Public Class ReciboMontoDinero
    Inherits FrmPlantilla

#Region "Variables"
    Public Cod_Cliente, Cod_Moneda As Integer
    Public Trans As SqlTransaction
    Dim PMU As New PerfilModulo_Class
    Dim Tabla As New DataTable
    Dim FrmVuelto As New Vuelto
    Dim Usua As Object
    Dim TopBuscador As Integer = 50
#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "

    Public Sub New()
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()

        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()

    End Sub

    Public Sub New(ByVal Usuario_Parametro As Object)
        MyBase.New()

        'El Dise�ador de Windows Forms requiere esta llamada.
        InitializeComponent()
        usua = Usuario_Parametro
        'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
    End Sub

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
    'Puede modificarse utilizando el Dise�ador de Windows Forms. 
    'No lo modifique con el editor de c�digo.
    Friend WithEvents AdapterAbonoccobrar As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents AdapterDetalleAbonocCobrar As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents txtUsuario As System.Windows.Forms.TextBox
    Friend WithEvents txtNombreUsuario As System.Windows.Forms.Label
    Friend WithEvents AdapterMoneda As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand3 As System.Data.SqlClient.SqlCommand
    Friend WithEvents DsRecibos1 As SeePOS_PVE.DsRecibos
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents txtCodigo As System.Windows.Forms.TextBox
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtNombre As System.Windows.Forms.TextBox
    Friend WithEvents txtNum_Recibo As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents dtFecha As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents txtTipoCambio As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents ComboMoneda As System.Windows.Forms.ComboBox
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtSaldoAct As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtAbono As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtSaldoAnt As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txtCedulaUsuario As System.Windows.Forms.Label
    Friend WithEvents Label_Id_Recibo As System.Windows.Forms.Label
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand2 As System.Data.SqlClient.SqlCommand
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ReciboMontoDinero))
        Me.AdapterAbonoccobrar = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.AdapterDetalleAbonocCobrar = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlInsertCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand2 = New System.Data.SqlClient.SqlCommand
        Me.Label36 = New System.Windows.Forms.Label
        Me.txtUsuario = New System.Windows.Forms.TextBox
        Me.txtNombreUsuario = New System.Windows.Forms.Label
        Me.AdapterMoneda = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlSelectCommand3 = New System.Data.SqlClient.SqlCommand
        Me.DsRecibos1 = New SeePOS_PVE.DsRecibos
        Me.GroupBox6 = New System.Windows.Forms.GroupBox
        Me.txtCodigo = New System.Windows.Forms.TextBox
        Me.Label37 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtNombre = New System.Windows.Forms.TextBox
        Me.txtNum_Recibo = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.dtFecha = New System.Windows.Forms.DateTimePicker
        Me.Label19 = New System.Windows.Forms.Label
        Me.txtTipoCambio = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.ComboMoneda = New System.Windows.Forms.ComboBox
        Me.Label30 = New System.Windows.Forms.Label
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.Label15 = New System.Windows.Forms.Label
        Me.txtSaldoAct = New DevExpress.XtraEditors.TextEdit
        Me.Label18 = New System.Windows.Forms.Label
        Me.txtAbono = New DevExpress.XtraEditors.TextEdit
        Me.Label16 = New System.Windows.Forms.Label
        Me.txtSaldoAnt = New DevExpress.XtraEditors.TextEdit
        Me.Label17 = New System.Windows.Forms.Label
        Me.txtCedulaUsuario = New System.Windows.Forms.Label
        Me.Label_Id_Recibo = New System.Windows.Forms.Label
        CType(Me.DsRecibos1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.txtSaldoAct.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAbono.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSaldoAnt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ToolBarNuevo
        '
        Me.ToolBarNuevo.Visible = False
        '
        'ToolBarBuscar
        '
        Me.ToolBarBuscar.Visible = False
        '
        'ToolBarRegistrar
        '
        Me.ToolBarRegistrar.Enabled = False
        '
        'ToolBarEliminar
        '
        Me.ToolBarEliminar.Enabled = False
        Me.ToolBarEliminar.Text = "Anular"
        Me.ToolBarEliminar.Visible = False
        '
        'ToolBarImprimir
        '
        Me.ToolBarImprimir.Enabled = False
        Me.ToolBarImprimir.Visible = False
        '
        'ImageList
        '
        Me.ImageList.ImageStream = CType(resources.GetObject("ImageList.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList.Images.SetKeyName(0, "")
        Me.ImageList.Images.SetKeyName(1, "")
        Me.ImageList.Images.SetKeyName(2, "")
        Me.ImageList.Images.SetKeyName(3, "")
        Me.ImageList.Images.SetKeyName(4, "")
        Me.ImageList.Images.SetKeyName(5, "")
        Me.ImageList.Images.SetKeyName(6, "")
        Me.ImageList.Images.SetKeyName(7, "")
        Me.ImageList.Images.SetKeyName(8, "")
        '
        'ToolBar1
        '
        Me.ToolBar1.Location = New System.Drawing.Point(0, 204)
        Me.ToolBar1.Size = New System.Drawing.Size(546, 52)
        '
        'DataNavigator
        '
        Me.DataNavigator.Buttons.Append.Visible = False
        Me.DataNavigator.Buttons.CancelEdit.Visible = False
        Me.DataNavigator.Buttons.EndEdit.Visible = False
        Me.DataNavigator.Buttons.Remove.Visible = False
        Me.DataNavigator.Location = New System.Drawing.Point(560, 234)
        Me.DataNavigator.Visible = False
        '
        'TituloModulo
        '
        Me.TituloModulo.Size = New System.Drawing.Size(546, 32)
        Me.TituloModulo.Text = "Recibo de Dinero"
        '
        'AdapterAbonoccobrar
        '
        Me.AdapterAbonoccobrar.DeleteCommand = Me.SqlDeleteCommand1
        Me.AdapterAbonoccobrar.InsertCommand = Me.SqlInsertCommand1
        Me.AdapterAbonoccobrar.SelectCommand = Me.SqlSelectCommand1
        Me.AdapterAbonoccobrar.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "abonoccobrar", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Id_Recibo", "Id_Recibo"), New System.Data.Common.DataColumnMapping("Num_Recibo", "Num_Recibo"), New System.Data.Common.DataColumnMapping("Cod_Cliente", "Cod_Cliente"), New System.Data.Common.DataColumnMapping("Nombre_Cliente", "Nombre_Cliente"), New System.Data.Common.DataColumnMapping("Saldo_Cuenta", "Saldo_Cuenta"), New System.Data.Common.DataColumnMapping("Monto", "Monto"), New System.Data.Common.DataColumnMapping("Saldo_Actual", "Saldo_Actual"), New System.Data.Common.DataColumnMapping("Fecha", "Fecha"), New System.Data.Common.DataColumnMapping("Observaciones", "Observaciones"), New System.Data.Common.DataColumnMapping("Anula", "Anula"), New System.Data.Common.DataColumnMapping("Ced_Usuario", "Ced_Usuario"), New System.Data.Common.DataColumnMapping("Contabilizado", "Contabilizado"), New System.Data.Common.DataColumnMapping("Asiento", "Asiento"), New System.Data.Common.DataColumnMapping("Cod_Moneda", "Cod_Moneda")})})
        Me.AdapterAbonoccobrar.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = resources.GetString("SqlDeleteCommand1.CommandText")
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand1.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_Id_Recibo", System.Data.SqlDbType.BigInt, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Recibo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Num_Recibo", System.Data.SqlDbType.BigInt, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Num_Recibo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Cod_Cliente", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cod_Cliente", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Nombre_Cliente", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre_Cliente", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Saldo_Cuenta", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Saldo_Cuenta", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Monto", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Monto", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Saldo_Actual", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Saldo_Actual", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Observaciones", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Observaciones", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Anula", System.Data.SqlDbType.Bit, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anula", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Ced_Usuario", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Ced_Usuario", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Contabilizado", System.Data.SqlDbType.Bit, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Contabilizado", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Asiento", System.Data.SqlDbType.BigInt, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Asiento", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Cod_Moneda", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cod_Moneda", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "workstation id=OSCAR;packet size=4096;integrated security=SSPI;data source=OSCAR;" & _
            "persist security info=False;initial catalog=SeePos"
        Me.SqlConnection1.FireInfoMessageEventOnUserErrors = False
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = resources.GetString("SqlInsertCommand1.CommandText")
        Me.SqlInsertCommand1.Connection = Me.SqlConnection1
        Me.SqlInsertCommand1.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Num_Recibo", System.Data.SqlDbType.BigInt, 0, "Num_Recibo"), New System.Data.SqlClient.SqlParameter("@Cod_Cliente", System.Data.SqlDbType.Int, 0, "Cod_Cliente"), New System.Data.SqlClient.SqlParameter("@Nombre_Cliente", System.Data.SqlDbType.VarChar, 0, "Nombre_Cliente"), New System.Data.SqlClient.SqlParameter("@Saldo_Cuenta", System.Data.SqlDbType.Float, 0, "Saldo_Cuenta"), New System.Data.SqlClient.SqlParameter("@Monto", System.Data.SqlDbType.Float, 0, "Monto"), New System.Data.SqlClient.SqlParameter("@Saldo_Actual", System.Data.SqlDbType.Float, 0, "Saldo_Actual"), New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 0, "Fecha"), New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.VarChar, 0, "Observaciones"), New System.Data.SqlClient.SqlParameter("@Anula", System.Data.SqlDbType.Bit, 0, "Anula"), New System.Data.SqlClient.SqlParameter("@Ced_Usuario", System.Data.SqlDbType.VarChar, 0, "Ced_Usuario"), New System.Data.SqlClient.SqlParameter("@Contabilizado", System.Data.SqlDbType.Bit, 0, "Contabilizado"), New System.Data.SqlClient.SqlParameter("@Asiento", System.Data.SqlDbType.BigInt, 0, "Asiento"), New System.Data.SqlClient.SqlParameter("@Cod_Moneda", System.Data.SqlDbType.Int, 0, "Cod_Moneda")})
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT     abonoccobrar.*" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "FROM         abonoccobrar"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = resources.GetString("SqlUpdateCommand1.CommandText")
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand1.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Num_Recibo", System.Data.SqlDbType.BigInt, 0, "Num_Recibo"), New System.Data.SqlClient.SqlParameter("@Cod_Cliente", System.Data.SqlDbType.Int, 0, "Cod_Cliente"), New System.Data.SqlClient.SqlParameter("@Nombre_Cliente", System.Data.SqlDbType.VarChar, 0, "Nombre_Cliente"), New System.Data.SqlClient.SqlParameter("@Saldo_Cuenta", System.Data.SqlDbType.Float, 0, "Saldo_Cuenta"), New System.Data.SqlClient.SqlParameter("@Monto", System.Data.SqlDbType.Float, 0, "Monto"), New System.Data.SqlClient.SqlParameter("@Saldo_Actual", System.Data.SqlDbType.Float, 0, "Saldo_Actual"), New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 0, "Fecha"), New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.VarChar, 0, "Observaciones"), New System.Data.SqlClient.SqlParameter("@Anula", System.Data.SqlDbType.Bit, 0, "Anula"), New System.Data.SqlClient.SqlParameter("@Ced_Usuario", System.Data.SqlDbType.VarChar, 0, "Ced_Usuario"), New System.Data.SqlClient.SqlParameter("@Contabilizado", System.Data.SqlDbType.Bit, 0, "Contabilizado"), New System.Data.SqlClient.SqlParameter("@Asiento", System.Data.SqlDbType.BigInt, 0, "Asiento"), New System.Data.SqlClient.SqlParameter("@Cod_Moneda", System.Data.SqlDbType.Int, 0, "Cod_Moneda"), New System.Data.SqlClient.SqlParameter("@Original_Id_Recibo", System.Data.SqlDbType.BigInt, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Recibo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Num_Recibo", System.Data.SqlDbType.BigInt, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Num_Recibo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Cod_Cliente", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cod_Cliente", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Nombre_Cliente", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre_Cliente", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Saldo_Cuenta", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Saldo_Cuenta", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Monto", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Monto", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Saldo_Actual", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Saldo_Actual", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Observaciones", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Observaciones", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Anula", System.Data.SqlDbType.Bit, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anula", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Ced_Usuario", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Ced_Usuario", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Contabilizado", System.Data.SqlDbType.Bit, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Contabilizado", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Asiento", System.Data.SqlDbType.BigInt, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Asiento", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Cod_Moneda", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cod_Moneda", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Id_Recibo", System.Data.SqlDbType.BigInt, 8, "Id_Recibo")})
        '
        'AdapterDetalleAbonocCobrar
        '
        Me.AdapterDetalleAbonocCobrar.DeleteCommand = Me.SqlDeleteCommand2
        Me.AdapterDetalleAbonocCobrar.InsertCommand = Me.SqlInsertCommand2
        Me.AdapterDetalleAbonocCobrar.SelectCommand = Me.SqlSelectCommand2
        Me.AdapterDetalleAbonocCobrar.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "detalle_abonoccobrar", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Consecutivo", "Consecutivo"), New System.Data.Common.DataColumnMapping("Id_Recibo", "Id_Recibo"), New System.Data.Common.DataColumnMapping("Factura", "Factura"), New System.Data.Common.DataColumnMapping("Tipo", "Tipo"), New System.Data.Common.DataColumnMapping("Monto", "Monto"), New System.Data.Common.DataColumnMapping("Saldo_Ant", "Saldo_Ant"), New System.Data.Common.DataColumnMapping("Intereses", "Intereses"), New System.Data.Common.DataColumnMapping("Abono", "Abono"), New System.Data.Common.DataColumnMapping("Abono_SuMoneda", "Abono_SuMoneda"), New System.Data.Common.DataColumnMapping("Saldo", "Saldo")})})
        Me.AdapterDetalleAbonocCobrar.UpdateCommand = Me.SqlUpdateCommand2
        '
        'SqlDeleteCommand2
        '
        Me.SqlDeleteCommand2.CommandText = resources.GetString("SqlDeleteCommand2.CommandText")
        Me.SqlDeleteCommand2.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand2.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_Consecutivo", System.Data.SqlDbType.BigInt, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Consecutivo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Id_Recibo", System.Data.SqlDbType.BigInt, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Recibo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Factura", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Factura", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Tipo", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Tipo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Monto", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Monto", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Saldo_Ant", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Saldo_Ant", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Intereses", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Intereses", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Abono", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Abono", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Abono_SuMoneda", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Abono_SuMoneda", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Saldo", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Saldo", System.Data.DataRowVersion.Original, Nothing)})
        '
        'SqlInsertCommand2
        '
        Me.SqlInsertCommand2.CommandText = resources.GetString("SqlInsertCommand2.CommandText")
        Me.SqlInsertCommand2.Connection = Me.SqlConnection1
        Me.SqlInsertCommand2.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Id_Recibo", System.Data.SqlDbType.BigInt, 0, "Id_Recibo"), New System.Data.SqlClient.SqlParameter("@Factura", System.Data.SqlDbType.Float, 0, "Factura"), New System.Data.SqlClient.SqlParameter("@Tipo", System.Data.SqlDbType.VarChar, 0, "Tipo"), New System.Data.SqlClient.SqlParameter("@Monto", System.Data.SqlDbType.Float, 0, "Monto"), New System.Data.SqlClient.SqlParameter("@Saldo_Ant", System.Data.SqlDbType.Float, 0, "Saldo_Ant"), New System.Data.SqlClient.SqlParameter("@Intereses", System.Data.SqlDbType.Float, 0, "Intereses"), New System.Data.SqlClient.SqlParameter("@Abono", System.Data.SqlDbType.Float, 0, "Abono"), New System.Data.SqlClient.SqlParameter("@Abono_SuMoneda", System.Data.SqlDbType.Float, 0, "Abono_SuMoneda"), New System.Data.SqlClient.SqlParameter("@Saldo", System.Data.SqlDbType.Float, 0, "Saldo")})
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = "SELECT     detalle_abonoccobrar.*" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "FROM         detalle_abonoccobrar"
        Me.SqlSelectCommand2.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand2
        '
        Me.SqlUpdateCommand2.CommandText = resources.GetString("SqlUpdateCommand2.CommandText")
        Me.SqlUpdateCommand2.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand2.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Id_Recibo", System.Data.SqlDbType.BigInt, 0, "Id_Recibo"), New System.Data.SqlClient.SqlParameter("@Factura", System.Data.SqlDbType.Float, 0, "Factura"), New System.Data.SqlClient.SqlParameter("@Tipo", System.Data.SqlDbType.VarChar, 0, "Tipo"), New System.Data.SqlClient.SqlParameter("@Monto", System.Data.SqlDbType.Float, 0, "Monto"), New System.Data.SqlClient.SqlParameter("@Saldo_Ant", System.Data.SqlDbType.Float, 0, "Saldo_Ant"), New System.Data.SqlClient.SqlParameter("@Intereses", System.Data.SqlDbType.Float, 0, "Intereses"), New System.Data.SqlClient.SqlParameter("@Abono", System.Data.SqlDbType.Float, 0, "Abono"), New System.Data.SqlClient.SqlParameter("@Abono_SuMoneda", System.Data.SqlDbType.Float, 0, "Abono_SuMoneda"), New System.Data.SqlClient.SqlParameter("@Saldo", System.Data.SqlDbType.Float, 0, "Saldo"), New System.Data.SqlClient.SqlParameter("@Original_Consecutivo", System.Data.SqlDbType.BigInt, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Consecutivo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Id_Recibo", System.Data.SqlDbType.BigInt, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Recibo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Factura", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Factura", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Tipo", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Tipo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Monto", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Monto", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Saldo_Ant", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Saldo_Ant", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Intereses", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Intereses", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Abono", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Abono", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Abono_SuMoneda", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Abono_SuMoneda", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Saldo", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Saldo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Consecutivo", System.Data.SqlDbType.BigInt, 8, "Consecutivo")})
        '
        'Label36
        '
        Me.Label36.BackColor = System.Drawing.Color.RoyalBlue
        Me.Label36.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.ForeColor = System.Drawing.Color.White
        Me.Label36.Location = New System.Drawing.Point(376, 240)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(72, 13)
        Me.Label36.TabIndex = 71
        Me.Label36.Text = "Usuario->"
        Me.Label36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtUsuario
        '
        Me.txtUsuario.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtUsuario.Enabled = False
        Me.txtUsuario.ForeColor = System.Drawing.Color.Blue
        Me.txtUsuario.Location = New System.Drawing.Point(448, 240)
        Me.txtUsuario.Name = "txtUsuario"
        Me.txtUsuario.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtUsuario.Size = New System.Drawing.Size(96, 13)
        Me.txtUsuario.TabIndex = 72
        '
        'txtNombreUsuario
        '
        Me.txtNombreUsuario.BackColor = System.Drawing.Color.Transparent
        Me.txtNombreUsuario.Enabled = False
        Me.txtNombreUsuario.ForeColor = System.Drawing.Color.Blue
        Me.txtNombreUsuario.Location = New System.Drawing.Point(376, 224)
        Me.txtNombreUsuario.Name = "txtNombreUsuario"
        Me.txtNombreUsuario.Size = New System.Drawing.Size(163, 13)
        Me.txtNombreUsuario.TabIndex = 73
        '
        'AdapterMoneda
        '
        Me.AdapterMoneda.SelectCommand = Me.SqlSelectCommand3
        Me.AdapterMoneda.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Monedas", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("CodMoneda", "CodMoneda"), New System.Data.Common.DataColumnMapping("MonedaNombre", "MonedaNombre"), New System.Data.Common.DataColumnMapping("ValorCompra", "ValorCompra"), New System.Data.Common.DataColumnMapping("ValorVenta", "ValorVenta"), New System.Data.Common.DataColumnMapping("Simbolo", "Simbolo")})})
        '
        'SqlSelectCommand3
        '
        Me.SqlSelectCommand3.CommandText = "SELECT CodMoneda, MonedaNombre, ValorCompra, ValorVenta, Simbolo FROM Monedas"
        Me.SqlSelectCommand3.Connection = Me.SqlConnection1
        '
        'DsRecibos1
        '
        Me.DsRecibos1.DataSetName = "DsRecibos"
        Me.DsRecibos1.Locale = New System.Globalization.CultureInfo("es-CR")
        Me.DsRecibos1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GroupBox6
        '
        Me.GroupBox6.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox6.Controls.Add(Me.txtCodigo)
        Me.GroupBox6.Controls.Add(Me.Label37)
        Me.GroupBox6.Controls.Add(Me.Label5)
        Me.GroupBox6.Controls.Add(Me.Label2)
        Me.GroupBox6.Controls.Add(Me.txtNombre)
        Me.GroupBox6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox6.ForeColor = System.Drawing.Color.RoyalBlue
        Me.GroupBox6.Location = New System.Drawing.Point(8, 40)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(536, 56)
        Me.GroupBox6.TabIndex = 74
        Me.GroupBox6.TabStop = False
        '
        'txtCodigo
        '
        Me.txtCodigo.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodigo.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsRecibos1, "abonoccobrar.Cod_Cliente", True))
        Me.txtCodigo.ForeColor = System.Drawing.Color.Blue
        Me.txtCodigo.Location = New System.Drawing.Point(13, 32)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.Size = New System.Drawing.Size(59, 13)
        Me.txtCodigo.TabIndex = 0
        '
        'Label37
        '
        Me.Label37.BackColor = System.Drawing.Color.RoyalBlue
        Me.Label37.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label37.ForeColor = System.Drawing.Color.White
        Me.Label37.Location = New System.Drawing.Point(0, -1)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(528, 16)
        Me.Label37.TabIndex = 157
        Me.Label37.Text = "Datos del Cliente"
        Me.Label37.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label5.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(80, 16)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(448, 12)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Nombre del Cliente"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label2.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(12, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(60, 12)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "C�digo"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtNombre
        '
        Me.txtNombre.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNombre.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsRecibos1, "abonoccobrar.Nombre_Cliente", True))
        Me.txtNombre.Enabled = False
        Me.txtNombre.ForeColor = System.Drawing.Color.Blue
        Me.txtNombre.Location = New System.Drawing.Point(80, 32)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(448, 13)
        Me.txtNombre.TabIndex = 1
        '
        'txtNum_Recibo
        '
        Me.txtNum_Recibo.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtNum_Recibo.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsRecibos1, "abonoccobrar.Num_Recibo", True))
        Me.txtNum_Recibo.Enabled = False
        Me.txtNum_Recibo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNum_Recibo.Location = New System.Drawing.Point(88, 16)
        Me.txtNum_Recibo.Name = "txtNum_Recibo"
        Me.txtNum_Recibo.Size = New System.Drawing.Size(72, 13)
        Me.txtNum_Recibo.TabIndex = 161
        Me.txtNum_Recibo.Text = "0"
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.White
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label1.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(16, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(64, 12)
        Me.Label1.TabIndex = 160
        Me.Label1.Text = "Recibo N�"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.dtFecha)
        Me.GroupBox1.Controls.Add(Me.Label19)
        Me.GroupBox1.Controls.Add(Me.txtTipoCambio)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.ComboMoneda)
        Me.GroupBox1.Controls.Add(Me.Label30)
        Me.GroupBox1.ForeColor = System.Drawing.Color.RoyalBlue
        Me.GroupBox1.Location = New System.Drawing.Point(8, 96)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(200, 80)
        Me.GroupBox1.TabIndex = 181
        Me.GroupBox1.TabStop = False
        '
        'dtFecha
        '
        Me.dtFecha.Checked = False
        Me.dtFecha.CustomFormat = "dd/MMMM/yyyy"
        Me.dtFecha.Enabled = False
        Me.dtFecha.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtFecha.Location = New System.Drawing.Point(92, 51)
        Me.dtFecha.Name = "dtFecha"
        Me.dtFecha.Size = New System.Drawing.Size(96, 20)
        Me.dtFecha.TabIndex = 182
        Me.dtFecha.Value = New Date(2006, 3, 23, 0, 0, 0, 0)
        '
        'Label19
        '
        Me.Label19.BackColor = System.Drawing.Color.Transparent
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label19.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label19.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label19.Location = New System.Drawing.Point(12, 32)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(72, 16)
        Me.Label19.TabIndex = 186
        Me.Label19.Text = "Tipo Cambio"
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtTipoCambio
        '
        Me.txtTipoCambio.BackColor = System.Drawing.Color.White
        Me.txtTipoCambio.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.txtTipoCambio.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsRecibos1, "Monedas.ValorVenta", True))
        Me.txtTipoCambio.Enabled = False
        Me.txtTipoCambio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTipoCambio.ForeColor = System.Drawing.Color.RoyalBlue
        Me.txtTipoCambio.Location = New System.Drawing.Point(92, 33)
        Me.txtTipoCambio.Name = "txtTipoCambio"
        Me.txtTipoCambio.Size = New System.Drawing.Size(96, 16)
        Me.txtTipoCambio.TabIndex = 185
        Me.txtTipoCambio.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label3.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(12, 53)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(72, 16)
        Me.Label3.TabIndex = 184
        Me.Label3.Text = "Fecha"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'ComboMoneda
        '
        Me.ComboMoneda.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.DsRecibos1, "abonoccobrar.Cod_Moneda", True))
        Me.ComboMoneda.DataSource = Me.DsRecibos1.Monedas
        Me.ComboMoneda.DisplayMember = "MonedaNombre"
        Me.ComboMoneda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboMoneda.Enabled = False
        Me.ComboMoneda.ForeColor = System.Drawing.Color.Blue
        Me.ComboMoneda.Location = New System.Drawing.Point(92, 10)
        Me.ComboMoneda.Name = "ComboMoneda"
        Me.ComboMoneda.Size = New System.Drawing.Size(97, 21)
        Me.ComboMoneda.TabIndex = 181
        Me.ComboMoneda.ValueMember = "CodMoneda"
        '
        'Label30
        '
        Me.Label30.BackColor = System.Drawing.Color.Transparent
        Me.Label30.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Label30.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label30.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label30.Location = New System.Drawing.Point(12, 12)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(72, 16)
        Me.Label30.TabIndex = 183
        Me.Label30.Text = "Moneda"
        Me.Label30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox2.Controls.Add(Me.Label15)
        Me.GroupBox2.Controls.Add(Me.txtSaldoAct)
        Me.GroupBox2.Controls.Add(Me.Label18)
        Me.GroupBox2.Controls.Add(Me.txtAbono)
        Me.GroupBox2.Controls.Add(Me.Label16)
        Me.GroupBox2.Controls.Add(Me.txtSaldoAnt)
        Me.GroupBox2.Controls.Add(Me.Label17)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.ForeColor = System.Drawing.Color.RoyalBlue
        Me.GroupBox2.Location = New System.Drawing.Point(216, 96)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(328, 104)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        '
        'Label15
        '
        Me.Label15.BackColor = System.Drawing.Color.RoyalBlue
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.White
        Me.Label15.Location = New System.Drawing.Point(96, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(152, 16)
        Me.Label15.TabIndex = 157
        Me.Label15.Text = "Saldos de la Cuenta"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtSaldoAct
        '
        Me.txtSaldoAct.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsRecibos1, "abonoccobrar.Saldo_Actual", True))
        Me.txtSaldoAct.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtSaldoAct.Location = New System.Drawing.Point(120, 80)
        Me.txtSaldoAct.Name = "txtSaldoAct"
        '
        '
        '
        Me.txtSaldoAct.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtSaldoAct.Properties.Enabled = False
        Me.txtSaldoAct.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.txtSaldoAct.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.txtSaldoAct.Size = New System.Drawing.Size(192, 22)
        Me.txtSaldoAct.TabIndex = 162
        '
        'Label18
        '
        Me.Label18.BackColor = System.Drawing.Color.White
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label18.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label18.Location = New System.Drawing.Point(16, 80)
        Me.Label18.Name = "Label18"
        Me.Label18.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label18.Size = New System.Drawing.Size(104, 16)
        Me.Label18.TabIndex = 161
        Me.Label18.Text = "Saldo Actual"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAbono
        '
        Me.txtAbono.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsRecibos1, "abonoccobrar.Monto", True))
        Me.txtAbono.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtAbono.Location = New System.Drawing.Point(120, 45)
        Me.txtAbono.Name = "txtAbono"
        '
        '
        '
        Me.txtAbono.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtAbono.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtAbono.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.txtAbono.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.txtAbono.Size = New System.Drawing.Size(192, 29)
        Me.txtAbono.TabIndex = 0
        '
        'Label16
        '
        Me.Label16.BackColor = System.Drawing.Color.White
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label16.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label16.Location = New System.Drawing.Point(24, 52)
        Me.Label16.Name = "Label16"
        Me.Label16.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label16.Size = New System.Drawing.Size(96, 16)
        Me.Label16.TabIndex = 159
        Me.Label16.Text = "Abono"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSaldoAnt
        '
        Me.txtSaldoAnt.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DsRecibos1, "abonoccobrar.Saldo_Cuenta", True))
        Me.txtSaldoAnt.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtSaldoAnt.Location = New System.Drawing.Point(120, 18)
        Me.txtSaldoAnt.Name = "txtSaldoAnt"
        '
        '
        '
        Me.txtSaldoAnt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.txtSaldoAnt.Properties.Enabled = False
        Me.txtSaldoAnt.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
        Me.txtSaldoAnt.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
                        Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.SystemColors.ControlText)
        Me.txtSaldoAnt.Size = New System.Drawing.Size(192, 22)
        Me.txtSaldoAnt.TabIndex = 158
        '
        'Label17
        '
        Me.Label17.BackColor = System.Drawing.Color.White
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.RoyalBlue
        Me.Label17.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label17.Location = New System.Drawing.Point(16, 21)
        Me.Label17.Name = "Label17"
        Me.Label17.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label17.Size = New System.Drawing.Size(104, 16)
        Me.Label17.TabIndex = 0
        Me.Label17.Text = "Saldo Anterior"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCedulaUsuario
        '
        Me.txtCedulaUsuario.BackColor = System.Drawing.Color.Transparent
        Me.txtCedulaUsuario.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsRecibos1, "abonoccobrar.Ced_Usuario", True))
        Me.txtCedulaUsuario.Enabled = False
        Me.txtCedulaUsuario.ForeColor = System.Drawing.Color.Blue
        Me.txtCedulaUsuario.Location = New System.Drawing.Point(-72, 216)
        Me.txtCedulaUsuario.Name = "txtCedulaUsuario"
        Me.txtCedulaUsuario.Size = New System.Drawing.Size(72, 13)
        Me.txtCedulaUsuario.TabIndex = 183
        '
        'Label_Id_Recibo
        '
        Me.Label_Id_Recibo.BackColor = System.Drawing.Color.FromArgb(CType(CType(56, Byte), Integer), CType(CType(91, Byte), Integer), CType(CType(165, Byte), Integer))
        Me.Label_Id_Recibo.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DsRecibos1, "abonoccobrar.Id_Recibo", True))
        Me.Label_Id_Recibo.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_Id_Recibo.ForeColor = System.Drawing.Color.White
        Me.Label_Id_Recibo.Location = New System.Drawing.Point(496, 8)
        Me.Label_Id_Recibo.Name = "Label_Id_Recibo"
        Me.Label_Id_Recibo.Size = New System.Drawing.Size(40, 12)
        Me.Label_Id_Recibo.TabIndex = 185
        Me.Label_Id_Recibo.Text = "000"
        '
        'ReciboMontoDinero
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(546, 256)
        Me.Controls.Add(Me.Label_Id_Recibo)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.txtCedulaUsuario)
        Me.Controls.Add(Me.txtUsuario)
        Me.Controls.Add(Me.txtNum_Recibo)
        Me.Controls.Add(Me.Label36)
        Me.Controls.Add(Me.txtNombreUsuario)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.GroupBox6)
        Me.Name = "ReciboMontoDinero"
        Me.Text = "Recibo Monto Dinero"
        Me.Controls.SetChildIndex(Me.TituloModulo, 0)
        Me.Controls.SetChildIndex(Me.ToolBar1, 0)
        Me.Controls.SetChildIndex(Me.GroupBox6, 0)
        Me.Controls.SetChildIndex(Me.Label1, 0)
        Me.Controls.SetChildIndex(Me.GroupBox1, 0)
        Me.Controls.SetChildIndex(Me.txtNombreUsuario, 0)
        Me.Controls.SetChildIndex(Me.Label36, 0)
        Me.Controls.SetChildIndex(Me.txtNum_Recibo, 0)
        Me.Controls.SetChildIndex(Me.txtUsuario, 0)
        Me.Controls.SetChildIndex(Me.DataNavigator, 0)
        Me.Controls.SetChildIndex(Me.txtCedulaUsuario, 0)
        Me.Controls.SetChildIndex(Me.GroupBox2, 0)
        Me.Controls.SetChildIndex(Me.Label_Id_Recibo, 0)
        CType(Me.DsRecibos1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.txtSaldoAct.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAbono.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSaldoAnt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

#Region "Load"
    Private Sub ReciboMontoDinero_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            SqlConnection1.ConnectionString = GetSetting("SeeSOFT", "SeePos", "Conexion")
            AdapterMoneda.Fill(DsRecibos1, "Monedas")
            dtFecha.Value = Date.Now
            Valoresdefecto()
            txtUsuario.Text = Usua.clave_interna
            Loggin_Usuario()
            ComboMoneda.SelectedValue = Cod_Moneda

            If Cod_Cliente <> 0 Then
                CargarInformacionCliente(Cod_Cliente)
                txtAbono.Focus()
            Else
                txtCodigo.Focus()
            End If

            '-----------------------------------------------------------------------
            'VERIFICA LA CANTIDAD DEL TOP EN LOS BUSCADORES - ORA
            Try
                TopBuscador = CInt(GetSetting("SeeSOFT", "SeePos", "TopBuscadores"))
            Catch ex As Exception
                SaveSetting("SeeSOFT", "SeePos", "TopBuscadores", "50")
                TopBuscador = 50
            End Try
            '-----------------------------------------------------------------------

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub CargarInformacionCliente(ByVal codigo As String)
        Dim cConexion As New Conexion
        Dim funciones As New cFunciones
        Dim rs As SqlDataReader
        If codigo <> Nothing Then
            rs = cConexion.GetRecorset(cConexion.Conectar, "SELECT Identificacion, Nombre from Clientes where Identificacion ='" & codigo & "'")
            Try
                If rs.Read Then
                    txtCodigo.Text = rs("Identificacion")
                    txtNombre.Text = rs("Nombre")
                    Tabla.Clear()
                    Tabla = cFunciones.BuscarFacturas(codigo)
                    Saldo_Cuenta(Tabla)
                    If Tabla.Rows.Count = 0 Then
                        MessageBox.Show("El cliente no tiene facturas pendientes...", "Atenci�n...", MessageBoxButtons.OK)
                        rs.Close()
                        txtCodigo.Focus()
                        Exit Sub
                    Else
                        txtAbono.Enabled = True
                        ToolBarRegistrar.Enabled = True
                        txtAbono.Focus()
                    End If

                Else
                    MsgBox("La identificaci�n del Cliente no se encuentra", MsgBoxStyle.Information, "Atenci�n...")
                    txtCodigo.Focus()
                    rs.Close()
                    Exit Sub
                End If

            Catch ex As Exception
                MessageBox.Show(ex.Message)
            Finally
                rs.Close()
                cConexion.DesConectar(cConexion.Conectar)
            End Try
        End If
    End Sub

    Private Sub Saldo_Cuenta(ByVal Tabla1 As DataTable)
        Dim fila As DataRow
        Dim rs As SqlDataReader
        Dim facturatemp, Totaltemp, ValorFactura, SaldoCuenta As Double
        Dim CodigoMoneda As Integer
        Dim funciones As New cFunciones
        Dim ConexionLocal As New Conexion

        Try
            SaldoCuenta = 0

            For i As Integer = 0 To Tabla1.Rows.Count - 1
                fila = Tabla1.Rows(i)
                facturatemp = fila("Factura")
                Totaltemp = fila("Total")
                CodigoMoneda = fila("Cod_Moneda")
                rs = ConexionLocal.GetRecorset(ConexionLocal.Conectar, "SELECT ValorVenta from Moneda where CodMoneda =" & CodigoMoneda)
                If rs.Read Then ValorFactura = rs("ValorVenta")
                rs.Close()
                ConexionLocal.DesConectar(ConexionLocal.Conectar)
                SaldoCuenta = SaldoCuenta + funciones.Saldo_de_Factura(facturatemp, ((Totaltemp * ValorFactura) / CDbl(txtTipoCambio.Text)), ValorFactura, CDbl(txtTipoCambio.Text))
            Next

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        Finally
            ConexionLocal = Nothing
            txtSaldoAnt.Text = Format(Math.Round(SaldoCuenta, 2), "#,#0.00")
            txtSaldoAct.Text = Format(Math.Round(SaldoCuenta, 2), "#,#0.00")
        End Try
    End Sub

    Private Sub Valoresdefecto()
        'Establecer valores por defecto Abonoccobrar
        DsRecibos1.abonoccobrar.Id_ReciboColumn.AutoIncrement = True
        DsRecibos1.abonoccobrar.Id_ReciboColumn.AutoIncrementSeed = -1
        DsRecibos1.abonoccobrar.Id_ReciboColumn.AutoIncrementStep = -1
        DsRecibos1.abonoccobrar.Num_ReciboColumn.DefaultValue = 0
        DsRecibos1.abonoccobrar.Cod_ClienteColumn.DefaultValue = 0
        DsRecibos1.abonoccobrar.Nombre_ClienteColumn.DefaultValue = ""
        DsRecibos1.abonoccobrar.Saldo_CuentaColumn.DefaultValue = 0
        DsRecibos1.abonoccobrar.MontoColumn.DefaultValue = 0
        DsRecibos1.abonoccobrar.Saldo_ActualColumn.DefaultValue = 0
        DsRecibos1.abonoccobrar.FechaColumn.DefaultValue = Date.Now
        DsRecibos1.abonoccobrar.ObservacionesColumn.DefaultValue = "Abono a facturas desde Facturaci�n"
        DsRecibos1.abonoccobrar.AnulaColumn.DefaultValue = 0
        DsRecibos1.abonoccobrar.Ced_UsuarioColumn.DefaultValue = Usua.cedula
        DsRecibos1.abonoccobrar.ContabilizadoColumn.DefaultValue = 0
        DsRecibos1.abonoccobrar.AsientoColumn.DefaultValue = 0
        DsRecibos1.abonoccobrar.Cod_MonedaColumn.DefaultValue = Cod_Moneda

        'Establecer valores por defecto Detalla_Abonoccobrar
        DsRecibos1.detalle_abonoccobrar.ConsecutivoColumn.AutoIncrement = True
        DsRecibos1.detalle_abonoccobrar.ConsecutivoColumn.AutoIncrementSeed = -1
        DsRecibos1.detalle_abonoccobrar.ConsecutivoColumn.AutoIncrementStep = -1
        DsRecibos1.detalle_abonoccobrar.Id_ReciboColumn.DefaultValue = 0
        DsRecibos1.detalle_abonoccobrar.FacturaColumn.DefaultValue = 0
        DsRecibos1.detalle_abonoccobrar.TipoColumn.DefaultValue = "CRE"
        DsRecibos1.detalle_abonoccobrar.MontoColumn.DefaultValue = 0
        DsRecibos1.detalle_abonoccobrar.Saldo_AntColumn.DefaultValue = 0
        DsRecibos1.detalle_abonoccobrar.InteresesColumn.DefaultValue = 0
        DsRecibos1.detalle_abonoccobrar.AbonoColumn.DefaultValue = 0
        DsRecibos1.detalle_abonoccobrar.Abono_SuMonedaColumn.DefaultValue = 0
        DsRecibos1.detalle_abonoccobrar.SaldoColumn.DefaultValue = 0
    End Sub
#End Region

#Region "Toolbar"
    Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar1.ButtonClick
        Select Case ToolBar1.Buttons.IndexOf(e.Button) + 1
            Case 3 : If PMU.Update Then Registrar() Else MsgBox("No tiene permiso para registrar o actualizar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
            Case 7 : Me.Close()
        End Select
    End Sub
#End Region

#Region "Buscar"
    Private Sub Buscar()
        Dim Fx As New cFunciones
        Dim identificador As Integer

        Try
            identificador = CDbl(Fx.Buscar_X_Descripcion_Fecha5C("SELECT TOP " & TopBuscador & " abonoccobrar.Id_Recibo, abonoccobrar.Num_Recibo as Recibo , abonoccobrar.Nombre_Cliente AS Nombre_Cliente, abonoccobrar.Fecha,  abonoccobrar.Monto FROM abonoccobrar INNER JOIN  Moneda ON abonoccobrar.Cod_Moneda = Moneda.CodMoneda ORDER BY abonoccobrar.Fecha DESC", "Nombre_Cliente", "Fecha", "Buscar Recibo de Dinero"))

            If identificador = 0 Then ' si se dio en el boton de cancelar
                Exit Sub
            End If

            If MessageBox.Show("�Desea Reimprimir el recibo de dinero?", "SeePos", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                Imprimir(identificador)
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
#End Region

#Region "Registrar"
    Private Sub Registrar()
        Dim i As Integer
        Dim Funciones As New Conexion
        Dim FactTemp As Double

        Try
            If CDbl(txtAbono.Text) = 0 Then
                MessageBox.Show("Debe de digitar un monto mayor que 0", "Atenci�n...", MessageBoxButtons.OK)
                txtAbono.Text = 0.0 : txtAbono.Focus() : txtAbono.SelectAll() : Exit Sub
            End If
            If CDbl(txtAbono.Text) > CDbl(txtSaldoAnt.Text) Then
                MessageBox.Show("No puede abonarle m�s de lo que adeuda, Favor revisar...", "Atenci�n...", MessageBoxButtons.OK)
                txtAbono.Text = 0.0 : txtAbono.Focus() : txtAbono.SelectAll() : Exit Sub
            End If

            If MessageBox.Show("�Desea guardar el recibo de dinero?", "SeePos", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                ToolBarRegistrar.Enabled = False
                txtAbono.Enabled = False
                txtNum_Recibo.Text = Numero_de_Recibo()

                If Funciones.SlqExecuteScalar(Funciones.Conectar, "SELECT Num_Recibo FROM abonoccobrar WHERE (Num_Recibo =" & CDbl(txtNum_Recibo.Text) & ")") <> Nothing Then
                    MsgBox("No se puede Registrar, ya existe un recibo con este n�mero de recibo", MsgBoxStyle.Critical)
                    Exit Sub
                End If

                AgregaDetalle()

                If BindingContext(DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar").Count = 0 Then
                    MsgBox("Debe abonar m�nimo una factura", MsgBoxStyle.Information)
                    Exit Sub
                End If

                BindingContext(DsRecibos1, "abonoccobrar").EndCurrentEdit()
                BindingContext(DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar").EndCurrentEdit()

                ''llamar al formulario de opciones de pago
                Dim Movimiento_Pago_Abonos As New frmMovimientoCajaPagoAbono(Usua)

                Movimiento_Pago_Abonos.Factura = CDbl(txtNum_Recibo.Text)
                Movimiento_Pago_Abonos.fecha = "" & dtFecha.Text
                Movimiento_Pago_Abonos.Total = CDbl(txtAbono.Text)
                Movimiento_Pago_Abonos.Tipo = "ABO"
                Movimiento_Pago_Abonos.codmod = ComboMoneda.SelectedValue
                Movimiento_Pago_Abonos.ShowDialog()
                FrmVuelto.MontoVuelto = Movimiento_Pago_Abonos.vuelto

                If Movimiento_Pago_Abonos.Registra Then
                     For i = 0 To BindingContext(DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar").Count - 1
                        BindingContext(DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar").Position = i
                        If BindingContext(DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar").Current("Saldo") = 0 Then
                            FactTemp = BindingContext(DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar").Current("Factura")
                            If (Funciones.UpdateRecords("Ventas", "FacturaCancelado = 1", "Num_Factura =" & FactTemp & "and Tipo = 'CRE'")) <> "" Then
                                MsgBox("Problemas al Registrar las facturas como canceladas, reintente hacer el abono", MsgBoxStyle.Critical)
                                Exit Sub
                            End If
                        End If
                    Next i
                    BindingContext(DsRecibos1, "abonoccobrar").Current("Num_Recibo") = Me.txtNum_Recibo.Text
                    If Registrar_Abono() And Movimiento_Pago_Abonos.RegistrarOpcionesPago() Then

                        Me.FrmVuelto.ShowDialog()
                        open_cashdrawer()
                        If MessageBox.Show("�Desea imprimir este Recibo de Dinero?", "Atenci�n...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
                            Imprimir(BindingContext(DsRecibos1, "abonoccobrar").Current("Id_Recibo"))
                        End If

                        DsRecibos1.detalle_abonoccobrar.Clear()
                        DsRecibos1.abonoccobrar.Clear()
                        Me.Close()
                    Else
                        MsgBox("Problemas al registrar el abono y/o pagos, intentelo de nuevo ", MsgBoxStyle.Critical)
                        Trans.Rollback()
                        Movimiento_Pago_Abonos.Trans.Rollback()
                        Exit Sub
                    End If
                End If
            Else
                txtAbono.Focus()
            End If

        Catch ex As System.Exception
            MsgBox(ex.Message)
        Finally
            ToolBarRegistrar.Enabled = True
            txtAbono.Enabled = True
        End Try
    End Sub


    Function Registrar_Abono() As Boolean
        If SqlConnection1.State <> ConnectionState.Open Then SqlConnection1.Open()
        Trans = SqlConnection1.BeginTransaction

        Try
            SqlInsertCommand1.CommandText = " INSERT INTO [abonoccobrar] ([Num_Recibo], [Cod_Cliente], [Nombre_Cliente], [Saldo_Cuenta], [Monto], [Saldo_Actual], [Fecha], [Observaciones], [Anula], [Ced_Usuario], [Contabilizado], [Asiento], [Cod_Moneda]) VALUES (@Num_Recibo, @Cod_Cliente, @Nombre_Cliente, @Saldo_Cuenta, @Monto, @Saldo_Actual, @Fecha, @Observaciones, @Anula, @Ced_Usuario, @Contabilizado, @Asiento, @Cod_Moneda);" & _
                                            " SELECT Id_Recibo, Num_Recibo, Cod_Cliente, Nombre_Cliente, Saldo_Cuenta, Monto, Saldo_Actual, Fecha, Observaciones, Anula, Ced_Usuario, Contabilizado, Asiento, Cod_Moneda FROM abonoccobrar WHERE (Id_Recibo = SCOPE_IDENTITY())"
            AdapterAbonoccobrar.InsertCommand.Transaction = Trans
            AdapterAbonoccobrar.DeleteCommand.Transaction = Trans
            AdapterAbonoccobrar.UpdateCommand.Transaction = Trans

            AdapterDetalleAbonocCobrar.InsertCommand.Transaction = Trans
            AdapterDetalleAbonocCobrar.DeleteCommand.Transaction = Trans
            AdapterDetalleAbonocCobrar.UpdateCommand.Transaction = Trans

            SqlInsertCommand2.CommandText = " INSERT INTO [detalle_abonoccobrar] ([Id_Recibo], [Factura], [Tipo], [Monto], [Saldo_Ant], [Intereses], [Abono], [Abono_SuMoneda], [Saldo]) VALUES (@Id_Recibo, @Factura, @Tipo, @Monto, @Saldo_Ant, @Intereses, @Abono, @Abono_SuMoneda, @Saldo);" & _
                                            " SELECT Consecutivo, Id_Recibo, Factura, Tipo, Monto, Saldo_Ant, Intereses, Abono, Abono_SuMoneda, Saldo FROM detalle_abonoccobrar WHERE (Consecutivo = SCOPE_IDENTITY())"
            AdapterAbonoccobrar.Update(DsRecibos1, "abonoccobrar")
            AdapterDetalleAbonocCobrar.Update(DsRecibos1, "detalle_abonoccobrar")

            Trans.Commit()
            Return True

        Catch ex As Exception
            Trans.Rollback()
            MsgBox(ex.Message)
            ToolBar1.Buttons(2).Enabled = True
            Return False
        End Try
    End Function
#End Region

#Region "Imprimir"
    Private Sub Imprimir(ByVal Id As Integer)
        Dim Reporte As New ReciboDinero
        Try
            CrystalReportsConexion.LoadReportViewer(Nothing, Reporte, True)
            Reporte.PrintOptions.PrinterName = Automatic_Printer_Dialog(0) 'FACTURACION
            Reporte.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
            Reporte.SetParameterValue(0, Id)
            Reporte.PrintToPrinter(1, True, 0, 0)

        Catch ex As System.Exception
            MsgBox(ex.Message)
        Finally
            Reporte.Dispose()
            Reporte.Close()
            Reporte = Nothing
        End Try
    End Sub

    Private Function Automatic_Printer_Dialog(ByVal PrinterToSelect As Byte) As String 'SAJ 01092006 
        Dim PrintDocument1 As New PrintDocument
        Dim DefaultPrinter As String = PrintDocument1.PrinterSettings.PrinterName
        Dim PrinterInstalled As String
        'BUSCA LA IMPRESORA PREDETERMINADA PARA EL SISTEMA
        For Each PrinterInstalled In PrinterSettings.InstalledPrinters
            Select Case Split(PrinterInstalled.ToUpper, "\").GetValue(Split(PrinterInstalled.ToUpper, "\").GetLength(0) - 1)
                Case "FACTURACION"
                    If PrinterToSelect = 0 Then
                        Return PrinterInstalled.ToString
                        Exit Function
                    End If
                Case "PUNTOVENTA"
                    If PrinterToSelect = 3 Then
                        Return PrinterInstalled.ToString
                        Exit Function
                    End If
            End Select
        Next
        If MsgBox("No se ha encontrado las impresoras predeterminadas para el sistema..." & vbCrLf & "Desea proceder a selecionar una impresora....", MsgBoxStyle.YesNo + MsgBoxStyle.Critical, "Atenci�n...") = MsgBoxResult.Yes Then
            Dim PrinterDialog As New PrintDialog
            Dim DocPrint As New PrintDocument
            PrinterDialog.Document = DocPrint
            PrinterDialog.ShowDialog()
            If Windows.Forms.DialogResult.Yes Then
                Return PrinterDialog.PrinterSettings.PrinterName 'DEVUELVE LA IMPRESORA  SELECCIONADA
            Else
                Return DefaultPrinter 'NO SE SELECCIONO IMPRESORA ALGUNA
            End If
        End If
    End Function
#End Region

#Region "Funciones"
    Private Sub AgregaDetalle()
        Dim fila As DataRow
        Dim cConexion As New Conexion
        Dim Conexion2 As New Conexion
        Dim rs As SqlDataReader
        Dim funciones As New cFunciones
        Dim Interes, SaldoAbono, DiasAtraso, TipoCambioFact As Double
        Dim FechaUltAbono As String

        Try
            If Tabla.Rows.Count > 0 Then
                BindingContext(DsRecibos1, "abonoccobrar").EndCurrentEdit()
                DsRecibos1.detalle_abonoccobrar.Clear()
                SaldoAbono = Math.Round(CDbl(txtAbono.Text), 2)
                For i As Integer = 0 To Tabla.Rows.Count - 1
                    If SaldoAbono <= 0 Then
                        Exit For
                    Else
                        fila = Tabla.Rows(i)
                        rs = cConexion.GetRecorset(cConexion.Conectar, "Select Num_Factura, Tipo, Fecha, Vence, Cod_Moneda, Total from Ventas where Tipo = 'CRE' and Num_Factura =" & fila("Factura"))
                        While rs.Read
                            Try
                                TipoCambioFact = Conexion2.SlqExecuteScalar(Conexion2.Conectar, "Select ValorVenta from Moneda Where CodMoneda =" & rs("Cod_moneda"))
                                Conexion2.DesConectar(Conexion2.Conectar)
                                Interes = Conexion2.SlqExecuteScalar(Conexion2.Conectar, "Select Intereses from configuraciones")
                                Conexion2.DesConectar(Conexion2.Conectar)
                                DiasAtraso = DateDiff(DateInterval.Day, rs("Vence"), Date.Now)
                                FechaUltAbono = Conexion2.SlqExecuteScalar(Conexion2.Conectar, "Select ISNULL(MAX(Fecha), 0) from UltimoRecibo where Tipo = 'CRE' and Factura = " & rs("Num_Factura"))
                                Conexion2.DesConectar(Conexion2.Conectar)
                                If FechaUltAbono <> "01/01/1900" Then
                                    If rs("Vence") < FechaUltAbono Then
                                        DiasAtraso = DateDiff(DateInterval.Day, CDate(FechaUltAbono), Date.Now)
                                    End If
                                End If

                                BindingContext(DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar").CancelCurrentEdit()
                                BindingContext(DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar").EndCurrentEdit()
                                BindingContext(DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar").AddNew()
                                BindingContext(DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar").Current("Factura") = rs("Num_Factura")
                                BindingContext(DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar").Current("Monto") = Math.Round((rs("Total") * TipoCambioFact) / txtTipoCambio.Text, 2)
                                BindingContext(DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar").Current("Saldo_Ant") = Math.Round(funciones.Saldo_de_Factura(rs("Num_Factura"), (rs("Total") * TipoCambioFact) / CDbl(txtTipoCambio.Text), TipoCambioFact, CDbl(txtTipoCambio.Text)), 2)

                                If DiasAtraso > 0 Then
                                    BindingContext(DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar").Current("Intereses") = Math.Round((DiasAtraso * (Interes / 100 / 30)) * CDbl(BindingContext(DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar").Current("Saldo_Ant")), 2)
                                Else
                                    BindingContext(DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar").Current("Intereses") = 0
                                End If

                                BindingContext(DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar").Current("Saldo_Ant") = Math.Round(CDbl(BindingContext(DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar").Current("Saldo_Ant")) + CDbl(BindingContext(DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar").Current("Intereses")), 2)

                                If BindingContext(DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar").Current("Saldo_Ant") <= SaldoAbono Then
                                    BindingContext(DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar").Current("Abono") = BindingContext(DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar").Current("Saldo_Ant")
                                Else
                                    BindingContext(DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar").Current("Abono") = SaldoAbono
                                End If

                                SaldoAbono = SaldoAbono - BindingContext(DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar").Current("Abono")
                                BindingContext(DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar").Current("Saldo") = BindingContext(DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar").Current("Saldo_Ant") - BindingContext(DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar").Current("Abono")
                                BindingContext(DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar").Current("Abono_SuMoneda") = Math.Round((CDbl(BindingContext(DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar").Current("Abono")) / TipoCambioFact) * CDbl(txtTipoCambio.Text), 2)
                                BindingContext(DsRecibos1, "abonoccobrar.abonoccobrardetalle_abonoccobrar").EndCurrentEdit()

                            Catch ex As Exception
                                MessageBox.Show(ex.Message)
                            End Try
                        End While
                        cConexion.DesConectar(cConexion.Conectar)
                    End If
                Next
            End If

        Catch ex As Exception
            MsgBox(ex.ToString)
        Finally
            Conexion2 = Nothing
            cConexion.DesConectar(cConexion.Conectar)
            cConexion = Nothing
        End Try
    End Sub

    Private Function Numero_de_Recibo() As Double
        Dim cConexion As New Conexion
        Dim Num_Recibo As Double
        Num_Recibo = CDbl(cConexion.SlqExecuteScalar(cConexion.Conectar, "SELECT isnull(Max(Num_Recibo),0) FROM AbonoCCobrar"))
        Numero_de_Recibo = Num_Recibo + 1
        cConexion.DesConectar(cConexion.Conectar)
    End Function
#End Region

#Region "Funciones Controles"
    Private Sub txtAbono_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtAbono.KeyDown
        If e.KeyCode = Keys.Enter Then
            If CDbl(txtCodigo.Text) = 0 Then
                txtCodigo.Focus()
            Else
                Registrar()
            End If
        End If

        If e.KeyCode = Keys.F1 Then
            Buscar()
        End If

        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If
    End Sub

    Private Sub txtAbono_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtAbono.KeyPress
        If (Not Char.IsDigit(e.KeyChar)) Then ' valida que en este campo solo se digiten numeros y/o "-"
            If Not (e.KeyChar = Convert.ToChar(Keys.Back)) And Not (e.KeyChar = "."c) Then
                e.Handled = True  ' esto invalida la tecla pulsada
            End If
        End If
    End Sub

    Private Sub txtAbono_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAbono.EditValueChanged
        If txtAbono.Text <> "" Then
            If CDbl(txtAbono.Text) >= 0 Then
                If BindingContext(DsRecibos1, "AbonocCobrar").Count > 0 Then
                    txtSaldoAct.Text = Format(Math.Round(CDbl(txtSaldoAnt.Text) - CDbl(txtAbono.Text), 2), "#,#0.00")
                End If
            End If
        End If
    End Sub

    Private Sub txtAbono_GotFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtAbono.GotFocus
        txtAbono.SelectAll()
    End Sub

    Private Sub txtCodigo_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCodigo.KeyDown
        If e.KeyCode = Keys.F1 Then
            Dim Cliente As New frmBuscarCliente
            Cliente.ShowDialog()

            If Cliente.Cancelado = False And Cliente.Codigo <> "" And Cliente.Codigo <> 0 Then
                txtCodigo.Text = Cliente.Codigo
                CargarInformacionCliente(txtCodigo.Text)
            Else
                Exit Sub
            End If
        End If

        If e.KeyCode = Keys.Enter Then
            CargarInformacionCliente(txtCodigo.Text)
        End If

        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If
    End Sub
#End Region

#Region "Validar Usuario"
    Private Sub Loggin_Usuario()
        Dim cConexion As New Conexion
        Dim rs As SqlDataReader

        If txtUsuario.Text <> "" Then
            rs = cConexion.GetRecorset(cConexion.Conectar, "SELECT Cedula, Nombre from Usuarios where Clave_Interna ='" & txtUsuario.Text & "'")
            If rs.HasRows = False Then
                MsgBox("Clave Incorrecta....", MsgBoxStyle.Information, "Atenci�n...")
                txtUsuario.Focus()
            End If

            While rs.Read
                Try
                    PMU = VSM(rs("Cedula"), Name) 'Carga los privilegios del usuario con el modulo 
                    If Not PMU.Execute Then MsgBox("No tiene permiso ejecutar el m�dulo " & Text, MsgBoxStyle.Information, "Atenci�n...") : Me.Close()

                    BindingContext(DsRecibos1, "abonoccobrar").CancelCurrentEdit()
                    BindingContext(DsRecibos1, "abonoccobrar").EndCurrentEdit()
                    BindingContext(DsRecibos1, "abonoccobrar").AddNew()
                    txtNum_Recibo.Text = Numero_de_Recibo()

                    txtNombreUsuario.Text = rs("Nombre")
                    txtCedulaUsuario.Text = rs("Cedula")

                Catch ex As SystemException
                    MsgBox(ex.Message)
                End Try
            End While
            rs.Close()
            cConexion.DesConectar(cConexion.Conectar)
        Else
            MsgBox("Debe de digitar la clave de usuario", MsgBoxStyle.Exclamation)
            txtUsuario.Focus()
        End If
    End Sub
#End Region

#Region "Abrir Caja"
    Public Sub open_cashdrawer()
        Dim Puerto As String

        '------------------------------------------------------------------------------
        'VALIDA SI DESEA ABRIR CAJA O NO - ORA
        If GetSetting("SeeSoft", "SeePos", "PuertoImp") <> "NO" Then
            Dim intFileNo As Integer = FreeFile()
            FileOpen(1, "c:\escapes.txt", OpenMode.Output)
            PrintLine(1, Chr(27) & "p" & Chr(0) & Chr(25) & Chr(250))
            FileClose(1)

            '------------------------------------------------------------------------------
            'VALIDA EL PUERTO DE LA IMPRESORA - ORA
            Try
                Puerto = GetSetting("SeeSoft", "SeePos", "PuertoImp")
                If Puerto = "" Then
                    SaveSetting("SeeSoft", "SeePos", "PuertoImp", "COM1")
                    Puerto = "COM1"
                End If
            Catch ex As Exception
                SaveSetting("SeeSoft", "SeePos", "PuertoImp", "COM1")
                Puerto = "COM1"
            End Try
            '------------------------------------------------------------------------------

            Shell("print /d:" & Puerto & " c:\escapes.txt", AppWinStyle.NormalFocus)
        End If
        '------------------------------------------------------------------------------
    End Sub
#End Region


    Private Sub txtCodigo_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCodigo.TextChanged

    End Sub
End Class
