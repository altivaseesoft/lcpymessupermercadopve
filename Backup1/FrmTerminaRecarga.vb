﻿Public Class frmTerminaRecarga
    Public Estado As Boolean

    Public pubCedula As String
    Public pubTelefono As String
    Public pubMonto As String
    Public pubOperador As String
    Public pubNombreOperador As String

    Dim priIdCliente As String
    Dim priProveedorRecarga As String
    Dim priPorcentajeGanancia As Double
    Dim priIdUsuario As String
    Dim priContraseña As String

    Dim CodigoResultado As String = ""
    Public IdRecarga_Coope As String = ""
    Dim IdRecarga_ICE As String = "0"


    Private Sub frmTerminaRecarga_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            spCargarDatos()
            spRealizaRecarga()
            If Estado Then
                btCheck.Visible = True
            Else
                btCheck.Visible = False
                btCheckNo.Visible = True
                txEstado.Text = "La Recarga no se realizó exitosamente."
            End If
        Catch ex As Exception
            Estado = False
            btCheck.Visible = False
            btCheckNo.Visible = True
            txEstado.Text = "La Recarga no se realizó exitosamente."
        End Try

        btRecargar.Focus()
      
    End Sub
    Private Sub spCargarDatos()

        priProveedorRecarga = GetSetting("Seesoft", "SeePos", "ProveedorRecarga")

        Dim sql As New SqlClient.SqlCommand
        Dim dt As New DataTable

        sql.CommandText = "SELECT *  FROM [Seepos].[dbo].[tb_S_ConfiguracionRecarga] WHERE [CodigoProveedorRecarga] = " & priProveedorRecarga

        clsEnlace.spCargarDatos(sql, dt)

        If dt.Rows.Count > 0 Then
            priPorcentajeGanancia = dt.Rows(0).Item("PorcentajeGanancia")
            priIdCliente = dt.Rows(0).Item("IdCliente")
            priIdUsuario = dt.Rows(0).Item("IdUsuario")
            priContraseña = dt.Rows(0).Item("Contraseña")
        End If

    End Sub
    Private Sub spRealizaRecarga()

        Dim clsEnviarRecarga As New clsEnviarRecarga
        Dim Cadena As String = ""

        Dim Token As String

        pubMonto = pubMonto.PadLeft(6, "0")

        Try
            Dim Ran As New Random
            Dim Aleatorio As Integer
            Aleatorio = Ran.Next(0, 9)

            Dim Fecha As String = Format(Now, "ddMMyyyyHHmmss")
            Token = fngenerarToken(Fecha, pubTelefono, pubMonto, priContraseña, Aleatorio)
            Cadena = clsEnviarRecarga.fnEnviarCoopeguanacaste(Fecha, pubTelefono, Token, priIdCliente, priIdUsuario, pubOperador)

            Dim datos() As String
            Dim Resultado As String
            datos = Cadena.Split("|")
            Resultado = datos(0)

            If Resultado = "TRUE" Then
                CodigoResultado = datos(1)
                IdRecarga_Coope = datos(2)
                IdRecarga_ICE = datos(3)

                spGuardarRecarga()
                Estado = True
            Else
                Estado = False
            End If

        Catch ex As Exception
            Estado = False
            DialogResult = Windows.Forms.DialogResult.No
            Exit Sub
        End Try
    End Sub
    Private Sub spGuardarRecarga()
        DtsProcesoRecarga.tb_S_Recarga.Clear()
        Dim Conexion As String
        Conexion = GetSetting("Seesoft", "SeePos", "CONEXION")

        If pubOperador = 1 Then pubNombreOperador = "Kolbi"
        If pubOperador = 2 Then pubNombreOperador = "Tuyo Movil"
        If pubOperador = 3 Then pubNombreOperador = "Full Movil"
        Try
            With bsRecarga
                .AddNew()
                .Current("IdRecargaCoope") = IdRecarga_Coope
                .Current("IdRecargaICE") = IdRecarga_ICE
                .Current("Cedula_Usuario") = pubCedula
                .Current("IdProveedor") = priProveedorRecarga
                .Current("CodigoOperador") = pubOperador
                .Current("Operador") = pubNombreOperador
                .Current("NumeroCelular") = pubTelefono
                .Current("Fecha") = Now.Date
                .Current("Monto") = pubMonto
                .Current("CodigoResultado") = CodigoResultado
                .EndEdit()
            End With
          
            Tb_S_RecargaTableAdapter.Connection.ConnectionString = Conexion
            Tb_S_RecargaTableAdapter.Update(DtsProcesoRecarga.tb_S_Recarga)

        Catch ex As Exception
            My.Computer.FileSystem.WriteAllText("C:\Recarga\Recarga.txt", _
                                             "IdRecarga_Coope: " & IdRecarga_Coope + vbCrLf & _
                                             "IdRecarga_ICE: " & IdRecarga_ICE + vbCrLf & _
                                             "pubCedula: " & pubCedula + vbCrLf & _
                                             "priProveedorRecarga: " & priProveedorRecarga + vbCrLf & _
                                             "pubOperador : " & pubOperador + vbCrLf & _
                                             "pubTelefono : " & pubTelefono + vbCrLf & _
                                             "Date : " & Now.Date + vbCrLf & _
                                             "pubMonto : " & pubMonto + vbCrLf & _
                                             "CodigoResultado: " & CodigoResultado + vbCrLf & _
                                             "MENSAJE : " & ex.Message + vbCrLf + vbCrLf, True)


            MsgBox("La recarga no se guardó en la base de datos.")
        End Try


    End Sub

    Public Function Transforma(ByVal Caracter As String, ByVal Aleatorio As Integer) As String
        Dim vPos As Integer
        Dim vPos2 As Integer
        Dim vChar2 As String
        Dim vDireccion As Integer

        'Valor de la línea de la llave
        Const vKey = "rCDcGIJx2BKLoNPFS8TyVHMAYE01k4pq56Z9abXU3defghijOlmQn7WstuvRwz"

        Try
            'Posicion del caracter en la llave
            vPos = InStr(vKey, Caracter)
            'Determina el caracter correspondiente en la mismo posicion pero contando del ultimo hacia el primero en la llave.
            vPos = Len(vKey) + 1 - vPos
            'Si el numero aleatorio es par nos adelantamos las posiciones que este indique, si es impar entonces nos vamos hacia tras tantas posiciones como este numero indique.
            If Aleatorio Mod 2 = 1 Then
                vDireccion = -1
            Else
                vDireccion = 1
            End If
            'Ya tenemos en que direccion nos desplazamos en la llave, ahora nos desplazamos para determinar el caracter correspondiente
            vPos2 = Aleatorio
            While vPos2 > 0
                vPos2 = vPos2 - 1
                vPos = vPos + vDireccion
                'Si nos desplazamos hacia adelante y llegamoa al final de la hilera pasamos al inicio (Circular)
                If vPos > Len(vKey) Then
                    vPos = 1
                ElseIf vPos = 0 Then
                    'Esto podria suceder si nos desplazamos hacia atras y llegamos al inicio de la hilera Llave.
                    vPos = Len(vKey)
                End If
            End While
            'Determina el caracter correspondiente dentro de la hilera Llave.
            vChar2 = Mid(vKey, vPos, 1)

            Transforma = vChar2

        Catch ex As Exception
            Transforma = ""
        End Try
    End Function


    Function fngenerarToken(ByVal pFecha As String, ByVal pTelefono As String, ByVal pMonto As String, ByVal pClave As String, ByVal pAleatorio As Integer) As String

        ' pFecha = "19022016105115" : pTelefono = "86457430" : pMonto = "000100" : pAleatorio = 6
        ' pClave = "0SPITI2016"
        '  1965CykbkE11kiMEPkQMk7TEEi1q1iPP1hk18M1iMP7
        Dim vPos_Impares As Integer = 5
        Dim vPos_Pares As Integer
        Dim vSuma_Verificacion As Integer = 0
        Dim vValor As Integer = 0
        Dim vToken As String = "----"

        ' -- Transforma los valores según la secuencia del TOKEN
        vToken = vToken & _
                       Transforma(Mid(pTelefono, 3, 1), pAleatorio) & _
                      Transforma(Mid(pTelefono, 2, 1), pAleatorio) & _
                      Transforma(Mid(pMonto, 6, 1), pAleatorio) & _
                      Transforma(Mid(pTelefono, 1, 1), pAleatorio) & _
                      Transforma(Mid(pMonto, 1, 1), pAleatorio) & _
                  Transforma(Mid(pTelefono, 4, 1), pAleatorio) & _
                  Transforma(Mid(pMonto, 3, 1), pAleatorio) & _
                  Transforma(Mid(pTelefono, 5, 1), pAleatorio) & _
                  Transforma(Mid(pMonto, 5, 1), pAleatorio) & _
                  Transforma(Mid(pTelefono, 6, 1), pAleatorio) & _
                  Transforma(Mid(pMonto, 4, 1), pAleatorio) & _
                  Transforma(Mid(pTelefono, 8, 1), pAleatorio) & _
                  Transforma(Mid(pTelefono, 7, 1), pAleatorio) & _
                  Transforma(Mid(pMonto, 2, 1), pAleatorio) & _
                  Transforma(Mid(pClave, 10, 1), pAleatorio) & _
                  Transforma(Mid(pFecha, 10, 1), pAleatorio) & _
                  Transforma(Mid(pFecha, 1, 1), pAleatorio) & _
                  Transforma(Mid(pClave, 3, 1), pAleatorio) & _
                  Transforma(Mid(pClave, 8, 1), pAleatorio) & _
                  Transforma(Mid(pFecha, 14, 1), pAleatorio) & _
                  Transforma(Mid(pClave, 7, 1), pAleatorio) & _
                  Transforma(Mid(pFecha, 13, 1), pAleatorio) & _
                  Transforma(Mid(pFecha, 3, 1), pAleatorio) & _
                  Transforma(Mid(pClave, 1, 1), pAleatorio) & _
                  Transforma(Mid(pFecha, 9, 1), pAleatorio) & _
                  Transforma(Mid(pFecha, 5, 1), pAleatorio) & _
                  Transforma(Mid(pClave, 6, 1), pAleatorio) & _
                  Transforma(Mid(pFecha, 11, 1), pAleatorio) & _
                  Transforma(Mid(pFecha, 7, 1), pAleatorio) & _
                  Transforma(Mid(pClave, 2, 1), pAleatorio) & _
                  Transforma(Mid(pFecha, 6, 1), pAleatorio) & _
                  Transforma(Mid(pFecha, 4, 1), pAleatorio) & _
                  Transforma(Mid(pClave, 9, 1), pAleatorio) & _
                  Transforma(Mid(pFecha, 12, 1), pAleatorio) & _
                  Transforma(Mid(pClave, 4, 1), pAleatorio) & _
                  Transforma(Mid(pClave, 5, 1), pAleatorio) & _
                  Transforma(Mid(pFecha, 2, 1), pAleatorio) & _
                  Transforma(Mid(pFecha, 8, 1), pAleatorio) & _
                  pAleatorio.ToString

        '-- Se generan los dígitos de verificación de impares
        vSuma_Verificacion = 0
        vPos_Impares = 5
        While vPos_Impares <= 43
            vSuma_Verificacion = vSuma_Verificacion + Asc(Mid(vToken, vPos_Impares, 1))
            vPos_Impares = vPos_Impares + 2
        End While
        '-- Se generan los dígitos de verificación de pares
        vPos_Pares = pAleatorio + 5
        While vPos_Pares <= 43
            If vPos_Pares Mod 2 = 0 Then
                vSuma_Verificacion = vSuma_Verificacion + Asc(Mid(vToken, vPos_Pares, 1))
            End If
            vPos_Pares = vPos_Pares + 3
        End While

        '--Se completa el TOKEN con el valor verificador
       vToken = CStr(vSuma_Verificacion) & Mid(vToken, 5, Len(vToken) - 5) & CStr(pAleatorio)

        Return (vToken)

    End Function



    Private Sub beRecargar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btRecargar.Click
        DialogResult = Windows.Forms.DialogResult.Yes
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        DialogResult = Windows.Forms.DialogResult.No
    End Sub

End Class