﻿Public Class clsEnlace

    Public Shared Sub spCargarDatos(ByVal _sqlcommand As SqlClient.SqlCommand, ByRef _dt As Data.DataTable)
        Try
            Try
                Dim strConexion As String
                strConexion = GetSetting("Seesoft", "SeePos", "CONEXION")
                _sqlcommand.Connection = New SqlClient.SqlConnection(strConexion)
                Dim Da As New SqlClient.SqlDataAdapter
                Da.SelectCommand = _sqlcommand
                _dt.Clear()
                Da.Fill(_dt)
            Catch ex As SqlClient.SqlException
                If ex.LineNumber = 65536 Or ex.LineNumber = 0 Then
                    MsgBox(ex.Message, MsgBoxStyle.Information)

                Else
                    MsgBox(ex.Message, MsgBoxStyle.Information)

                End If
            End Try
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)

        End Try

    End Sub

End Class
