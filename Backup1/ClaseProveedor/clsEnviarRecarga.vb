﻿Public Class clsEnviarRecarga

    Public Function fnEnviarCoopeguanacaste(ByVal strFecha As String, ByVal strTelefono As String, ByVal strToken As String, ByVal strIdCliente As String, ByVal strUsuario As String, ByVal strOperador As String) As String
        Dim Recarga As New Recarga.wsRecargasCG
        Return Recarga.Recarga(strFecha, strTelefono, strToken, strIdCliente, strUsuario, strOperador)
    End Function

    Public Function fnDisponibleCoopeGuanacaste(ByVal strIdCliente As String) As String
        Dim Recarga As New Recarga.wsRecargasCG
        Return Recarga.Disponible(strIdCliente)
    End Function
End Class
