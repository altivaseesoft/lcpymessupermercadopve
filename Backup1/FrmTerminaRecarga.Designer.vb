﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTerminaRecarga
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTerminaRecarga))
        Me.Imagenes = New System.Windows.Forms.ImageList(Me.components)
        Me.btKolbi = New System.Windows.Forms.Button
        Me.btTuyoMovil = New System.Windows.Forms.Button
        Me.btFullMovil = New System.Windows.Forms.Button
        Me.txEstado = New System.Windows.Forms.Label
        Me.btRecargar = New System.Windows.Forms.Button
        Me.Button1 = New System.Windows.Forms.Button
        Me.btCheck = New System.Windows.Forms.Button
        Me.Check = New System.Windows.Forms.ImageList(Me.components)
        Me.btCheckNo = New System.Windows.Forms.Button
        Me.DtsProcesoRecarga = New ProcesaRecarga.dtsProcesoRecarga
        Me.bsRecarga = New System.Windows.Forms.BindingSource(Me.components)
        Me.Tb_S_RecargaTableAdapter = New ProcesaRecarga.dtsProcesoRecargaTableAdapters.tb_S_RecargaTableAdapter
        CType(Me.DtsProcesoRecarga, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bsRecarga, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Imagenes
        '
        Me.Imagenes.ImageStream = CType(resources.GetObject("Imagenes.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.Imagenes.TransparentColor = System.Drawing.Color.Transparent
        Me.Imagenes.Images.SetKeyName(0, "fullmovil.png")
        Me.Imagenes.Images.SetKeyName(1, "kolbi.png")
        Me.Imagenes.Images.SetKeyName(2, "Tuyo-Movil1.png")
        '
        'btKolbi
        '
        Me.btKolbi.FlatAppearance.BorderSize = 0
        Me.btKolbi.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btKolbi.ImageIndex = 1
        Me.btKolbi.ImageList = Me.Imagenes
        Me.btKolbi.Location = New System.Drawing.Point(23, 13)
        Me.btKolbi.Name = "btKolbi"
        Me.btKolbi.Size = New System.Drawing.Size(174, 110)
        Me.btKolbi.TabIndex = 0
        Me.btKolbi.UseVisualStyleBackColor = False
        '
        'btTuyoMovil
        '
        Me.btTuyoMovil.FlatAppearance.BorderSize = 0
        Me.btTuyoMovil.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btTuyoMovil.ImageIndex = 2
        Me.btTuyoMovil.ImageList = Me.Imagenes
        Me.btTuyoMovil.Location = New System.Drawing.Point(215, 13)
        Me.btTuyoMovil.Name = "btTuyoMovil"
        Me.btTuyoMovil.Size = New System.Drawing.Size(174, 110)
        Me.btTuyoMovil.TabIndex = 1
        Me.btTuyoMovil.UseVisualStyleBackColor = True
        '
        'btFullMovil
        '
        Me.btFullMovil.FlatAppearance.BorderSize = 0
        Me.btFullMovil.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btFullMovil.ImageIndex = 0
        Me.btFullMovil.ImageList = Me.Imagenes
        Me.btFullMovil.Location = New System.Drawing.Point(410, 13)
        Me.btFullMovil.Name = "btFullMovil"
        Me.btFullMovil.Size = New System.Drawing.Size(174, 110)
        Me.btFullMovil.TabIndex = 2
        Me.btFullMovil.UseVisualStyleBackColor = True
        '
        'txEstado
        '
        Me.txEstado.AutoSize = True
        Me.txEstado.Font = New System.Drawing.Font("Trebuchet MS", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txEstado.Location = New System.Drawing.Point(150, 249)
        Me.txEstado.Name = "txEstado"
        Me.txEstado.Size = New System.Drawing.Size(318, 24)
        Me.txEstado.TabIndex = 23
        Me.txEstado.Text = "La Recarga se realizó exitosamente."
        '
        'btRecargar
        '
        Me.btRecargar.BackColor = System.Drawing.Color.PowderBlue
        Me.btRecargar.FlatAppearance.BorderSize = 0
        Me.btRecargar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btRecargar.Font = New System.Drawing.Font("Trebuchet MS", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btRecargar.ForeColor = System.Drawing.Color.ForestGreen
        Me.btRecargar.Location = New System.Drawing.Point(23, 318)
        Me.btRecargar.Name = "btRecargar"
        Me.btRecargar.Size = New System.Drawing.Size(273, 53)
        Me.btRecargar.TabIndex = 32
        Me.btRecargar.Text = "Nueva Recarga"
        Me.btRecargar.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.PowderBlue
        Me.Button1.FlatAppearance.BorderSize = 0
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Trebuchet MS", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Red
        Me.Button1.Location = New System.Drawing.Point(311, 318)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(273, 53)
        Me.Button1.TabIndex = 33
        Me.Button1.Text = "Salir"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'btCheck
        '
        Me.btCheck.FlatAppearance.BorderSize = 0
        Me.btCheck.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btCheck.ImageIndex = 0
        Me.btCheck.ImageList = Me.Check
        Me.btCheck.Location = New System.Drawing.Point(215, 136)
        Me.btCheck.Name = "btCheck"
        Me.btCheck.Size = New System.Drawing.Size(174, 110)
        Me.btCheck.TabIndex = 34
        Me.btCheck.UseVisualStyleBackColor = True
        '
        'Check
        '
        Me.Check.ImageStream = CType(resources.GetObject("Check.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.Check.TransparentColor = System.Drawing.Color.Transparent
        Me.Check.Images.SetKeyName(0, "check.png")
        Me.Check.Images.SetKeyName(1, "no.png")
        '
        'btCheckNo
        '
        Me.btCheckNo.FlatAppearance.BorderSize = 0
        Me.btCheckNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btCheckNo.ImageIndex = 1
        Me.btCheckNo.ImageList = Me.Check
        Me.btCheckNo.Location = New System.Drawing.Point(215, 136)
        Me.btCheckNo.Name = "btCheckNo"
        Me.btCheckNo.Size = New System.Drawing.Size(174, 110)
        Me.btCheckNo.TabIndex = 35
        Me.btCheckNo.UseVisualStyleBackColor = True
        Me.btCheckNo.Visible = False
        '
        'DtsProcesoRecarga
        '
        Me.DtsProcesoRecarga.DataSetName = "dtsProcesoRecarga"
        Me.DtsProcesoRecarga.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'bsRecarga
        '
        Me.bsRecarga.DataMember = "tb_S_Recarga"
        Me.bsRecarga.DataSource = Me.DtsProcesoRecarga
        '
        'Tb_S_RecargaTableAdapter
        '
        Me.Tb_S_RecargaTableAdapter.ClearBeforeFill = True
        '
        'frmTerminaRecarga
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.ClientSize = New System.Drawing.Size(607, 400)
        Me.Controls.Add(Me.btCheckNo)
        Me.Controls.Add(Me.btCheck)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.btRecargar)
        Me.Controls.Add(Me.txEstado)
        Me.Controls.Add(Me.btFullMovil)
        Me.Controls.Add(Me.btTuyoMovil)
        Me.Controls.Add(Me.btKolbi)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(623, 438)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(623, 438)
        Me.Name = "frmTerminaRecarga"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Recarga Telefónica"
        CType(Me.DtsProcesoRecarga, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bsRecarga, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Imagenes As System.Windows.Forms.ImageList
    Friend WithEvents btKolbi As System.Windows.Forms.Button
    Friend WithEvents btTuyoMovil As System.Windows.Forms.Button
    Friend WithEvents btFullMovil As System.Windows.Forms.Button
    Friend WithEvents txEstado As System.Windows.Forms.Label
    Friend WithEvents btRecargar As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents btCheck As System.Windows.Forms.Button
    Friend WithEvents Check As System.Windows.Forms.ImageList
    Friend WithEvents btCheckNo As System.Windows.Forms.Button
    Friend WithEvents DtsProcesoRecarga As ProcesaRecarga.dtsProcesoRecarga
    Friend WithEvents bsRecarga As System.Windows.Forms.BindingSource
    Friend WithEvents Tb_S_RecargaTableAdapter As ProcesaRecarga.dtsProcesoRecargaTableAdapters.tb_S_RecargaTableAdapter
End Class
