﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPrincipal
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPrincipal))
        Me.Imagenes = New System.Windows.Forms.ImageList(Me.components)
        Me.btKolbi = New System.Windows.Forms.Button
        Me.btTuyoMovil = New System.Windows.Forms.Button
        Me.btFullMovil = New System.Windows.Forms.Button
        Me.txMontoRecargar = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.btRecargar = New System.Windows.Forms.Button
        Me.bt10000 = New System.Windows.Forms.Button
        Me.bt5000 = New System.Windows.Forms.Button
        Me.bt4000 = New System.Windows.Forms.Button
        Me.bt3000 = New System.Windows.Forms.Button
        Me.bt2000 = New System.Windows.Forms.Button
        Me.bt1500 = New System.Windows.Forms.Button
        Me.bt1000 = New System.Windows.Forms.Button
        Me.bt500 = New System.Windows.Forms.Button
        Me.Label5 = New System.Windows.Forms.Label
        Me.txNumeroCelular = New System.Windows.Forms.MaskedTextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtDisponible = New System.Windows.Forms.TextBox
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.SuspendLayout()
        '
        'Imagenes
        '
        Me.Imagenes.ImageStream = CType(resources.GetObject("Imagenes.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.Imagenes.TransparentColor = System.Drawing.Color.Transparent
        Me.Imagenes.Images.SetKeyName(0, "fullmovil.png")
        Me.Imagenes.Images.SetKeyName(1, "kolbi.png")
        Me.Imagenes.Images.SetKeyName(2, "Tuyo-Movil1.png")
        '
        'btKolbi
        '
        Me.btKolbi.FlatAppearance.BorderSize = 0
        Me.btKolbi.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btKolbi.ImageIndex = 1
        Me.btKolbi.ImageList = Me.Imagenes
        Me.btKolbi.Location = New System.Drawing.Point(23, 13)
        Me.btKolbi.Name = "btKolbi"
        Me.btKolbi.Size = New System.Drawing.Size(174, 110)
        Me.btKolbi.TabIndex = 0
        Me.btKolbi.UseVisualStyleBackColor = True
        '
        'btTuyoMovil
        '
        Me.btTuyoMovil.FlatAppearance.BorderSize = 0
        Me.btTuyoMovil.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btTuyoMovil.ImageIndex = 2
        Me.btTuyoMovil.ImageList = Me.Imagenes
        Me.btTuyoMovil.Location = New System.Drawing.Point(215, 13)
        Me.btTuyoMovil.Name = "btTuyoMovil"
        Me.btTuyoMovil.Size = New System.Drawing.Size(174, 110)
        Me.btTuyoMovil.TabIndex = 1
        Me.btTuyoMovil.UseVisualStyleBackColor = True
        '
        'btFullMovil
        '
        Me.btFullMovil.FlatAppearance.BorderSize = 0
        Me.btFullMovil.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btFullMovil.ImageIndex = 0
        Me.btFullMovil.ImageList = Me.Imagenes
        Me.btFullMovil.Location = New System.Drawing.Point(410, 13)
        Me.btFullMovil.Name = "btFullMovil"
        Me.btFullMovil.Size = New System.Drawing.Size(174, 110)
        Me.btFullMovil.TabIndex = 2
        Me.btFullMovil.UseVisualStyleBackColor = True
        '
        'txMontoRecargar
        '
        Me.txMontoRecargar.Font = New System.Drawing.Font("Trebuchet MS", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txMontoRecargar.Location = New System.Drawing.Point(23, 167)
        Me.txMontoRecargar.MaxLength = 6
        Me.txMontoRecargar.Name = "txMontoRecargar"
        Me.txMontoRecargar.Size = New System.Drawing.Size(233, 39)
        Me.txMontoRecargar.TabIndex = 19
        Me.txMontoRecargar.Text = "0"
        Me.txMontoRecargar.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Trebuchet MS", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(20, 137)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(195, 27)
        Me.Label3.TabIndex = 18
        Me.Label3.Text = "Monto a Recargar : "
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Trebuchet MS", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(348, 137)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(176, 27)
        Me.Label1.TabIndex = 20
        Me.Label1.Text = "Número Celular : "
        '
        'btRecargar
        '
        Me.btRecargar.BackColor = System.Drawing.Color.PowderBlue
        Me.btRecargar.FlatAppearance.BorderSize = 0
        Me.btRecargar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btRecargar.Font = New System.Drawing.Font("Trebuchet MS", 32.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btRecargar.ForeColor = System.Drawing.Color.ForestGreen
        Me.btRecargar.Location = New System.Drawing.Point(379, 291)
        Me.btRecargar.Name = "btRecargar"
        Me.btRecargar.Size = New System.Drawing.Size(205, 66)
        Me.btRecargar.TabIndex = 31
        Me.btRecargar.Text = "Recargar"
        Me.btRecargar.UseVisualStyleBackColor = False
        '
        'bt10000
        '
        Me.bt10000.Font = New System.Drawing.Font("Trebuchet MS", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bt10000.Location = New System.Drawing.Point(261, 327)
        Me.bt10000.Name = "bt10000"
        Me.bt10000.Size = New System.Drawing.Size(75, 30)
        Me.bt10000.TabIndex = 30
        Me.bt10000.Text = "10,000"
        Me.bt10000.UseVisualStyleBackColor = True
        '
        'bt5000
        '
        Me.bt5000.Font = New System.Drawing.Font("Trebuchet MS", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bt5000.Location = New System.Drawing.Point(184, 327)
        Me.bt5000.Name = "bt5000"
        Me.bt5000.Size = New System.Drawing.Size(75, 30)
        Me.bt5000.TabIndex = 29
        Me.bt5000.Text = "5,000"
        Me.bt5000.UseVisualStyleBackColor = True
        '
        'bt4000
        '
        Me.bt4000.Font = New System.Drawing.Font("Trebuchet MS", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bt4000.Location = New System.Drawing.Point(105, 327)
        Me.bt4000.Name = "bt4000"
        Me.bt4000.Size = New System.Drawing.Size(75, 30)
        Me.bt4000.TabIndex = 28
        Me.bt4000.Text = "4,000"
        Me.bt4000.UseVisualStyleBackColor = True
        '
        'bt3000
        '
        Me.bt3000.Font = New System.Drawing.Font("Trebuchet MS", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bt3000.Location = New System.Drawing.Point(28, 327)
        Me.bt3000.Name = "bt3000"
        Me.bt3000.Size = New System.Drawing.Size(75, 30)
        Me.bt3000.TabIndex = 27
        Me.bt3000.Text = "3,000"
        Me.bt3000.UseVisualStyleBackColor = True
        '
        'bt2000
        '
        Me.bt2000.Font = New System.Drawing.Font("Trebuchet MS", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bt2000.Location = New System.Drawing.Point(261, 291)
        Me.bt2000.Name = "bt2000"
        Me.bt2000.Size = New System.Drawing.Size(75, 30)
        Me.bt2000.TabIndex = 26
        Me.bt2000.Text = "2,000"
        Me.bt2000.UseVisualStyleBackColor = True
        '
        'bt1500
        '
        Me.bt1500.Font = New System.Drawing.Font("Trebuchet MS", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bt1500.Location = New System.Drawing.Point(184, 291)
        Me.bt1500.Name = "bt1500"
        Me.bt1500.Size = New System.Drawing.Size(75, 30)
        Me.bt1500.TabIndex = 25
        Me.bt1500.Text = "1,500"
        Me.bt1500.UseVisualStyleBackColor = True
        '
        'bt1000
        '
        Me.bt1000.Font = New System.Drawing.Font("Trebuchet MS", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bt1000.Location = New System.Drawing.Point(105, 291)
        Me.bt1000.Name = "bt1000"
        Me.bt1000.Size = New System.Drawing.Size(75, 30)
        Me.bt1000.TabIndex = 24
        Me.bt1000.Text = "1,000"
        Me.bt1000.UseVisualStyleBackColor = True
        '
        'bt500
        '
        Me.bt500.Font = New System.Drawing.Font("Trebuchet MS", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bt500.Location = New System.Drawing.Point(28, 291)
        Me.bt500.Name = "bt500"
        Me.bt500.Size = New System.Drawing.Size(75, 30)
        Me.bt500.TabIndex = 23
        Me.bt500.Text = "500"
        Me.bt500.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Trebuchet MS", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(25, 244)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(209, 27)
        Me.Label5.TabIndex = 22
        Me.Label5.Text = "Montos Disponibles : "
        '
        'txNumeroCelular
        '
        Me.txNumeroCelular.Font = New System.Drawing.Font("Trebuchet MS", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txNumeroCelular.Location = New System.Drawing.Point(351, 167)
        Me.txNumeroCelular.Mask = "0000-00-00"
        Me.txNumeroCelular.Name = "txNumeroCelular"
        Me.txNumeroCelular.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.txNumeroCelular.Size = New System.Drawing.Size(233, 39)
        Me.txNumeroCelular.TabIndex = 32
        Me.txNumeroCelular.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(67, 380)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(115, 18)
        Me.Label2.TabIndex = 33
        Me.Label2.Text = "Total Disponible : "
        '
        'txtDisponible
        '
        Me.txtDisponible.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDisponible.Location = New System.Drawing.Point(184, 379)
        Me.txtDisponible.Name = "txtDisponible"
        Me.txtDisponible.ReadOnly = True
        Me.txtDisponible.Size = New System.Drawing.Size(152, 20)
        Me.txtDisponible.TabIndex = 34
        Me.txtDisponible.Text = "asas"
        Me.txtDisponible.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Timer1
        '
        Me.Timer1.Interval = 1000
        '
        'frmPrincipal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.ClientSize = New System.Drawing.Size(607, 400)
        Me.Controls.Add(Me.txtDisponible)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txNumeroCelular)
        Me.Controls.Add(Me.btRecargar)
        Me.Controls.Add(Me.bt10000)
        Me.Controls.Add(Me.bt5000)
        Me.Controls.Add(Me.bt4000)
        Me.Controls.Add(Me.bt3000)
        Me.Controls.Add(Me.bt2000)
        Me.Controls.Add(Me.bt1500)
        Me.Controls.Add(Me.bt1000)
        Me.Controls.Add(Me.bt500)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txMontoRecargar)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.btFullMovil)
        Me.Controls.Add(Me.btTuyoMovil)
        Me.Controls.Add(Me.btKolbi)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(623, 438)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(623, 438)
        Me.Name = "frmPrincipal"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Recarga Telefónica"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Imagenes As System.Windows.Forms.ImageList
    Friend WithEvents btKolbi As System.Windows.Forms.Button
    Friend WithEvents btTuyoMovil As System.Windows.Forms.Button
    Friend WithEvents btFullMovil As System.Windows.Forms.Button
    Friend WithEvents txMontoRecargar As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btRecargar As System.Windows.Forms.Button
    Friend WithEvents bt10000 As System.Windows.Forms.Button
    Friend WithEvents bt5000 As System.Windows.Forms.Button
    Friend WithEvents bt4000 As System.Windows.Forms.Button
    Friend WithEvents bt3000 As System.Windows.Forms.Button
    Friend WithEvents bt2000 As System.Windows.Forms.Button
    Friend WithEvents bt1500 As System.Windows.Forms.Button
    Friend WithEvents bt1000 As System.Windows.Forms.Button
    Friend WithEvents bt500 As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txNumeroCelular As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtDisponible As System.Windows.Forms.TextBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
End Class
