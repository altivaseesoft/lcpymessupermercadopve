﻿Imports System.Windows.Forms
Imports System.Globalization

Public Class frmPrincipal

    Public pubOperador As String = "0"
    Public pubTelefono As String = ""
    Public pubMontoMinimo As Double = 0
    Public pubMontoRecargar As String = ""

    Dim priIdCliente As String = ""
    Dim Segundos As Integer = 0
    Private Sub frmPrincipal_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        spCargarDatos()
        spDisponible(True)
        Timer1.Start()
    End Sub
    Dim MontoDisponible As Double = 0
    Private Sub spDisponible(ByVal _Mostrar As Boolean)
        Dim clsEnviarRecarga As New clsEnviarRecarga
        Dim Cadena As String = ""

        Try
            Cadena = clsEnviarRecarga.fnDisponibleCoopeGuanacaste(priIdCliente)

            Dim datos() As String
            Dim Resultado As String
            MontoDisponible = 0
            datos = Cadena.Split("|")
            Resultado = datos(0)

            If Resultado = "FALSE" Then
                btRecargar.Enabled = False
                If _Mostrar Then MessageBox.Show("No tiene disponible para realizar la recarga.", "Atención...", MessageBoxButtons.OK)
            End If
            If Resultado = "TRUE" Then
                MontoDisponible = CDbl(Replace(datos(1), ",", "."))
                txtDisponible.Text = String.Format(CultureInfo.InvariantCulture, "{0:0,0.00}", MontoDisponible)
                montoTemporal = MontoDisponible
                If MontoDisponible < pubMontoMinimo Then
                    btRecargar.Enabled = False
                    If _Mostrar Then MessageBox.Show("No tiene disponible para realizar la recarga.", "Atención...", MessageBoxButtons.OK)
                End If
            End If
        Catch ex As Exception
            btRecargar.Enabled = False
            MessageBox.Show("No tiene disponible para realizar la recarga.", "Atención...", MessageBoxButtons.OK)
        End Try
    End Sub

    Private Function fnValidarRecarga() As Boolean
        Dim clsEnviarRecarga As New clsEnviarRecarga
        Dim Cadena As String = ""

        Try
            Cadena = clsEnviarRecarga.fnDisponibleCoopeGuanacaste(priIdCliente)

            Dim datos() As String
            Dim Resultado As String
            MontoDisponible = 0
            datos = Cadena.Split("|")
            Resultado = datos(0)

            If Resultado = "FALSE" Then
                MessageBox.Show("No tiene disponible para realizar la recarga.", "Atención...", MessageBoxButtons.OK)
                Return False
            End If
            If Resultado = "TRUE" Then
                MontoDisponible = CDbl(Replace(datos(1), ",", "."))
                If MontoDisponible < CDbl(txMontoRecargar.Text) Then
                    MessageBox.Show("No tiene disponible para realizar la recarga.", "Atención...", MessageBoxButtons.OK)
                    Return False
                End If
            End If
        Catch ex As Exception
            MessageBox.Show("No tiene disponible para realizar la recarga.", "Atención...", MessageBoxButtons.OK)
            Return False
        End Try
        Return True
    End Function


    Private Sub btKolbi_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btKolbi.Click
        Me.btKolbi.FlatAppearance.BorderSize = 1
        Me.btTuyoMovil.FlatAppearance.BorderSize = 0
        Me.btFullMovil.FlatAppearance.BorderSize = 0
        pubOperador = "1"
        txMontoRecargar.Focus()
    End Sub

    Private Sub btTuyoKolbi_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btTuyoMovil.Click
        Me.btKolbi.FlatAppearance.BorderSize = 0
        Me.btTuyoMovil.FlatAppearance.BorderSize = 1
        Me.btFullMovil.FlatAppearance.BorderSize = 0
        pubOperador = "2"
        txMontoRecargar.Focus()
    End Sub

    Private Sub btFullMovil_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btFullMovil.Click
        Me.btKolbi.FlatAppearance.BorderSize = 0
        Me.btTuyoMovil.FlatAppearance.BorderSize = 0
        Me.btFullMovil.FlatAppearance.BorderSize = 1
        pubOperador = "3"
        txMontoRecargar.Focus()
    End Sub

    Private Sub bt500_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bt500.Click
        txMontoRecargar.Text = "500"
        txNumeroCelular.Focus()
    End Sub

    Private Sub bt1000_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bt1000.Click
        txMontoRecargar.Text = "1000"
        txNumeroCelular.Focus()
    End Sub

    Private Sub bt1500_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bt1500.Click
        txMontoRecargar.Text = "1500"
        txNumeroCelular.Focus()
    End Sub

    Private Sub bt2000_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bt2000.Click
        txMontoRecargar.Text = "2000"
        txNumeroCelular.Focus()
    End Sub

    Private Sub bt3000_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bt3000.Click
        txMontoRecargar.Text = "3000"
        txNumeroCelular.Focus()
    End Sub

    Private Sub bt4000_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bt4000.Click
        txMontoRecargar.Text = "4000"
        txNumeroCelular.Focus()
    End Sub

    Private Sub bt5000_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bt5000.Click
        txMontoRecargar.Text = "5000"
        txNumeroCelular.Focus()
    End Sub

    Private Sub bt10000_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bt10000.Click
        txMontoRecargar.Text = "10000"
        txNumeroCelular.Focus()
    End Sub



    Private Sub txMontoRecargar_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txMontoRecargar.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            txNumeroCelular.Focus()
            Exit Sub
        End If
        If (Not Char.IsDigit(e.KeyChar)) Then ' valida que en este campo solo se digiten numeros y/o "-"
            If Not (e.KeyChar = Convert.ToChar(Keys.Back)) Then
                e.Handled = True  ' esto invalida la tecla pulsada
            End If
        End If
    End Sub

    Private Sub txNumeroCelular_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txNumeroCelular.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            If txNumeroCelular.MaskCompleted Then
                btRecargar.Focus()
                Exit Sub
            Else
                MsgBox("Por favor ingrese un Número Celular valido.", MsgBoxStyle.Information)
                Exit Sub
            End If
        End If
        If (Not Char.IsDigit(e.KeyChar)) Then ' valida que en este campo solo se digiten numeros y/o "-"
            If Not (e.KeyChar = Convert.ToChar(Keys.Back)) Then
                e.Handled = True  ' esto invalida la tecla pulsada
                Exit Sub
            End If
        End If
    End Sub

    Private Sub btRecargar_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btRecargar.Click
        If Not fnValidarRecarga() Then
            Exit Sub
        End If
        If pubOperador = "0" Then
            MsgBox("Por favor seleccione un Operador.", MsgBoxStyle.Information)
            Exit Sub
        End If
        If txMontoRecargar.Text = "0" Or CDbl(txMontoRecargar.Text) < pubMontoMinimo Then
            MsgBox("El monto a recargar debe ser igual o mayor a : " & pubMontoMinimo & " Colones", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Not txNumeroCelular.MaskCompleted Then
            MsgBox("Por favor ingrese un Número Celular valido.", MsgBoxStyle.Information)
            Exit Sub
        End If
        pubMontoRecargar = Me.txMontoRecargar.Text
        pubTelefono = Replace(txNumeroCelular.Text, "-", "")
        DialogResult = Windows.Forms.DialogResult.OK
    End Sub
    Private Sub spCargarDatos()
        Dim priProveedorRecarga As Integer = GetSetting("Seesoft", "SeePos", "ProveedorRecarga")

        Dim sql As New SqlClient.SqlCommand
        Dim dt As New DataTable

        sql.CommandText = "SELECT IdCliente, MontoMinimoRecarga  FROM [Seepos].[dbo].[tb_S_ConfiguracionRecarga] WHERE [CodigoProveedorRecarga] = " & priProveedorRecarga

        clsEnlace.spCargarDatos(sql, dt)

        If dt.Rows.Count > 0 Then
            pubMontoMinimo = dt.Rows(0).Item("MontoMinimoRecarga")
            priIdCliente = dt.Rows(0).Item("IdCliente")
        End If

    End Sub
    Dim montoTemporal As String
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Segundos += 1

        If txtDisponible.Text = montoTemporal Then
            txtDisponible.ForeColor = Drawing.Color.Black
        Else
            txtDisponible.ForeColor = Drawing.Color.Red
        End If

        If Segundos = 3 Then
            Segundos = 0
            spDisponible(False)
            montoTemporal = txtDisponible.Text
        End If

    End Sub
End Class