﻿Public Class clsProcesaRecarga
    Dim cedula As String
    Public Sub New(ByVal Usuario_Parametro As String)
        MyBase.New()
        cedula = Usuario_Parametro
    End Sub

    Public Sub spProcesarLLamada(ByRef _dt As DataTable)

        Dim frmPrincipal As New frmPrincipal
        Dim frmTerminaRecarga As New frmTerminaRecarga

        Dim Fila As DataRow = _dt.NewRow

        If frmPrincipal.ShowDialog = Windows.Forms.DialogResult.OK Then
            frmTerminaRecarga.pubMonto = frmPrincipal.pubMontoRecargar
            frmTerminaRecarga.pubTelefono = frmPrincipal.pubTelefono
            frmTerminaRecarga.pubOperador = frmPrincipal.pubOperador
            frmTerminaRecarga.pubCedula = cedula
            If frmTerminaRecarga.ShowDialog = Windows.Forms.DialogResult.Yes Then
                If frmTerminaRecarga.Estado Then
                    Fila("IdRecarga") = frmTerminaRecarga.IdRecarga_Coope
                    Fila("Numero") = frmTerminaRecarga.pubTelefono
                    _dt.Rows.Add(Fila)
                End If
                frmPrincipal.Close()
                frmTerminaRecarga.Close()
                spNuevo(_dt)

            Else
                If frmTerminaRecarga.Estado Then
                    Fila("IdRecarga") = frmTerminaRecarga.IdRecarga_Coope
                    Fila("Numero") = frmTerminaRecarga.pubTelefono
                    _dt.Rows.Add(Fila)
                End If
                frmPrincipal.Close()
                frmTerminaRecarga.Close()

            End If

        End If

    End Sub

    Private Sub spNuevo(ByRef _dt As DataTable)

        Dim frmPrincipal As New frmPrincipal
       Dim frmTerminaRecarga As New frmTerminaRecarga

        Dim Fila As DataRow = _dt.NewRow

        If frmPrincipal.ShowDialog = Windows.Forms.DialogResult.OK Then
            frmTerminaRecarga.pubMonto = frmPrincipal.pubMontoRecargar
            frmTerminaRecarga.pubTelefono = frmPrincipal.pubTelefono
            frmTerminaRecarga.pubOperador = frmPrincipal.pubOperador
            frmTerminaRecarga.pubCedula = cedula
            If frmTerminaRecarga.ShowDialog = Windows.Forms.DialogResult.Yes Then
                If frmTerminaRecarga.Estado Then
                    Fila("IdRecarga") = frmTerminaRecarga.IdRecarga_Coope
                    Fila("Numero") = frmTerminaRecarga.pubTelefono
                    _dt.Rows.Add(Fila)
                End If
                frmPrincipal.Close()
                frmTerminaRecarga.Close()
                spNuevo(_dt)

            Else
                If frmTerminaRecarga.Estado Then
                    Fila("IdRecarga") = frmTerminaRecarga.IdRecarga_Coope
                    Fila("Numero") = frmTerminaRecarga.pubTelefono
                    _dt.Rows.Add(Fila)
                End If
                frmPrincipal.Close()
                frmTerminaRecarga.Close()

            End If

        End If

    End Sub



End Class
