﻿Imports System.Deployment

Public Class frmActualiza

    Dim ver As clsVersion
    Sub New(_ver As clsVersion)
        InitializeComponent()
        ver = _ver

    End Sub
    Private Sub frmActualiza_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        spCargar()
    End Sub

    Sub spCargar()
        Text = "Centro de Actualización - Versión Actual: " & ver.fnVersionActual
        If ver.fnPermiteUpdate(txEstado.Text) Then
            btActualizar.Enabled = True
            btComprobar.Enabled = True
        Else
            btActualizar.Enabled = False
            btComprobar.Enabled = False
        End If

    End Sub

    Private Sub btComprobar_Click(sender As Object, e As EventArgs) Handles btComprobar.Click
        spComprobar()

    End Sub

    Sub spComprobar()
        If ver.fnHayUpdate(txEstado.Text) Then
        End If
    End Sub

    Private Sub btDescargar_Click(sender As Object, e As EventArgs)
        spDescargar()
    End Sub

    Sub spDescargar()
        If ver.fnDescarga(txEstado.Text) Then
        End If
    End Sub

    Private Sub btActualizar_Click(sender As Object, e As EventArgs) Handles btActualizar.Click
        spActualizar()
    End Sub

    Sub spActualizar()
        If ver.fnActualizar(txEstado.Text) Then
            MsgBox("El sistema fue actualizado. Se cerrará el sistema.", MsgBoxStyle.OkOnly)
            DialogResult = Windows.Forms.DialogResult.OK
            Close()
        End If
    End Sub

End Class