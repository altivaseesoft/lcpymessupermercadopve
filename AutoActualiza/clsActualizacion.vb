﻿Public Class clsActualizacion
    Public Shared cerrar As Boolean = False
    Public Shared Sub spAbrir(_aplicacion As Deployment.Application.ApplicationDeployment, IsNetWorkDeployed As Boolean)

        Try
            Dim ver As New AutoActualiza.clsVersion(_aplicacion)
            ver.EsPosible = IsNetWorkDeployed
            Dim fr As New AutoActualiza.frmActualiza(ver)
            If fr.ShowDialog = Windows.Forms.DialogResult.OK Then
                cerrar = True
            End If
            cerrar = False
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.OkOnly)

        End Try
    End Sub

End Class
