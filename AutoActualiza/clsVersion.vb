﻿Imports System.Deployment

Public Class clsVersion
    Public EsPosible As Boolean = True
    Public app As Deployment.Application.ApplicationDeployment
    Sub New(_aplicacion As Deployment.Application.ApplicationDeployment)
        app = _aplicacion
    End Sub
    Function fnVersionActual() As String
        Return app.CurrentVersion.ToString()
    End Function
    Function fnPermiteUpdate(ByRef msj As String) As Boolean
        If EsPosible Then

            msj = "Versión Actual: " & fnVersionActual()
            EsPosible = True
            Return True
        Else
            EsPosible = False
            msj = "No es posible actualizar esta versión del sistema."
            Return False
        End If
    End Function
    Function fnHayUpdate(ByRef msj As String) As Boolean
        Try

            If EsPosible Then
                If app.CheckForUpdate() Then
                    msj = "Existe una nueva actualización."
                    Return True
                Else
                    If fnPermiteUpdate(msj) Then
                        msj = "Tienes la última versión." & vbCrLf & msj

                    End If
                    Return False
                End If
            Else
                msj = "No es posible actualizar esta versión del sistema."
                Return False
            End If
        Catch ex As Exception
            MsgBox("Error al comprobar descargas", MsgBoxStyle.OkOnly)
            msj = "No es posible descargar la actualización. " & ex.Message
            Return False
        End Try
    End Function
    Function fnDescarga(ByRef msj As String) As Boolean
        If Not fnHayUpdate(msj) Then
            Return False
        End If
        If Not (app.IsFileGroupDownloaded("SeeSoft")) Then
            app.DownloadFileGroup("SeeSoft")
        End If
        msj = "Descarga realizada satisfactoriamente"
        Return True
    End Function
    Function fnActualizar(ByRef msj As String) As Boolean
        If Not fnHayUpdate(msj) Then
            Return False
        Else
            Try
                If EsPosible Then
                    msj = "Actualizando el sistema"
                    app.UpdateAsync()
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                msj = "Error al actualizar: " & ex.Message
                Return False
            End Try
        End If

    End Function
End Class
