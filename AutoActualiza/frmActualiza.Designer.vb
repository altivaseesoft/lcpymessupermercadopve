﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmActualiza
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmActualiza))
        Me.btComprobar = New System.Windows.Forms.Button()
        Me.btActualizar = New System.Windows.Forms.Button()
        Me.lbEstado = New System.Windows.Forms.Label()
        Me.txEstado = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'btComprobar
        '
        Me.btComprobar.Location = New System.Drawing.Point(12, 12)
        Me.btComprobar.Name = "btComprobar"
        Me.btComprobar.Size = New System.Drawing.Size(75, 23)
        Me.btComprobar.TabIndex = 0
        Me.btComprobar.Text = "Comprobar"
        Me.btComprobar.UseVisualStyleBackColor = True
        '
        'btActualizar
        '
        Me.btActualizar.Location = New System.Drawing.Point(93, 12)
        Me.btActualizar.Name = "btActualizar"
        Me.btActualizar.Size = New System.Drawing.Size(75, 23)
        Me.btActualizar.TabIndex = 2
        Me.btActualizar.Text = "Actualizar"
        Me.btActualizar.UseVisualStyleBackColor = True
        '
        'lbEstado
        '
        Me.lbEstado.AutoSize = True
        Me.lbEstado.Location = New System.Drawing.Point(12, 48)
        Me.lbEstado.Name = "lbEstado"
        Me.lbEstado.Size = New System.Drawing.Size(134, 13)
        Me.lbEstado.TabIndex = 3
        Me.lbEstado.Text = "Estado de la actualización:"
        '
        'txEstado
        '
        Me.txEstado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txEstado.Location = New System.Drawing.Point(11, 67)
        Me.txEstado.Multiline = True
        Me.txEstado.Name = "txEstado"
        Me.txEstado.ReadOnly = True
        Me.txEstado.Size = New System.Drawing.Size(437, 87)
        Me.txEstado.TabIndex = 4
        '
        'frmActualiza
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(457, 163)
        Me.Controls.Add(Me.txEstado)
        Me.Controls.Add(Me.lbEstado)
        Me.Controls.Add(Me.btActualizar)
        Me.Controls.Add(Me.btComprobar)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(465, 190)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(465, 190)
        Me.Name = "frmActualiza"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Actualización"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btComprobar As System.Windows.Forms.Button
    Friend WithEvents btActualizar As System.Windows.Forms.Button
    Friend WithEvents lbEstado As System.Windows.Forms.Label
    Friend WithEvents txEstado As System.Windows.Forms.TextBox
End Class
