Imports System.Data.SqlClient

Public Class frmBuscarCliente
    Inherits System.Windows.Forms.Form
    Public Shared NuevaConexion As String

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents btnAceptar As DevExpress.XtraEditors.SimpleButton     'System.Windows.Forms.Button
    Friend WithEvents btnCancelar As DevExpress.XtraEditors.SimpleButton  'System.Windows.Forms.Button
    Friend WithEvents txtBusqueda As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents AdvBandedGridView1 As DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView
    Friend WithEvents GridBand2 As DevExpress.XtraGrid.Views.BandedGrid.GridBand
    Friend WithEvents AdapterClientes As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection
    Friend WithEvents DsCliente1 As DsCliente
    Friend WithEvents colIdentificacion As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents colNombre As DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
    Friend WithEvents dg As DevExpress.XtraGrid.GridControl
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.txtBusqueda = New System.Windows.Forms.TextBox
        Me.btnAceptar = New DevExpress.XtraEditors.SimpleButton
        Me.btnCancelar = New DevExpress.XtraEditors.SimpleButton
        Me.Label1 = New System.Windows.Forms.Label
        Me.dg = New DevExpress.XtraGrid.GridControl
        Me.DsCliente1 = New DsCliente
        Me.AdvBandedGridView1 = New DevExpress.XtraGrid.Views.BandedGrid.AdvBandedGridView
        Me.GridBand2 = New DevExpress.XtraGrid.Views.BandedGrid.GridBand
        Me.colIdentificacion = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.colNombre = New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn
        Me.AdapterClientes = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        CType(Me.dg, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsCliente1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AdvBandedGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtBusqueda
        '
        Me.txtBusqueda.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtBusqueda.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtBusqueda.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBusqueda.ForeColor = System.Drawing.Color.RoyalBlue
        Me.txtBusqueda.Location = New System.Drawing.Point(88, 290)
        Me.txtBusqueda.Name = "txtBusqueda"
        Me.txtBusqueda.Size = New System.Drawing.Size(376, 16)
        Me.txtBusqueda.TabIndex = 0
        Me.txtBusqueda.Text = ""
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(304, 312)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(72, 24)
        Me.btnAceptar.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.Transparent, System.Drawing.Color.MidnightBlue)
        Me.btnAceptar.TabIndex = 2
        Me.btnAceptar.Text = "Aceptar"
        '
        'btnCancelar
        '
        Me.btnCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancelar.Location = New System.Drawing.Point(384, 313)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(72, 23)
        Me.btnCancelar.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.Color.MidnightBlue)
        Me.btnCancelar.TabIndex = 3
        Me.btnCancelar.Text = "Cancelar"
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.RoyalBlue
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(0, 290)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(80, 16)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Busqueda  -->"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'dg
        '
        Me.dg.BackColor = System.Drawing.Color.WhiteSmoke
        Me.dg.DataMember = "Clientes"
        Me.dg.DataSource = Me.DsCliente1
        '
        'dg.EmbeddedNavigator
        '
        Me.dg.EmbeddedNavigator.Name = ""
        Me.dg.Location = New System.Drawing.Point(8, 8)
        Me.dg.MainView = Me.AdvBandedGridView1
        Me.dg.Name = "dg"
        Me.dg.Size = New System.Drawing.Size(448, 264)
        Me.dg.Styles.AddReplace("ColumnFilterButtonActive", New DevExpress.Utils.ViewStyleEx("ColumnFilterButtonActive", "Grid", System.Drawing.SystemColors.Control, System.Drawing.Color.Blue, System.Drawing.SystemColors.ControlLightLight, System.Drawing.Drawing2D.LinearGradientMode.Horizontal))
        Me.dg.TabIndex = 5
        '
        'DsCliente1
        '
        Me.DsCliente1.DataSetName = "DsCliente"
        Me.DsCliente1.Locale = New System.Globalization.CultureInfo("es-CR")
        '
        'AdvBandedGridView1
        '
        Me.AdvBandedGridView1.Bands.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.GridBand() {Me.GridBand2})
        Me.AdvBandedGridView1.Columns.AddRange(New DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn() {Me.colIdentificacion, Me.colNombre})
        Me.AdvBandedGridView1.Name = "AdvBandedGridView1"
        Me.AdvBandedGridView1.OptionsView.ShowGroupPanel = False
        '
        'GridBand2
        '
        Me.GridBand2.Caption = "Seleccione Cliente"
        Me.GridBand2.Columns.Add(Me.colIdentificacion)
        Me.GridBand2.Columns.Add(Me.colNombre)
        Me.GridBand2.Name = "GridBand2"
        Me.GridBand2.Options = CType(((DevExpress.XtraGrid.Views.BandedGrid.BandOptions.CanResized Or DevExpress.XtraGrid.Views.BandedGrid.BandOptions.CanHotTracked) _
                    Or DevExpress.XtraGrid.Views.BandedGrid.BandOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Views.BandedGrid.BandOptions)
        Me.GridBand2.Width = 263
        '
        'colIdentificacion
        '
        Me.colIdentificacion.Caption = "Identificación"
        Me.colIdentificacion.FieldName = "Identificacion"
        Me.colIdentificacion.Name = "colIdentificacion"
        Me.colIdentificacion.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.FixedWidth) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colIdentificacion.Visible = True
        Me.colIdentificacion.Width = 50
        '
        'colNombre
        '
        Me.colNombre.Caption = "Nombre"
        Me.colNombre.FieldName = "Nombre"
        Me.colNombre.Name = "colNombre"
        Me.colNombre.Options = CType((((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanFiltered Or DevExpress.XtraGrid.Columns.ColumnOptions.CanGrouped) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.FixedWidth) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
                    Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
        Me.colNombre.Visible = True
        Me.colNombre.Width = 213
        '
        'AdapterClientes
        '
        Me.AdapterClientes.DeleteCommand = Me.SqlDeleteCommand1
        Me.AdapterClientes.InsertCommand = Me.SqlInsertCommand1
        Me.AdapterClientes.SelectCommand = Me.SqlSelectCommand1
        Me.AdapterClientes.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Clientes", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Identificacion", "Identificacion"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre")})})
        Me.AdapterClientes.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM Clientes WHERE (identificacion = @Original_Identificacion) AND (nombr" & _
        "e = @Original_Nombre)"
        Me.SqlDeleteCommand1.Connection = Me.SqlConnection1
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Identificacion", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Identificacion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlConnection1
        '
        Me.SqlConnection1.ConnectionString = "workstation id=OSCAR;packet size=4096;integrated security=SSPI;data source=OSCAR;" & _
        "persist security info=False;initial catalog=SeePos"
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO Clientes(identificacion, nombre) VALUES (@identificacion, @nombre); S" & _
        "ELECT TOP 50 identificacion AS Identificacion, nombre AS Nombre FROM Clientes WH" & _
        "ERE (identificacion = @Identificacion)"
        Me.SqlInsertCommand1.Connection = Me.SqlConnection1
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@identificacion", System.Data.SqlDbType.Int, 4, "Identificacion"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@nombre", System.Data.SqlDbType.VarChar, 255, "Nombre"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Identificacion", System.Data.SqlDbType.Int, 4, "Identificacion"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT TOP 50 identificacion AS Identificacion, nombre AS Nombre FROM Clientes"
        Me.SqlSelectCommand1.Connection = Me.SqlConnection1
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE Clientes SET identificacion = @identificacion, nombre = @nombre WHERE (ide" & _
        "ntificacion = @Original_Identificacion) AND (nombre = @Original_Nombre); SELECT " & _
        "TOP 50 identificacion AS Identificacion, nombre AS Nombre FROM Clientes WHERE (i" & _
        "dentificacion = @Identificacion)"
        Me.SqlUpdateCommand1.Connection = Me.SqlConnection1
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@identificacion", System.Data.SqlDbType.Int, 4, "Identificacion"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@nombre", System.Data.SqlDbType.VarChar, 255, "Nombre"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Identificacion", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Identificacion", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Nombre", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Identificacion", System.Data.SqlDbType.Int, 4, "Identificacion"))
        '
        'frmBuscarCliente
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.CancelButton = Me.btnCancelar
        Me.ClientSize = New System.Drawing.Size(464, 342)
        Me.ControlBox = False
        Me.Controls.Add(Me.dg)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.txtBusqueda)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmBuscarCliente"
        Me.Opacity = 0.95
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Buscar Cliente"
        CType(Me.dg, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsCliente1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AdvBandedGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Variables"
    Public Codigo As String
    Public Descrip As String
    Public Cancelado As Boolean
    Dim TopBuscador As Integer = 50
#End Region

#Region "Load"
    Private Sub Buscar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.SqlConnection1.ConnectionString = GetSetting("SeeSOFT", "SeePos", "Conexion")

        Me.DsCliente1.Clientes.DefaultView.AllowDelete = False
        Me.DsCliente1.Clientes.DefaultView.AllowEdit = False
        Me.DsCliente1.Clientes.DefaultView.AllowNew = False

        Me.AdapterClientes.Fill(Me.DsCliente1, "Clientes")
        If Me.BindingContext(Me.DsCliente1.Clientes.DefaultView).Count Then
            Me.BindingContext(Me.DsCliente1.Clientes.DefaultView).Position = 0
        End If

        '-----------------------------------------------------------------------
        'VERIFICA LA CANTIDAD DEL TOP EN LOS BUSCADORES - ORA
        Try
            TopBuscador = CInt(GetSetting("SeeSOFT", "SeePos", "TopBuscadores"))
        Catch ex As Exception
            SaveSetting("SeeSOFT", "SeePos", "TopBuscadores", "50")
            TopBuscador = 50
        End Try
        '-----------------------------------------------------------------------

        Me.txtBusqueda.Focus()
    End Sub
#End Region

#Region "Cargar Informacion"
    Private Sub cargarInformacion(ByVal sWhere As String)
        Try
            Me.DsCliente1.Clear()
            Me.AdapterClientes.SelectCommand.CommandText = "Select TOP " & TopBuscador & " identificacion as Identificacion,nombre as Nombre from Clientes " & sWhere
            Me.AdapterClientes.Fill(Me.DsCliente1, "Clientes")
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
#End Region

#Region "Seleccionar Codigo"
    Sub SeleccionarCodigo()
        Dim hi As DevExpress.XtraGrid.Views.BandedGrid.ViewInfo.BandedGridHitInfo = AdvBandedGridView1.CalcHitInfo(CType(dg, Control).PointToClient(Control.MousePosition))
        If hi.RowHandle >= 0 Then
            Codigo = AdvBandedGridView1.GetDataRow(AdvBandedGridView1.FocusedRowHandle).ItemArray(0)
        ElseIf AdvBandedGridView1.FocusedRowHandle >= 0 Then
            Codigo = AdvBandedGridView1.GetDataRow(AdvBandedGridView1.FocusedRowHandle).ItemArray(0)
        Else
            Codigo = 0
        End If
    End Sub
#End Region

#Region "Funciones Controles"
    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click, dg.DoubleClick
        If Me.BindingContext(Me.DsCliente1.Clientes).Count > 0 Then
            SeleccionarCodigo()
            Cancelado = False
        Else
            Cancelado = True
        End If
        Close()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Cancelado = True
        Close()
    End Sub

    Private Sub txtBusqueda_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtBusqueda.TextChanged
        If Len(txtBusqueda.Text) > 2 Then
            cargarInformacion(" where Nombre like '%" & txtBusqueda.Text & "%'")
        ElseIf Trim(txtBusqueda.Text) = vbNullString Then
            cargarInformacion("")
        End If
    End Sub

    Private Sub Buscar_Closed(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Closed
        Me.Dispose(True)
    End Sub

    Private Sub btnAceptar_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles btnAceptar.KeyDown
        If e.KeyCode = Keys.Enter Then
            If Me.BindingContext(Me.DsCliente1.Clientes).Count > 0 Then
                SeleccionarCodigo()
                Cancelado = False
            Else
                Cancelado = True
            End If
            Close()
        End If
    End Sub

    Private Sub dg_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dg.KeyDown
        If e.KeyCode = Keys.Enter Then
            If Me.BindingContext(Me.DsCliente1.Clientes).Count > 0 Then
                SeleccionarCodigo
                Cancelado = False
            Else
                Cancelado = True
            End If
            Close()
        End If
    End Sub

    Private Sub txtBusqueda_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtBusqueda.KeyDown
        If e.KeyCode = Keys.Enter Then
            dg.Focus()
        End If
    End Sub
#End Region

End Class
