Imports System.Data
Imports System.Data.SqlClient

Public Class GestioDatos
    Private conexion As New SqlClient.SqlConnection(GetSetting("Seesoft", "SeePOS", "Conexion"))
    Private adaptador As SqlClient.SqlDataAdapter
    Public Function Ejecuta(ByVal sql As String) As DataTable
        Try
            Dim dts As New DataTable
            conexion.Open()
            adaptador = New SqlClient.SqlDataAdapter(sql, conexion)
            adaptador.SelectCommand.CommandType = CommandType.Text
            adaptador.Fill(dts)
            conexion.Close()
            Return dts
        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message)
            Return Nothing
        End Try
    End Function
End Class
