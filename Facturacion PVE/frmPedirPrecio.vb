﻿

Imports System.Windows.Forms

Public Class frmPedirPrecio

    Dim frmFacturacion As FacturacionPVE
    Public PrecioBase As Double = 0
    Private Sub btAceptar_Click(sender As Object, e As EventArgs) Handles btAceptar.Click
        spAceptar()
    End Sub

    Private Sub spAceptar()
        Try
            If CDbl(txtPrecio.Text) > 0 Then
                Dim frm As FacturacionPVE = CType(Owner, FacturacionPVE)
                Dim Cantidad As Double = 0
                Cantidad = CDbl(txtPrecio.Text) / PrecioBase
                frm.txtCantidad.Text = Cantidad
                Me.Close()
            End If

        Catch ex As Exception
            MsgBox("Solo números para este campo. ", MsgBoxStyle.Information)
        End Try
    End Sub

    Private Sub txtPrecio_KeyPress(sender As Object, e As Windows.Forms.KeyPressEventArgs) Handles txtPrecio.KeyPress

    End Sub

    Private Sub frmPedirPrecio_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub txtPrecio_KeyDown(sender As Object, e As Windows.Forms.KeyEventArgs) Handles txtPrecio.KeyDown
        If e.KeyCode = Keys.Enter Then
            spAceptar()
        End If
    End Sub
End Class