Imports System.Data.SqlClient
Imports System.Drawing.Printing
Imports System
Imports ProcesaRecarga
Imports System.IO.Ports
Imports System.IO
Imports AutoActualiza

Public Class FacturacionPVE
	Inherits System.Windows.Forms.Form

#Region "Variables"
	Dim iddebodega As Integer
	Dim Cedula_usuario As String
	Dim Nombre_usuario As String
	Dim porcentaje_descuento As Double
	Dim Existencia, Comision As Double
	Dim perfil_administrador As Boolean
	Dim cliente_cargado As Boolean
	Dim Imp_Conf As Double ' almacena el impuesto d eventa traido de la tabla configuraciones
	Dim vende_existecias_negativas As Boolean
	Dim impuesto_cliente As Double
	Dim variacion_Punit As Double
	Dim Monto_Adeudado As Double
	Dim ayuda As Boolean
	Dim Usua As Usuario_Logeado
	Dim Cambio_Cantidad As Boolean
	Dim max_aplicar As Double 'almacena el maximo porcentaje de descuento que se puede aplicar a determinado articulo
	Dim buscando As Boolean = False
	'varibles de articulos
	Dim PrecioBase As Double
	Dim PrecioCosto As Double
	Dim Flete As Double
	Dim OtrosMontos As Double
	Dim PrecioA As Double
	Dim PrecioB As Double
	Dim PrecioC As Double
	Dim PrecioD As Double
	Dim ValorCosto As Double
	Dim ValorVenta As Double
	Dim MonedaCosto As Integer
	Dim MonedaVenta As Integer
	Dim MonedaBase As Integer
	Dim ValorBase As Double
	Dim MontoImpuesto As Double
	Dim precio_unitario As Double
	Dim Max_Descuento_Articulo As Double
	Dim promo_activa_valor As Boolean
	Dim precio_promo_valor As Double
	Dim monto_Perdido As Double
	Dim CConexion As New Conexion
	Dim mensaje As String ' almacena el mensaje de los descuentos
	Dim password_antiguo As String
	Dim logeado As Boolean
	Dim coti As Boolean
	Dim Importando, RequierePrecio As Boolean
	Dim CargoCliente As Boolean
	Dim PremiosLealtad As Boolean = False
	Dim Recarga As Boolean = False
	Dim EscanerBalanza As Boolean = False
	Dim RedondeoTotal As Integer = 2
	' Dim Factu_Reporte As New Reporte_FacturaPVEs ' Reporte_Factura
	'    Dim FacturaPVE As New CrystalDecisions.CrystalReports.Engine.ReportDocument 'As New Reporte_FacturaPVEs

	Dim FacturaPVE As New CrystalDecisions.CrystalReports.Engine.ReportDocument 'Reporte_FacturaPVEs
	Dim FacturaPVE1 As New CrystalDecisions.CrystalReports.Engine.ReportDocument 'Reporte_FacturaPVEs1
	Dim Factura_Generica As New CrystalDecisions.CrystalReports.Engine.ReportDocument 'Factura_Generica

	'Dim FacturaPVESug As New Reporte_FacturaPVEs_Sugerido
	Dim FrmVuelto As New Vuelto
	Dim Movimiento_Pago_Factura As frmMovimientoCajaPagoAbono
	Dim PMU As New PerfilModulo_Class
	Dim TopBuscador As Integer = 50
	Dim generarConsec As New GenerarConsecutivoHA

	Dim AgregandoNuevoItem As Boolean
	'CABYS Y TIPO CLIENTE
	Dim imp_iva_cliente As Double = 13
	Dim tipoCliente As Integer = 0
	Dim imp_producto As Double
	Dim ImpuestoIV As Double
	Dim UsaLector As String = "0"
	Dim BanderaImpuesto As Boolean = False

#Region " Variable "                 'Definicion de Variable 
	Private sqlConexion As SqlConnection
	Private nuevo As Boolean = True
	Private PorcCambiarPrecio As Double
	Private PorcDescuento As Double
	Private Anula As Boolean
	Private abierto As Boolean
	Private impuesto As Double
	Private max_credito As Double
	Private plazo_credito As Integer
	Private descuento As Double
	Private tipoprecio As Integer
	Private sinrestriccion As Boolean
	Private Exento As Double
	Private Gravado As Double
	Private DescuentoCalc As Double
	Friend WithEvents txtNombreArt As System.Windows.Forms.TextBox
	Friend WithEvents txtNomComisionista As System.Windows.Forms.TextBox
	Friend WithEvents txtComisionista As ValidText.ValidText
	Friend WithEvents Label8 As System.Windows.Forms.Label
	Friend WithEvents SqlSelectCommand6 As System.Data.SqlClient.SqlCommand
	Friend WithEvents SqlInsertCommand4 As System.Data.SqlClient.SqlCommand
	Friend WithEvents SqlUpdateCommand4 As System.Data.SqlClient.SqlCommand
	Friend WithEvents SqlDeleteCommand4 As System.Data.SqlClient.SqlCommand
	Friend WithEvents Adapter_Ventas As System.Data.SqlClient.SqlDataAdapter
	Friend WithEvents txtMontoComision As System.Windows.Forms.TextBox
	Friend WithEvents Txtbodega As DevExpress.XtraEditors.TextEdit
	Friend WithEvents AdapterConfig As System.Data.SqlClient.SqlDataAdapter
	Friend WithEvents SqlCommand1 As System.Data.SqlClient.SqlCommand
	Friend WithEvents CB_Exonerar As System.Windows.Forms.CheckBox
	Friend WithEvents Label10 As System.Windows.Forms.Label
	Friend WithEvents CB_Moneda As System.Windows.Forms.ComboBox
	Friend WithEvents SimpleButton4 As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents Label11 As System.Windows.Forms.Label
	Friend WithEvents txtObservacion As System.Windows.Forms.TextBox
	Friend WithEvents SqlDeleteCommand As System.Data.SqlClient.SqlCommand
	Friend WithEvents SqlInsertCommand As System.Data.SqlClient.SqlCommand
	Friend WithEvents SqlUpdateCommand As System.Data.SqlClient.SqlCommand
	Friend WithEvents colBarra As DevExpress.XtraGrid.Columns.GridColumn
	Friend WithEvents ToolBarRecarga As System.Windows.Forms.ToolBarButton
	Friend WithEvents SerialPort1 As System.IO.Ports.SerialPort
	Friend WithEvents bwEnviarSolicitudPeso As System.ComponentModel.BackgroundWorker
	Friend WithEvents ToolBarPesa As System.Windows.Forms.ToolBarButton
	Friend WithEvents Label14 As Label
	Friend WithEvents Label18 As Label
	Friend WithEvents txtIVA As DevExpress.XtraEditors.TextEdit
	Friend WithEvents colIVA As DevExpress.XtraGrid.Columns.GridColumn
	Friend WithEvents TxtBarras As DevExpress.XtraEditors.TextEdit
	Friend WithEvents Label20 As Label
	Friend WithEvents TxtPorc_imp As DevExpress.XtraEditors.TextEdit
	Private ImpuestoCalc As Double
#End Region

#End Region

#Region " C�digo generado por el Dise�ador de Windows Forms "

	Public Sub New()
		MyBase.New()

		'El Dise�ador de Windows Forms requiere esta llamada.
		InitializeComponent()

		'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()


	End Sub

	Public Sub New(ByVal Usuario_Parametro As Object)
		MyBase.New()

		'El Dise�ador de Windows Forms requiere esta llamada.
		InitializeComponent()
		Usua = Usuario_Parametro
		'Agregar cualquier inicializaci�n despu�s de la llamada a InitializeComponent()
		AddHandler Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").PositionChanged, AddressOf Me.Position_Changed
		AddHandler Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CurrentChanged, AddressOf Me.Current_Changed
		Me.PictureEdit1.DataBindings.Add(New Binding("EditValue", Me.DataSet_Facturaciones, "configuraciones.Logo"))
	End Sub


	'Form reemplaza a Dispose para limpiar la lista de componentes.
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub

	'Requerido por el Dise�ador de Windows Forms
	Private components As System.ComponentModel.IContainer

	'NOTA: el Dise�ador de Windows Forms requiere el siguiente procedimiento
	'Puede modificarse utilizando el Dise�ador de Windows Forms. 
	'No lo modifique con el editor de c�digo.
	Friend WithEvents txtCostoBase As System.Windows.Forms.TextBox
	Friend WithEvents Panel2 As System.Windows.Forms.Panel
	Friend WithEvents Label1 As System.Windows.Forms.Label
	Friend WithEvents txtMontoImpuesto As System.Windows.Forms.TextBox
	Friend WithEvents txtSGravado As System.Windows.Forms.TextBox
	Friend WithEvents txtSubFamilia As System.Windows.Forms.TextBox
	Friend WithEvents txtSubtotal As DevExpress.XtraEditors.TextEdit
	Friend WithEvents txtImpVenta As DevExpress.XtraEditors.TextEdit
	Friend WithEvents Label32 As System.Windows.Forms.Label
	Friend WithEvents Label31 As System.Windows.Forms.Label
	Friend WithEvents Label21 As System.Windows.Forms.Label
	Friend WithEvents Label19 As System.Windows.Forms.Label
	Friend WithEvents Label17 As System.Windows.Forms.Label
	Friend WithEvents txtPrecioUnit As DevExpress.XtraEditors.TextEdit
	Friend WithEvents Label16 As System.Windows.Forms.Label
	Friend WithEvents txtCantidad As DevExpress.XtraEditors.TextEdit
	Friend WithEvents Label15 As System.Windows.Forms.Label
	Friend WithEvents Label13 As System.Windows.Forms.Label
	Friend WithEvents txtDescuento As DevExpress.XtraEditors.TextEdit
	Friend WithEvents txtSExcento As System.Windows.Forms.TextBox
	Friend WithEvents txtOtros As System.Windows.Forms.TextBox
	Friend WithEvents txtFlete As System.Windows.Forms.TextBox
	Friend WithEvents txtImpVentaT As DevExpress.XtraEditors.TextEdit
	Friend WithEvents txtDescuentoT As DevExpress.XtraEditors.TextEdit
	Friend WithEvents txtSubtotalT As DevExpress.XtraEditors.TextEdit
	Friend WithEvents Label25 As System.Windows.Forms.Label
	Friend WithEvents Label24 As System.Windows.Forms.Label
	Friend WithEvents Label23 As System.Windows.Forms.Label
	Friend WithEvents Label22 As System.Windows.Forms.Label
	Friend WithEvents txtSubtotalGravado As System.Windows.Forms.TextBox
	Friend WithEvents Label7 As System.Windows.Forms.Label
	Friend WithEvents opCredito As System.Windows.Forms.RadioButton
	Friend WithEvents opContado As System.Windows.Forms.RadioButton
	Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
	Friend WithEvents txtCodigo As ValidText.ValidText
	Friend WithEvents Label9 As System.Windows.Forms.Label
	Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
	Friend WithEvents ToolBarImprimir As System.Windows.Forms.ToolBarButton
	Friend WithEvents ToolBarNuevo As System.Windows.Forms.ToolBarButton
	Friend WithEvents ToolBarCerrar As System.Windows.Forms.ToolBarButton
	Friend WithEvents txtUsuario As System.Windows.Forms.TextBox
	Friend WithEvents txtNombreUsuario As System.Windows.Forms.TextBox
	Friend WithEvents ErrorProvider1 As System.Windows.Forms.ErrorProvider
	Friend WithEvents txtmontodescuento As System.Windows.Forms.TextBox
	Friend WithEvents ToolBarAnular As System.Windows.Forms.ToolBarButton
	Friend WithEvents ToolBar1 As System.Windows.Forms.ToolBar
	Friend WithEvents ToolBarBuscar As System.Windows.Forms.ToolBarButton
	Friend WithEvents ToolBarRegistrar As System.Windows.Forms.ToolBarButton

	Friend WithEvents SqlConnection1 As System.Data.SqlClient.SqlConnection

	Friend WithEvents SqlSelectCommand3 As System.Data.SqlClient.SqlCommand
	Friend WithEvents SqlInsertCommand3 As System.Data.SqlClient.SqlCommand
	Friend WithEvents SqlUpdateCommand3 As System.Data.SqlClient.SqlCommand
	Friend WithEvents SqlDeleteCommand3 As System.Data.SqlClient.SqlCommand
	Friend WithEvents Adapter_Moneda As System.Data.SqlClient.SqlDataAdapter
	Friend WithEvents txtFactura As System.Windows.Forms.Label
	Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
	Friend WithEvents Label30 As System.Windows.Forms.Label
	Friend WithEvents Label3 As System.Windows.Forms.Label
	Friend WithEvents txtTipoCambio As System.Windows.Forms.Label
	Friend WithEvents Label26 As System.Windows.Forms.Label
	Friend WithEvents CkEntregado As System.Windows.Forms.CheckBox
	Friend WithEvents txtTelefono As ValidText.ValidText
	Friend WithEvents Label40 As System.Windows.Forms.Label
	Friend WithEvents txtorden As System.Windows.Forms.TextBox
	Friend WithEvents Txtdireccion As System.Windows.Forms.TextBox
	Friend WithEvents Adapter_Clientes As System.Data.SqlClient.SqlDataAdapter
	Friend WithEvents TxtprecioCosto As System.Windows.Forms.TextBox

	Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
	Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView

	Friend WithEvents colCodigo As DevExpress.XtraGrid.Columns.GridColumn
	Friend WithEvents colDescripcion As DevExpress.XtraGrid.Columns.GridColumn
	Friend WithEvents colCantidad As DevExpress.XtraGrid.Columns.GridColumn
	Friend WithEvents colPrecio_Unit As DevExpress.XtraGrid.Columns.GridColumn
	Friend WithEvents colMonto_Descuento As DevExpress.XtraGrid.Columns.GridColumn
	Friend WithEvents colMonto_Impuesto As DevExpress.XtraGrid.Columns.GridColumn
	Friend WithEvents colSubtotalGravado As DevExpress.XtraGrid.Columns.GridColumn
	Friend WithEvents colSubTotalExcento As DevExpress.XtraGrid.Columns.GridColumn
	Friend WithEvents colSubTotal As DevExpress.XtraGrid.Columns.GridColumn
	Friend WithEvents dtFecha As System.Windows.Forms.Label
	Friend WithEvents Label12 As System.Windows.Forms.Label
	Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
	Friend WithEvents TxtMaxdescuento As System.Windows.Forms.TextBox
	Friend WithEvents Adapter_Usuarios As System.Data.SqlClient.SqlDataAdapter
	Friend WithEvents Label29 As System.Windows.Forms.Label
	Friend WithEvents Label43 As System.Windows.Forms.Label
	Friend WithEvents Lb_SubExento As DevExpress.XtraEditors.TextEdit
	Friend WithEvents Lb_Subgravado As DevExpress.XtraEditors.TextEdit
	Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents Adapter_Encargados_Compra As System.Data.SqlClient.SqlDataAdapter
	Friend WithEvents SqlSelectCommand11 As System.Data.SqlClient.SqlCommand
	Friend WithEvents Combo_Encargado As System.Windows.Forms.ComboBox
	Friend WithEvents Label46 As DevExpress.XtraEditors.TextEdit
	Friend WithEvents txtDiasPlazo As System.Windows.Forms.Label
	Friend WithEvents fecha_vence As System.Windows.Forms.DateTimePicker
	Friend WithEvents DtVence As System.Windows.Forms.Label
	Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents Txtcodmoneda_Venta As System.Windows.Forms.TextBox
	Friend WithEvents Txt_TipoCambio_Valor_Compra As System.Windows.Forms.TextBox
	Friend WithEvents Adapter_Ventas_Detalles As System.Data.SqlClient.SqlDataAdapter
	Friend WithEvents SqlSelectCommand10 As System.Data.SqlClient.SqlCommand
	Friend WithEvents SimpleButton3 As DevExpress.XtraEditors.SimpleButton
	Friend WithEvents ToolBarButton1 As System.Windows.Forms.ToolBarButton
	Friend WithEvents ToolBarImportar As System.Windows.Forms.ToolBarButton

	Friend WithEvents SqlSelectCommand12 As System.Data.SqlClient.SqlCommand
	Friend WithEvents Adapter_Configuraciones As System.Data.SqlClient.SqlDataAdapter
	Friend WithEvents DataSet_Facturaciones As DataSet_Facturaciones
	Friend WithEvents Adapter_Coti As System.Data.SqlClient.SqlDataAdapter
	Friend WithEvents Adapter_CotiDetalle As System.Data.SqlClient.SqlDataAdapter
	Friend WithEvents SqlSelectCommand14 As System.Data.SqlClient.SqlCommand
	Friend WithEvents SqlSelectCommand13 As System.Data.SqlClient.SqlCommand
	Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
	Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
	Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
	Friend WithEvents Label48 As System.Windows.Forms.Label
	Friend WithEvents Label49 As System.Windows.Forms.Label
	Friend WithEvents TxtUtilidad As DevExpress.XtraEditors.TextEdit
	Friend WithEvents TxtTipo As System.Windows.Forms.TextBox
	Friend WithEvents Label_Anulada As System.Windows.Forms.Label
	Friend WithEvents StatusBar1 As System.Windows.Forms.StatusBar
	Friend WithEvents StatusBarPanel2 As System.Windows.Forms.StatusBarPanel
	Friend WithEvents StatusBarPanel3 As System.Windows.Forms.StatusBarPanel
	Friend WithEvents StatusBarPanel4 As System.Windows.Forms.StatusBarPanel
	Friend WithEvents StatusBarPanel5 As System.Windows.Forms.StatusBarPanel
	Friend WithEvents StatusBarPanel6 As System.Windows.Forms.StatusBarPanel
	Friend WithEvents StatusBarPanel7 As System.Windows.Forms.StatusBarPanel
	Friend WithEvents StatusBarPanel8 As System.Windows.Forms.StatusBarPanel
	Friend WithEvents Panel3 As System.Windows.Forms.Panel
	Friend WithEvents Label5 As System.Windows.Forms.Label
	Friend WithEvents txtNombre As System.Windows.Forms.TextBox
	Friend WithEvents Panel1 As System.Windows.Forms.Panel
	Friend WithEvents txtSubtotalExcento As System.Windows.Forms.TextBox
	Friend WithEvents Panel4 As System.Windows.Forms.Panel
	Friend WithEvents StatusBarPanel1 As System.Windows.Forms.StatusBarPanel
	Friend WithEvents PictureEdit1 As DevExpress.XtraEditors.PictureEdit
	Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
	Friend WithEvents Text_Ficticio As System.Windows.Forms.TextBox
	Friend WithEvents ColFicticia As DevExpress.XtraGrid.Columns.GridColumn
	Friend WithEvents CK_PVE As System.Windows.Forms.CheckBox
	Friend WithEvents TxtTotal As DevExpress.XtraEditors.TextEdit
	Friend WithEvents TxtCodArticulo As DevExpress.XtraEditors.TextEdit 'System.Windows.Forms.TextBox
	Friend WithEvents GroupBox3_oLD As System.Windows.Forms.GroupBox
	Friend WithEvents GroupBox3 As System.Windows.Forms.Panel
	Friend WithEvents GroupBox1 As System.Windows.Forms.Panel
	Friend WithEvents Panel6 As System.Windows.Forms.Panel
	Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
	Friend WithEvents ToolBarReciboDinero As System.Windows.Forms.ToolBarButton
	Friend WithEvents SqlSelectCommand4 As System.Data.SqlClient.SqlCommand
	Friend WithEvents SqlInsertCommand2 As System.Data.SqlClient.SqlCommand
	Friend WithEvents SqlUpdateCommand2 As System.Data.SqlClient.SqlCommand
	Friend WithEvents SqlDeleteCommand2 As System.Data.SqlClient.SqlCommand
	Friend WithEvents Label2 As System.Windows.Forms.Label
	Friend WithEvents Adapter_Apertura As System.Data.SqlClient.SqlDataAdapter
	Friend WithEvents SqlSelectCommand5 As System.Data.SqlClient.SqlCommand
	Friend WithEvents Panel5 As System.Windows.Forms.Panel
	Friend WithEvents Panel7 As System.Windows.Forms.Panel
	Friend WithEvents Label4 As System.Windows.Forms.Label
	Friend WithEvents txtDeuda As DevExpress.XtraEditors.TextEdit
	Friend WithEvents Label6 As System.Windows.Forms.Label
	Friend WithEvents txtExistencia As DevExpress.XtraEditors.TextEdit
	Friend WithEvents txtTotalArt As DevExpress.XtraEditors.TextEdit
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Me.components = New System.ComponentModel.Container()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FacturacionPVE))
		Dim ColumnFilterInfo1 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
		Dim ColumnFilterInfo2 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
		Dim ColumnFilterInfo3 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
		Dim ColumnFilterInfo4 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
		Dim ColumnFilterInfo5 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
		Dim ColumnFilterInfo6 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
		Dim ColumnFilterInfo7 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
		Dim ColumnFilterInfo8 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
		Dim ColumnFilterInfo9 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
		Dim ColumnFilterInfo10 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
		Dim ColumnFilterInfo11 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
		Dim ColumnFilterInfo12 As DevExpress.XtraGrid.Columns.ColumnFilterInfo = New DevExpress.XtraGrid.Columns.ColumnFilterInfo()
		Me.txtCostoBase = New System.Windows.Forms.TextBox()
		Me.Panel2 = New System.Windows.Forms.Panel()
		Me.txtFactura = New System.Windows.Forms.Label()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.TxtTipo = New System.Windows.Forms.TextBox()
		Me.txtMontoImpuesto = New System.Windows.Forms.TextBox()
		Me.txtSGravado = New System.Windows.Forms.TextBox()
		Me.txtSubFamilia = New System.Windows.Forms.TextBox()
		Me.GroupBox3_oLD = New System.Windows.Forms.GroupBox()
		Me.txtMontoComision = New System.Windows.Forms.TextBox()
		Me.Label40 = New System.Windows.Forms.Label()
		Me.ComboBox1 = New System.Windows.Forms.ComboBox()
		Me.DataSet_Facturaciones = New SeePOS_PVE.DataSet_Facturaciones()
		Me.Panel5 = New System.Windows.Forms.Panel()
		Me.txtorden = New System.Windows.Forms.TextBox()
		Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
		Me.Combo_Encargado = New System.Windows.Forms.ComboBox()
		Me.Text_Ficticio = New System.Windows.Forms.TextBox()
		Me.fecha_vence = New System.Windows.Forms.DateTimePicker()
		Me.GroupBox1 = New System.Windows.Forms.Panel()
		Me.SimpleButton3 = New DevExpress.XtraEditors.SimpleButton()
		Me.Label30 = New System.Windows.Forms.Label()
		Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
		Me.TxtUtilidad = New DevExpress.XtraEditors.TextEdit()
		Me.txtFlete = New System.Windows.Forms.TextBox()
		Me.TxtMaxdescuento = New System.Windows.Forms.TextBox()
		Me.Label46 = New DevExpress.XtraEditors.TextEdit()
		Me.txtTelefono = New ValidText.ValidText()
		Me.Lb_SubExento = New DevExpress.XtraEditors.TextEdit()
		Me.Lb_Subgravado = New DevExpress.XtraEditors.TextEdit()
		Me.txtOtros = New System.Windows.Forms.TextBox()
		Me.TxtprecioCosto = New System.Windows.Forms.TextBox()
		Me.txtmontodescuento = New System.Windows.Forms.TextBox()
		Me.txtSubtotalGravado = New System.Windows.Forms.TextBox()
		Me.CkEntregado = New System.Windows.Forms.CheckBox()
		Me.CheckBox1 = New System.Windows.Forms.CheckBox()
		Me.Txtcodmoneda_Venta = New System.Windows.Forms.TextBox()
		Me.Txt_TipoCambio_Valor_Compra = New System.Windows.Forms.TextBox()
		Me.txtTipoCambio = New System.Windows.Forms.Label()
		Me.Label3 = New System.Windows.Forms.Label()
		Me.Label_Anulada = New System.Windows.Forms.Label()
		Me.Label49 = New System.Windows.Forms.Label()
		Me.Label19 = New System.Windows.Forms.Label()
		Me.txtSExcento = New System.Windows.Forms.TextBox()
		Me.Label7 = New System.Windows.Forms.Label()
		Me.txtDiasPlazo = New System.Windows.Forms.Label()
		Me.DtVence = New System.Windows.Forms.Label()
		Me.Label12 = New System.Windows.Forms.Label()
		Me.dtFecha = New System.Windows.Forms.Label()
		Me.txtSubtotalExcento = New System.Windows.Forms.TextBox()
		Me.Label43 = New System.Windows.Forms.Label()
		Me.Label26 = New System.Windows.Forms.Label()
		Me.Label29 = New System.Windows.Forms.Label()
		Me.Txtdireccion = New System.Windows.Forms.TextBox()
		Me.txtImpVenta = New DevExpress.XtraEditors.TextEdit()
		Me.txtSubtotal = New DevExpress.XtraEditors.TextEdit()
		Me.Label32 = New System.Windows.Forms.Label()
		Me.Label21 = New System.Windows.Forms.Label()
		Me.txtCantidad = New DevExpress.XtraEditors.TextEdit()
		Me.Label15 = New System.Windows.Forms.Label()
		Me.Label13 = New System.Windows.Forms.Label()
		Me.TxtCodArticulo = New DevExpress.XtraEditors.TextEdit()
		Me.Label31 = New System.Windows.Forms.Label()
		Me.txtPrecioUnit = New DevExpress.XtraEditors.TextEdit()
		Me.Label16 = New System.Windows.Forms.Label()
		Me.Label17 = New System.Windows.Forms.Label()
		Me.txtDescuento = New DevExpress.XtraEditors.TextEdit()
		Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
		Me.Label25 = New System.Windows.Forms.Label()
		Me.TxtTotal = New DevExpress.XtraEditors.TextEdit()
		Me.txtImpVentaT = New DevExpress.XtraEditors.TextEdit()
		Me.txtDescuentoT = New DevExpress.XtraEditors.TextEdit()
		Me.txtSubtotalT = New DevExpress.XtraEditors.TextEdit()
		Me.Label24 = New System.Windows.Forms.Label()
		Me.Label23 = New System.Windows.Forms.Label()
		Me.Label22 = New System.Windows.Forms.Label()
		Me.opCredito = New System.Windows.Forms.RadioButton()
		Me.opContado = New System.Windows.Forms.RadioButton()
		Me.txtCodigo = New ValidText.ValidText()
		Me.Label9 = New System.Windows.Forms.Label()
		Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
		Me.ToolBarImprimir = New System.Windows.Forms.ToolBarButton()
		Me.ToolBarNuevo = New System.Windows.Forms.ToolBarButton()
		Me.ToolBarCerrar = New System.Windows.Forms.ToolBarButton()
		Me.txtNombreUsuario = New System.Windows.Forms.TextBox()
		Me.txtUsuario = New System.Windows.Forms.TextBox()
		Me.ErrorProvider1 = New System.Windows.Forms.ErrorProvider(Me.components)
		Me.ToolBarAnular = New System.Windows.Forms.ToolBarButton()
		Me.ToolBar1 = New System.Windows.Forms.ToolBar()
		Me.ToolBarBuscar = New System.Windows.Forms.ToolBarButton()
		Me.ToolBarRegistrar = New System.Windows.Forms.ToolBarButton()
		Me.ToolBarImportar = New System.Windows.Forms.ToolBarButton()
		Me.ToolBarReciboDinero = New System.Windows.Forms.ToolBarButton()
		Me.ToolBarRecarga = New System.Windows.Forms.ToolBarButton()
		Me.ToolBarPesa = New System.Windows.Forms.ToolBarButton()
		Me.SqlConnection1 = New System.Data.SqlClient.SqlConnection()
		Me.SqlSelectCommand3 = New System.Data.SqlClient.SqlCommand()
		Me.SqlInsertCommand3 = New System.Data.SqlClient.SqlCommand()
		Me.SqlUpdateCommand3 = New System.Data.SqlClient.SqlCommand()
		Me.SqlDeleteCommand3 = New System.Data.SqlClient.SqlCommand()
		Me.Adapter_Moneda = New System.Data.SqlClient.SqlDataAdapter()
		Me.Adapter_Clientes = New System.Data.SqlClient.SqlDataAdapter()
		Me.SqlDeleteCommand2 = New System.Data.SqlClient.SqlCommand()
		Me.SqlInsertCommand2 = New System.Data.SqlClient.SqlCommand()
		Me.SqlSelectCommand4 = New System.Data.SqlClient.SqlCommand()
		Me.SqlUpdateCommand2 = New System.Data.SqlClient.SqlCommand()
		Me.Adapter_Usuarios = New System.Data.SqlClient.SqlDataAdapter()
		Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand()
		Me.Adapter_Encargados_Compra = New System.Data.SqlClient.SqlDataAdapter()
		Me.SqlSelectCommand11 = New System.Data.SqlClient.SqlCommand()
		Me.Adapter_Ventas_Detalles = New System.Data.SqlClient.SqlDataAdapter()
		Me.SqlDeleteCommand = New System.Data.SqlClient.SqlCommand()
		Me.SqlInsertCommand = New System.Data.SqlClient.SqlCommand()
		Me.SqlSelectCommand10 = New System.Data.SqlClient.SqlCommand()
		Me.SqlUpdateCommand = New System.Data.SqlClient.SqlCommand()
		Me.ToolBarButton1 = New System.Windows.Forms.ToolBarButton()
		Me.SqlSelectCommand12 = New System.Data.SqlClient.SqlCommand()
		Me.Adapter_Configuraciones = New System.Data.SqlClient.SqlDataAdapter()
		Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand()
		Me.Adapter_Coti = New System.Data.SqlClient.SqlDataAdapter()
		Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand()
		Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand()
		Me.SqlSelectCommand13 = New System.Data.SqlClient.SqlCommand()
		Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand()
		Me.Adapter_CotiDetalle = New System.Data.SqlClient.SqlDataAdapter()
		Me.SqlSelectCommand14 = New System.Data.SqlClient.SqlCommand()
		Me.Label48 = New System.Windows.Forms.Label()
		Me.StatusBar1 = New System.Windows.Forms.StatusBar()
		Me.StatusBarPanel1 = New System.Windows.Forms.StatusBarPanel()
		Me.StatusBarPanel2 = New System.Windows.Forms.StatusBarPanel()
		Me.StatusBarPanel3 = New System.Windows.Forms.StatusBarPanel()
		Me.StatusBarPanel4 = New System.Windows.Forms.StatusBarPanel()
		Me.StatusBarPanel5 = New System.Windows.Forms.StatusBarPanel()
		Me.StatusBarPanel6 = New System.Windows.Forms.StatusBarPanel()
		Me.StatusBarPanel7 = New System.Windows.Forms.StatusBarPanel()
		Me.StatusBarPanel8 = New System.Windows.Forms.StatusBarPanel()
		Me.Panel3 = New System.Windows.Forms.Panel()
		Me.txtDeuda = New DevExpress.XtraEditors.TextEdit()
		Me.Label5 = New System.Windows.Forms.Label()
		Me.txtNombre = New System.Windows.Forms.TextBox()
		Me.Panel1 = New System.Windows.Forms.Panel()
		Me.Panel4 = New System.Windows.Forms.Panel()
		Me.PictureEdit1 = New DevExpress.XtraEditors.PictureEdit()
		Me.CK_PVE = New System.Windows.Forms.CheckBox()
		Me.GroupBox3 = New System.Windows.Forms.Panel()
		Me.Label20 = New System.Windows.Forms.Label()
		Me.Label18 = New System.Windows.Forms.Label()
		Me.TxtBarras = New DevExpress.XtraEditors.TextEdit()
		Me.txtIVA = New DevExpress.XtraEditors.TextEdit()
		Me.txtTotalArt = New DevExpress.XtraEditors.TextEdit()
		Me.txtExistencia = New DevExpress.XtraEditors.TextEdit()
		Me.Label6 = New System.Windows.Forms.Label()
		Me.Label4 = New System.Windows.Forms.Label()
		Me.Panel6 = New System.Windows.Forms.Panel()
		Me.Label11 = New System.Windows.Forms.Label()
		Me.txtObservacion = New System.Windows.Forms.TextBox()
		Me.SimpleButton4 = New DevExpress.XtraEditors.SimpleButton()
		Me.CB_Exonerar = New System.Windows.Forms.CheckBox()
		Me.Label10 = New System.Windows.Forms.Label()
		Me.CB_Moneda = New System.Windows.Forms.ComboBox()
		Me.Label2 = New System.Windows.Forms.Label()
		Me.Adapter_Apertura = New System.Data.SqlClient.SqlDataAdapter()
		Me.SqlSelectCommand5 = New System.Data.SqlClient.SqlCommand()
		Me.Panel7 = New System.Windows.Forms.Panel()
		Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
		Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
		Me.colCodigo = New DevExpress.XtraGrid.Columns.GridColumn()
		Me.colDescripcion = New DevExpress.XtraGrid.Columns.GridColumn()
		Me.colCantidad = New DevExpress.XtraGrid.Columns.GridColumn()
		Me.colPrecio_Unit = New DevExpress.XtraGrid.Columns.GridColumn()
		Me.colMonto_Descuento = New DevExpress.XtraGrid.Columns.GridColumn()
		Me.colMonto_Impuesto = New DevExpress.XtraGrid.Columns.GridColumn()
		Me.colSubtotalGravado = New DevExpress.XtraGrid.Columns.GridColumn()
		Me.colSubTotalExcento = New DevExpress.XtraGrid.Columns.GridColumn()
		Me.colSubTotal = New DevExpress.XtraGrid.Columns.GridColumn()
		Me.ColFicticia = New DevExpress.XtraGrid.Columns.GridColumn()
		Me.colBarra = New DevExpress.XtraGrid.Columns.GridColumn()
		Me.colIVA = New DevExpress.XtraGrid.Columns.GridColumn()
		Me.Txtbodega = New DevExpress.XtraEditors.TextEdit()
		Me.txtNombreArt = New System.Windows.Forms.TextBox()
		Me.Label8 = New System.Windows.Forms.Label()
		Me.txtComisionista = New ValidText.ValidText()
		Me.txtNomComisionista = New System.Windows.Forms.TextBox()
		Me.SqlSelectCommand6 = New System.Data.SqlClient.SqlCommand()
		Me.SqlInsertCommand4 = New System.Data.SqlClient.SqlCommand()
		Me.SqlUpdateCommand4 = New System.Data.SqlClient.SqlCommand()
		Me.SqlDeleteCommand4 = New System.Data.SqlClient.SqlCommand()
		Me.Adapter_Ventas = New System.Data.SqlClient.SqlDataAdapter()
		Me.AdapterConfig = New System.Data.SqlClient.SqlDataAdapter()
		Me.SqlCommand1 = New System.Data.SqlClient.SqlCommand()
		Me.SerialPort1 = New System.IO.Ports.SerialPort(Me.components)
		Me.bwEnviarSolicitudPeso = New System.ComponentModel.BackgroundWorker()
		Me.Label14 = New System.Windows.Forms.Label()
		Me.TxtPorc_imp = New DevExpress.XtraEditors.TextEdit()
		Me.Panel2.SuspendLayout()
		Me.GroupBox3_oLD.SuspendLayout()
		CType(Me.DataSet_Facturaciones, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.Panel5.SuspendLayout()
		Me.GroupBox1.SuspendLayout()
		CType(Me.TxtUtilidad.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.Label46.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.Lb_SubExento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.Lb_Subgravado.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtImpVenta.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtSubtotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtCantidad.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TxtCodArticulo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtPrecioUnit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtDescuento.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TxtTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtImpVentaT.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtDescuentoT.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtSubtotalT.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.StatusBarPanel1, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.StatusBarPanel2, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.StatusBarPanel3, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.StatusBarPanel4, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.StatusBarPanel5, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.StatusBarPanel6, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.StatusBarPanel7, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.StatusBarPanel8, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.Panel3.SuspendLayout()
		CType(Me.txtDeuda.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.Panel1.SuspendLayout()
		Me.Panel4.SuspendLayout()
		CType(Me.PictureEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.GroupBox3.SuspendLayout()
		CType(Me.TxtBarras.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtIVA.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtTotalArt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.txtExistencia.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.Panel6.SuspendLayout()
		Me.Panel7.SuspendLayout()
		CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.Txtbodega.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.TxtPorc_imp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.SuspendLayout()
		'
		'txtCostoBase
		'
		Me.txtCostoBase.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.txtCostoBase.Enabled = False
		Me.txtCostoBase.ForeColor = System.Drawing.Color.Blue
		Me.txtCostoBase.Location = New System.Drawing.Point(696, 120)
		Me.txtCostoBase.Name = "txtCostoBase"
		Me.txtCostoBase.Size = New System.Drawing.Size(32, 13)
		Me.txtCostoBase.TabIndex = 168
		'
		'Panel2
		'
		Me.Panel2.BackColor = System.Drawing.Color.White
		Me.Panel2.Controls.Add(Me.txtFactura)
		Me.Panel2.Controls.Add(Me.Label1)
		Me.Panel2.Controls.Add(Me.TxtTipo)
		Me.Panel2.Location = New System.Drawing.Point(0, 16)
		Me.Panel2.Name = "Panel2"
		Me.Panel2.Size = New System.Drawing.Size(176, 16)
		Me.Panel2.TabIndex = 182
		'
		'txtFactura
		'
		Me.txtFactura.BackColor = System.Drawing.Color.White
		Me.txtFactura.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtFactura.ForeColor = System.Drawing.Color.Red
		Me.txtFactura.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.txtFactura.Location = New System.Drawing.Point(64, 1)
		Me.txtFactura.Name = "txtFactura"
		Me.txtFactura.Size = New System.Drawing.Size(64, 12)
		Me.txtFactura.TabIndex = 64
		Me.txtFactura.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'Label1
		'
		Me.Label1.BackColor = System.Drawing.Color.White
		Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
		Me.Label1.ForeColor = System.Drawing.Color.RoyalBlue
		Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.Label1.Location = New System.Drawing.Point(1, 1)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(63, 12)
		Me.Label1.TabIndex = 71
		Me.Label1.Text = "Factura N�"
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter
		'
		'TxtTipo
		'
		Me.TxtTipo.BackColor = System.Drawing.Color.White
		Me.TxtTipo.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.TxtTipo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.TxtTipo.ForeColor = System.Drawing.Color.Red
		Me.TxtTipo.Location = New System.Drawing.Point(130, 1)
		Me.TxtTipo.MaxLength = 25
		Me.TxtTipo.Name = "TxtTipo"
		Me.TxtTipo.ReadOnly = True
		Me.TxtTipo.Size = New System.Drawing.Size(32, 14)
		Me.TxtTipo.TabIndex = 193
		'
		'txtMontoImpuesto
		'
		Me.txtMontoImpuesto.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.txtMontoImpuesto.ForeColor = System.Drawing.Color.Blue
		Me.txtMontoImpuesto.Location = New System.Drawing.Point(664, 120)
		Me.txtMontoImpuesto.Name = "txtMontoImpuesto"
		Me.txtMontoImpuesto.Size = New System.Drawing.Size(24, 13)
		Me.txtMontoImpuesto.TabIndex = 165
		'
		'txtSGravado
		'
		Me.txtSGravado.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.txtSGravado.ForeColor = System.Drawing.Color.Blue
		Me.txtSGravado.Location = New System.Drawing.Point(464, 128)
		Me.txtSGravado.Name = "txtSGravado"
		Me.txtSGravado.Size = New System.Drawing.Size(40, 13)
		Me.txtSGravado.TabIndex = 166
		'
		'txtSubFamilia
		'
		Me.txtSubFamilia.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.txtSubFamilia.ForeColor = System.Drawing.Color.Blue
		Me.txtSubFamilia.Location = New System.Drawing.Point(664, 136)
		Me.txtSubFamilia.Name = "txtSubFamilia"
		Me.txtSubFamilia.Size = New System.Drawing.Size(24, 13)
		Me.txtSubFamilia.TabIndex = 167
		'
		'GroupBox3_oLD
		'
		Me.GroupBox3_oLD.BackColor = System.Drawing.Color.Gainsboro
		Me.GroupBox3_oLD.Controls.Add(Me.txtMontoComision)
		Me.GroupBox3_oLD.Controls.Add(Me.Label40)
		Me.GroupBox3_oLD.Controls.Add(Me.ComboBox1)
		Me.GroupBox3_oLD.Controls.Add(Me.Panel5)
		Me.GroupBox3_oLD.Controls.Add(Me.SimpleButton1)
		Me.GroupBox3_oLD.Controls.Add(Me.Combo_Encargado)
		Me.GroupBox3_oLD.Controls.Add(Me.Text_Ficticio)
		Me.GroupBox3_oLD.Controls.Add(Me.fecha_vence)
		Me.GroupBox3_oLD.Controls.Add(Me.GroupBox1)
		Me.GroupBox3_oLD.Controls.Add(Me.DateTimePicker1)
		Me.GroupBox3_oLD.Controls.Add(Me.TxtUtilidad)
		Me.GroupBox3_oLD.Controls.Add(Me.txtFlete)
		Me.GroupBox3_oLD.Controls.Add(Me.txtSGravado)
		Me.GroupBox3_oLD.Controls.Add(Me.TxtMaxdescuento)
		Me.GroupBox3_oLD.Controls.Add(Me.Label46)
		Me.GroupBox3_oLD.Controls.Add(Me.txtTelefono)
		Me.GroupBox3_oLD.Controls.Add(Me.Lb_SubExento)
		Me.GroupBox3_oLD.Controls.Add(Me.Lb_Subgravado)
		Me.GroupBox3_oLD.Controls.Add(Me.txtOtros)
		Me.GroupBox3_oLD.Controls.Add(Me.TxtprecioCosto)
		Me.GroupBox3_oLD.Controls.Add(Me.txtmontodescuento)
		Me.GroupBox3_oLD.Controls.Add(Me.txtSubtotalGravado)
		Me.GroupBox3_oLD.Controls.Add(Me.txtSubFamilia)
		Me.GroupBox3_oLD.Controls.Add(Me.txtMontoImpuesto)
		Me.GroupBox3_oLD.Controls.Add(Me.txtCostoBase)
		Me.GroupBox3_oLD.Controls.Add(Me.CkEntregado)
		Me.GroupBox3_oLD.Controls.Add(Me.CheckBox1)
		Me.GroupBox3_oLD.Controls.Add(Me.Txtcodmoneda_Venta)
		Me.GroupBox3_oLD.Controls.Add(Me.Txt_TipoCambio_Valor_Compra)
		Me.GroupBox3_oLD.Controls.Add(Me.txtTipoCambio)
		Me.GroupBox3_oLD.Controls.Add(Me.Label3)
		Me.GroupBox3_oLD.Controls.Add(Me.Label_Anulada)
		Me.GroupBox3_oLD.Controls.Add(Me.Label49)
		Me.GroupBox3_oLD.Controls.Add(Me.Label19)
		Me.GroupBox3_oLD.Controls.Add(Me.txtSExcento)
		Me.GroupBox3_oLD.Controls.Add(Me.Label7)
		Me.GroupBox3_oLD.Controls.Add(Me.txtDiasPlazo)
		Me.GroupBox3_oLD.Controls.Add(Me.DtVence)
		Me.GroupBox3_oLD.Controls.Add(Me.Label12)
		Me.GroupBox3_oLD.Controls.Add(Me.dtFecha)
		Me.GroupBox3_oLD.Controls.Add(Me.txtSubtotalExcento)
		Me.GroupBox3_oLD.Controls.Add(Me.Label43)
		Me.GroupBox3_oLD.Controls.Add(Me.Label26)
		Me.GroupBox3_oLD.Controls.Add(Me.Label29)
		Me.GroupBox3_oLD.Controls.Add(Me.Txtdireccion)
		Me.GroupBox3_oLD.Enabled = False
		Me.GroupBox3_oLD.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.GroupBox3_oLD.ForeColor = System.Drawing.Color.RoyalBlue
		Me.GroupBox3_oLD.Location = New System.Drawing.Point(24, 88)
		Me.GroupBox3_oLD.Name = "GroupBox3_oLD"
		Me.GroupBox3_oLD.Size = New System.Drawing.Size(104, 83)
		Me.GroupBox3_oLD.TabIndex = 1
		Me.GroupBox3_oLD.TabStop = False
		'
		'txtMontoComision
		'
		Me.txtMontoComision.Location = New System.Drawing.Point(-128, 31)
		Me.txtMontoComision.Name = "txtMontoComision"
		Me.txtMontoComision.Size = New System.Drawing.Size(48, 20)
		Me.txtMontoComision.TabIndex = 229
		'
		'Label40
		'
		Me.Label40.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Label40.BackColor = System.Drawing.Color.Transparent
		Me.Label40.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
		Me.Label40.ForeColor = System.Drawing.Color.RoyalBlue
		Me.Label40.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.Label40.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.Label40.Location = New System.Drawing.Point(-293, 201)
		Me.Label40.Name = "Label40"
		Me.Label40.Size = New System.Drawing.Size(112, 14)
		Me.Label40.TabIndex = 77
		Me.Label40.Text = "Orden de Compra"
		Me.Label40.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'ComboBox1
		'
		Me.ComboBox1.DataSource = Me.DataSet_Facturaciones
		Me.ComboBox1.DisplayMember = "Moneda.MonedaNombre"
		Me.ComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.ComboBox1.Enabled = False
		Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.ComboBox1.ForeColor = System.Drawing.Color.Blue
		Me.ComboBox1.ItemHeight = 13
		Me.ComboBox1.Location = New System.Drawing.Point(528, 197)
		Me.ComboBox1.Name = "ComboBox1"
		Me.ComboBox1.Size = New System.Drawing.Size(120, 21)
		Me.ComboBox1.TabIndex = 0
		Me.ComboBox1.ValueMember = "Moneda.MonedaNombre"
		'
		'DataSet_Facturaciones
		'
		Me.DataSet_Facturaciones.DataSetName = "DataSet_Facturaciones"
		Me.DataSet_Facturaciones.Locale = New System.Globalization.CultureInfo("es-MX")
		Me.DataSet_Facturaciones.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
		'
		'Panel5
		'
		Me.Panel5.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Panel5.Controls.Add(Me.txtorden)
		Me.Panel5.Location = New System.Drawing.Point(-155, 167)
		Me.Panel5.Name = "Panel5"
		Me.Panel5.Size = New System.Drawing.Size(216, 24)
		Me.Panel5.TabIndex = 228
		'
		'txtorden
		'
		Me.txtorden.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.txtorden.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.txtorden.Enabled = False
		Me.txtorden.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtorden.ForeColor = System.Drawing.Color.Blue
		Me.txtorden.Location = New System.Drawing.Point(121, 6)
		Me.txtorden.Name = "txtorden"
		Me.txtorden.Size = New System.Drawing.Size(88, 13)
		Me.txtorden.TabIndex = 2
		Me.txtorden.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
		'
		'SimpleButton1
		'
		Me.SimpleButton1.DialogResult = System.Windows.Forms.DialogResult.Cancel
		Me.SimpleButton1.Enabled = False
		Me.SimpleButton1.Location = New System.Drawing.Point(80, 80)
		Me.SimpleButton1.Name = "SimpleButton1"
		Me.SimpleButton1.Size = New System.Drawing.Size(19, 18)
		Me.SimpleButton1.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.Color.RoyalBlue)
		Me.SimpleButton1.TabIndex = 191
		Me.SimpleButton1.Text = "T"
		'
		'Combo_Encargado
		'
		Me.Combo_Encargado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.Combo_Encargado.ForeColor = System.Drawing.Color.RoyalBlue
		Me.Combo_Encargado.Location = New System.Drawing.Point(356, 10)
		Me.Combo_Encargado.Name = "Combo_Encargado"
		Me.Combo_Encargado.Size = New System.Drawing.Size(208, 21)
		Me.Combo_Encargado.TabIndex = 160
		Me.Combo_Encargado.Visible = False
		'
		'Text_Ficticio
		'
		Me.Text_Ficticio.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle.Total_Ficticio", True))
		Me.Text_Ficticio.Location = New System.Drawing.Point(536, 48)
		Me.Text_Ficticio.Name = "Text_Ficticio"
		Me.Text_Ficticio.Size = New System.Drawing.Size(48, 20)
		Me.Text_Ficticio.TabIndex = 221
		'
		'fecha_vence
		'
		Me.fecha_vence.Location = New System.Drawing.Point(528, 120)
		Me.fecha_vence.Name = "fecha_vence"
		Me.fecha_vence.Size = New System.Drawing.Size(88, 20)
		Me.fecha_vence.TabIndex = 187
		'
		'GroupBox1
		'
		Me.GroupBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.GroupBox1.Controls.Add(Me.SimpleButton3)
		Me.GroupBox1.Controls.Add(Me.Label30)
		Me.GroupBox1.Location = New System.Drawing.Point(-133, 10)
		Me.GroupBox1.Name = "GroupBox1"
		Me.GroupBox1.Size = New System.Drawing.Size(218, 24)
		Me.GroupBox1.TabIndex = 224
		'
		'SimpleButton3
		'
		Me.SimpleButton3.Enabled = False
		Me.SimpleButton3.Location = New System.Drawing.Point(192, 3)
		Me.SimpleButton3.Name = "SimpleButton3"
		Me.SimpleButton3.Size = New System.Drawing.Size(24, 16)
		Me.SimpleButton3.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.Color.RoyalBlue)
		Me.SimpleButton3.TabIndex = 1
		Me.SimpleButton3.Text = "..."
		Me.SimpleButton3.ToolTip = "Cambio de la denominaci�n de la moneda"
		'
		'Label30
		'
		Me.Label30.BackColor = System.Drawing.Color.Transparent
		Me.Label30.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Label30.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
		Me.Label30.ForeColor = System.Drawing.Color.RoyalBlue
		Me.Label30.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.Label30.Location = New System.Drawing.Point(3, 3)
		Me.Label30.Name = "Label30"
		Me.Label30.Size = New System.Drawing.Size(56, 14)
		Me.Label30.TabIndex = 68
		Me.Label30.Text = "Moneda"
		Me.Label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'DateTimePicker1
		'
		Me.DateTimePicker1.Location = New System.Drawing.Point(528, 144)
		Me.DateTimePicker1.Name = "DateTimePicker1"
		Me.DateTimePicker1.Size = New System.Drawing.Size(88, 20)
		Me.DateTimePicker1.TabIndex = 186
		'
		'TxtUtilidad
		'
		Me.TxtUtilidad.EditValue = ""
		Me.TxtUtilidad.Location = New System.Drawing.Point(552, 72)
		Me.TxtUtilidad.Name = "TxtUtilidad"
		'
		'
		'
		Me.TxtUtilidad.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
		Me.TxtUtilidad.Properties.DisplayFormat.FormatString = "#,#0.00"
		Me.TxtUtilidad.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
		Me.TxtUtilidad.Properties.EditFormat.FormatString = "#,#0.00"
		Me.TxtUtilidad.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
		Me.TxtUtilidad.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
		Me.TxtUtilidad.Size = New System.Drawing.Size(32, 17)
		Me.TxtUtilidad.TabIndex = 192
		Me.TxtUtilidad.ToolTip = "Porcentaje de Utilidad."
		Me.TxtUtilidad.Visible = False
		'
		'txtFlete
		'
		Me.txtFlete.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.txtFlete.ForeColor = System.Drawing.Color.Blue
		Me.txtFlete.Location = New System.Drawing.Point(456, 72)
		Me.txtFlete.Name = "txtFlete"
		Me.txtFlete.Size = New System.Drawing.Size(32, 13)
		Me.txtFlete.TabIndex = 169
		'
		'TxtMaxdescuento
		'
		Me.TxtMaxdescuento.Location = New System.Drawing.Point(456, 96)
		Me.TxtMaxdescuento.Name = "TxtMaxdescuento"
		Me.TxtMaxdescuento.Size = New System.Drawing.Size(40, 20)
		Me.TxtMaxdescuento.TabIndex = 160
		Me.TxtMaxdescuento.Text = "TextBox1"
		'
		'Label46
		'
		Me.Label46.EditValue = "0.00"
		Me.Label46.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.Label46.Location = New System.Drawing.Point(616, 224)
		Me.Label46.Name = "Label46"
		'
		'
		'
		Me.Label46.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
		Me.Label46.Properties.DisplayFormat.FormatString = "#,#0.00"
		Me.Label46.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
		Me.Label46.Properties.EditFormat.FormatString = "#,#0.00"
		Me.Label46.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
		Me.Label46.Properties.ReadOnly = True
		Me.Label46.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.InactiveCaptionText, System.Drawing.Color.RoyalBlue)
		Me.Label46.RightToLeft = System.Windows.Forms.RightToLeft.Yes
		Me.Label46.Size = New System.Drawing.Size(80, 17)
		Me.Label46.TabIndex = 189
		'
		'txtTelefono
		'
		Me.txtTelefono.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.txtTelefono.FieldReference = Nothing
		Me.txtTelefono.ForeColor = System.Drawing.Color.RoyalBlue
		Me.txtTelefono.Location = New System.Drawing.Point(592, 296)
		Me.txtTelefono.MaskEdit = ""
		Me.txtTelefono.Name = "txtTelefono"
		Me.txtTelefono.RegExPattern = ValidText.ValidText.RegularExpressionModes.Custom
		Me.txtTelefono.Required = False
		Me.txtTelefono.ShowErrorIcon = True
		Me.txtTelefono.Size = New System.Drawing.Size(104, 13)
		Me.txtTelefono.TabIndex = 2
		Me.txtTelefono.ValidationMode = ValidText.ValidText.ValidationModes.None
		Me.txtTelefono.ValidText = ""
		'
		'Lb_SubExento
		'
		Me.Lb_SubExento.EditValue = "0.00"
		Me.Lb_SubExento.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.Lb_SubExento.Location = New System.Drawing.Point(232, 184)
		Me.Lb_SubExento.Name = "Lb_SubExento"
		'
		'
		'
		Me.Lb_SubExento.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
		Me.Lb_SubExento.Properties.DisplayFormat.FormatString = "#,#0.00"
		Me.Lb_SubExento.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
		Me.Lb_SubExento.Properties.EditFormat.FormatString = "#,#0.00"
		Me.Lb_SubExento.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
		Me.Lb_SubExento.Properties.ReadOnly = True
		Me.Lb_SubExento.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.Color.RoyalBlue)
		Me.Lb_SubExento.RightToLeft = System.Windows.Forms.RightToLeft.Yes
		Me.Lb_SubExento.Size = New System.Drawing.Size(80, 17)
		Me.Lb_SubExento.TabIndex = 35
		'
		'Lb_Subgravado
		'
		Me.Lb_Subgravado.EditValue = "0.00"
		Me.Lb_Subgravado.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.Lb_Subgravado.Location = New System.Drawing.Point(232, 168)
		Me.Lb_Subgravado.Name = "Lb_Subgravado"
		'
		'
		'
		Me.Lb_Subgravado.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
		Me.Lb_Subgravado.Properties.DisplayFormat.FormatString = "#,#0.00"
		Me.Lb_Subgravado.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
		Me.Lb_Subgravado.Properties.EditFormat.FormatString = "#,#0.00"
		Me.Lb_Subgravado.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
		Me.Lb_Subgravado.Properties.ReadOnly = True
		Me.Lb_Subgravado.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.Color.RoyalBlue)
		Me.Lb_Subgravado.RightToLeft = System.Windows.Forms.RightToLeft.Yes
		Me.Lb_Subgravado.Size = New System.Drawing.Size(80, 17)
		Me.Lb_Subgravado.TabIndex = 38
		'
		'txtOtros
		'
		Me.txtOtros.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.txtOtros.ForeColor = System.Drawing.Color.Blue
		Me.txtOtros.Location = New System.Drawing.Point(664, 152)
		Me.txtOtros.Name = "txtOtros"
		Me.txtOtros.Size = New System.Drawing.Size(24, 13)
		Me.txtOtros.TabIndex = 170
		'
		'TxtprecioCosto
		'
		Me.TxtprecioCosto.Location = New System.Drawing.Point(656, 176)
		Me.TxtprecioCosto.Name = "TxtprecioCosto"
		Me.TxtprecioCosto.Size = New System.Drawing.Size(32, 20)
		Me.TxtprecioCosto.TabIndex = 183
		Me.TxtprecioCosto.Text = "TextBox1"
		'
		'txtmontodescuento
		'
		Me.txtmontodescuento.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.txtmontodescuento.ForeColor = System.Drawing.Color.Blue
		Me.txtmontodescuento.Location = New System.Drawing.Point(664, 104)
		Me.txtmontodescuento.Name = "txtmontodescuento"
		Me.txtmontodescuento.Size = New System.Drawing.Size(24, 13)
		Me.txtmontodescuento.TabIndex = 181
		'
		'txtSubtotalGravado
		'
		Me.txtSubtotalGravado.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.txtSubtotalGravado.Enabled = False
		Me.txtSubtotalGravado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtSubtotalGravado.ForeColor = System.Drawing.Color.Blue
		Me.txtSubtotalGravado.Location = New System.Drawing.Point(696, 104)
		Me.txtSubtotalGravado.Name = "txtSubtotalGravado"
		Me.txtSubtotalGravado.Size = New System.Drawing.Size(24, 13)
		Me.txtSubtotalGravado.TabIndex = 178
		Me.txtSubtotalGravado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
		'
		'CkEntregado
		'
		Me.CkEntregado.Enabled = False
		Me.CkEntregado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.CkEntregado.ForeColor = System.Drawing.Color.Green
		Me.CkEntregado.Location = New System.Drawing.Point(304, 104)
		Me.CkEntregado.Name = "CkEntregado"
		Me.CkEntregado.Size = New System.Drawing.Size(80, 16)
		Me.CkEntregado.TabIndex = 156
		Me.CkEntregado.Text = "Entregado"
		'
		'CheckBox1
		'
		Me.CheckBox1.BackColor = System.Drawing.Color.Transparent
		Me.CheckBox1.Enabled = False
		Me.CheckBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.CheckBox1.ForeColor = System.Drawing.Color.Red
		Me.CheckBox1.Location = New System.Drawing.Point(304, 88)
		Me.CheckBox1.Name = "CheckBox1"
		Me.CheckBox1.Size = New System.Drawing.Size(72, 16)
		Me.CheckBox1.TabIndex = 155
		Me.CheckBox1.Text = "Anulada"
		Me.CheckBox1.UseVisualStyleBackColor = False
		'
		'Txtcodmoneda_Venta
		'
		Me.Txtcodmoneda_Venta.Location = New System.Drawing.Point(384, 160)
		Me.Txtcodmoneda_Venta.Name = "Txtcodmoneda_Venta"
		Me.Txtcodmoneda_Venta.Size = New System.Drawing.Size(40, 20)
		Me.Txtcodmoneda_Venta.TabIndex = 188
		'
		'Txt_TipoCambio_Valor_Compra
		'
		Me.Txt_TipoCambio_Valor_Compra.Location = New System.Drawing.Point(432, 160)
		Me.Txt_TipoCambio_Valor_Compra.Name = "Txt_TipoCambio_Valor_Compra"
		Me.Txt_TipoCambio_Valor_Compra.Size = New System.Drawing.Size(32, 20)
		Me.Txt_TipoCambio_Valor_Compra.TabIndex = 189
		'
		'txtTipoCambio
		'
		Me.txtTipoCambio.BackColor = System.Drawing.Color.LightGray
		Me.txtTipoCambio.Enabled = False
		Me.txtTipoCambio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtTipoCambio.ForeColor = System.Drawing.Color.RoyalBlue
		Me.txtTipoCambio.Location = New System.Drawing.Point(137, 29)
		Me.txtTipoCambio.Name = "txtTipoCambio"
		Me.txtTipoCambio.Size = New System.Drawing.Size(80, 16)
		Me.txtTipoCambio.TabIndex = 70
		Me.txtTipoCambio.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'Label3
		'
		Me.Label3.BackColor = System.Drawing.SystemColors.ControlLight
		Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
		Me.Label3.ForeColor = System.Drawing.Color.RoyalBlue
		Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.Label3.Location = New System.Drawing.Point(288, 232)
		Me.Label3.Name = "Label3"
		Me.Label3.Size = New System.Drawing.Size(72, 16)
		Me.Label3.TabIndex = 74
		Me.Label3.Text = "Fecha"
		Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'Label_Anulada
		'
		Me.Label_Anulada.BackColor = System.Drawing.Color.White
		Me.Label_Anulada.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label_Anulada.ForeColor = System.Drawing.Color.Red
		Me.Label_Anulada.Location = New System.Drawing.Point(312, 128)
		Me.Label_Anulada.Name = "Label_Anulada"
		Me.Label_Anulada.Size = New System.Drawing.Size(96, 24)
		Me.Label_Anulada.TabIndex = 191
		Me.Label_Anulada.Text = "ANULADA"
		Me.Label_Anulada.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.Label_Anulada.Visible = False
		'
		'Label49
		'
		Me.Label49.BackColor = System.Drawing.SystemColors.ActiveCaptionText
		Me.Label49.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
		Me.Label49.ForeColor = System.Drawing.Color.RoyalBlue
		Me.Label49.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.Label49.Location = New System.Drawing.Point(272, 24)
		Me.Label49.Name = "Label49"
		Me.Label49.Size = New System.Drawing.Size(16, 17)
		Me.Label49.TabIndex = 191
		Me.Label49.Text = "%"
		Me.Label49.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.Label49.Visible = False
		'
		'Label19
		'
		Me.Label19.BackColor = System.Drawing.SystemColors.ActiveCaptionText
		Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
		Me.Label19.ForeColor = System.Drawing.Color.RoyalBlue
		Me.Label19.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.Label19.Location = New System.Drawing.Point(192, 64)
		Me.Label19.Name = "Label19"
		Me.Label19.Size = New System.Drawing.Size(16, 17)
		Me.Label19.TabIndex = 13
		Me.Label19.Text = "%�"
		Me.Label19.TextAlign = System.Drawing.ContentAlignment.TopCenter
		'
		'txtSExcento
		'
		Me.txtSExcento.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.txtSExcento.ForeColor = System.Drawing.Color.Blue
		Me.txtSExcento.Location = New System.Drawing.Point(121, 120)
		Me.txtSExcento.Name = "txtSExcento"
		Me.txtSExcento.Size = New System.Drawing.Size(48, 13)
		Me.txtSExcento.TabIndex = 17
		'
		'Label7
		'
		Me.Label7.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
		Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
		Me.Label7.ForeColor = System.Drawing.Color.RoyalBlue
		Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.Label7.Location = New System.Drawing.Point(304, 56)
		Me.Label7.Name = "Label7"
		Me.Label7.Size = New System.Drawing.Size(32, 12)
		Me.Label7.TabIndex = 2
		Me.Label7.Text = "d�as"
		Me.Label7.Visible = False
		'
		'txtDiasPlazo
		'
		Me.txtDiasPlazo.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
		Me.txtDiasPlazo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
		Me.txtDiasPlazo.ForeColor = System.Drawing.Color.RoyalBlue
		Me.txtDiasPlazo.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.txtDiasPlazo.Location = New System.Drawing.Point(304, 72)
		Me.txtDiasPlazo.Name = "txtDiasPlazo"
		Me.txtDiasPlazo.Size = New System.Drawing.Size(32, 12)
		Me.txtDiasPlazo.TabIndex = 157
		Me.txtDiasPlazo.Text = "0"
		Me.txtDiasPlazo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.txtDiasPlazo.Visible = False
		'
		'DtVence
		'
		Me.DtVence.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
		Me.DtVence.ForeColor = System.Drawing.Color.RoyalBlue
		Me.DtVence.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.DtVence.Location = New System.Drawing.Point(280, 280)
		Me.DtVence.Name = "DtVence"
		Me.DtVence.Size = New System.Drawing.Size(136, 12)
		Me.DtVence.TabIndex = 164
		Me.DtVence.Text = "fecha vence"
		Me.DtVence.Visible = False
		'
		'Label12
		'
		Me.Label12.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
		Me.Label12.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
		Me.Label12.ForeColor = System.Drawing.Color.RoyalBlue
		Me.Label12.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.Label12.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.Label12.Location = New System.Drawing.Point(432, 248)
		Me.Label12.Name = "Label12"
		Me.Label12.Size = New System.Drawing.Size(80, 14)
		Me.Label12.TabIndex = 163
		Me.Label12.Text = "Tipo Cambio"
		Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'dtFecha
		'
		Me.dtFecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
		Me.dtFecha.ForeColor = System.Drawing.Color.RoyalBlue
		Me.dtFecha.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.dtFecha.Location = New System.Drawing.Point(288, 248)
		Me.dtFecha.Name = "dtFecha"
		Me.dtFecha.Size = New System.Drawing.Size(136, 12)
		Me.dtFecha.TabIndex = 162
		Me.dtFecha.Text = "fecha factura"
		'
		'txtSubtotalExcento
		'
		Me.txtSubtotalExcento.Location = New System.Drawing.Point(232, 72)
		Me.txtSubtotalExcento.Name = "txtSubtotalExcento"
		Me.txtSubtotalExcento.Size = New System.Drawing.Size(40, 20)
		Me.txtSubtotalExcento.TabIndex = 217
		'
		'Label43
		'
		Me.Label43.BackColor = System.Drawing.Color.White
		Me.Label43.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label43.ForeColor = System.Drawing.Color.RoyalBlue
		Me.Label43.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.Label43.Location = New System.Drawing.Point(192, 179)
		Me.Label43.Name = "Label43"
		Me.Label43.Size = New System.Drawing.Size(16, 18)
		Me.Label43.TabIndex = 39
		Me.Label43.TextAlign = System.Drawing.ContentAlignment.TopCenter
		'
		'Label26
		'
		Me.Label26.BackColor = System.Drawing.SystemColors.ControlLight
		Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
		Me.Label26.ForeColor = System.Drawing.Color.RoyalBlue
		Me.Label26.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.Label26.Location = New System.Drawing.Point(168, 264)
		Me.Label26.Name = "Label26"
		Me.Label26.Size = New System.Drawing.Size(72, 16)
		Me.Label26.TabIndex = 76
		Me.Label26.Text = "Vence"
		Me.Label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.Label26.Visible = False
		'
		'Label29
		'
		Me.Label29.BackColor = System.Drawing.Color.White
		Me.Label29.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label29.ForeColor = System.Drawing.Color.RoyalBlue
		Me.Label29.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.Label29.Location = New System.Drawing.Point(162, 197)
		Me.Label29.Name = "Label29"
		Me.Label29.Size = New System.Drawing.Size(16, 18)
		Me.Label29.TabIndex = 36
		Me.Label29.TextAlign = System.Drawing.ContentAlignment.TopCenter
		'
		'Txtdireccion
		'
		Me.Txtdireccion.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Txtdireccion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
		Me.Txtdireccion.ForeColor = System.Drawing.Color.Blue
		Me.Txtdireccion.Location = New System.Drawing.Point(0, 240)
		Me.Txtdireccion.Name = "Txtdireccion"
		Me.Txtdireccion.Size = New System.Drawing.Size(304, 13)
		Me.Txtdireccion.TabIndex = 3
		'
		'txtImpVenta
		'
		Me.txtImpVenta.EditValue = "0.00"
		Me.txtImpVenta.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.txtImpVenta.Location = New System.Drawing.Point(760, 105)
		Me.txtImpVenta.Name = "txtImpVenta"
		'
		'
		'
		Me.txtImpVenta.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
		Me.txtImpVenta.Properties.DisplayFormat.FormatString = "#,#0.00"
		Me.txtImpVenta.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
		Me.txtImpVenta.Properties.Enabled = False
		Me.txtImpVenta.Properties.ReadOnly = True
		Me.txtImpVenta.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
		Me.txtImpVenta.Size = New System.Drawing.Size(64, 17)
		Me.txtImpVenta.TabIndex = 154
		Me.txtImpVenta.Visible = False
		'
		'txtSubtotal
		'
		Me.txtSubtotal.EditValue = "0.00"
		Me.txtSubtotal.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.txtSubtotal.Location = New System.Drawing.Point(407, 15)
		Me.txtSubtotal.Name = "txtSubtotal"
		'
		'
		'
		Me.txtSubtotal.Properties.DisplayFormat.FormatString = "#,#0.00"
		Me.txtSubtotal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
		Me.txtSubtotal.Properties.Enabled = False
		Me.txtSubtotal.Properties.ReadOnly = True
		Me.txtSubtotal.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
		Me.txtSubtotal.RightToLeft = System.Windows.Forms.RightToLeft.Yes
		Me.txtSubtotal.Size = New System.Drawing.Size(127, 19)
		Me.txtSubtotal.TabIndex = 4
		'
		'Label32
		'
		Me.Label32.BackColor = System.Drawing.Color.White
		Me.Label32.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Facturaciones, "Moneda.Simbolo", True))
		Me.Label32.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
		Me.Label32.ForeColor = System.Drawing.Color.RoyalBlue
		Me.Label32.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.Label32.Location = New System.Drawing.Point(391, 15)
		Me.Label32.Name = "Label32"
		Me.Label32.Size = New System.Drawing.Size(14, 21)
		Me.Label32.TabIndex = 24
		Me.Label32.Text = "M"
		Me.Label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'Label21
		'
		Me.Label21.BackColor = System.Drawing.Color.FromArgb(CType(CType(61, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(139, Byte), Integer))
		Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
		Me.Label21.ForeColor = System.Drawing.Color.White
		Me.Label21.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.Label21.Location = New System.Drawing.Point(382, 0)
		Me.Label21.Name = "Label21"
		Me.Label21.Size = New System.Drawing.Size(152, 14)
		Me.Label21.TabIndex = 14
		Me.Label21.Text = "Monto"
		Me.Label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'txtCantidad
		'
		Me.txtCantidad.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
		Me.txtCantidad.Location = New System.Drawing.Point(3, 15)
		Me.txtCantidad.Name = "txtCantidad"
		'
		'
		'
		Me.txtCantidad.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
		Me.txtCantidad.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
		Me.txtCantidad.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
		Me.txtCantidad.RightToLeft = System.Windows.Forms.RightToLeft.Yes
		Me.txtCantidad.Size = New System.Drawing.Size(72, 19)
		Me.txtCantidad.TabIndex = 0
		'
		'Label15
		'
		Me.Label15.BackColor = System.Drawing.Color.FromArgb(CType(CType(61, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(139, Byte), Integer))
		Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
		Me.Label15.ForeColor = System.Drawing.Color.White
		Me.Label15.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.Label15.Location = New System.Drawing.Point(3, 0)
		Me.Label15.Name = "Label15"
		Me.Label15.Size = New System.Drawing.Size(72, 14)
		Me.Label15.TabIndex = 9
		Me.Label15.Text = "Cantidad"
		Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'Label13
		'
		Me.Label13.BackColor = System.Drawing.Color.FromArgb(CType(CType(61, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(139, Byte), Integer))
		Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
		Me.Label13.ForeColor = System.Drawing.Color.White
		Me.Label13.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.Label13.Location = New System.Drawing.Point(72, 0)
		Me.Label13.Name = "Label13"
		Me.Label13.Size = New System.Drawing.Size(80, 14)
		Me.Label13.TabIndex = 0
		Me.Label13.Text = "C�digo"
		Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'TxtCodArticulo
		'
		Me.TxtCodArticulo.EditValue = ""
		Me.TxtCodArticulo.Location = New System.Drawing.Point(77, 15)
		Me.TxtCodArticulo.Name = "TxtCodArticulo"
		'
		'
		'
		Me.TxtCodArticulo.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
		Me.TxtCodArticulo.Size = New System.Drawing.Size(75, 19)
		Me.TxtCodArticulo.TabIndex = 1
		'
		'Label31
		'
		Me.Label31.BackColor = System.Drawing.Color.White
		Me.Label31.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Facturaciones, "Moneda.Simbolo", True))
		Me.Label31.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
		Me.Label31.ForeColor = System.Drawing.Color.RoyalBlue
		Me.Label31.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.Label31.Location = New System.Drawing.Point(155, 15)
		Me.Label31.Name = "Label31"
		Me.Label31.Size = New System.Drawing.Size(14, 21)
		Me.Label31.TabIndex = 23
		Me.Label31.Text = "M"
		Me.Label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'txtPrecioUnit
		'
		Me.txtPrecioUnit.EditValue = 0
		Me.txtPrecioUnit.Location = New System.Drawing.Point(173, 15)
		Me.txtPrecioUnit.Name = "txtPrecioUnit"
		'
		'
		'
		Me.txtPrecioUnit.Properties.DisplayFormat.FormatString = "#,#0.00"
		Me.txtPrecioUnit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
		Me.txtPrecioUnit.Properties.EditFormat.FormatString = "#,#0.00"
		Me.txtPrecioUnit.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
		Me.txtPrecioUnit.Properties.MaxLength = 25
		Me.txtPrecioUnit.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
		Me.txtPrecioUnit.RightToLeft = System.Windows.Forms.RightToLeft.Yes
		Me.txtPrecioUnit.Size = New System.Drawing.Size(85, 19)
		Me.txtPrecioUnit.TabIndex = 2
		'
		'Label16
		'
		Me.Label16.BackColor = System.Drawing.Color.FromArgb(CType(CType(61, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(139, Byte), Integer))
		Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
		Me.Label16.ForeColor = System.Drawing.Color.White
		Me.Label16.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.Label16.Location = New System.Drawing.Point(152, 0)
		Me.Label16.Name = "Label16"
		Me.Label16.Size = New System.Drawing.Size(104, 14)
		Me.Label16.TabIndex = 4
		Me.Label16.Text = "Precio Unit."
		Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'Label17
		'
		Me.Label17.BackColor = System.Drawing.Color.FromArgb(CType(CType(61, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(139, Byte), Integer))
		Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
		Me.Label17.ForeColor = System.Drawing.Color.White
		Me.Label17.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.Label17.Location = New System.Drawing.Point(309, 0)
		Me.Label17.Name = "Label17"
		Me.Label17.Size = New System.Drawing.Size(79, 14)
		Me.Label17.TabIndex = 6
		Me.Label17.Text = "Desc.(%)"
		Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'txtDescuento
		'
		Me.txtDescuento.EditValue = 0
		Me.txtDescuento.Location = New System.Drawing.Point(330, 15)
		Me.txtDescuento.Name = "txtDescuento"
		'
		'
		'
		Me.txtDescuento.Properties.DisplayFormat.FormatString = "#,#0.00"
		Me.txtDescuento.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
		Me.txtDescuento.Properties.EditFormat.FormatString = "#,#0.00"
		Me.txtDescuento.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
		Me.txtDescuento.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
		Me.txtDescuento.RightToLeft = System.Windows.Forms.RightToLeft.Yes
		Me.txtDescuento.Size = New System.Drawing.Size(56, 19)
		Me.txtDescuento.TabIndex = 3
		'
		'SimpleButton2
		'
		Me.SimpleButton2.Enabled = False
		Me.SimpleButton2.Location = New System.Drawing.Point(11, 78)
		Me.SimpleButton2.Name = "SimpleButton2"
		Me.SimpleButton2.Size = New System.Drawing.Size(19, 18)
		Me.SimpleButton2.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.Color.RoyalBlue)
		Me.SimpleButton2.TabIndex = 187
		Me.SimpleButton2.Text = "D"
		'
		'Label25
		'
		Me.Label25.BackColor = System.Drawing.Color.Transparent
		Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label25.ForeColor = System.Drawing.Color.RoyalBlue
		Me.Label25.Image = CType(resources.GetObject("Label25.Image"), System.Drawing.Image)
		Me.Label25.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.Label25.Location = New System.Drawing.Point(6, 127)
		Me.Label25.Name = "Label25"
		Me.Label25.Size = New System.Drawing.Size(208, 24)
		Me.Label25.TabIndex = 7
		Me.Label25.Text = "Total de la Compra"
		Me.Label25.TextAlign = System.Drawing.ContentAlignment.BottomCenter
		'
		'TxtTotal
		'
		Me.TxtTotal.BackgroundImage = CType(resources.GetObject("TxtTotal.BackgroundImage"), System.Drawing.Image)
		Me.TxtTotal.EditValue = "0.00"
		Me.TxtTotal.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.TxtTotal.Location = New System.Drawing.Point(6, 154)
		Me.TxtTotal.Name = "TxtTotal"
		'
		'
		'
		Me.TxtTotal.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
		Me.TxtTotal.Properties.DisplayFormat.FormatString = "#,#0.00"
		Me.TxtTotal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
		Me.TxtTotal.Properties.EditFormat.FormatString = "#,#0.00"
		Me.TxtTotal.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
		Me.TxtTotal.Properties.ReadOnly = True
		Me.TxtTotal.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.Color.Blue)
		Me.TxtTotal.RightToLeft = System.Windows.Forms.RightToLeft.Yes
		Me.TxtTotal.Size = New System.Drawing.Size(208, 44)
		Me.TxtTotal.TabIndex = 29
		'
		'txtImpVentaT
		'
		Me.txtImpVentaT.EditValue = "0.00"
		Me.txtImpVentaT.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.txtImpVentaT.Location = New System.Drawing.Point(86, 105)
		Me.txtImpVentaT.Name = "txtImpVentaT"
		'
		'
		'
		Me.txtImpVentaT.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
		Me.txtImpVentaT.Properties.DisplayFormat.FormatString = "#,#0.00"
		Me.txtImpVentaT.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
		Me.txtImpVentaT.Properties.EditFormat.FormatString = "#,#0.00"
		Me.txtImpVentaT.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
		Me.txtImpVentaT.Properties.ReadOnly = True
		Me.txtImpVentaT.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.Color.RoyalBlue)
		Me.txtImpVentaT.RightToLeft = System.Windows.Forms.RightToLeft.Yes
		Me.txtImpVentaT.Size = New System.Drawing.Size(128, 19)
		Me.txtImpVentaT.TabIndex = 28
		'
		'txtDescuentoT
		'
		Me.txtDescuentoT.EditValue = "0.00"
		Me.txtDescuentoT.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.txtDescuentoT.Location = New System.Drawing.Point(86, 81)
		Me.txtDescuentoT.Name = "txtDescuentoT"
		'
		'
		'
		Me.txtDescuentoT.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
		Me.txtDescuentoT.Properties.DisplayFormat.FormatString = "#,#0.00"
		Me.txtDescuentoT.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
		Me.txtDescuentoT.Properties.ReadOnly = True
		Me.txtDescuentoT.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.Color.RoyalBlue)
		Me.txtDescuentoT.RightToLeft = System.Windows.Forms.RightToLeft.Yes
		Me.txtDescuentoT.Size = New System.Drawing.Size(128, 19)
		Me.txtDescuentoT.TabIndex = 27
		'
		'txtSubtotalT
		'
		Me.txtSubtotalT.EditValue = "0.00"
		Me.txtSubtotalT.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.txtSubtotalT.Location = New System.Drawing.Point(86, 57)
		Me.txtSubtotalT.Name = "txtSubtotalT"
		'
		'
		'
		Me.txtSubtotalT.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
		Me.txtSubtotalT.Properties.DisplayFormat.FormatString = "#,#0.00"
		Me.txtSubtotalT.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
		Me.txtSubtotalT.Properties.EditFormat.FormatString = "#,#0.00"
		Me.txtSubtotalT.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
		Me.txtSubtotalT.Properties.ReadOnly = True
		Me.txtSubtotalT.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.Color.White, System.Drawing.Color.RoyalBlue)
		Me.txtSubtotalT.RightToLeft = System.Windows.Forms.RightToLeft.Yes
		Me.txtSubtotalT.Size = New System.Drawing.Size(128, 19)
		Me.txtSubtotalT.TabIndex = 26
		'
		'Label24
		'
		Me.Label24.BackColor = System.Drawing.SystemColors.ActiveCaptionText
		Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label24.ForeColor = System.Drawing.Color.RoyalBlue
		Me.Label24.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.Label24.Location = New System.Drawing.Point(14, 105)
		Me.Label24.Name = "Label24"
		Me.Label24.Size = New System.Drawing.Size(64, 15)
		Me.Label24.TabIndex = 5
		Me.Label24.Text = "IVA"
		Me.Label24.TextAlign = System.Drawing.ContentAlignment.TopRight
		'
		'Label23
		'
		Me.Label23.BackColor = System.Drawing.SystemColors.ActiveCaptionText
		Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label23.ForeColor = System.Drawing.Color.RoyalBlue
		Me.Label23.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.Label23.Location = New System.Drawing.Point(14, 81)
		Me.Label23.Name = "Label23"
		Me.Label23.Size = New System.Drawing.Size(64, 15)
		Me.Label23.TabIndex = 2
		Me.Label23.Text = "Descuento"
		Me.Label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'Label22
		'
		Me.Label22.BackColor = System.Drawing.Color.White
		Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label22.ForeColor = System.Drawing.Color.RoyalBlue
		Me.Label22.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.Label22.Location = New System.Drawing.Point(14, 57)
		Me.Label22.Name = "Label22"
		Me.Label22.Size = New System.Drawing.Size(64, 15)
		Me.Label22.TabIndex = 0
		Me.Label22.Text = "SubTotal"
		Me.Label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'opCredito
		'
		Me.opCredito.Enabled = False
		Me.opCredito.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.opCredito.Location = New System.Drawing.Point(75, 4)
		Me.opCredito.Name = "opCredito"
		Me.opCredito.Size = New System.Drawing.Size(86, 18)
		Me.opCredito.TabIndex = 1
		Me.opCredito.Text = "Cr�dito"
		'
		'opContado
		'
		Me.opContado.Checked = True
		Me.opContado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.opContado.Location = New System.Drawing.Point(3, 4)
		Me.opContado.Name = "opContado"
		Me.opContado.Size = New System.Drawing.Size(80, 18)
		Me.opContado.TabIndex = 0
		Me.opContado.TabStop = True
		Me.opContado.Text = "Contado"
		'
		'txtCodigo
		'
		Me.txtCodigo.FieldReference = Nothing
		Me.txtCodigo.ForeColor = System.Drawing.Color.RoyalBlue
		Me.txtCodigo.Location = New System.Drawing.Point(59, 0)
		Me.txtCodigo.MaskEdit = ""
		Me.txtCodigo.Name = "txtCodigo"
		Me.txtCodigo.RegExPattern = ValidText.ValidText.RegularExpressionModes.Custom
		Me.txtCodigo.Required = False
		Me.txtCodigo.ShowErrorIcon = True
		Me.txtCodigo.Size = New System.Drawing.Size(56, 20)
		Me.txtCodigo.TabIndex = 0
		Me.txtCodigo.ValidationMode = ValidText.ValidText.ValidationModes.Numbers
		Me.txtCodigo.ValidText = "No se permiten caracteres"
		'
		'Label9
		'
		Me.Label9.BackColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(165, Byte), Integer))
		Me.Label9.Dock = System.Windows.Forms.DockStyle.Top
		Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!)
		Me.Label9.ForeColor = System.Drawing.Color.White
		Me.Label9.Image = CType(resources.GetObject("Label9.Image"), System.Drawing.Image)
		Me.Label9.ImageAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label9.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.Label9.Location = New System.Drawing.Point(0, 0)
		Me.Label9.Name = "Label9"
		Me.Label9.Size = New System.Drawing.Size(994, 32)
		Me.Label9.TabIndex = 171
		Me.Label9.Text = "                                        Sistema de Facturaci�n"
		'
		'ImageList1
		'
		Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
		Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
		Me.ImageList1.Images.SetKeyName(0, "")
		Me.ImageList1.Images.SetKeyName(1, "")
		Me.ImageList1.Images.SetKeyName(2, "")
		Me.ImageList1.Images.SetKeyName(3, "")
		Me.ImageList1.Images.SetKeyName(4, "")
		Me.ImageList1.Images.SetKeyName(5, "")
		Me.ImageList1.Images.SetKeyName(6, "")
		Me.ImageList1.Images.SetKeyName(7, "")
		Me.ImageList1.Images.SetKeyName(8, "")
		Me.ImageList1.Images.SetKeyName(9, "")
		Me.ImageList1.Images.SetKeyName(10, "telefono.png")
		Me.ImageList1.Images.SetKeyName(11, "B�SCULAS (5).png")
		'
		'ToolBarImprimir
		'
		Me.ToolBarImprimir.Enabled = False
		Me.ToolBarImprimir.ImageIndex = 7
		Me.ToolBarImprimir.Name = "ToolBarImprimir"
		Me.ToolBarImprimir.Text = "Imprimir"
		'
		'ToolBarNuevo
		'
		Me.ToolBarNuevo.Enabled = False
		Me.ToolBarNuevo.ImageIndex = 0
		Me.ToolBarNuevo.Name = "ToolBarNuevo"
		Me.ToolBarNuevo.Text = "Nuevo"
		'
		'ToolBarCerrar
		'
		Me.ToolBarCerrar.ImageIndex = 6
		Me.ToolBarCerrar.Name = "ToolBarCerrar"
		Me.ToolBarCerrar.Text = "Cerrar"
		'
		'txtNombreUsuario
		'
		Me.txtNombreUsuario.BackColor = System.Drawing.SystemColors.Control
		Me.txtNombreUsuario.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.txtNombreUsuario.Enabled = False
		Me.txtNombreUsuario.ForeColor = System.Drawing.Color.Blue
		Me.txtNombreUsuario.Location = New System.Drawing.Point(131, 1)
		Me.txtNombreUsuario.Name = "txtNombreUsuario"
		Me.txtNombreUsuario.ReadOnly = True
		Me.txtNombreUsuario.Size = New System.Drawing.Size(163, 13)
		Me.txtNombreUsuario.TabIndex = 3
		'
		'txtUsuario
		'
		Me.txtUsuario.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.txtUsuario.ForeColor = System.Drawing.Color.Blue
		Me.txtUsuario.Location = New System.Drawing.Point(75, 1)
		Me.txtUsuario.Name = "txtUsuario"
		Me.txtUsuario.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
		Me.txtUsuario.Size = New System.Drawing.Size(56, 13)
		Me.txtUsuario.TabIndex = 0
		'
		'ErrorProvider1
		'
		Me.ErrorProvider1.ContainerControl = Me
		'
		'ToolBarAnular
		'
		Me.ToolBarAnular.Enabled = False
		Me.ToolBarAnular.ImageIndex = 3
		Me.ToolBarAnular.Name = "ToolBarAnular"
		Me.ToolBarAnular.Text = "Anular"
		'
		'ToolBar1
		'
		Me.ToolBar1.Appearance = System.Windows.Forms.ToolBarAppearance.Flat
		Me.ToolBar1.Buttons.AddRange(New System.Windows.Forms.ToolBarButton() {Me.ToolBarNuevo, Me.ToolBarBuscar, Me.ToolBarRegistrar, Me.ToolBarAnular, Me.ToolBarImprimir, Me.ToolBarImportar, Me.ToolBarReciboDinero, Me.ToolBarRecarga, Me.ToolBarPesa, Me.ToolBarCerrar})
		Me.ToolBar1.ButtonSize = New System.Drawing.Size(77, 30)
		Me.ToolBar1.Dock = System.Windows.Forms.DockStyle.Bottom
		Me.ToolBar1.DropDownArrows = True
		Me.ToolBar1.ImageList = Me.ImageList1
		Me.ToolBar1.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.ToolBar1.Location = New System.Drawing.Point(0, 591)
		Me.ToolBar1.Name = "ToolBar1"
		Me.ToolBar1.ShowToolTips = True
		Me.ToolBar1.Size = New System.Drawing.Size(994, 58)
		Me.ToolBar1.TabIndex = 0
		'
		'ToolBarBuscar
		'
		Me.ToolBarBuscar.ImageIndex = 1
		Me.ToolBarBuscar.Name = "ToolBarBuscar"
		Me.ToolBarBuscar.Text = "Buscar"
		'
		'ToolBarRegistrar
		'
		Me.ToolBarRegistrar.Enabled = False
		Me.ToolBarRegistrar.ImageIndex = 2
		Me.ToolBarRegistrar.Name = "ToolBarRegistrar"
		Me.ToolBarRegistrar.Text = "Registrar"
		'
		'ToolBarImportar
		'
		Me.ToolBarImportar.Enabled = False
		Me.ToolBarImportar.ImageIndex = 9
		Me.ToolBarImportar.Name = "ToolBarImportar"
		Me.ToolBarImportar.Text = "Importar"
		'
		'ToolBarReciboDinero
		'
		Me.ToolBarReciboDinero.ImageIndex = 5
		Me.ToolBarReciboDinero.Name = "ToolBarReciboDinero"
		Me.ToolBarReciboDinero.Text = "Abono"
		'
		'ToolBarRecarga
		'
		Me.ToolBarRecarga.ImageIndex = 10
		Me.ToolBarRecarga.Name = "ToolBarRecarga"
		Me.ToolBarRecarga.Text = "Recarga"
		'
		'ToolBarPesa
		'
		Me.ToolBarPesa.ImageIndex = 11
		Me.ToolBarPesa.Name = "ToolBarPesa"
		Me.ToolBarPesa.Text = "Pesa"
		'
		'SqlConnection1
		'
		Me.SqlConnection1.ConnectionString = "Data Source=.;Initial Catalog=SeePos;Integrated Security=True"
		Me.SqlConnection1.FireInfoMessageEventOnUserErrors = False
		'
		'SqlSelectCommand3
		'
		Me.SqlSelectCommand3.CommandText = "SELECT CodMoneda, MonedaNombre, ValorCompra, ValorVenta, Simbolo FROM Moneda"
		Me.SqlSelectCommand3.Connection = Me.SqlConnection1
		'
		'SqlInsertCommand3
		'
		Me.SqlInsertCommand3.CommandText = resources.GetString("SqlInsertCommand3.CommandText")
		Me.SqlInsertCommand3.Connection = Me.SqlConnection1
		Me.SqlInsertCommand3.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@CodMoneda", System.Data.SqlDbType.Int, 4, "CodMoneda"), New System.Data.SqlClient.SqlParameter("@MonedaNombre", System.Data.SqlDbType.VarChar, 50, "MonedaNombre"), New System.Data.SqlClient.SqlParameter("@ValorCompra", System.Data.SqlDbType.Float, 8, "ValorCompra"), New System.Data.SqlClient.SqlParameter("@ValorVenta", System.Data.SqlDbType.Float, 8, "ValorVenta"), New System.Data.SqlClient.SqlParameter("@Simbolo", System.Data.SqlDbType.VarChar, 2, "Simbolo")})
		'
		'SqlUpdateCommand3
		'
		Me.SqlUpdateCommand3.CommandText = resources.GetString("SqlUpdateCommand3.CommandText")
		Me.SqlUpdateCommand3.Connection = Me.SqlConnection1
		Me.SqlUpdateCommand3.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@CodMoneda", System.Data.SqlDbType.Int, 4, "CodMoneda"), New System.Data.SqlClient.SqlParameter("@MonedaNombre", System.Data.SqlDbType.VarChar, 50, "MonedaNombre"), New System.Data.SqlClient.SqlParameter("@ValorCompra", System.Data.SqlDbType.Float, 8, "ValorCompra"), New System.Data.SqlClient.SqlParameter("@ValorVenta", System.Data.SqlDbType.Float, 8, "ValorVenta"), New System.Data.SqlClient.SqlParameter("@Simbolo", System.Data.SqlDbType.VarChar, 2, "Simbolo"), New System.Data.SqlClient.SqlParameter("@Original_CodMoneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMoneda", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_MonedaNombre", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MonedaNombre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Simbolo", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Simbolo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_ValorCompra", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValorCompra", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_ValorVenta", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValorVenta", System.Data.DataRowVersion.Original, Nothing)})
		'
		'SqlDeleteCommand3
		'
		Me.SqlDeleteCommand3.CommandText = resources.GetString("SqlDeleteCommand3.CommandText")
		Me.SqlDeleteCommand3.Connection = Me.SqlConnection1
		Me.SqlDeleteCommand3.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_CodMoneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMoneda", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_MonedaNombre", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MonedaNombre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Simbolo", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Simbolo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_ValorCompra", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValorCompra", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_ValorVenta", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ValorVenta", System.Data.DataRowVersion.Original, Nothing)})
		'
		'Adapter_Moneda
		'
		Me.Adapter_Moneda.DeleteCommand = Me.SqlDeleteCommand3
		Me.Adapter_Moneda.InsertCommand = Me.SqlInsertCommand3
		Me.Adapter_Moneda.SelectCommand = Me.SqlSelectCommand3
		Me.Adapter_Moneda.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Moneda", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("CodMoneda", "CodMoneda"), New System.Data.Common.DataColumnMapping("MonedaNombre", "MonedaNombre"), New System.Data.Common.DataColumnMapping("ValorCompra", "ValorCompra"), New System.Data.Common.DataColumnMapping("ValorVenta", "ValorVenta"), New System.Data.Common.DataColumnMapping("Simbolo", "Simbolo")})})
		Me.Adapter_Moneda.UpdateCommand = Me.SqlUpdateCommand3
		'
		'Adapter_Clientes
		'
		Me.Adapter_Clientes.DeleteCommand = Me.SqlDeleteCommand2
		Me.Adapter_Clientes.InsertCommand = Me.SqlInsertCommand2
		Me.Adapter_Clientes.SelectCommand = Me.SqlSelectCommand4
		Me.Adapter_Clientes.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Clientes", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("identificacion", "identificacion"), New System.Data.Common.DataColumnMapping("cedula", "cedula"), New System.Data.Common.DataColumnMapping("nombre", "nombre"), New System.Data.Common.DataColumnMapping("observaciones", "observaciones"), New System.Data.Common.DataColumnMapping("Telefono_01", "Telefono_01"), New System.Data.Common.DataColumnMapping("telefono_02", "telefono_02"), New System.Data.Common.DataColumnMapping("fax_01", "fax_01"), New System.Data.Common.DataColumnMapping("fax_02", "fax_02"), New System.Data.Common.DataColumnMapping("e_mail", "e_mail"), New System.Data.Common.DataColumnMapping("abierto", "abierto"), New System.Data.Common.DataColumnMapping("direccion", "direccion"), New System.Data.Common.DataColumnMapping("impuesto", "impuesto"), New System.Data.Common.DataColumnMapping("max_credito", "max_credito"), New System.Data.Common.DataColumnMapping("Plazo_credito", "Plazo_credito"), New System.Data.Common.DataColumnMapping("descuento", "descuento"), New System.Data.Common.DataColumnMapping("empresa", "empresa"), New System.Data.Common.DataColumnMapping("tipoprecio", "tipoprecio"), New System.Data.Common.DataColumnMapping("sinrestriccion", "sinrestriccion"), New System.Data.Common.DataColumnMapping("usuario", "usuario"), New System.Data.Common.DataColumnMapping("nombreusuario", "nombreusuario"), New System.Data.Common.DataColumnMapping("agente", "agente"), New System.Data.Common.DataColumnMapping("CodMonedaCredito", "CodMonedaCredito"), New System.Data.Common.DataColumnMapping("PrecioSugerido", "PrecioSugerido")})})
		Me.Adapter_Clientes.UpdateCommand = Me.SqlUpdateCommand2
		'
		'SqlDeleteCommand2
		'
		Me.SqlDeleteCommand2.CommandText = resources.GetString("SqlDeleteCommand2.CommandText")
		Me.SqlDeleteCommand2.Connection = Me.SqlConnection1
		Me.SqlDeleteCommand2.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_identificacion", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "identificacion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_CodMonedaCredito", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMonedaCredito", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Plazo_credito", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Plazo_credito", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_PrecioSugerido", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PrecioSugerido", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Telefono_01", System.Data.SqlDbType.VarChar, 16, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Telefono_01", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_abierto", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "abierto", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_agente", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "agente", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_cedula", System.Data.SqlDbType.VarChar, 30, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "cedula", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_descuento", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "descuento", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_direccion", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "direccion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_e_mail", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "e_mail", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_empresa", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "empresa", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_fax_01", System.Data.SqlDbType.VarChar, 16, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "fax_01", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_fax_02", System.Data.SqlDbType.VarChar, 16, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "fax_02", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_impuesto", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "impuesto", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_max_credito", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "max_credito", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_nombre", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "nombre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_nombreusuario", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "nombreusuario", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_observaciones", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "observaciones", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_sinrestriccion", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "sinrestriccion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_telefono_02", System.Data.SqlDbType.VarChar, 16, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "telefono_02", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_tipoprecio", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "tipoprecio", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_usuario", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "usuario", System.Data.DataRowVersion.Original, Nothing)})
		'
		'SqlInsertCommand2
		'
		Me.SqlInsertCommand2.CommandText = resources.GetString("SqlInsertCommand2.CommandText")
		Me.SqlInsertCommand2.Connection = Me.SqlConnection1
		Me.SqlInsertCommand2.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@identificacion", System.Data.SqlDbType.Int, 4, "identificacion"), New System.Data.SqlClient.SqlParameter("@cedula", System.Data.SqlDbType.VarChar, 30, "cedula"), New System.Data.SqlClient.SqlParameter("@nombre", System.Data.SqlDbType.VarChar, 255, "nombre"), New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.VarChar, 255, "observaciones"), New System.Data.SqlClient.SqlParameter("@Telefono_01", System.Data.SqlDbType.VarChar, 16, "Telefono_01"), New System.Data.SqlClient.SqlParameter("@telefono_02", System.Data.SqlDbType.VarChar, 16, "telefono_02"), New System.Data.SqlClient.SqlParameter("@fax_01", System.Data.SqlDbType.VarChar, 16, "fax_01"), New System.Data.SqlClient.SqlParameter("@fax_02", System.Data.SqlDbType.VarChar, 16, "fax_02"), New System.Data.SqlClient.SqlParameter("@e_mail", System.Data.SqlDbType.VarChar, 255, "e_mail"), New System.Data.SqlClient.SqlParameter("@abierto", System.Data.SqlDbType.VarChar, 2, "abierto"), New System.Data.SqlClient.SqlParameter("@direccion", System.Data.SqlDbType.VarChar, 255, "direccion"), New System.Data.SqlClient.SqlParameter("@impuesto", System.Data.SqlDbType.Float, 8, "impuesto"), New System.Data.SqlClient.SqlParameter("@max_credito", System.Data.SqlDbType.Float, 8, "max_credito"), New System.Data.SqlClient.SqlParameter("@Plazo_credito", System.Data.SqlDbType.Int, 4, "Plazo_credito"), New System.Data.SqlClient.SqlParameter("@descuento", System.Data.SqlDbType.Float, 8, "descuento"), New System.Data.SqlClient.SqlParameter("@empresa", System.Data.SqlDbType.VarChar, 2, "empresa"), New System.Data.SqlClient.SqlParameter("@tipoprecio", System.Data.SqlDbType.SmallInt, 2, "tipoprecio"), New System.Data.SqlClient.SqlParameter("@sinrestriccion", System.Data.SqlDbType.VarChar, 2, "sinrestriccion"), New System.Data.SqlClient.SqlParameter("@usuario", System.Data.SqlDbType.VarChar, 50, "usuario"), New System.Data.SqlClient.SqlParameter("@nombreusuario", System.Data.SqlDbType.VarChar, 50, "nombreusuario"), New System.Data.SqlClient.SqlParameter("@agente", System.Data.SqlDbType.VarChar, 50, "agente"), New System.Data.SqlClient.SqlParameter("@CodMonedaCredito", System.Data.SqlDbType.Int, 4, "CodMonedaCredito"), New System.Data.SqlClient.SqlParameter("@PrecioSugerido", System.Data.SqlDbType.Bit, 1, "PrecioSugerido")})
		'
		'SqlSelectCommand4
		'
		Me.SqlSelectCommand4.CommandText = resources.GetString("SqlSelectCommand4.CommandText")
		Me.SqlSelectCommand4.Connection = Me.SqlConnection1
		'
		'SqlUpdateCommand2
		'
		Me.SqlUpdateCommand2.CommandText = resources.GetString("SqlUpdateCommand2.CommandText")
		Me.SqlUpdateCommand2.Connection = Me.SqlConnection1
		Me.SqlUpdateCommand2.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@identificacion", System.Data.SqlDbType.Int, 4, "identificacion"), New System.Data.SqlClient.SqlParameter("@cedula", System.Data.SqlDbType.VarChar, 30, "cedula"), New System.Data.SqlClient.SqlParameter("@nombre", System.Data.SqlDbType.VarChar, 255, "nombre"), New System.Data.SqlClient.SqlParameter("@observaciones", System.Data.SqlDbType.VarChar, 255, "observaciones"), New System.Data.SqlClient.SqlParameter("@Telefono_01", System.Data.SqlDbType.VarChar, 16, "Telefono_01"), New System.Data.SqlClient.SqlParameter("@telefono_02", System.Data.SqlDbType.VarChar, 16, "telefono_02"), New System.Data.SqlClient.SqlParameter("@fax_01", System.Data.SqlDbType.VarChar, 16, "fax_01"), New System.Data.SqlClient.SqlParameter("@fax_02", System.Data.SqlDbType.VarChar, 16, "fax_02"), New System.Data.SqlClient.SqlParameter("@e_mail", System.Data.SqlDbType.VarChar, 255, "e_mail"), New System.Data.SqlClient.SqlParameter("@abierto", System.Data.SqlDbType.VarChar, 2, "abierto"), New System.Data.SqlClient.SqlParameter("@direccion", System.Data.SqlDbType.VarChar, 255, "direccion"), New System.Data.SqlClient.SqlParameter("@impuesto", System.Data.SqlDbType.Float, 8, "impuesto"), New System.Data.SqlClient.SqlParameter("@max_credito", System.Data.SqlDbType.Float, 8, "max_credito"), New System.Data.SqlClient.SqlParameter("@Plazo_credito", System.Data.SqlDbType.Int, 4, "Plazo_credito"), New System.Data.SqlClient.SqlParameter("@descuento", System.Data.SqlDbType.Float, 8, "descuento"), New System.Data.SqlClient.SqlParameter("@empresa", System.Data.SqlDbType.VarChar, 2, "empresa"), New System.Data.SqlClient.SqlParameter("@tipoprecio", System.Data.SqlDbType.SmallInt, 2, "tipoprecio"), New System.Data.SqlClient.SqlParameter("@sinrestriccion", System.Data.SqlDbType.VarChar, 2, "sinrestriccion"), New System.Data.SqlClient.SqlParameter("@usuario", System.Data.SqlDbType.VarChar, 50, "usuario"), New System.Data.SqlClient.SqlParameter("@nombreusuario", System.Data.SqlDbType.VarChar, 50, "nombreusuario"), New System.Data.SqlClient.SqlParameter("@agente", System.Data.SqlDbType.VarChar, 50, "agente"), New System.Data.SqlClient.SqlParameter("@CodMonedaCredito", System.Data.SqlDbType.Int, 4, "CodMonedaCredito"), New System.Data.SqlClient.SqlParameter("@PrecioSugerido", System.Data.SqlDbType.Bit, 1, "PrecioSugerido"), New System.Data.SqlClient.SqlParameter("@Original_identificacion", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "identificacion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_CodMonedaCredito", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMonedaCredito", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Plazo_credito", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Plazo_credito", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_PrecioSugerido", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PrecioSugerido", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Telefono_01", System.Data.SqlDbType.VarChar, 16, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Telefono_01", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_abierto", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "abierto", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_agente", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "agente", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_cedula", System.Data.SqlDbType.VarChar, 30, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "cedula", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_descuento", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "descuento", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_direccion", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "direccion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_e_mail", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "e_mail", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_empresa", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "empresa", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_fax_01", System.Data.SqlDbType.VarChar, 16, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "fax_01", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_fax_02", System.Data.SqlDbType.VarChar, 16, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "fax_02", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_impuesto", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "impuesto", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_max_credito", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "max_credito", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_nombre", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "nombre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_nombreusuario", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "nombreusuario", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_observaciones", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "observaciones", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_sinrestriccion", System.Data.SqlDbType.VarChar, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "sinrestriccion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_telefono_02", System.Data.SqlDbType.VarChar, 16, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "telefono_02", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_tipoprecio", System.Data.SqlDbType.SmallInt, 2, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "tipoprecio", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_usuario", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "usuario", System.Data.DataRowVersion.Original, Nothing)})
		'
		'Adapter_Usuarios
		'
		Me.Adapter_Usuarios.SelectCommand = Me.SqlSelectCommand2
		Me.Adapter_Usuarios.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Usuarios", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Cedula", "Cedula"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre"), New System.Data.Common.DataColumnMapping("Clave_Entrada", "Clave_Entrada"), New System.Data.Common.DataColumnMapping("Clave_Interna", "Clave_Interna"), New System.Data.Common.DataColumnMapping("CambiarPrecio", "CambiarPrecio"), New System.Data.Common.DataColumnMapping("Aplicar_Desc", "Aplicar_Desc"), New System.Data.Common.DataColumnMapping("Exist_Negativa", "Exist_Negativa"), New System.Data.Common.DataColumnMapping("Porc_Desc", "Porc_Desc"), New System.Data.Common.DataColumnMapping("Porc_Precio", "Porc_Precio")})})
		'
		'SqlSelectCommand2
		'
		Me.SqlSelectCommand2.CommandText = "SELECT Cedula, Nombre, Clave_Entrada, Clave_Interna, CambiarPrecio, Aplicar_Desc," &
	" Exist_Negativa, Porc_Desc, Porc_Precio FROM Usuarios"
		Me.SqlSelectCommand2.Connection = Me.SqlConnection1
		'
		'Adapter_Encargados_Compra
		'
		Me.Adapter_Encargados_Compra.SelectCommand = Me.SqlSelectCommand11
		Me.Adapter_Encargados_Compra.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "encargadocompras", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Identificacion", "Identificacion"), New System.Data.Common.DataColumnMapping("Cliente", "Cliente"), New System.Data.Common.DataColumnMapping("Nombre", "Nombre")})})
		'
		'SqlSelectCommand11
		'
		Me.SqlSelectCommand11.CommandText = "SELECT Identificacion, Cliente, Nombre FROM encargadocompras"
		Me.SqlSelectCommand11.Connection = Me.SqlConnection1
		'
		'Adapter_Ventas_Detalles
		'
		Me.Adapter_Ventas_Detalles.DeleteCommand = Me.SqlDeleteCommand
		Me.Adapter_Ventas_Detalles.InsertCommand = Me.SqlInsertCommand
		Me.Adapter_Ventas_Detalles.SelectCommand = Me.SqlSelectCommand10
		Me.Adapter_Ventas_Detalles.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Ventas_Detalle", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("id_venta_detalle", "id_venta_detalle"), New System.Data.Common.DataColumnMapping("Id_Factura", "Id_Factura"), New System.Data.Common.DataColumnMapping("Codigo", "Codigo"), New System.Data.Common.DataColumnMapping("Descripcion", "Descripcion"), New System.Data.Common.DataColumnMapping("Cantidad", "Cantidad"), New System.Data.Common.DataColumnMapping("Precio_Costo", "Precio_Costo"), New System.Data.Common.DataColumnMapping("Precio_Base", "Precio_Base"), New System.Data.Common.DataColumnMapping("Precio_Flete", "Precio_Flete"), New System.Data.Common.DataColumnMapping("Precio_Otros", "Precio_Otros"), New System.Data.Common.DataColumnMapping("Precio_Unit", "Precio_Unit"), New System.Data.Common.DataColumnMapping("Descuento", "Descuento"), New System.Data.Common.DataColumnMapping("Monto_Descuento", "Monto_Descuento"), New System.Data.Common.DataColumnMapping("Impuesto", "Impuesto"), New System.Data.Common.DataColumnMapping("Monto_Impuesto", "Monto_Impuesto"), New System.Data.Common.DataColumnMapping("SubtotalGravado", "SubtotalGravado"), New System.Data.Common.DataColumnMapping("SubTotalExcento", "SubTotalExcento"), New System.Data.Common.DataColumnMapping("SubTotal", "SubTotal"), New System.Data.Common.DataColumnMapping("Devoluciones", "Devoluciones"), New System.Data.Common.DataColumnMapping("Numero_Entrega", "Numero_Entrega"), New System.Data.Common.DataColumnMapping("Max_Descuento", "Max_Descuento"), New System.Data.Common.DataColumnMapping("Tipo_Cambio_ValorCompra", "Tipo_Cambio_ValorCompra"), New System.Data.Common.DataColumnMapping("Cod_MonedaVenta", "Cod_MonedaVenta"), New System.Data.Common.DataColumnMapping("Id_bodega", "Id_bodega"), New System.Data.Common.DataColumnMapping("Comision", "Comision"), New System.Data.Common.DataColumnMapping("MontoComision", "MontoComision")})})
		Me.Adapter_Ventas_Detalles.UpdateCommand = Me.SqlUpdateCommand
		'
		'SqlDeleteCommand
		'
		Me.SqlDeleteCommand.CommandText = resources.GetString("SqlDeleteCommand.CommandText")
		Me.SqlDeleteCommand.Connection = Me.SqlConnection1
		Me.SqlDeleteCommand.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_id_venta_detalle", System.Data.SqlDbType.BigInt, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "id_venta_detalle", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Id_Factura", System.Data.SqlDbType.BigInt, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_Factura", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Codigo", System.Data.SqlDbType.BigInt, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Codigo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Descripcion", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Descripcion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Cantidad", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cantidad", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Precio_Costo", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Precio_Costo", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Precio_Base", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Precio_Base", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Precio_Flete", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Precio_Flete", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Precio_Otros", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Precio_Otros", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Precio_Unit", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Precio_Unit", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Descuento", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Descuento", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Monto_Descuento", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Monto_Descuento", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Impuesto", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Impuesto", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Monto_Impuesto", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Monto_Impuesto", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_SubtotalGravado", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubtotalGravado", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_SubTotalExcento", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubTotalExcento", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_SubTotal", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubTotal", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Devoluciones", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Devoluciones", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Numero_Entrega", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Numero_Entrega", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Max_Descuento", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Max_Descuento", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Tipo_Cambio_ValorCompra", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Tipo_Cambio_ValorCompra", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Cod_MonedaVenta", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cod_MonedaVenta", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Id_bodega", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id_bodega", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Comision", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Comision", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_MontoComision", System.Data.SqlDbType.Float, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MontoComision", System.Data.DataRowVersion.Original, Nothing)})
		'
		'SqlInsertCommand
		'
		Me.SqlInsertCommand.CommandText = resources.GetString("SqlInsertCommand.CommandText")
		Me.SqlInsertCommand.Connection = Me.SqlConnection1
		Me.SqlInsertCommand.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Id_Factura", System.Data.SqlDbType.BigInt, 8, "Id_Factura"), New System.Data.SqlClient.SqlParameter("@Codigo", System.Data.SqlDbType.BigInt, 8, "Codigo"), New System.Data.SqlClient.SqlParameter("@Descripcion", System.Data.SqlDbType.VarChar, 250, "Descripcion"), New System.Data.SqlClient.SqlParameter("@Cantidad", System.Data.SqlDbType.Float, 8, "Cantidad"), New System.Data.SqlClient.SqlParameter("@Precio_Costo", System.Data.SqlDbType.Float, 8, "Precio_Costo"), New System.Data.SqlClient.SqlParameter("@Precio_Base", System.Data.SqlDbType.Float, 8, "Precio_Base"), New System.Data.SqlClient.SqlParameter("@Precio_Flete", System.Data.SqlDbType.Float, 8, "Precio_Flete"), New System.Data.SqlClient.SqlParameter("@Precio_Otros", System.Data.SqlDbType.Float, 8, "Precio_Otros"), New System.Data.SqlClient.SqlParameter("@Precio_Unit", System.Data.SqlDbType.Float, 8, "Precio_Unit"), New System.Data.SqlClient.SqlParameter("@Descuento", System.Data.SqlDbType.Float, 8, "Descuento"), New System.Data.SqlClient.SqlParameter("@Monto_Descuento", System.Data.SqlDbType.Float, 8, "Monto_Descuento"), New System.Data.SqlClient.SqlParameter("@Impuesto", System.Data.SqlDbType.Float, 8, "Impuesto"), New System.Data.SqlClient.SqlParameter("@Monto_Impuesto", System.Data.SqlDbType.Float, 8, "Monto_Impuesto"), New System.Data.SqlClient.SqlParameter("@SubtotalGravado", System.Data.SqlDbType.Float, 8, "SubtotalGravado"), New System.Data.SqlClient.SqlParameter("@SubTotalExcento", System.Data.SqlDbType.Float, 8, "SubTotalExcento"), New System.Data.SqlClient.SqlParameter("@SubTotal", System.Data.SqlDbType.Float, 8, "SubTotal"), New System.Data.SqlClient.SqlParameter("@Devoluciones", System.Data.SqlDbType.Float, 8, "Devoluciones"), New System.Data.SqlClient.SqlParameter("@Numero_Entrega", System.Data.SqlDbType.Float, 8, "Numero_Entrega"), New System.Data.SqlClient.SqlParameter("@Max_Descuento", System.Data.SqlDbType.Float, 8, "Max_Descuento"), New System.Data.SqlClient.SqlParameter("@Tipo_Cambio_ValorCompra", System.Data.SqlDbType.Float, 8, "Tipo_Cambio_ValorCompra"), New System.Data.SqlClient.SqlParameter("@Cod_MonedaVenta", System.Data.SqlDbType.Int, 4, "Cod_MonedaVenta"), New System.Data.SqlClient.SqlParameter("@Comision", System.Data.SqlDbType.Float, 8, "Comision"), New System.Data.SqlClient.SqlParameter("@MontoComision", System.Data.SqlDbType.Float, 8, "MontoComision"), New System.Data.SqlClient.SqlParameter("@Id_bodega", System.Data.SqlDbType.Int, 4, "Id_bodega"), New System.Data.SqlClient.SqlParameter("@Barras", System.Data.SqlDbType.VarChar, 255, "Barras"), New System.Data.SqlClient.SqlParameter("@Porc_Exonerado", System.Data.SqlDbType.Float, 8, "Porc_Exonerado")})
		'
		'SqlSelectCommand10
		'
		Me.SqlSelectCommand10.CommandText = resources.GetString("SqlSelectCommand10.CommandText")
		Me.SqlSelectCommand10.Connection = Me.SqlConnection1
		'
		'SqlUpdateCommand
		'
		Me.SqlUpdateCommand.CommandText = resources.GetString("SqlUpdateCommand.CommandText")
		Me.SqlUpdateCommand.Connection = Me.SqlConnection1
		Me.SqlUpdateCommand.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Id_Factura", System.Data.SqlDbType.BigInt, 8, "Id_Factura"), New System.Data.SqlClient.SqlParameter("@Codigo", System.Data.SqlDbType.BigInt, 8, "Codigo"), New System.Data.SqlClient.SqlParameter("@Descripcion", System.Data.SqlDbType.VarChar, 250, "Descripcion"), New System.Data.SqlClient.SqlParameter("@Cantidad", System.Data.SqlDbType.Float, 8, "Cantidad"), New System.Data.SqlClient.SqlParameter("@Precio_Costo", System.Data.SqlDbType.Float, 8, "Precio_Costo"), New System.Data.SqlClient.SqlParameter("@Precio_Base", System.Data.SqlDbType.Float, 8, "Precio_Base"), New System.Data.SqlClient.SqlParameter("@Precio_Flete", System.Data.SqlDbType.Float, 8, "Precio_Flete"), New System.Data.SqlClient.SqlParameter("@Precio_Otros", System.Data.SqlDbType.Float, 8, "Precio_Otros"), New System.Data.SqlClient.SqlParameter("@Precio_Unit", System.Data.SqlDbType.Float, 8, "Precio_Unit"), New System.Data.SqlClient.SqlParameter("@Descuento", System.Data.SqlDbType.Float, 8, "Descuento"), New System.Data.SqlClient.SqlParameter("@Monto_Descuento", System.Data.SqlDbType.Float, 8, "Monto_Descuento"), New System.Data.SqlClient.SqlParameter("@Impuesto", System.Data.SqlDbType.Float, 8, "Impuesto"), New System.Data.SqlClient.SqlParameter("@Monto_Impuesto", System.Data.SqlDbType.Float, 8, "Monto_Impuesto"), New System.Data.SqlClient.SqlParameter("@SubtotalGravado", System.Data.SqlDbType.Float, 8, "SubtotalGravado"), New System.Data.SqlClient.SqlParameter("@SubTotalExcento", System.Data.SqlDbType.Float, 8, "SubTotalExcento"), New System.Data.SqlClient.SqlParameter("@SubTotal", System.Data.SqlDbType.Float, 8, "SubTotal"), New System.Data.SqlClient.SqlParameter("@Devoluciones", System.Data.SqlDbType.Float, 8, "Devoluciones"), New System.Data.SqlClient.SqlParameter("@Numero_Entrega", System.Data.SqlDbType.Float, 8, "Numero_Entrega"), New System.Data.SqlClient.SqlParameter("@Max_Descuento", System.Data.SqlDbType.Float, 8, "Max_Descuento"), New System.Data.SqlClient.SqlParameter("@Tipo_Cambio_ValorCompra", System.Data.SqlDbType.Float, 8, "Tipo_Cambio_ValorCompra"), New System.Data.SqlClient.SqlParameter("@Cod_MonedaVenta", System.Data.SqlDbType.Int, 4, "Cod_MonedaVenta"), New System.Data.SqlClient.SqlParameter("@Id_bodega", System.Data.SqlDbType.Int, 4, "Id_bodega"), New System.Data.SqlClient.SqlParameter("@Barras", System.Data.SqlDbType.VarChar, 255, "Barras"), New System.Data.SqlClient.SqlParameter("@Porc_Exonerado", System.Data.SqlDbType.Float, 8, "Porc_Exonerado"), New System.Data.SqlClient.SqlParameter("@CodVendedor", System.Data.SqlDbType.VarChar, 50, "CodVendedor")})
		'
		'ToolBarButton1
		'
		Me.ToolBarButton1.Name = "ToolBarButton1"
		Me.ToolBarButton1.Text = "Importar Cotizaci�n"
		'
		'SqlSelectCommand12
		'
		Me.SqlSelectCommand12.CommandText = resources.GetString("SqlSelectCommand12.CommandText")
		Me.SqlSelectCommand12.Connection = Me.SqlConnection1
		'
		'Adapter_Configuraciones
		'
		Me.Adapter_Configuraciones.SelectCommand = Me.SqlSelectCommand1
		Me.Adapter_Configuraciones.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "configuraciones", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Cedula", "Cedula"), New System.Data.Common.DataColumnMapping("Intereses", "Intereses"), New System.Data.Common.DataColumnMapping("UnicoConsecutivo", "UnicoConsecutivo"), New System.Data.Common.DataColumnMapping("NumeroConsecutivo", "NumeroConsecutivo"), New System.Data.Common.DataColumnMapping("ConsContado", "ConsContado"), New System.Data.Common.DataColumnMapping("NumeroContado", "NumeroContado"), New System.Data.Common.DataColumnMapping("ConsCredito", "ConsCredito"), New System.Data.Common.DataColumnMapping("NumeroCredito", "NumeroCredito"), New System.Data.Common.DataColumnMapping("ConsPuntoVenta", "ConsPuntoVenta"), New System.Data.Common.DataColumnMapping("NumeroPuntoVenta", "NumeroPuntoVenta"), New System.Data.Common.DataColumnMapping("Logo", "Logo")})})
		'
		'SqlSelectCommand1
		'
		Me.SqlSelectCommand1.CommandText = "SELECT Cedula, Intereses, UnicoConsecutivo, NumeroConsecutivo, ConsContado, Numer" &
	"oContado, ConsCredito, NumeroCredito, ConsPuntoVenta, NumeroPuntoVenta, Logo FRO" &
	"M configuraciones"
		Me.SqlSelectCommand1.Connection = Me.SqlConnection1
		'
		'Adapter_Coti
		'
		Me.Adapter_Coti.DeleteCommand = Me.SqlDeleteCommand1
		Me.Adapter_Coti.InsertCommand = Me.SqlInsertCommand1
		Me.Adapter_Coti.SelectCommand = Me.SqlSelectCommand13
		Me.Adapter_Coti.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Cotizacion", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Cotizacion", "Cotizacion"), New System.Data.Common.DataColumnMapping("Fecha", "Fecha"), New System.Data.Common.DataColumnMapping("Cod_Cliente", "Cod_Cliente"), New System.Data.Common.DataColumnMapping("Nombre_Cliente", "Nombre_Cliente"), New System.Data.Common.DataColumnMapping("Contacto", "Contacto"), New System.Data.Common.DataColumnMapping("Validez", "Validez"), New System.Data.Common.DataColumnMapping("TiempoEntrega", "TiempoEntrega"), New System.Data.Common.DataColumnMapping("Contado", "Contado"), New System.Data.Common.DataColumnMapping("Credito", "Credito"), New System.Data.Common.DataColumnMapping("Anulado", "Anulado"), New System.Data.Common.DataColumnMapping("Dias", "Dias"), New System.Data.Common.DataColumnMapping("Observaciones", "Observaciones"), New System.Data.Common.DataColumnMapping("SubTotalGravada", "SubTotalGravada"), New System.Data.Common.DataColumnMapping("SubTotalExento", "SubTotalExento"), New System.Data.Common.DataColumnMapping("Descuento", "Descuento"), New System.Data.Common.DataColumnMapping("PagoImpuesto", "PagoImpuesto"), New System.Data.Common.DataColumnMapping("Total", "Total"), New System.Data.Common.DataColumnMapping("Cedula", "Cedula"), New System.Data.Common.DataColumnMapping("CodMoneda", "CodMoneda"), New System.Data.Common.DataColumnMapping("MonedaNombre", "MonedaNombre"), New System.Data.Common.DataColumnMapping("SubTotal", "SubTotal"), New System.Data.Common.DataColumnMapping("Tipo_Cambio", "Tipo_Cambio"), New System.Data.Common.DataColumnMapping("Imp_Venta", "Imp_Venta"), New System.Data.Common.DataColumnMapping("Transporte", "Transporte"), New System.Data.Common.DataColumnMapping("Venta", "Venta")})})
		Me.Adapter_Coti.UpdateCommand = Me.SqlUpdateCommand1
		'
		'SqlDeleteCommand1
		'
		Me.SqlDeleteCommand1.CommandText = resources.GetString("SqlDeleteCommand1.CommandText")
		Me.SqlDeleteCommand1.Connection = Me.SqlConnection1
		Me.SqlDeleteCommand1.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_Cotizacion", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cotizacion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Anulado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anulado", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Cedula", System.Data.SqlDbType.VarChar, 75, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cedula", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_CodMoneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMoneda", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Cod_Cliente", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cod_Cliente", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Contacto", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Contacto", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Contado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Contado", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Credito", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Credito", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Descuento", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Descuento", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Dias", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Dias", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Imp_Venta", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Imp_Venta", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_MonedaNombre", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MonedaNombre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Nombre_Cliente", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre_Cliente", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Observaciones", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Observaciones", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_PagoImpuesto", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PagoImpuesto", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_SubTotal", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubTotal", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_SubTotalExento", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubTotalExento", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_SubTotalGravada", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubTotalGravada", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_TiempoEntrega", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TiempoEntrega", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Tipo_Cambio", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Tipo_Cambio", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Total", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Total", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Transporte", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Transporte", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Validez", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Validez", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Venta", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Venta", System.Data.DataRowVersion.Original, Nothing)})
		'
		'SqlInsertCommand1
		'
		Me.SqlInsertCommand1.CommandText = resources.GetString("SqlInsertCommand1.CommandText")
		Me.SqlInsertCommand1.Connection = Me.SqlConnection1
		Me.SqlInsertCommand1.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"), New System.Data.SqlClient.SqlParameter("@Cod_Cliente", System.Data.SqlDbType.Int, 4, "Cod_Cliente"), New System.Data.SqlClient.SqlParameter("@Nombre_Cliente", System.Data.SqlDbType.VarChar, 250, "Nombre_Cliente"), New System.Data.SqlClient.SqlParameter("@Contacto", System.Data.SqlDbType.VarChar, 250, "Contacto"), New System.Data.SqlClient.SqlParameter("@Validez", System.Data.SqlDbType.Int, 4, "Validez"), New System.Data.SqlClient.SqlParameter("@TiempoEntrega", System.Data.SqlDbType.Int, 4, "TiempoEntrega"), New System.Data.SqlClient.SqlParameter("@Contado", System.Data.SqlDbType.Bit, 1, "Contado"), New System.Data.SqlClient.SqlParameter("@Credito", System.Data.SqlDbType.Bit, 1, "Credito"), New System.Data.SqlClient.SqlParameter("@Anulado", System.Data.SqlDbType.Bit, 1, "Anulado"), New System.Data.SqlClient.SqlParameter("@Dias", System.Data.SqlDbType.Int, 4, "Dias"), New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.VarChar, 250, "Observaciones"), New System.Data.SqlClient.SqlParameter("@SubTotalGravada", System.Data.SqlDbType.Float, 8, "SubTotalGravada"), New System.Data.SqlClient.SqlParameter("@SubTotalExento", System.Data.SqlDbType.Float, 8, "SubTotalExento"), New System.Data.SqlClient.SqlParameter("@Descuento", System.Data.SqlDbType.Float, 8, "Descuento"), New System.Data.SqlClient.SqlParameter("@PagoImpuesto", System.Data.SqlDbType.Float, 8, "PagoImpuesto"), New System.Data.SqlClient.SqlParameter("@Total", System.Data.SqlDbType.Float, 8, "Total"), New System.Data.SqlClient.SqlParameter("@Cedula", System.Data.SqlDbType.VarChar, 75, "Cedula"), New System.Data.SqlClient.SqlParameter("@CodMoneda", System.Data.SqlDbType.Int, 4, "CodMoneda"), New System.Data.SqlClient.SqlParameter("@MonedaNombre", System.Data.SqlDbType.VarChar, 50, "MonedaNombre"), New System.Data.SqlClient.SqlParameter("@SubTotal", System.Data.SqlDbType.Float, 8, "SubTotal"), New System.Data.SqlClient.SqlParameter("@Tipo_Cambio", System.Data.SqlDbType.Float, 8, "Tipo_Cambio"), New System.Data.SqlClient.SqlParameter("@Imp_Venta", System.Data.SqlDbType.Float, 8, "Imp_Venta"), New System.Data.SqlClient.SqlParameter("@Transporte", System.Data.SqlDbType.Float, 8, "Transporte"), New System.Data.SqlClient.SqlParameter("@Venta", System.Data.SqlDbType.Bit, 1, "Venta")})
		'
		'SqlSelectCommand13
		'
		Me.SqlSelectCommand13.CommandText = resources.GetString("SqlSelectCommand13.CommandText")
		Me.SqlSelectCommand13.Connection = Me.SqlConnection1
		'
		'SqlUpdateCommand1
		'
		Me.SqlUpdateCommand1.CommandText = resources.GetString("SqlUpdateCommand1.CommandText")
		Me.SqlUpdateCommand1.Connection = Me.SqlConnection1
		Me.SqlUpdateCommand1.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 8, "Fecha"), New System.Data.SqlClient.SqlParameter("@Cod_Cliente", System.Data.SqlDbType.Int, 4, "Cod_Cliente"), New System.Data.SqlClient.SqlParameter("@Nombre_Cliente", System.Data.SqlDbType.VarChar, 250, "Nombre_Cliente"), New System.Data.SqlClient.SqlParameter("@Contacto", System.Data.SqlDbType.VarChar, 250, "Contacto"), New System.Data.SqlClient.SqlParameter("@Validez", System.Data.SqlDbType.Int, 4, "Validez"), New System.Data.SqlClient.SqlParameter("@TiempoEntrega", System.Data.SqlDbType.Int, 4, "TiempoEntrega"), New System.Data.SqlClient.SqlParameter("@Contado", System.Data.SqlDbType.Bit, 1, "Contado"), New System.Data.SqlClient.SqlParameter("@Credito", System.Data.SqlDbType.Bit, 1, "Credito"), New System.Data.SqlClient.SqlParameter("@Anulado", System.Data.SqlDbType.Bit, 1, "Anulado"), New System.Data.SqlClient.SqlParameter("@Dias", System.Data.SqlDbType.Int, 4, "Dias"), New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.VarChar, 250, "Observaciones"), New System.Data.SqlClient.SqlParameter("@SubTotalGravada", System.Data.SqlDbType.Float, 8, "SubTotalGravada"), New System.Data.SqlClient.SqlParameter("@SubTotalExento", System.Data.SqlDbType.Float, 8, "SubTotalExento"), New System.Data.SqlClient.SqlParameter("@Descuento", System.Data.SqlDbType.Float, 8, "Descuento"), New System.Data.SqlClient.SqlParameter("@PagoImpuesto", System.Data.SqlDbType.Float, 8, "PagoImpuesto"), New System.Data.SqlClient.SqlParameter("@Total", System.Data.SqlDbType.Float, 8, "Total"), New System.Data.SqlClient.SqlParameter("@Cedula", System.Data.SqlDbType.VarChar, 75, "Cedula"), New System.Data.SqlClient.SqlParameter("@CodMoneda", System.Data.SqlDbType.Int, 4, "CodMoneda"), New System.Data.SqlClient.SqlParameter("@MonedaNombre", System.Data.SqlDbType.VarChar, 50, "MonedaNombre"), New System.Data.SqlClient.SqlParameter("@SubTotal", System.Data.SqlDbType.Float, 8, "SubTotal"), New System.Data.SqlClient.SqlParameter("@Tipo_Cambio", System.Data.SqlDbType.Float, 8, "Tipo_Cambio"), New System.Data.SqlClient.SqlParameter("@Imp_Venta", System.Data.SqlDbType.Float, 8, "Imp_Venta"), New System.Data.SqlClient.SqlParameter("@Transporte", System.Data.SqlDbType.Float, 8, "Transporte"), New System.Data.SqlClient.SqlParameter("@Venta", System.Data.SqlDbType.Bit, 1, "Venta"), New System.Data.SqlClient.SqlParameter("@Original_Cotizacion", System.Data.SqlDbType.BigInt, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cotizacion", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Anulado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Anulado", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Cedula", System.Data.SqlDbType.VarChar, 75, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cedula", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_CodMoneda", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "CodMoneda", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Cod_Cliente", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Cod_Cliente", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Contacto", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Contacto", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Contado", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Contado", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Credito", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Credito", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Descuento", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Descuento", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Dias", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Dias", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Fecha", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Fecha", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Imp_Venta", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Imp_Venta", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_MonedaNombre", System.Data.SqlDbType.VarChar, 50, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MonedaNombre", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Nombre_Cliente", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Nombre_Cliente", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Observaciones", System.Data.SqlDbType.VarChar, 250, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Observaciones", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_PagoImpuesto", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PagoImpuesto", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_SubTotal", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubTotal", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_SubTotalExento", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubTotalExento", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_SubTotalGravada", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubTotalGravada", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_TiempoEntrega", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TiempoEntrega", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Tipo_Cambio", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Tipo_Cambio", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Total", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Total", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Transporte", System.Data.SqlDbType.Float, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Transporte", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Validez", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Validez", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Original_Venta", System.Data.SqlDbType.Bit, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Venta", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Cotizacion", System.Data.SqlDbType.BigInt, 8, "Cotizacion")})
		'
		'Adapter_CotiDetalle
		'
		Me.Adapter_CotiDetalle.SelectCommand = Me.SqlSelectCommand14
		Me.Adapter_CotiDetalle.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Cotizacion_Detalle", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Numero", "Numero"), New System.Data.Common.DataColumnMapping("Cotizacion", "Cotizacion"), New System.Data.Common.DataColumnMapping("Codigo", "Codigo"), New System.Data.Common.DataColumnMapping("Descripcion", "Descripcion"), New System.Data.Common.DataColumnMapping("Cantidad", "Cantidad"), New System.Data.Common.DataColumnMapping("Precio_Costo", "Precio_Costo"), New System.Data.Common.DataColumnMapping("Precio_Base", "Precio_Base"), New System.Data.Common.DataColumnMapping("Precio_Flete", "Precio_Flete"), New System.Data.Common.DataColumnMapping("Precio_Otros", "Precio_Otros"), New System.Data.Common.DataColumnMapping("Precio_Unit", "Precio_Unit"), New System.Data.Common.DataColumnMapping("Descuento", "Descuento"), New System.Data.Common.DataColumnMapping("Monto_Descuento", "Monto_Descuento"), New System.Data.Common.DataColumnMapping("Impuesto", "Impuesto"), New System.Data.Common.DataColumnMapping("Monto_Impuesto", "Monto_Impuesto"), New System.Data.Common.DataColumnMapping("SubtotalGravado", "SubtotalGravado"), New System.Data.Common.DataColumnMapping("SubTotalExcento", "SubTotalExcento"), New System.Data.Common.DataColumnMapping("SubTotal", "SubTotal"), New System.Data.Common.DataColumnMapping("SubFamilia", "SubFamilia"), New System.Data.Common.DataColumnMapping("Max_Descuento", "Max_Descuento"), New System.Data.Common.DataColumnMapping("Tipo_Cambio_ValorCompra", "Tipo_Cambio_ValorCompra"), New System.Data.Common.DataColumnMapping("Cod_MonedaVenta", "Cod_MonedaVenta")})})
		'
		'SqlSelectCommand14
		'
		Me.SqlSelectCommand14.CommandText = resources.GetString("SqlSelectCommand14.CommandText")
		Me.SqlSelectCommand14.Connection = Me.SqlConnection1
		'
		'Label48
		'
		Me.Label48.BackColor = System.Drawing.Color.RoyalBlue
		Me.Label48.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label48.ForeColor = System.Drawing.Color.White
		Me.Label48.Location = New System.Drawing.Point(2, 1)
		Me.Label48.Name = "Label48"
		Me.Label48.Size = New System.Drawing.Size(72, 13)
		Me.Label48.TabIndex = 190
		Me.Label48.Text = "Usuario->"
		Me.Label48.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'StatusBar1
		'
		Me.StatusBar1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
			Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.StatusBar1.CausesValidation = False
		Me.StatusBar1.Dock = System.Windows.Forms.DockStyle.None
		Me.StatusBar1.Location = New System.Drawing.Point(0, 575)
		Me.StatusBar1.Name = "StatusBar1"
		Me.StatusBar1.Panels.AddRange(New System.Windows.Forms.StatusBarPanel() {Me.StatusBarPanel1, Me.StatusBarPanel2, Me.StatusBarPanel3, Me.StatusBarPanel4, Me.StatusBarPanel5, Me.StatusBarPanel6, Me.StatusBarPanel7, Me.StatusBarPanel8})
		Me.StatusBar1.ShowPanels = True
		Me.StatusBar1.Size = New System.Drawing.Size(994, 16)
		Me.StatusBar1.TabIndex = 212
		'
		'StatusBarPanel1
		'
		Me.StatusBarPanel1.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Contents
		Me.StatusBarPanel1.BorderStyle = System.Windows.Forms.StatusBarPanelBorderStyle.None
		Me.StatusBarPanel1.Name = "StatusBarPanel1"
		Me.StatusBarPanel1.Text = "Fecha"
		Me.StatusBarPanel1.Width = 46
		'
		'StatusBarPanel2
		'
		Me.StatusBarPanel2.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Contents
		Me.StatusBarPanel2.Name = "StatusBarPanel2"
		Me.StatusBarPanel2.Text = "04/09/2006"
		Me.StatusBarPanel2.Width = 71
		'
		'StatusBarPanel3
		'
		Me.StatusBarPanel3.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Contents
		Me.StatusBarPanel3.BorderStyle = System.Windows.Forms.StatusBarPanelBorderStyle.None
		Me.StatusBarPanel3.Name = "StatusBarPanel3"
		Me.StatusBarPanel3.Text = "Estado"
		Me.StatusBarPanel3.Width = 49
		'
		'StatusBarPanel4
		'
		Me.StatusBarPanel4.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Contents
		Me.StatusBarPanel4.Icon = CType(resources.GetObject("StatusBarPanel4.Icon"), System.Drawing.Icon)
		Me.StatusBarPanel4.Name = "StatusBarPanel4"
		Me.StatusBarPanel4.Text = "Activa"
		Me.StatusBarPanel4.Width = 62
		'
		'StatusBarPanel5
		'
		Me.StatusBarPanel5.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Contents
		Me.StatusBarPanel5.BorderStyle = System.Windows.Forms.StatusBarPanelBorderStyle.None
		Me.StatusBarPanel5.Name = "StatusBarPanel5"
		Me.StatusBarPanel5.Text = "Dias Credito"
		Me.StatusBarPanel5.Width = 76
		'
		'StatusBarPanel6
		'
		Me.StatusBarPanel6.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Contents
		Me.StatusBarPanel6.Name = "StatusBarPanel6"
		Me.StatusBarPanel6.Text = "30"
		Me.StatusBarPanel6.Width = 27
		'
		'StatusBarPanel7
		'
		Me.StatusBarPanel7.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Contents
		Me.StatusBarPanel7.Name = "StatusBarPanel7"
		Me.StatusBarPanel7.Text = "Tipo Cambio"
		Me.StatusBarPanel7.Width = 78
		'
		'StatusBarPanel8
		'
		Me.StatusBarPanel8.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Contents
		Me.StatusBarPanel8.Name = "StatusBarPanel8"
		Me.StatusBarPanel8.Text = "550"
		Me.StatusBarPanel8.Width = 33
		'
		'Panel3
		'
		Me.Panel3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
			Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Panel3.BackColor = System.Drawing.Color.Transparent
		Me.Panel3.Controls.Add(Me.txtDeuda)
		Me.Panel3.Controls.Add(Me.Label5)
		Me.Panel3.Controls.Add(Me.txtNombre)
		Me.Panel3.Controls.Add(Me.txtCodigo)
		Me.Panel3.Location = New System.Drawing.Point(1, 42)
		Me.Panel3.Name = "Panel3"
		Me.Panel3.Size = New System.Drawing.Size(763, 20)
		Me.Panel3.TabIndex = 2
		'
		'txtDeuda
		'
		Me.txtDeuda.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.txtDeuda.EditValue = "0.00"
		Me.txtDeuda.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.txtDeuda.Location = New System.Drawing.Point(658, 1)
		Me.txtDeuda.Name = "txtDeuda"
		'
		'
		'
		Me.txtDeuda.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
		Me.txtDeuda.Properties.ReadOnly = True
		Me.txtDeuda.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
		Me.txtDeuda.Properties.StyleDisabled = New DevExpress.Utils.ViewStyle("ControlStyleDisabled", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.0!), "", CType(((DevExpress.Utils.StyleOptions.StyleEnabled Or DevExpress.Utils.StyleOptions.UseBackColor) _
				Or DevExpress.Utils.StyleOptions.UseForeColor), DevExpress.Utils.StyleOptions), True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.Color.RoyalBlue)
		Me.txtDeuda.RightToLeft = System.Windows.Forms.RightToLeft.Yes
		Me.txtDeuda.Size = New System.Drawing.Size(104, 19)
		Me.txtDeuda.TabIndex = 29
		'
		'Label5
		'
		Me.Label5.BackColor = System.Drawing.Color.Transparent
		Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
		Me.Label5.ForeColor = System.Drawing.Color.RoyalBlue
		Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.Label5.Location = New System.Drawing.Point(8, 0)
		Me.Label5.Name = "Label5"
		Me.Label5.Size = New System.Drawing.Size(48, 14)
		Me.Label5.TabIndex = 2
		Me.Label5.Text = "Cliente"
		Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		'
		'txtNombre
		'
		Me.txtNombre.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
			Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.txtNombre.BackColor = System.Drawing.Color.White
		Me.txtNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
		Me.txtNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtNombre.ForeColor = System.Drawing.Color.Blue
		Me.txtNombre.Location = New System.Drawing.Point(121, 0)
		Me.txtNombre.Name = "txtNombre"
		Me.txtNombre.Size = New System.Drawing.Size(537, 20)
		Me.txtNombre.TabIndex = 1
		'
		'Panel1
		'
		Me.Panel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(165, Byte), Integer))
		Me.Panel1.Controls.Add(Me.opCredito)
		Me.Panel1.Controls.Add(Me.opContado)
		Me.Panel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.209303!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Panel1.ForeColor = System.Drawing.Color.White
		Me.Panel1.Location = New System.Drawing.Point(821, 2)
		Me.Panel1.Name = "Panel1"
		Me.Panel1.Size = New System.Drawing.Size(173, 22)
		Me.Panel1.TabIndex = 216
		'
		'Panel4
		'
		Me.Panel4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Panel4.Controls.Add(Me.txtNombreUsuario)
		Me.Panel4.Controls.Add(Me.txtUsuario)
		Me.Panel4.Controls.Add(Me.Label48)
		Me.Panel4.Location = New System.Drawing.Point(693, 634)
		Me.Panel4.Name = "Panel4"
		Me.Panel4.Size = New System.Drawing.Size(296, 16)
		Me.Panel4.TabIndex = 218
		'
		'PictureEdit1
		'
		Me.PictureEdit1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.PictureEdit1.Location = New System.Drawing.Point(766, 26)
		Me.PictureEdit1.Name = "PictureEdit1"
		'
		'
		'
		Me.PictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
		Me.PictureEdit1.Size = New System.Drawing.Size(216, 166)
		Me.PictureEdit1.TabIndex = 219
		'
		'CK_PVE
		'
		Me.CK_PVE.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.CK_PVE.BackColor = System.Drawing.Color.FromArgb(CType(CType(57, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(165, Byte), Integer))
		Me.CK_PVE.ForeColor = System.Drawing.Color.White
		Me.CK_PVE.Location = New System.Drawing.Point(752, 5)
		Me.CK_PVE.Name = "CK_PVE"
		Me.CK_PVE.Size = New System.Drawing.Size(47, 16)
		Me.CK_PVE.TabIndex = 222
		Me.CK_PVE.Text = "PVE"
		Me.CK_PVE.UseVisualStyleBackColor = False
		'
		'GroupBox3
		'
		Me.GroupBox3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.GroupBox3.BackColor = System.Drawing.Color.White
		Me.GroupBox3.Controls.Add(Me.Label20)
		Me.GroupBox3.Controls.Add(Me.Label18)
		Me.GroupBox3.Controls.Add(Me.TxtBarras)
		Me.GroupBox3.Controls.Add(Me.txtIVA)
		Me.GroupBox3.Controls.Add(Me.txtTotalArt)
		Me.GroupBox3.Controls.Add(Me.txtExistencia)
		Me.GroupBox3.Controls.Add(Me.Label6)
		Me.GroupBox3.Controls.Add(Me.Label4)
		Me.GroupBox3.Controls.Add(Me.Label17)
		Me.GroupBox3.Controls.Add(Me.txtPrecioUnit)
		Me.GroupBox3.Controls.Add(Me.Label16)
		Me.GroupBox3.Controls.Add(Me.TxtCodArticulo)
		Me.GroupBox3.Controls.Add(Me.Label31)
		Me.GroupBox3.Controls.Add(Me.txtSubtotal)
		Me.GroupBox3.Controls.Add(Me.Label32)
		Me.GroupBox3.Controls.Add(Me.Label21)
		Me.GroupBox3.Controls.Add(Me.txtCantidad)
		Me.GroupBox3.Controls.Add(Me.Label15)
		Me.GroupBox3.Controls.Add(Me.Label13)
		Me.GroupBox3.Controls.Add(Me.txtDescuento)
		Me.GroupBox3.Location = New System.Drawing.Point(6, 513)
		Me.GroupBox3.Name = "GroupBox3"
		Me.GroupBox3.Size = New System.Drawing.Size(725, 40)
		Me.GroupBox3.TabIndex = 223
		'
		'Label20
		'
		Me.Label20.BackColor = System.Drawing.Color.FromArgb(CType(CType(61, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(139, Byte), Integer))
		Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
		Me.Label20.ForeColor = System.Drawing.Color.White
		Me.Label20.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.Label20.Location = New System.Drawing.Point(631, -1)
		Me.Label20.Name = "Label20"
		Me.Label20.Size = New System.Drawing.Size(74, 14)
		Me.Label20.TabIndex = 232
		Me.Label20.Text = "Barras"
		Me.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.Label20.Visible = False
		'
		'Label18
		'
		Me.Label18.BackColor = System.Drawing.Color.FromArgb(CType(CType(61, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(139, Byte), Integer))
		Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
		Me.Label18.ForeColor = System.Drawing.Color.White
		Me.Label18.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.Label18.Location = New System.Drawing.Point(254, 0)
		Me.Label18.Name = "Label18"
		Me.Label18.Size = New System.Drawing.Size(64, 14)
		Me.Label18.TabIndex = 33
		Me.Label18.Text = "I.V.A (%)"
		Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'TxtBarras
		'
		Me.TxtBarras.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
			Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.TxtBarras.EditValue = ""
		Me.TxtBarras.Location = New System.Drawing.Point(634, 16)
		Me.TxtBarras.Name = "TxtBarras"
		'
		'
		'
		Me.TxtBarras.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
		Me.TxtBarras.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
		Me.TxtBarras.Properties.MaxLength = 19
		Me.TxtBarras.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
		Me.TxtBarras.Size = New System.Drawing.Size(71, 21)
		Me.TxtBarras.TabIndex = 195
		Me.TxtBarras.Visible = False
		'
		'txtIVA
		'
		Me.txtIVA.EditValue = 0
		Me.txtIVA.Location = New System.Drawing.Point(260, 15)
		Me.txtIVA.Name = "txtIVA"
		'
		'
		'
		Me.txtIVA.Properties.DisplayFormat.FormatString = "#,#0.00"
		Me.txtIVA.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
		Me.txtIVA.Properties.EditFormat.FormatString = "#,#0.00"
		Me.txtIVA.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
		Me.txtIVA.Properties.ReadOnly = True
		Me.txtIVA.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.Blue)
		Me.txtIVA.RightToLeft = System.Windows.Forms.RightToLeft.Yes
		Me.txtIVA.Size = New System.Drawing.Size(56, 19)
		Me.txtIVA.TabIndex = 32
		'
		'txtTotalArt
		'
		Me.txtTotalArt.EditValue = 0
		Me.txtTotalArt.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.txtTotalArt.Location = New System.Drawing.Point(588, 16)
		Me.txtTotalArt.Name = "txtTotalArt"
		'
		'
		'
		Me.txtTotalArt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
		Me.txtTotalArt.Properties.Enabled = False
		Me.txtTotalArt.Properties.ReadOnly = True
		Me.txtTotalArt.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
		Me.txtTotalArt.RightToLeft = System.Windows.Forms.RightToLeft.Yes
		Me.txtTotalArt.Size = New System.Drawing.Size(40, 19)
		Me.txtTotalArt.TabIndex = 31
		'
		'txtExistencia
		'
		Me.txtExistencia.EditValue = 0
		Me.txtExistencia.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.txtExistencia.Location = New System.Drawing.Point(540, 16)
		Me.txtExistencia.Name = "txtExistencia"
		'
		'
		'
		Me.txtExistencia.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
		Me.txtExistencia.Properties.Enabled = False
		Me.txtExistencia.Properties.ReadOnly = True
		Me.txtExistencia.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Window, System.Drawing.Color.RoyalBlue)
		Me.txtExistencia.RightToLeft = System.Windows.Forms.RightToLeft.Yes
		Me.txtExistencia.Size = New System.Drawing.Size(40, 19)
		Me.txtExistencia.TabIndex = 30
		'
		'Label6
		'
		Me.Label6.BackColor = System.Drawing.Color.FromArgb(CType(CType(61, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(139, Byte), Integer))
		Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
		Me.Label6.ForeColor = System.Drawing.Color.White
		Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.Label6.Location = New System.Drawing.Point(540, 0)
		Me.Label6.Name = "Label6"
		Me.Label6.Size = New System.Drawing.Size(40, 14)
		Me.Label6.TabIndex = 29
		Me.Label6.Text = "Exist. Art"
		Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'Label4
		'
		Me.Label4.BackColor = System.Drawing.Color.FromArgb(CType(CType(61, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(139, Byte), Integer))
		Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
		Me.Label4.ForeColor = System.Drawing.Color.White
		Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.Label4.Location = New System.Drawing.Point(588, 0)
		Me.Label4.Name = "Label4"
		Me.Label4.Size = New System.Drawing.Size(40, 14)
		Me.Label4.TabIndex = 27
		Me.Label4.Text = "Lineas"
		Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'Panel6
		'
		Me.Panel6.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Panel6.Controls.Add(Me.Label11)
		Me.Panel6.Controls.Add(Me.txtObservacion)
		Me.Panel6.Controls.Add(Me.SimpleButton4)
		Me.Panel6.Controls.Add(Me.CB_Exonerar)
		Me.Panel6.Controls.Add(Me.Label10)
		Me.Panel6.Controls.Add(Me.CB_Moneda)
		Me.Panel6.Controls.Add(Me.SimpleButton2)
		Me.Panel6.Controls.Add(Me.txtImpVentaT)
		Me.Panel6.Controls.Add(Me.txtDescuentoT)
		Me.Panel6.Controls.Add(Me.txtSubtotalT)
		Me.Panel6.Controls.Add(Me.Label24)
		Me.Panel6.Controls.Add(Me.Label23)
		Me.Panel6.Controls.Add(Me.Label22)
		Me.Panel6.Controls.Add(Me.Label25)
		Me.Panel6.Controls.Add(Me.TxtTotal)
		Me.Panel6.Location = New System.Drawing.Point(768, 264)
		Me.Panel6.Name = "Panel6"
		Me.Panel6.Size = New System.Drawing.Size(224, 289)
		Me.Panel6.TabIndex = 225
		'
		'Label11
		'
		Me.Label11.BackColor = System.Drawing.SystemColors.ActiveCaptionText
		Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label11.ForeColor = System.Drawing.Color.RoyalBlue
		Me.Label11.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.Label11.Location = New System.Drawing.Point(3, 200)
		Me.Label11.Name = "Label11"
		Me.Label11.Size = New System.Drawing.Size(92, 15)
		Me.Label11.TabIndex = 234
		Me.Label11.Text = "Observacion"
		Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		'
		'txtObservacion
		'
		Me.txtObservacion.ForeColor = System.Drawing.Color.RoyalBlue
		Me.txtObservacion.Location = New System.Drawing.Point(3, 219)
		Me.txtObservacion.Multiline = True
		Me.txtObservacion.Name = "txtObservacion"
		Me.txtObservacion.Size = New System.Drawing.Size(218, 66)
		Me.txtObservacion.TabIndex = 233
		'
		'SimpleButton4
		'
		Me.SimpleButton4.Location = New System.Drawing.Point(191, 6)
		Me.SimpleButton4.Name = "SimpleButton4"
		Me.SimpleButton4.Size = New System.Drawing.Size(28, 20)
		Me.SimpleButton4.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.Color.RoyalBlue)
		Me.SimpleButton4.TabIndex = 231
		Me.SimpleButton4.Text = "..."
		Me.SimpleButton4.ToolTip = "Cambio de la denominaci�n de la moneda"
		'
		'CB_Exonerar
		'
		Me.CB_Exonerar.AutoSize = True
		Me.CB_Exonerar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.CB_Exonerar.ForeColor = System.Drawing.Color.RoyalBlue
		Me.CB_Exonerar.Location = New System.Drawing.Point(5, 33)
		Me.CB_Exonerar.Name = "CB_Exonerar"
		Me.CB_Exonerar.Size = New System.Drawing.Size(76, 17)
		Me.CB_Exonerar.TabIndex = 231
		Me.CB_Exonerar.Text = "Exonerar"
		Me.CB_Exonerar.UseVisualStyleBackColor = True
		'
		'Label10
		'
		Me.Label10.Anchor = System.Windows.Forms.AnchorStyles.Right
		Me.Label10.BackColor = System.Drawing.Color.Transparent
		Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
		Me.Label10.ForeColor = System.Drawing.Color.RoyalBlue
		Me.Label10.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.Label10.Location = New System.Drawing.Point(2, 8)
		Me.Label10.Name = "Label10"
		Me.Label10.Size = New System.Drawing.Size(63, 14)
		Me.Label10.TabIndex = 231
		Me.Label10.Text = "Moneda:"
		Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'CB_Moneda
		'
		Me.CB_Moneda.DataSource = Me.DataSet_Facturaciones
		Me.CB_Moneda.DisplayMember = "Moneda.MonedaNombre"
		Me.CB_Moneda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.CB_Moneda.Enabled = False
		Me.CB_Moneda.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
		Me.CB_Moneda.ForeColor = System.Drawing.Color.Blue
		Me.CB_Moneda.FormattingEnabled = True
		Me.CB_Moneda.Location = New System.Drawing.Point(72, 6)
		Me.CB_Moneda.Name = "CB_Moneda"
		Me.CB_Moneda.Size = New System.Drawing.Size(115, 21)
		Me.CB_Moneda.TabIndex = 232
		Me.CB_Moneda.ValueMember = "Moneda.CodMoneda"
		'
		'Label2
		'
		Me.Label2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
			Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Label2.BackColor = System.Drawing.Color.FromArgb(CType(CType(61, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(139, Byte), Integer))
		Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label2.ForeColor = System.Drawing.Color.White
		Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.Label2.Location = New System.Drawing.Point(-2, 556)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(996, 14)
		Me.Label2.TabIndex = 226
		Me.Label2.Text = "F1:Buscar|F2: Registrar|F3: Buscar Fact|F4:Apartados|F5:Camb. Usuario|F6:Modifica" &
	"r|F7:Recomendado|F8:Devoluciones|F9:Cliente|F10:Tipo Fact.|F11:Abono|F12:Mov. Ca" &
	"ja|Supr: Eliminar|Insert: Agregar"
		Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		'
		'Adapter_Apertura
		'
		Me.Adapter_Apertura.SelectCommand = Me.SqlSelectCommand5
		Me.Adapter_Apertura.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "aperturacaja", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("NApertura", "NApertura")})})
		'
		'SqlSelectCommand5
		'
		Me.SqlSelectCommand5.CommandText = "SELECT NApertura FROM aperturacaja"
		Me.SqlSelectCommand5.Connection = Me.SqlConnection1
		'
		'Panel7
		'
		Me.Panel7.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
			Or System.Windows.Forms.AnchorStyles.Left) _
			Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Panel7.Controls.Add(Me.GridControl1)
		Me.Panel7.Controls.Add(Me.Txtbodega)
		Me.Panel7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Panel7.Location = New System.Drawing.Point(8, 88)
		Me.Panel7.Name = "Panel7"
		Me.Panel7.Size = New System.Drawing.Size(744, 391)
		Me.Panel7.TabIndex = 229
		'
		'GridControl1
		'
		Me.GridControl1.DataMember = "Ventas.VentasVentas_Detalle"
		Me.GridControl1.DataSource = Me.DataSet_Facturaciones
		Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
		'
		'
		'
		Me.GridControl1.EmbeddedNavigator.Name = ""
		Me.GridControl1.EmbeddedNavigator.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.Center, DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.Control, System.Drawing.SystemColors.ControlText)
		Me.GridControl1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.GridControl1.Location = New System.Drawing.Point(0, 0)
		Me.GridControl1.MainView = Me.GridView1
		Me.GridControl1.Name = "GridControl1"
		Me.GridControl1.Size = New System.Drawing.Size(742, 389)
		Me.GridControl1.TabIndex = 183
		Me.GridControl1.Text = "GridControl1"
		'
		'GridView1
		'
		Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colCodigo, Me.colDescripcion, Me.colCantidad, Me.colPrecio_Unit, Me.colMonto_Descuento, Me.colMonto_Impuesto, Me.colSubtotalGravado, Me.colSubTotalExcento, Me.colSubTotal, Me.ColFicticia, Me.colBarra, Me.colIVA})
		Me.GridView1.GroupPanelText = "Detalle de Cotizaci�n"
		Me.GridView1.GroupSummary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SubtotalGravado", Nothing, "")})
		Me.GridView1.Name = "GridView1"
		Me.GridView1.OptionsNavigation.AutoFocusNewRow = True
		Me.GridView1.OptionsView.ShowGroupPanel = False
		Me.GridView1.OptionsView.ShowVertLines = False
		Me.GridView1.ViewCaption = "Lista de Art�culos Facturados"
		'
		'colCodigo
		'
		Me.colCodigo.Caption = "Codigo"
		Me.colCodigo.FieldName = "Codigo"
		Me.colCodigo.FilterInfo = ColumnFilterInfo1
		Me.colCodigo.Name = "colCodigo"
		Me.colCodigo.Options = CType(((DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
			Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
		Me.colCodigo.VisibleIndex = 1
		Me.colCodigo.Width = 69
		'
		'colDescripcion
		'
		Me.colDescripcion.Caption = "Descripcion"
		Me.colDescripcion.FieldName = "Descripcion"
		Me.colDescripcion.FilterInfo = ColumnFilterInfo2
		Me.colDescripcion.Name = "colDescripcion"
		Me.colDescripcion.Options = CType((DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm), DevExpress.XtraGrid.Columns.ColumnOptions)
		Me.colDescripcion.VisibleIndex = 3
		Me.colDescripcion.Width = 180
		'
		'colCantidad
		'
		Me.colCantidad.Caption = "Cant."
		Me.colCantidad.DisplayFormat.FormatString = "#,#0.000"
		Me.colCantidad.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
		Me.colCantidad.FieldName = "Cantidad"
		Me.colCantidad.FilterInfo = ColumnFilterInfo3
		Me.colCantidad.Name = "colCantidad"
		Me.colCantidad.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly] Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
			Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
			Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
		Me.colCantidad.VisibleIndex = 0
		Me.colCantidad.Width = 55
		'
		'colPrecio_Unit
		'
		Me.colPrecio_Unit.Caption = "P.Unit"
		Me.colPrecio_Unit.DisplayFormat.FormatString = "#,#0.00"
		Me.colPrecio_Unit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
		Me.colPrecio_Unit.FieldName = "Precio_Unit"
		Me.colPrecio_Unit.FilterInfo = ColumnFilterInfo4
		Me.colPrecio_Unit.Name = "colPrecio_Unit"
		Me.colPrecio_Unit.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly] Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
			Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
			Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
		Me.colPrecio_Unit.VisibleIndex = 5
		Me.colPrecio_Unit.Width = 78
		'
		'colMonto_Descuento
		'
		Me.colMonto_Descuento.Caption = "M.Desc."
		Me.colMonto_Descuento.DisplayFormat.FormatString = "#,#0.00"
		Me.colMonto_Descuento.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
		Me.colMonto_Descuento.FieldName = "Monto_Descuento"
		Me.colMonto_Descuento.FilterInfo = ColumnFilterInfo5
		Me.colMonto_Descuento.Name = "colMonto_Descuento"
		Me.colMonto_Descuento.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly] Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
			Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
			Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
		Me.colMonto_Descuento.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
		Me.colMonto_Descuento.VisibleIndex = 4
		Me.colMonto_Descuento.Width = 53
		'
		'colMonto_Impuesto
		'
		Me.colMonto_Impuesto.Caption = "M. Imp."
		Me.colMonto_Impuesto.DisplayFormat.FormatString = "#,#0.00"
		Me.colMonto_Impuesto.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
		Me.colMonto_Impuesto.FieldName = "Monto_Impuesto"
		Me.colMonto_Impuesto.FilterInfo = ColumnFilterInfo6
		Me.colMonto_Impuesto.Name = "colMonto_Impuesto"
		Me.colMonto_Impuesto.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly] Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
			Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
			Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
		Me.colMonto_Impuesto.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
		Me.colMonto_Impuesto.Width = 53
		'
		'colSubtotalGravado
		'
		Me.colSubtotalGravado.Caption = "S. Grav."
		Me.colSubtotalGravado.DisplayFormat.FormatString = "#,#0.00"
		Me.colSubtotalGravado.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
		Me.colSubtotalGravado.FieldName = "SubtotalGravado"
		Me.colSubtotalGravado.FilterInfo = ColumnFilterInfo7
		Me.colSubtotalGravado.Name = "colSubtotalGravado"
		Me.colSubtotalGravado.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly] Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
			Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
			Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
		Me.colSubtotalGravado.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
		Me.colSubtotalGravado.Width = 67
		'
		'colSubTotalExcento
		'
		Me.colSubTotalExcento.Caption = "S.Exc."
		Me.colSubTotalExcento.DisplayFormat.FormatString = "#,#0.00"
		Me.colSubTotalExcento.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
		Me.colSubTotalExcento.FieldName = "SubTotalExcento"
		Me.colSubTotalExcento.FilterInfo = ColumnFilterInfo8
		Me.colSubTotalExcento.MinWidth = 10
		Me.colSubTotalExcento.Name = "colSubTotalExcento"
		Me.colSubTotalExcento.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly] Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
			Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
			Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
		Me.colSubTotalExcento.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
		Me.colSubTotalExcento.Width = 67
		'
		'colSubTotal
		'
		Me.colSubTotal.Caption = "SubTotal"
		Me.colSubTotal.DisplayFormat.FormatString = "#,#0.00"
		Me.colSubTotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
		Me.colSubTotal.FieldName = "SubTotal"
		Me.colSubTotal.FilterInfo = ColumnFilterInfo9
		Me.colSubTotal.Name = "colSubTotal"
		Me.colSubTotal.Options = CType((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanResized Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted) _
			Or DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly]) _
			Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
			Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
			Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
		Me.colSubTotal.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
		Me.colSubTotal.Width = 81
		'
		'ColFicticia
		'
		Me.ColFicticia.Caption = "Total"
		Me.ColFicticia.DisplayFormat.FormatString = "#,#0.00"
		Me.ColFicticia.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
		Me.ColFicticia.FieldName = "Total_Ficticio"
		Me.ColFicticia.FilterInfo = ColumnFilterInfo10
		Me.ColFicticia.Name = "ColFicticia"
		Me.ColFicticia.Options = CType((((DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly] Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
			Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
			Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
		Me.ColFicticia.VisibleIndex = 7
		Me.ColFicticia.Width = 108
		'
		'colBarra
		'
		Me.colBarra.Caption = "Barras"
		Me.colBarra.FieldName = "Barras"
		Me.colBarra.FilterInfo = ColumnFilterInfo11
		Me.colBarra.Name = "colBarra"
		Me.colBarra.Options = CType(((DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
			Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
		Me.colBarra.VisibleIndex = 2
		'
		'colIVA
		'
		Me.colIVA.Caption = "IVA (%)"
		Me.colIVA.FieldName = "Impuesto"
		Me.colIVA.FilterInfo = ColumnFilterInfo12
		Me.colIVA.Name = "colIVA"
		Me.colIVA.Options = CType(((((DevExpress.XtraGrid.Columns.ColumnOptions.CanResized Or DevExpress.XtraGrid.Columns.ColumnOptions.[ReadOnly]) _
			Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
			Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
			Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)
		Me.colIVA.VisibleIndex = 6
		'
		'Txtbodega
		'
		Me.Txtbodega.EditValue = ""
		Me.Txtbodega.Location = New System.Drawing.Point(95, 34)
		Me.Txtbodega.Name = "Txtbodega"
		Me.Txtbodega.Size = New System.Drawing.Size(31, 19)
		Me.Txtbodega.TabIndex = 184
		'
		'txtNombreArt
		'
		Me.txtNombreArt.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
			Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.txtNombreArt.BackColor = System.Drawing.Color.FromArgb(CType(CType(61, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(139, Byte), Integer))
		Me.txtNombreArt.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.txtNombreArt.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.90698!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtNombreArt.ForeColor = System.Drawing.Color.White
		Me.txtNombreArt.Location = New System.Drawing.Point(10, 485)
		Me.txtNombreArt.Name = "txtNombreArt"
		Me.txtNombreArt.Size = New System.Drawing.Size(754, 25)
		Me.txtNombreArt.TabIndex = 3
		'
		'Label8
		'
		Me.Label8.Anchor = System.Windows.Forms.AnchorStyles.Right
		Me.Label8.BackColor = System.Drawing.Color.Transparent
		Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
		Me.Label8.ForeColor = System.Drawing.Color.RoyalBlue
		Me.Label8.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.Label8.Location = New System.Drawing.Point(767, 216)
		Me.Label8.Name = "Label8"
		Me.Label8.Size = New System.Drawing.Size(96, 14)
		Me.Label8.TabIndex = 230
		Me.Label8.Text = "Recomendado:"
		Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		'
		'txtComisionista
		'
		Me.txtComisionista.Anchor = System.Windows.Forms.AnchorStyles.Right
		Me.txtComisionista.FieldReference = Nothing
		Me.txtComisionista.ForeColor = System.Drawing.Color.RoyalBlue
		Me.txtComisionista.Location = New System.Drawing.Point(869, 214)
		Me.txtComisionista.MaskEdit = ""
		Me.txtComisionista.Name = "txtComisionista"
		Me.txtComisionista.RegExPattern = ValidText.ValidText.RegularExpressionModes.Custom
		Me.txtComisionista.Required = False
		Me.txtComisionista.ShowErrorIcon = True
		Me.txtComisionista.Size = New System.Drawing.Size(114, 20)
		Me.txtComisionista.TabIndex = 30
		Me.txtComisionista.ValidationMode = ValidText.ValidText.ValidationModes.None
		Me.txtComisionista.ValidText = ""
		'
		'txtNomComisionista
		'
		Me.txtNomComisionista.Anchor = System.Windows.Forms.AnchorStyles.Right
		Me.txtNomComisionista.BackColor = System.Drawing.Color.White
		Me.txtNomComisionista.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
		Me.txtNomComisionista.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtNomComisionista.ForeColor = System.Drawing.Color.Blue
		Me.txtNomComisionista.Location = New System.Drawing.Point(785, 240)
		Me.txtNomComisionista.Name = "txtNomComisionista"
		Me.txtNomComisionista.Size = New System.Drawing.Size(201, 20)
		Me.txtNomComisionista.TabIndex = 30
		'
		'SqlSelectCommand6
		'
		Me.SqlSelectCommand6.CommandText = "SELECT     Ventas.*" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "FROM         Ventas"
		Me.SqlSelectCommand6.Connection = Me.SqlConnection1
		'
		'SqlInsertCommand4
		'
		Me.SqlInsertCommand4.CommandText = resources.GetString("SqlInsertCommand4.CommandText")
		Me.SqlInsertCommand4.Connection = Me.SqlConnection1
		Me.SqlInsertCommand4.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Num_Factura", System.Data.SqlDbType.Float, 0, "Num_Factura"), New System.Data.SqlClient.SqlParameter("@Tipo", System.Data.SqlDbType.VarChar, 0, "Tipo"), New System.Data.SqlClient.SqlParameter("@Cod_Cliente", System.Data.SqlDbType.Int, 0, "Cod_Cliente"), New System.Data.SqlClient.SqlParameter("@Nombre_Cliente", System.Data.SqlDbType.VarChar, 0, "Nombre_Cliente"), New System.Data.SqlClient.SqlParameter("@Orden", System.Data.SqlDbType.BigInt, 0, "Orden"), New System.Data.SqlClient.SqlParameter("@Cedula_Usuario", System.Data.SqlDbType.VarChar, 0, "Cedula_Usuario"), New System.Data.SqlClient.SqlParameter("@Nombre_Usuario", System.Data.SqlDbType.NVarChar, 0, "Nombre_Usuario"), New System.Data.SqlClient.SqlParameter("@Pago_Comision", System.Data.SqlDbType.Bit, 0, "Pago_Comision"), New System.Data.SqlClient.SqlParameter("@SubTotal", System.Data.SqlDbType.Float, 0, "SubTotal"), New System.Data.SqlClient.SqlParameter("@Descuento", System.Data.SqlDbType.Float, 0, "Descuento"), New System.Data.SqlClient.SqlParameter("@Imp_Venta", System.Data.SqlDbType.Float, 0, "Imp_Venta"), New System.Data.SqlClient.SqlParameter("@Total", System.Data.SqlDbType.Float, 0, "Total"), New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 0, "Fecha"), New System.Data.SqlClient.SqlParameter("@Vence", System.Data.SqlDbType.SmallDateTime, 0, "Vence"), New System.Data.SqlClient.SqlParameter("@Cod_Encargado_Compra", System.Data.SqlDbType.VarChar, 0, "Cod_Encargado_Compra"), New System.Data.SqlClient.SqlParameter("@Contabilizado", System.Data.SqlDbType.Bit, 0, "Contabilizado"), New System.Data.SqlClient.SqlParameter("@AsientoVenta", System.Data.SqlDbType.BigInt, 0, "AsientoVenta"), New System.Data.SqlClient.SqlParameter("@ContabilizadoCVenta", System.Data.SqlDbType.Bit, 0, "ContabilizadoCVenta"), New System.Data.SqlClient.SqlParameter("@AsientoCosto", System.Data.SqlDbType.BigInt, 0, "AsientoCosto"), New System.Data.SqlClient.SqlParameter("@Anulado", System.Data.SqlDbType.Bit, 0, "Anulado"), New System.Data.SqlClient.SqlParameter("@PagoImpuesto", System.Data.SqlDbType.Int, 0, "PagoImpuesto"), New System.Data.SqlClient.SqlParameter("@FacturaCancelado", System.Data.SqlDbType.Bit, 0, "FacturaCancelado"), New System.Data.SqlClient.SqlParameter("@Num_Apertura", System.Data.SqlDbType.Int, 0, "Num_Apertura"), New System.Data.SqlClient.SqlParameter("@Entregado", System.Data.SqlDbType.Bit, 0, "Entregado"), New System.Data.SqlClient.SqlParameter("@Cod_Moneda", System.Data.SqlDbType.Int, 0, "Cod_Moneda"), New System.Data.SqlClient.SqlParameter("@Moneda_Nombre", System.Data.SqlDbType.VarChar, 0, "Moneda_Nombre"), New System.Data.SqlClient.SqlParameter("@Direccion", System.Data.SqlDbType.VarChar, 0, "Direccion"), New System.Data.SqlClient.SqlParameter("@Telefono", System.Data.SqlDbType.VarChar, 0, "Telefono"), New System.Data.SqlClient.SqlParameter("@SubTotalGravada", System.Data.SqlDbType.Float, 0, "SubTotalGravada"), New System.Data.SqlClient.SqlParameter("@SubTotalExento", System.Data.SqlDbType.Float, 0, "SubTotalExento"), New System.Data.SqlClient.SqlParameter("@Transporte", System.Data.SqlDbType.Float, 0, "Transporte"), New System.Data.SqlClient.SqlParameter("@Tipo_Cambio", System.Data.SqlDbType.Float, 0, "Tipo_Cambio"), New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.VarChar, 0, "Observaciones"), New System.Data.SqlClient.SqlParameter("@Exonerar", System.Data.SqlDbType.Bit, 0, "Exonerar"), New System.Data.SqlClient.SqlParameter("@CodComisionista", System.Data.SqlDbType.BigInt, 0, "CodComisionista"), New System.Data.SqlClient.SqlParameter("@NombreComisionista", System.Data.SqlDbType.VarChar, 0, "NombreComisionista"), New System.Data.SqlClient.SqlParameter("@PagoComision", System.Data.SqlDbType.Float, 0, "PagoComision"), New System.Data.SqlClient.SqlParameter("@Apartado", System.Data.SqlDbType.BigInt, 0, "Apartado")})
		'
		'SqlUpdateCommand4
		'
		Me.SqlUpdateCommand4.CommandText = resources.GetString("SqlUpdateCommand4.CommandText")
		Me.SqlUpdateCommand4.Connection = Me.SqlConnection1
		Me.SqlUpdateCommand4.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Num_Factura", System.Data.SqlDbType.Float, 0, "Num_Factura"), New System.Data.SqlClient.SqlParameter("@Tipo", System.Data.SqlDbType.VarChar, 0, "Tipo"), New System.Data.SqlClient.SqlParameter("@Cod_Cliente", System.Data.SqlDbType.Int, 0, "Cod_Cliente"), New System.Data.SqlClient.SqlParameter("@Nombre_Cliente", System.Data.SqlDbType.VarChar, 0, "Nombre_Cliente"), New System.Data.SqlClient.SqlParameter("@Orden", System.Data.SqlDbType.BigInt, 0, "Orden"), New System.Data.SqlClient.SqlParameter("@Cedula_Usuario", System.Data.SqlDbType.VarChar, 0, "Cedula_Usuario"), New System.Data.SqlClient.SqlParameter("@Nombre_Usuario", System.Data.SqlDbType.NVarChar, 0, "Nombre_Usuario"), New System.Data.SqlClient.SqlParameter("@Pago_Comision", System.Data.SqlDbType.Bit, 0, "Pago_Comision"), New System.Data.SqlClient.SqlParameter("@SubTotal", System.Data.SqlDbType.Float, 0, "SubTotal"), New System.Data.SqlClient.SqlParameter("@Descuento", System.Data.SqlDbType.Float, 0, "Descuento"), New System.Data.SqlClient.SqlParameter("@Imp_Venta", System.Data.SqlDbType.Float, 0, "Imp_Venta"), New System.Data.SqlClient.SqlParameter("@Total", System.Data.SqlDbType.Float, 0, "Total"), New System.Data.SqlClient.SqlParameter("@Fecha", System.Data.SqlDbType.DateTime, 0, "Fecha"), New System.Data.SqlClient.SqlParameter("@Vence", System.Data.SqlDbType.SmallDateTime, 0, "Vence"), New System.Data.SqlClient.SqlParameter("@Cod_Encargado_Compra", System.Data.SqlDbType.VarChar, 0, "Cod_Encargado_Compra"), New System.Data.SqlClient.SqlParameter("@Contabilizado", System.Data.SqlDbType.Bit, 0, "Contabilizado"), New System.Data.SqlClient.SqlParameter("@AsientoVenta", System.Data.SqlDbType.BigInt, 0, "AsientoVenta"), New System.Data.SqlClient.SqlParameter("@ContabilizadoCVenta", System.Data.SqlDbType.Bit, 0, "ContabilizadoCVenta"), New System.Data.SqlClient.SqlParameter("@AsientoCosto", System.Data.SqlDbType.BigInt, 0, "AsientoCosto"), New System.Data.SqlClient.SqlParameter("@Anulado", System.Data.SqlDbType.Bit, 0, "Anulado"), New System.Data.SqlClient.SqlParameter("@PagoImpuesto", System.Data.SqlDbType.Int, 0, "PagoImpuesto"), New System.Data.SqlClient.SqlParameter("@FacturaCancelado", System.Data.SqlDbType.Bit, 0, "FacturaCancelado"), New System.Data.SqlClient.SqlParameter("@Num_Apertura", System.Data.SqlDbType.Int, 0, "Num_Apertura"), New System.Data.SqlClient.SqlParameter("@Entregado", System.Data.SqlDbType.Bit, 0, "Entregado"), New System.Data.SqlClient.SqlParameter("@Cod_Moneda", System.Data.SqlDbType.Int, 0, "Cod_Moneda"), New System.Data.SqlClient.SqlParameter("@Moneda_Nombre", System.Data.SqlDbType.VarChar, 0, "Moneda_Nombre"), New System.Data.SqlClient.SqlParameter("@Direccion", System.Data.SqlDbType.VarChar, 0, "Direccion"), New System.Data.SqlClient.SqlParameter("@Telefono", System.Data.SqlDbType.VarChar, 0, "Telefono"), New System.Data.SqlClient.SqlParameter("@SubTotalGravada", System.Data.SqlDbType.Float, 0, "SubTotalGravada"), New System.Data.SqlClient.SqlParameter("@SubTotalExento", System.Data.SqlDbType.Float, 0, "SubTotalExento"), New System.Data.SqlClient.SqlParameter("@Transporte", System.Data.SqlDbType.Float, 0, "Transporte"), New System.Data.SqlClient.SqlParameter("@Tipo_Cambio", System.Data.SqlDbType.Float, 0, "Tipo_Cambio"), New System.Data.SqlClient.SqlParameter("@Observaciones", System.Data.SqlDbType.VarChar, 0, "Observaciones"), New System.Data.SqlClient.SqlParameter("@Exonerar", System.Data.SqlDbType.Bit, 0, "Exonerar"), New System.Data.SqlClient.SqlParameter("@CodComisionista", System.Data.SqlDbType.BigInt, 0, "CodComisionista"), New System.Data.SqlClient.SqlParameter("@NombreComisionista", System.Data.SqlDbType.VarChar, 0, "NombreComisionista"), New System.Data.SqlClient.SqlParameter("@PagoComision", System.Data.SqlDbType.Float, 0, "PagoComision"), New System.Data.SqlClient.SqlParameter("@Apartado", System.Data.SqlDbType.BigInt, 0, "Apartado"), New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.BigInt, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing), New System.Data.SqlClient.SqlParameter("@Id", System.Data.SqlDbType.BigInt, 8, "Id")})
		'
		'SqlDeleteCommand4
		'
		Me.SqlDeleteCommand4.CommandText = "DELETE FROM [Ventas] WHERE (([Id] = @Original_Id))"
		Me.SqlDeleteCommand4.Connection = Me.SqlConnection1
		Me.SqlDeleteCommand4.Parameters.AddRange(New System.Data.SqlClient.SqlParameter() {New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.BigInt, 0, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing)})
		'
		'Adapter_Ventas
		'
		Me.Adapter_Ventas.DeleteCommand = Me.SqlDeleteCommand4
		Me.Adapter_Ventas.InsertCommand = Me.SqlInsertCommand4
		Me.Adapter_Ventas.SelectCommand = Me.SqlSelectCommand6
		Me.Adapter_Ventas.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Ventas", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Id", "Id"), New System.Data.Common.DataColumnMapping("Num_Factura", "Num_Factura"), New System.Data.Common.DataColumnMapping("Tipo", "Tipo"), New System.Data.Common.DataColumnMapping("Cod_Cliente", "Cod_Cliente"), New System.Data.Common.DataColumnMapping("Nombre_Cliente", "Nombre_Cliente"), New System.Data.Common.DataColumnMapping("Orden", "Orden"), New System.Data.Common.DataColumnMapping("Cedula_Usuario", "Cedula_Usuario"), New System.Data.Common.DataColumnMapping("Nombre_Usuario", "Nombre_Usuario"), New System.Data.Common.DataColumnMapping("Pago_Comision", "Pago_Comision"), New System.Data.Common.DataColumnMapping("SubTotal", "SubTotal"), New System.Data.Common.DataColumnMapping("Descuento", "Descuento"), New System.Data.Common.DataColumnMapping("Imp_Venta", "Imp_Venta"), New System.Data.Common.DataColumnMapping("Total", "Total"), New System.Data.Common.DataColumnMapping("Fecha", "Fecha"), New System.Data.Common.DataColumnMapping("Vence", "Vence"), New System.Data.Common.DataColumnMapping("Cod_Encargado_Compra", "Cod_Encargado_Compra"), New System.Data.Common.DataColumnMapping("Contabilizado", "Contabilizado"), New System.Data.Common.DataColumnMapping("AsientoVenta", "AsientoVenta"), New System.Data.Common.DataColumnMapping("ContabilizadoCVenta", "ContabilizadoCVenta"), New System.Data.Common.DataColumnMapping("AsientoCosto", "AsientoCosto"), New System.Data.Common.DataColumnMapping("Anulado", "Anulado"), New System.Data.Common.DataColumnMapping("PagoImpuesto", "PagoImpuesto"), New System.Data.Common.DataColumnMapping("FacturaCancelado", "FacturaCancelado"), New System.Data.Common.DataColumnMapping("Num_Apertura", "Num_Apertura"), New System.Data.Common.DataColumnMapping("Entregado", "Entregado"), New System.Data.Common.DataColumnMapping("Cod_Moneda", "Cod_Moneda"), New System.Data.Common.DataColumnMapping("Moneda_Nombre", "Moneda_Nombre"), New System.Data.Common.DataColumnMapping("Direccion", "Direccion"), New System.Data.Common.DataColumnMapping("Telefono", "Telefono"), New System.Data.Common.DataColumnMapping("SubTotalGravada", "SubTotalGravada"), New System.Data.Common.DataColumnMapping("SubTotalExento", "SubTotalExento"), New System.Data.Common.DataColumnMapping("Transporte", "Transporte"), New System.Data.Common.DataColumnMapping("Tipo_Cambio", "Tipo_Cambio"), New System.Data.Common.DataColumnMapping("Observaciones", "Observaciones"), New System.Data.Common.DataColumnMapping("Exonerar", "Exonerar"), New System.Data.Common.DataColumnMapping("CodComisionista", "CodComisionista"), New System.Data.Common.DataColumnMapping("NombreComisionista", "NombreComisionista"), New System.Data.Common.DataColumnMapping("PagoComision", "PagoComision"), New System.Data.Common.DataColumnMapping("Apartado", "Apartado")})})
		Me.Adapter_Ventas.UpdateCommand = Me.SqlUpdateCommand4
		'
		'AdapterConfig
		'
		Me.AdapterConfig.SelectCommand = Me.SqlCommand1
		Me.AdapterConfig.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "Config", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("PreguntaImprimir", "PreguntaImprimir"), New System.Data.Common.DataColumnMapping("Comisionista", "Comisionista")})})
		'
		'SqlCommand1
		'
		Me.SqlCommand1.CommandText = "SELECT        PreguntaImprimir, Comisionista" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "FROM            Config"
		Me.SqlCommand1.Connection = Me.SqlConnection1
		'
		'bwEnviarSolicitudPeso
		'
		'
		'Label14
		'
		Me.Label14.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Label14.AutoSize = True
		Me.Label14.BackColor = System.Drawing.Color.Transparent
		Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label14.ForeColor = System.Drawing.Color.Blue
		Me.Label14.Location = New System.Drawing.Point(922, 605)
		Me.Label14.Name = "Label14"
		Me.Label14.Size = New System.Drawing.Size(63, 13)
		Me.Label14.TabIndex = 231
		Me.Label14.Text = "Actualizar"
		'
		'TxtPorc_imp
		'
		Me.TxtPorc_imp.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.TxtPorc_imp.EditValue = "0"
		Me.TxtPorc_imp.ImeMode = System.Windows.Forms.ImeMode.NoControl
		Me.TxtPorc_imp.Location = New System.Drawing.Point(714, 528)
		Me.TxtPorc_imp.Name = "TxtPorc_imp"
		'
		'
		'
		Me.TxtPorc_imp.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
		Me.TxtPorc_imp.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
		Me.TxtPorc_imp.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
		Me.TxtPorc_imp.Properties.Enabled = False
		Me.TxtPorc_imp.Properties.ReadOnly = True
		Me.TxtPorc_imp.Properties.Style = New DevExpress.Utils.ViewStyle("ControlStyle", Nothing, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), "", DevExpress.Utils.StyleOptions.StyleEnabled, True, False, False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Utils.VertAlignment.Center, Nothing, System.Drawing.SystemColors.ControlLightLight, System.Drawing.Color.Black)
		Me.TxtPorc_imp.Size = New System.Drawing.Size(49, 17)
		Me.TxtPorc_imp.TabIndex = 232
		'
		'FacturacionPVE
		'
		Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
		Me.AutoSize = True
		Me.BackColor = System.Drawing.Color.White
		Me.ClientSize = New System.Drawing.Size(994, 649)
		Me.Controls.Add(Me.TxtPorc_imp)
		Me.Controls.Add(Me.Label14)
		Me.Controls.Add(Me.txtImpVenta)
		Me.Controls.Add(Me.txtNomComisionista)
		Me.Controls.Add(Me.txtComisionista)
		Me.Controls.Add(Me.Label8)
		Me.Controls.Add(Me.Panel7)
		Me.Controls.Add(Me.txtNombreArt)
		Me.Controls.Add(Me.Label2)
		Me.Controls.Add(Me.Panel6)
		Me.Controls.Add(Me.GroupBox3_oLD)
		Me.Controls.Add(Me.Panel4)
		Me.Controls.Add(Me.CK_PVE)
		Me.Controls.Add(Me.StatusBar1)
		Me.Controls.Add(Me.PictureEdit1)
		Me.Controls.Add(Me.Panel1)
		Me.Controls.Add(Me.Panel3)
		Me.Controls.Add(Me.Panel2)
		Me.Controls.Add(Me.Label9)
		Me.Controls.Add(Me.GroupBox3)
		Me.Controls.Add(Me.ToolBar1)
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
		Me.Name = "FacturacionPVE"
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
		Me.Text = "Facturaci�n en Punto de Venta"
		Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
		Me.Panel2.ResumeLayout(False)
		Me.Panel2.PerformLayout()
		Me.GroupBox3_oLD.ResumeLayout(False)
		Me.GroupBox3_oLD.PerformLayout()
		CType(Me.DataSet_Facturaciones, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Panel5.ResumeLayout(False)
		Me.Panel5.PerformLayout()
		Me.GroupBox1.ResumeLayout(False)
		CType(Me.TxtUtilidad.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.Label46.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.Lb_SubExento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.Lb_Subgravado.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtImpVenta.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtSubtotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtCantidad.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TxtCodArticulo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtPrecioUnit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtDescuento.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TxtTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtImpVentaT.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtDescuentoT.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtSubtotalT.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.StatusBarPanel1, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.StatusBarPanel2, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.StatusBarPanel3, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.StatusBarPanel4, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.StatusBarPanel5, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.StatusBarPanel6, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.StatusBarPanel7, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.StatusBarPanel8, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Panel3.ResumeLayout(False)
		Me.Panel3.PerformLayout()
		CType(Me.txtDeuda.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Panel1.ResumeLayout(False)
		Me.Panel4.ResumeLayout(False)
		Me.Panel4.PerformLayout()
		CType(Me.PictureEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		Me.GroupBox3.ResumeLayout(False)
		CType(Me.TxtBarras.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtIVA.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtTotalArt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.txtExistencia.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Panel6.ResumeLayout(False)
		Me.Panel6.PerformLayout()
		Me.Panel7.ResumeLayout(False)
		CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.Txtbodega.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.TxtPorc_imp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub

#End Region
	Dim acuseProblema As Boolean = False
	Private Sub inicializa_archivosimprecion()
		Try
			FacturaPVE.Load(GetSetting("SeeSoft", "SeePOs Report", "Reporte_FacturaPVEs"))
			FacturaPVE1.Load(GetSetting("SeeSoft", "SeePOs Report", "Reporte_FacturaPVEs1"))
			Factura_Generica.Load(GetSetting("SeeSoft", "SeePOs Report", "Factura_Generica"))
		Catch ex As Exception
			MsgBox("Problema cargando el machote de factura", MsgBoxStyle.Critical)
			acuseProblema = True
		End Try
	End Sub

#Region "Load"

	Public Function GetVersionPublicacion() As String

		Dim ver As String = ""
		If Deployment.Application.ApplicationDeployment.IsNetworkDeployed Then
			Dim ad As Deployment.Application.ApplicationDeployment
			ad = Deployment.Application.ApplicationDeployment.CurrentDeployment
			ver = ad.CurrentVersion.ToString()
		End If

		Return ver
	End Function

	Private Sub Facturacion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
		Try
			Application.EnableVisualStyles()

			StatusBarPanel5.Text = "Facturaci�n en Punto de Venta     v. " & GetVersionPublicacion()

			If GetSetting("Seesoft", "SeePos", "PremiosLealtad") = "1" Then PremiosLealtad = True Else SaveSetting("Seesoft", "SeePos", "PremiosLealtad", "0")
			If GetSetting("Seesoft", "SeePos", "Recarga") = "1" Then Recarga = True Else SaveSetting("Seesoft", "SeePos", "Recarga", "0")
			If GetSetting("Seesoft", "SeePos", "RedondeoTotal") = "1" Then RedondeoTotal = 0 Else SaveSetting("Seesoft", "SeePos", "RedondeoTotal", "0")
			If GetSetting("Seesoft", "SeePos", "EscanerBalanza") = "1" Then spIniciarPuertoSerial() Else SaveSetting("Seesoft", "SeePos", "EscanerBalanza", "0")

			Me.ToolBarRecarga.Visible = Recarga
			Me.ToolBarPesa.Visible = EscanerBalanza

			inicializa_archivosimprecion()

			SqlConnection1.ConnectionString = GetSetting("SeeSOFT", "SeePOS", "CONEXION")
			If GetSetting("SeeSOFT", "SeePOS", "PVEFullScreen") = 1 Then
				FormBorderStyle = Windows.Forms.FormBorderStyle.None
			Else
				FormBorderStyle = Windows.Forms.FormBorderStyle.FixedSingle
			End If

			Adapter_Configuraciones.Fill(DataSet_Facturaciones, "configuraciones")
			AdapterConfig.Fill(DataSet_Facturaciones, "Config")
			Adapter_Moneda.Fill(DataSet_Facturaciones, "Moneda")
			Adapter_Usuarios.Fill(DataSet_Facturaciones, "Usuarios")
			Adapter_Encargados_Compra.Fill(DataSet_Facturaciones, "encargadocompras")

			'-----------------------------------------------------------------------
			'VERIFICA LA BODEGA DE DESCARGA - ORA
			Try
				iddebodega = GetSetting("SeeSOFT", "SeePOS", "Bodega")
			Catch ex As Exception
				SaveSetting("SeeSOFT", "SeePos", "Bodega", "0")
				iddebodega = 0
			End Try
			'-----------------------------------------------------------------------

			'-----------------------------------------------------------------------
			'VERIFICA LA CANTIDAD DEL TOP EN LOS BUSCADORES - ORA
			Try
				TopBuscador = CInt(GetSetting("SeeSOFT", "SeePos", "TopBuscaFactura"))
			Catch ex As Exception
				SaveSetting("SeeSOFT", "SeePos", "TopBuscaFactura", "1000")
				TopBuscador = 1000
			End Try
			'-----------------------------------------------------------------------
			'-----------------------------------------------------------------------
			'VERIFICA FORMATO DE IMPRESION - ORA
			Dim PVE As Boolean
			Try
				PVE = CBool(GetSetting("SeeSOFT", "SeePos", "FormatoPVE"))
			Catch ex As Exception
				SaveSetting("SeeSOFT", "SeePos", "FormatoPVE", "True")
				PVE = True
			Finally
				CK_PVE.Checked = PVE
			End Try

			ValoresDefecto()
			opContado.Checked = True
			bindings()
			logeado = False
			Loggin_Usuario()
			Application.DoEvents()
			StatusBarPanel2.Text = Now.Date
			txtNomComisionista.ReadOnly = True
			'-----------------------------------------------------------------------
			Dim mi_moneda As Integer = 1
			Try
				mi_moneda = CInt(GetSetting("SeeSOFT", "SeePos", "Moneda"))
			Catch ex As Exception
				SaveSetting("SeeSOFT", "SeePos", "Moneda", "1")
			Finally
				CB_Moneda.SelectedValue = mi_moneda
			End Try
			If Not acuseProblema Then
				'-----------------------------------------------------------------------
				If GetSetting("seesoft", "seepos", "tipo") = "1" Then
					CrystalReportsConexion.LoadReportViewer(Nothing, FacturaPVE1, True)

				Else
					CrystalReportsConexion.LoadReportViewer(Nothing, FacturaPVE, True)

				End If


				CrystalReportsConexion.LoadReportViewer(Nothing, Factura_Generica, True)
				' CrystalReportsConexion.LoadReportViewer(Nothing, FacturaPVESug, True)
			End If


		Catch ex As SystemException
			MsgBox(ex.ToString)
		End Try
	End Sub

	Private Sub ValoresDefecto()
		Me.DataSet_Facturaciones.Ventas.IdColumn.AutoIncrement = True
		Me.DataSet_Facturaciones.Ventas.IdColumn.AutoIncrementSeed = -1
		Me.DataSet_Facturaciones.Ventas.IdColumn.AutoIncrementStep = -1
		Me.DataSet_Facturaciones.Ventas_Detalle.id_venta_detalleColumn.AutoIncrement = True
		Me.DataSet_Facturaciones.Ventas_Detalle.id_venta_detalleColumn.AutoIncrementSeed = -1
		Me.DataSet_Facturaciones.Ventas_Detalle.id_venta_detalleColumn.AutoIncrementStep = -1
		''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		' establecer valores por defecto
		Me.DataSet_Facturaciones.Ventas.TipoColumn.DefaultValue = "CON"
		Me.DataSet_Facturaciones.Ventas.Cod_ClienteColumn.DefaultValue = "0"
		Me.DataSet_Facturaciones.Ventas.Nombre_ClienteColumn.DefaultValue = "CLIENTE CONTADO"
		Me.DataSet_Facturaciones.Ventas.OrdenColumn.DefaultValue = "0"
		Me.DataSet_Facturaciones.Ventas.SubTotalColumn.DefaultValue = "0.00"
		Me.DataSet_Facturaciones.Ventas.DescuentoColumn.DefaultValue = "0.00"
		Me.DataSet_Facturaciones.Ventas.Imp_VentaColumn.DefaultValue = "0.00"
		Me.DataSet_Facturaciones.Ventas.Tipo_CambioColumn.DefaultValue = "1"
		Me.DataSet_Facturaciones.Ventas.TotalColumn.DefaultValue = "0.00"
		Me.DataSet_Facturaciones.Ventas.Num_FacturaColumn.DefaultValue = 0
		Me.DataSet_Facturaciones.Ventas.FechaColumn.DefaultValue = Date.Now
		Me.DataSet_Facturaciones.Ventas.VenceColumn.DefaultValue = Date.Now
		Me.DataSet_Facturaciones.Ventas.TransporteColumn.DefaultValue = "0.00"
		Me.DataSet_Facturaciones.Ventas.Cod_Encargado_CompraColumn.DefaultValue = "NINGUNO"
		Me.DataSet_Facturaciones.Ventas.ContabilizadoColumn.DefaultValue = False
		Me.DataSet_Facturaciones.Ventas.AsientoCostoColumn.DefaultValue = "0"
		Me.DataSet_Facturaciones.Ventas.AsientoVentaColumn.DefaultValue = "0"
		Me.DataSet_Facturaciones.Ventas.ContabilizadoCVentaColumn.DefaultValue = False
		Me.DataSet_Facturaciones.Ventas.AnuladoColumn.DefaultValue = False
		Me.DataSet_Facturaciones.Ventas.PagoImpuestoColumn.DefaultValue = "0"
		Me.DataSet_Facturaciones.Ventas.FacturaCanceladoColumn.DefaultValue = False
		Me.DataSet_Facturaciones.Ventas.Num_AperturaColumn.DefaultValue = "0"
		Me.DataSet_Facturaciones.Ventas.EntregadoColumn.DefaultValue = False
		Me.DataSet_Facturaciones.Ventas.DireccionColumn.DefaultValue = ""
		Me.DataSet_Facturaciones.Ventas.TelefonoColumn.DefaultValue = ""
		Me.DataSet_Facturaciones.Ventas.Pago_ComisionColumn.DefaultValue = False
		Me.DataSet_Facturaciones.Ventas.SubTotalExentoColumn.DefaultValue = 0.0
		Me.DataSet_Facturaciones.Ventas.SubTotalGravadaColumn.DefaultValue = 0.0
		Me.DataSet_Facturaciones.Ventas.Cedula_UsuarioColumn.DefaultValue = ""
		Me.DataSet_Facturaciones.Ventas.Cod_MonedaColumn.DefaultValue = Me.DataSet_Facturaciones.Moneda.Rows(0).Item(0)
		Me.DataSet_Facturaciones.Ventas.CodComisionistaColumn.DefaultValue = 0
		Me.DataSet_Facturaciones.Ventas.NombreComisionistaColumn.DefaultValue = ""
		Me.DataSet_Facturaciones.Ventas.ObservacionesColumn.DefaultValue = ""
		Me.DataSet_Facturaciones.Ventas.ExonerarColumn.DefaultValue = False
		Me.DataSet_Facturaciones.Ventas.PagoComisionColumn.DefaultValue = 0
		Me.DataSet_Facturaciones.Ventas.Nombre_UsuarioColumn.DefaultValue = ""
		Me.DataSet_Facturaciones.Ventas.ObservacionesColumn.DefaultValue = ""
		'valores por defecto en Ventas_Detalles
		Me.DataSet_Facturaciones.Ventas_Detalle.DevolucionesColumn.DefaultValue = 0
		Me.DataSet_Facturaciones.Ventas_Detalle.Numero_EntregaColumn.DefaultValue = 0
		Me.DataSet_Facturaciones.Ventas_Detalle.CantidadColumn.DefaultValue = 1
		Me.DataSet_Facturaciones.Ventas_Detalle.DescuentoColumn.DefaultValue = 0.0
		Me.DataSet_Facturaciones.Ventas_Detalle.Monto_DescuentoColumn.DefaultValue = 0.0
		Me.DataSet_Facturaciones.Ventas_Detalle.SubTotalExcentoColumn.DefaultValue = 0.0
		Me.DataSet_Facturaciones.Ventas_Detalle.SubtotalGravadoColumn.DefaultValue = 0.0
		Me.DataSet_Facturaciones.Ventas_Detalle.ImpuestoColumn.DefaultValue = 0.0
		Me.DataSet_Facturaciones.Ventas_Detalle.Monto_ImpuestoColumn.DefaultValue = 0.0
		Me.DataSet_Facturaciones.Ventas_Detalle.SubTotalColumn.DefaultValue = 0.0
		Me.DataSet_Facturaciones.Ventas_Detalle.Total_FicticioColumn.DefaultValue = 0
		Me.DataSet_Facturaciones.Ventas_Detalle.ComisionColumn.DefaultValue = 0.0
		Me.DataSet_Facturaciones.Ventas_Detalle.MontoComisionColumn.DefaultValue = 0.0
		Me.DataSet_Facturaciones.Ventas_Detalle.SobrePrecioColumn.DefaultValue = 0.0
		Me.DataSet_Facturaciones.Ventas_Detalle.Id_bodegaColumn.DefaultValue = iddebodega
		Me.DataSet_Facturaciones.Ventas_Detalle.ExistenciaColumn.DefaultValue = 0
		Me.DataSet_Facturaciones.Ventas_Detalle.BarrasColumn.DefaultValue = 0
	End Sub

	Private Sub bindings()
		TxtTipo.DataBindings.Add(New System.Windows.Forms.Binding("Text", DataSet_Facturaciones, "Ventas.Tipo"))
		Lb_Subgravado.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSet_Facturaciones, "Ventas.SubtotalGravada"))
		Lb_SubExento.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSet_Facturaciones, "Ventas.SubTotalExento"))
		Label46.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSet_Facturaciones, "Ventas.Transporte"))
		Combo_Encargado.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Facturaciones, "Ventas.Cod_Encargado_Compra"))
		txtFactura.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Facturaciones, "Ventas.Num_Factura"))
		txtMontoImpuesto.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle.Monto_Impuesto"))
		txtSubtotal.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle.SubTotal"))
		txtImpVenta.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle.Impuesto"))
		TxtMaxdescuento.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle.Max_Descuento"))
		txtPrecioUnit.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle.Precio_Unit"))
		txtCantidad.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle.Cantidad"))
		txtNombreArt.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle.Descripcion"))
		TxtCodArticulo.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle.Codigo"))
		TxtBarras.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle.Barras"))
		txtDescuento.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle.Descuento"))
		txtOtros.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle.Precio_Otros"))
		txtFlete.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle.Precio_Flete"))
		Txtcodmoneda_Venta.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle.Cod_MonedaVenta"))
		Txt_TipoCambio_Valor_Compra.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle.Tipo_Cambio_ValorCompra"))
		txtCostoBase.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle.Precio_Base"))
		TxtTotal.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSet_Facturaciones, "Ventas.Total"))
		txtImpVentaT.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSet_Facturaciones, "Ventas.Imp_Venta"))
		txtDescuentoT.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSet_Facturaciones, "Ventas.Descuento"))
		txtSubtotalT.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.DataSet_Facturaciones, "Ventas.SubTotal"))
		txtSGravado.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle.SubtotalGravado"))
		txtSExcento.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle.SubTotalExcento"))
		CkEntregado.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.DataSet_Facturaciones, "Ventas.Entregado"))
		CheckBox1.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.DataSet_Facturaciones, "Ventas.Anulado"))
		CB_Exonerar.DataBindings.Add(New System.Windows.Forms.Binding("Checked", Me.DataSet_Facturaciones, "Ventas.Exonerar"))
		txtTelefono.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Facturaciones, "Ventas.Telefono"))
		Txtdireccion.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Facturaciones, "Ventas.Direccion"))
		txtCodigo.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Facturaciones, "Ventas.Cod_Cliente"))
		txtNombre.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Facturaciones, "Ventas.Nombre_Cliente"))
		TxtprecioCosto.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle.Precio_Costo"))
		txtmontodescuento.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle.Monto_Descuento"))
		ComboBox1.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.DataSet_Facturaciones, "Ventas.Moneda_Nombre"))
		dtFecha.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Facturaciones, "Ventas.Fecha"))
		txtTipoCambio.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Facturaciones, "Moneda.ValorCompra"))
		txtorden.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Facturaciones, "Ventas.Orden"))
		DtVence.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Facturaciones, "Ventas.Vence"))
		txtComisionista.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Facturaciones, "Ventas.CodComisionista"))
		txtNomComisionista.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Facturaciones, "Ventas.NombreComisionista"))
		txtSubtotalExcento.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle.SubTotalExcento"))
		txtMontoComision.DataBindings.Add(New System.Windows.Forms.Binding("Text", DataSet_Facturaciones, "Ventas.VentasVentas_Detalle.MontoComision"))
		Txtbodega.DataBindings.Add(New System.Windows.Forms.Binding("Text", DataSet_Facturaciones, "Ventas.VentasVentas_Detalle.Id_bodega"))
		txtExistencia.DataBindings.Add(New System.Windows.Forms.Binding("Text", DataSet_Facturaciones, "Ventas.VentasVentas_Detalle.Existencia"))
		txtNombreUsuario.DataBindings.Add(New System.Windows.Forms.Binding("Text", DataSet_Facturaciones, "Ventas.Nombre_Usuario"))
		txtObservacion.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DataSet_Facturaciones, "Ventas.Observaciones"))
		txtIVA.DataBindings.Add(New System.Windows.Forms.Binding("Text", DataSet_Facturaciones, "Ventas.VentasVentas_Detalle.Impuesto"))
	End Sub

	Private Sub Loggin_Usuario()
		Try
			If Me.BindingContext(Me.DataSet_Facturaciones.Usuarios).Count > 0 Then
				Dim Usuario_autorizadores() As System.Data.DataRow
				Usuario_autorizadores = Me.DataSet_Facturaciones.Usuarios.Select("Cedula ='" & Me.Usua.Cedula & "'")
				If Usuario_autorizadores.Length <> 0 Then
					PMU = VSM(Usua.Cedula, Me.Name) 'Carga los privilegios del usuario con el modulo 
					If Not PMU.Execute Then
						MsgBox("Usted no tiene permisos para realizar ventas...", MsgBoxStyle.Exclamation)
						Me.txtUsuario.Text = ""
						Me.txtUsuario.Focus()
						Exit Sub
					End If

					Me.logeado = True
					txtNombreUsuario.Text = Usua.Nombre
					Me.Cedula_usuario = Usua.Cedula
					Me.Nombre_usuario = Usua.Nombre
					txtUsuario.Enabled = False ' se inabilita el campo de la contrase�a

					Me.ToolBar1.Buttons(0).Text = "Cancelar"
					Me.ToolBar1.Buttons(0).ImageIndex = 8
					Me.ToolBar1.Buttons(3).Enabled = False
					Me.ToolBar1.Buttons(0).Enabled = True
					Me.ToolBar1.Buttons(1).Enabled = True
					Me.ToolBar1.Buttons(5).Enabled = True
					Me.ToolBar1.Buttons(2).Enabled = False
					Me.TxtUtilidad.Text = ""
					'Validar si el usuario puede o no anular una cotizaci�n
					txtPrecioUnit.Enabled = Usua.CambiarPrecio
					txtPrecioUnit.Properties.ReadOnly = Not Usua.CambiarPrecio
					variacion_Punit = IIf(Usua.CambiarPrecio, Usua.Porc_Precio, 0)
					porcentaje_descuento = Usua.Porc_Desc

					'si el usuario no puede dar descuento
					Me.SimpleButton2.Enabled = Usua.Aplicar_Desc
					Me.txtDescuento.Enabled = Usua.Aplicar_Desc
					Me.txtDescuento.Properties.ReadOnly = Not Usua.Aplicar_Desc

					TxtUtilidad.Visible = PMU.Others
					Label49.Visible = PMU.Others
					CB_Exonerar.Enabled = PMU.Others
					Me.vende_existecias_negativas = Usua.Exist_Negativa ' si el vendedor puede vender con existencias negativas
					Me.ComboBox1.Enabled = True

					Me.Nueva_Factura()
					Me.ComboBox1.Focus()

					inicializar()
				Else ' si no existe una contrase�la como esta
					MsgBox("Contrase�a interna incorrecta", MsgBoxStyle.Exclamation)
					Me.logeado = False
					txtUsuario.Text = ""
				End If
			Else
				MsgBox("No Existen Usuarios, ingrese datos")
			End If

		Catch ex As SystemException
			MsgBox(ex.ToString & " punto 32")
		End Try
	End Sub

	Private Sub Position_Changed(ByVal sender As System.Object, ByVal e As System.EventArgs)
		If Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Count = 0 Then
			Exit Sub
		End If

		If Not buscando And Me.txtNombreArt.Text <> "" Then
			Buscar_datos_articulo(Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Codigo"))
		End If

		If txtCostoBase.Text <> "" And txtPrecioUnit.Text <> "" And txtFlete.Text <> "" And txtOtros.Text <> "" Then
			TxtUtilidad.Text = Utilidad(Me.txtCostoBase.Text, (Me.txtPrecioUnit.Text - txtFlete.Text - txtOtros.Text))
		End If
	End Sub

	Private Sub Current_Changed(ByVal sender As System.Object, ByVal e As System.EventArgs)
		If TxtTotal.Text <> "" Then
			If Val(TxtTotal.Text) = 0 Then
				Me.SimpleButton3.Enabled = True
				Me.ComboBox1.Enabled = True
			Else
				Me.SimpleButton3.Enabled = False
				Me.ComboBox1.Enabled = False
			End If
		Else
			Me.SimpleButton3.Enabled = True
			Me.ComboBox1.Enabled = True
		End If
	End Sub
#End Region

#Region "Validar Articulo"

	Private Function Validar_articulo(ByVal codigo As String) As Boolean
		If codigo = "0" Then
			Exit Function
		End If
		Dim rs As SqlDataReader
		Dim Encontrado As Boolean = False
		Try
			If codigo <> Nothing Then
				sqlConexion = CConexion.Conectar
				rs = CConexion.GetRecorset(sqlConexion, "SELECT Inventario.Codigo FROM Inventario INNER JOIN  Presentaciones ON Presentaciones.CodPres = Inventario.CodPresentacion INNER JOIN SubFamilias ON Inventario.SubFamilia = SubFamilias.Codigo INNER JOIN  Familia ON SubFamilias.CodigoFamilia = Familia.Codigo INNER JOIN  ArticulosXBodega ON Inventario.Codigo = ArticulosXBodega.Codigo WHERE (Inhabilitado = 0) and (ArticulosXBodega.Idbodega=" & iddebodega & ") and (Inventario.Codigo ='" & codigo & "' or Barras = '" & codigo & "')")
				While rs.Read
					Encontrado = True
				End While
				rs.Close()
			End If
			If Not Encontrado Then
				MsgBox("No existe un art�culo con este c�digo", MsgBoxStyle.Exclamation)
				Me.TxtCodArticulo.Text = ""
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
				Me.TxtCodArticulo.Focus()
			End If
			CConexion.DesConectar(CConexion.Conectar)
		Catch ex As Exception
			CConexion.DesConectar(CConexion.Conectar)
			MsgBox(ex.Message, MsgBoxStyle.Information)
			Return False
		End Try
		Return Encontrado
	End Function
#End Region

#Region "Articulo"
	Private Sub Buscar_datos_articulo(ByVal codigo As String)
		If codigo = "0" Then
			Exit Sub
		End If
		Dim rs As SqlDataReader
		Dim Encontrado As Boolean
		If codigo <> Nothing Then
			sqlConexion = CConexion.Conectar
			rs = CConexion.GetRecorset(sqlConexion, "SELECT Inventario.Max_Descuento, Inventario.Precio_Promo, Inventario.Promo_Activa, Inventario.Codigo, Inventario.Barras, Inventario.Descripcion, Inventario.SubFamilia, ArticulosXBodega.Existencia, Inventario.PrecioBase, Inventario.Fletes, Inventario.OtrosCargos, Inventario.Costo, Inventario.MonedaCosto, Inventario.MonedaVenta, Inventario.Precio_A, Inventario.Precio_B, Inventario.Precio_C, Inventario.Precio_D, Inventario.IVenta, Inventario.PreguntaPrecio, Inventario.Servicio, Familia.Comision, Familia.SobrePrecio FROM Inventario INNER JOIN  Presentaciones ON Presentaciones.CodPres = Inventario.CodPresentacion INNER JOIN SubFamilias ON Inventario.SubFamilia = SubFamilias.Codigo INNER JOIN  Familia ON SubFamilias.CodigoFamilia = Familia.Codigo INNER JOIN  ArticulosXBodega ON Inventario.Codigo = ArticulosXBodega.Codigo WHERE (Inhabilitado = 0) and (ArticulosXBodega.Idbodega=" & iddebodega & ") and (Inventario.Codigo ='" & codigo & "' or Barras = '" & codigo & "')")
			Encontrado = False
			While rs.Read
				Try
					Encontrado = True
					If CB_Exonerar.Checked Then
						txtImpVenta.Text = Format(0, "#,#0.00")
					Else
						txtImpVenta.Text = Format(rs("IVenta"), "#,#0.00")
					End If

					If rs("PreguntaPrecio") Then
						PrecioA = Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Precio_Unit")
						PrecioB = Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Precio_Unit")
						PrecioC = Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Precio_Unit")
						PrecioD = Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Precio_Unit")
					Else
						PrecioA = rs("Precio_A")
						PrecioB = rs("Precio_B")
						PrecioC = rs("Precio_C")
						PrecioD = rs("Precio_D")
					End If

					If rs("Servicio") Then
						Existencia = 1000
					Else
						Existencia = rs("Existencia")
					End If
					txtExistencia.EditValue = rs("Existencia")
					BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Comision") = rs("Comision")
					BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("SobrePrecio") = rs("SobrePrecio")
					precio_promo_valor = rs("Precio_Promo")
					MonedaCosto = rs("MonedaVenta")
					MonedaBase = rs("MonedaCosto")
					MonedaVenta = Me.BindingContext(Me.DataSet_Facturaciones, "Moneda").Current("CodMoneda")
					If rs("Promo_Activa") = True Then ' si el articulo esta en promocion
						Me.promo_activa_valor = True
						Me.txtDescuento.Enabled = False
					Else
						Me.promo_activa_valor = False ' se habilita el text
						Me.txtDescuento.Enabled = True
					End If
				Catch ex As Exception
					MessageBox.Show(ex.Message)
					CConexion.DesConectar(CConexion.Conectar)
				End Try
			End While
			rs.Close()
			If Not Encontrado Then
				MsgBox("No existe un art�culo con este c�digo", MsgBoxStyle.Exclamation)
				Me.TxtCodArticulo.Text = ""
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
				Me.TxtCodArticulo.Focus()
				CConexion.DesConectar(CConexion.Conectar)
				Exit Sub
			End If
			rs = CConexion.GetRecorset(sqlConexion, "Select ValorCompra from Moneda where CodMoneda = " & MonedaCosto)
			While rs.Read
				Try
					ValorCosto = rs("ValorCompra")
				Catch ex As Exception
					MessageBox.Show(ex.Message)
					CConexion.DesConectar(CConexion.Conectar)
				End Try
			End While
			rs.Close()

			rs = CConexion.GetRecorset(sqlConexion, "Select ValorCompra from Moneda where CodMoneda = " & MonedaBase)
			While rs.Read
				Try
					ValorBase = rs("ValorCompra")
				Catch ex As Exception
					MessageBox.Show(ex.Message)
					CConexion.DesConectar(CConexion.Conectar)
				End Try
			End While
			rs.Close()


			ValorVenta = CDbl(Me.txtTipoCambio.Text)


			rs = CConexion.GetRecorset(sqlConexion, "Select ValorCompra from Moneda where CodMoneda = " & MonedaBase)
			While rs.Read
				Try
					ValorBase = rs("ValorCompra")
				Catch ex As Exception
					MessageBox.Show(ex.Message)
					CConexion.DesConectar(CConexion.Conectar)
				End Try
			End While
			rs.Close()

			'Calculo_precio_unitario()
			'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
			Try


				If Me.promo_activa_valor Then ' si el articulo esta actualmente en promoci�n 
					Me.precio_unitario = Math.Round((Me.precio_promo_valor * (ValorCosto / ValorVenta)), 2)
					''Calculo de Costo
					'txtCostoBase.Text = (CDbl(txtCostoBase.Text) * (ValorBase / ValorVenta))
					'PrecioBase = txtCostoBase.Text
					'txtFlete.Text = (CDbl(txtFlete.Text) * (ValorBase / ValorVenta))
					'txtOtros.Text = (CDbl(txtOtros.Text) * (ValorBase / ValorVenta))
					'Me.precio_unitario = txtPrecioUnit.Text

					Exit Sub
				End If

				If Me.tipoprecio = 0 Then
					Me.tipoprecio = 1
				End If

				'Calculos para el Precio Unitario
				Select Case tipoprecio
					Case 1 : Me.precio_unitario = Math.Round((PrecioA * (ValorCosto / ValorVenta)), 2)
					Case 2 : Me.precio_unitario = Math.Round((PrecioB * (ValorCosto / ValorVenta)), 2)
					Case 3 : Me.precio_unitario = Math.Round((PrecioC * (ValorCosto / ValorVenta)), 2)
					Case 4 : Me.precio_unitario = Math.Round((PrecioD * (ValorCosto / ValorVenta)), 2)
				End Select

				If txtNomComisionista.Text <> "" Then
					precio_unitario += (precio_unitario * (BindingContext(DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("sobrePrecio") / 100))
				End If

				'CALCULAR EL MONTO DE COMISI�N - ORA
				'BindingContext(DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("MontoComision") = (precio_unitario * (BindingContext(DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Comision") / 100))

				''Calculo de Costo
				'txtCostoBase.Text = (CDbl(txtCostoBase.Text) * (ValorBase / ValorVenta))
				'PrecioBase = txtCostoBase.Text
				'txtFlete.Text = (CDbl(txtFlete.Text) * (ValorBase / ValorVenta))
				'txtOtros.Text = (CDbl(txtOtros.Text) * (ValorBase / ValorVenta))
				'Me.TxtprecioCosto.Text = (CDbl(Me.TxtprecioCosto.Text) * (ValorBase / ValorVenta))
				'Me.precio_unitario = txtPrecioUnit.Text

			Catch ex As SystemException
				MsgBox(ex.Message)
			End Try

			If Me.txtNombreArt.Text <> "" Then 'si se recuperaron los datos de un articulo

				'If Me.txtPrecioUnit.Enabled = True Then 'si el usuario puede disminuir o aumentar el costo del articulo
				'    Me.txtPrecioUnit.Focus()
				'Else
				'    If Me.txtDescuento.Enabled = True Then
				'        txtDescuento.Focus()

				'    Else
				'        Me.txtCantidad.Focus()

				'    End If

				'End If



				'txtCantidad.Focus()
			End If


		Else ' si no se recupero ningun articulo, se termina la edicion

			Try

				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
				Me.TxtCodArticulo.Focus()

			Catch ex As SystemException
				MsgBox(ex.Message)
				CConexion.DesConectar(CConexion.Conectar)
			End Try

			CConexion.DesConectar(CConexion.Conectar)

		End If
	End Sub
#End Region

#Region "Validar Usuario"
	Private Sub Reloggin_Usuario()
		Try

			If Me.BindingContext(Me.DataSet_Facturaciones.Usuarios).Count > 0 Then
				Dim Usuario_autorizadores() As System.Data.DataRow
				Dim Usua As System.Data.DataRow

				Usuario_autorizadores = Me.DataSet_Facturaciones.Usuarios.Select("Clave_Interna ='" & txtUsuario.Text & "'")
				If Usuario_autorizadores.Length <> 0 Then

					Usua = Usuario_autorizadores(0)
					PMU = VSM(Usua("Cedula"), Me.Name) 'Carga los privilegios del usuario con el modulo 
					'Dim tipo As String = Usua("Perfil")

					If Not PMU.Execute Then
						MsgBox("Usted no tiene permisos para realizar ventas realizar ventas", MsgBoxStyle.Exclamation)
						Me.txtUsuario.Text = ""
						Me.txtUsuario.Focus()
						Exit Sub
					End If
					If Usua("Aplicar_Desc") = False Then 'si el usuario no puede dar descuento
						Me.SimpleButton2.Enabled = False
						Me.txtDescuento.Enabled = False
					Else
						Me.SimpleButton2.Enabled = True
						Me.txtDescuento.Enabled = True

					End If


					'Me.GroupBox6.Enabled = True
					Me.GroupBox3.Enabled = True


					If Usua("CambiarPrecio") = True Then
						Me.txtPrecioUnit.Properties.ReadOnly = False
						Me.txtPrecioUnit.Properties.Enabled = True

					Else
						Me.txtPrecioUnit.Properties.ReadOnly = True
						Me.txtPrecioUnit.Properties.Enabled = False
					End If


					If Usua("Aplicar_Desc") = True Then
						Me.txtDescuento.Properties.ReadOnly = False
						Me.txtDescuento.Properties.Enabled = True

					Else
						Me.txtDescuento.Properties.ReadOnly = True
						Me.txtDescuento.Properties.Enabled = False
					End If



					txtorden.Enabled = True
					txtNombreUsuario.Text = Usua("Nombre")

					If Usua("CambiarPrecio") = True Then
						txtPrecioUnit.Enabled = True
						variacion_Punit = Usua("Porc_Precio")
					Else
						txtPrecioUnit.Enabled = False
						variacion_Punit = 0
					End If

					Me.porcentaje_descuento = Usua("Porc_Desc")

					'Me.perfil_administrador = PMU.Others ' perfil administrador
					TxtUtilidad.Visible = PMU.Others
					Label49.Visible = PMU.Others
					CB_Exonerar.Enabled = PMU.Others
					Me.vende_existecias_negativas = Usua("Exist_Negativa") ' si el vendedor puede vender con existencias negativas
					Me.txtDescuento.Focus()

				Else ' si no existe una contrase�la como esta
					MsgBox("Contrase�a interna incorrecta", MsgBoxStyle.Exclamation)
					txtUsuario.Text = ""
				End If
			Else
				MsgBox("No Existen Usuarios, ingrese datos")
			End If

		Catch ex As SystemException
			MsgBox(ex.Message)
		End Try
	End Sub

	Private Sub txtUsuario_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtUsuario.KeyDown
		If e.KeyCode = Keys.Enter Then
			If Not logeado Then ' si es la primera vez que se logea un usuario
				Loggin_Usuario()
			Else
				Me.Reloggin_Usuario()
			End If
		End If
	End Sub
#End Region

#Region "Nueva Factura"
	Private Sub Nueva_Factura()
		Try
			Me.DataSet_Facturaciones.Ventas_Detalle.Clear()
			Me.DataSet_Facturaciones.Ventas.Clear()

			Me.DataSet_Facturaciones.Cotizacion_Detalle.Clear()
			Me.DataSet_Facturaciones.Cotizacion.Clear()

			Me.Combo_Encargado.Items.Clear()
			'Me.Label41.Visible = False
			Me.Combo_Encargado.Visible = False
			Me.txtDiasPlazo.Visible = False
			Me.Label7.Visible = False
			txtTotalArt.Text = 0


			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").EndCurrentEdit()
			Me.DataSet_Facturaciones.Ventas.FechaColumn.DefaultValue = Date.Now
			Me.DataSet_Facturaciones.Ventas.VenceColumn.DefaultValue = Date.Now
			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").AddNew()
			Me.ComboBox1.SelectedIndex = 0

			Me.opContado.Checked = True
			Verificar_Consecutivos()
			'Me.txtFactura.Text = Label39.Text
			Me.impuesto_cliente = 100
			Me.tipoprecio = 1
			'-----------------------------------------------------------------------
			Dim mi_moneda As Integer = 1
			Try
				mi_moneda = CInt(GetSetting("SeeSOFT", "SeePos", "Moneda"))
			Catch ex As Exception
				SaveSetting("SeeSOFT", "SeePos", "Moneda", "1")
			Finally
				CB_Moneda.SelectedValue = mi_moneda
			End Try
			'-----------------------------------------------------------------------


			If Me.buscando Then buscando = False ' si se estaba buscando, buscando se pone en falso
			'Me.perfil_administrador = True
			If PMU.Others Then


				Me.colCodigo.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
									Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted = False) _
									Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
									Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
									Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
									Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)


				Me.colCantidad.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
												Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted = False) _
												Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
												Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
												Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
												Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)


				Me.colSubTotalExcento.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
												Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted = False) _
												Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
												Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
												Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
												Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)



				Me.colSubtotalGravado.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
												Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted = False) _
												Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
												Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
												Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
												Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)



				Me.colDescripcion.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
												Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted = False) _
												Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
												Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
												Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
												Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)

				Me.colPrecio_Unit.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
															Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted = False) _
															Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
															Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
															Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
															Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)

				Me.colMonto_Descuento.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
												Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted = False) _
												Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
												Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
												Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
												Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)


				Me.colSubTotal.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
												Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted = False) _
												Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
												Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
												Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
												Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)

				Me.colMonto_Impuesto.Options = CType(((((((DevExpress.XtraGrid.Columns.ColumnOptions.CanMoved Or DevExpress.XtraGrid.Columns.ColumnOptions.CanResized) _
												Or DevExpress.XtraGrid.Columns.ColumnOptions.CanSorted = False) _
												Or DevExpress.XtraGrid.Columns.ColumnOptions.ReadOnly) _
												Or DevExpress.XtraGrid.Columns.ColumnOptions.CanFocused) _
												Or DevExpress.XtraGrid.Columns.ColumnOptions.ShowInCustomizationForm) _
												Or DevExpress.XtraGrid.Columns.ColumnOptions.NonEditable), DevExpress.XtraGrid.Columns.ColumnOptions)

			End If
			ComboBox1.Enabled = False
			txtDeuda.EditValue = 0

		Catch ex As SystemException
			MsgBox(ex.Message)
		End Try

	End Sub
#End Region

#Region "Imprimir"
	'Private Sub ImprimirFactura()
	'    Try
	'        Dim id As Long = Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("Id") ' se envia  aimprimir la factura actual
	'        Me.ToolBar1.Buttons(4).Enabled = False

	'        ' Me.CheckBoxPVE.Checked = True
	'        Me.Imprimir(id)

	'        Me.ToolBar1.Buttons(4).Enabled = True
	'    Catch ex As SystemException
	'        MsgBox(ex.Message)
	'    End Try
	'End Sub

	Private Sub ImprimirFactura()

		Try
			Me.ToolBar1.Buttons(4).Enabled = False
			Dim id As Long = Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("Id") ' se envia  aimprimir la factura actual

			If CK_PVE.Checked Then
				Imprimir(id, True)

			Else
				Imprimir(id, False)
			End If

			Me.ToolBar1.Buttons(4).Enabled = True

		Catch ex As SystemException
			MsgBox(ex.Message)
		End Try
	End Sub
	Public impresora As String = ""

	Private Sub Imprimir(ByVal Id_Factura As Double, ByVal PVE As Boolean)
		Try
			Dim PrinterSettings1 As New Printing.PrinterSettings
	     	Dim PageSettings1 As New Printing.PageSettings

			If acuseProblema Then
				Exit Sub
			End If
			Dim Sugerido As Boolean = False
			Dim NuevoSaldo As Double = 0
			Dim cod As String
			Dim func1 As New Conexion

			'  cod = func1.SQLExeScalar("Select Codigo From Inventario where Barras='" & TxtCodArticulo.Text & "'")
			' MsgBox(cod, MsgBoxStyle.Exclamation)
			If CDbl(txtCodigo.Text) <> 0 And txtCodigo.Text <> "" Then
				Dim func As New Conexion
				Sugerido = func.SQLExeScalar("SELECT ISNULL(PrecioSugerido,0) FROM Clientes WHERE Identificacion = " & CDbl(txtCodigo.Text))
			End If
			If Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("Tipo") = "CRE" Then
				NuevoSaldo = BuscaSaldoCliente(Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("Cod_Cliente"))
				If MessageBox.Show("�Desea mostrar el saldo total de la cuenta?", "Atenci�n...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No Then
					NuevoSaldo = 0
				End If
			End If
			Dim vistapreliminar As Integer = 0
			Try
				vistapreliminar = GetSetting("SeeSoft", "SeePos", "VistaPreliminar")
			Catch ex As Exception
				SaveSetting("SeeSoft", "SeePos", "VistaPreliminar", "0")
				vistapreliminar = 0
			End Try
			If PVE Then
				If Sugerido Then
					'FacturaPVESug.PrintOptions.PrinterName = Automatic_Printer_Dialog(0)
					'FacturaPVESug.SetParameterValue(0, Id_Factura)
					'FacturaPVESug.SetParameterValue(1, Sugerido)
					'FacturaPVESug.PrintToPrinter(1, True, 0, 0)
				Else
					If impresora = "" Then
						impresora = Automatic_Printer_Dialog(0)
						'FacturaPVE1.PrintOptions.PrinterName = impresora
						'FacturaPVE.PrintOptions.PrinterName = impresora
						'FacturaPVE1.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
						'FacturaPVE.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize

						PrinterSettings1.PrinterName = impresora
						PrinterSettings1.Copies = 1
						PrinterSettings1.Collate = True
					End If
					FacturaPVE.Refresh()
					'FacturaPVE1.Refresh()

					If vistapreliminar = 0 Then
						If GetSetting("seesoft", "seepos", "tipo") = "1" Then
							'FacturaPVE1.PrintOptions.PrinterName = Automatic_Printer_Dialog(0)
							'FacturaPVE1.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
							FacturaPVE1.SetParameterValue(0, Id_Factura)
							FacturaPVE1.SetParameterValue(1, NuevoSaldo)
						'	FacturaPVE1.PrintToPrinter(1, True, 0, 0)
							FacturaPVE1.PrintToPrinter(PrinterSettings1, PageSettings1, False)
						Else
							'FacturaPVE.PrintOptions.PrinterName = Automatic_Printer_Dialog(0)
							'FacturaPVE.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
							FacturaPVE.SetParameterValue(0, Id_Factura)
							FacturaPVE.SetParameterValue(1, NuevoSaldo)
							'FacturaPVE.PrintToPrinter(1, True, 0, 0)
							FacturaPVE.PrintToPrinter(PrinterSettings1, PageSettings1, False)
						End If

					Else
						Dim visor As New frmVisorReportes()
						CrystalReportsConexion.LoadReportViewer(visor.rptViewer, FacturaPVE)
						FacturaPVE.SetParameterValue(0, Id_Factura)
						FacturaPVE.SetParameterValue(1, NuevoSaldo)
						visor.ShowDialog()
					End If
				End If

			Else
				If Sugerido Then
					'Factura_Generica.PrintOptions.PrinterName = Automatic_Printer_Dialog(0)
					'Factura_Generica.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
					'Factura_Generica.SetParameterValue(0, Id_Factura)
					'Factura_Generica.SetParameterValue(1, Sugerido)
					'Factura_Generica.PrintToPrinter(1, True, 0, 0)

				Else
					If vistapreliminar = 0 Then
						Factura_Generica.PrintOptions.PrinterName = Automatic_Printer_Dialog(0)
						Factura_Generica.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.DefaultPaperSize
						Factura_Generica.SetParameterValue(0, Id_Factura)
						Factura_Generica.SetParameterValue(1, NuevoSaldo)
						Factura_Generica.PrintToPrinter(1, True, 0, 0)
					Else

						Dim visor As New frmVisorReportes()
						CrystalReportsConexion.LoadReportViewer(visor.rptViewer, Factura_Generica)
						Factura_Generica.SetParameterValue(0, Id_Factura)
						Factura_Generica.SetParameterValue(1, NuevoSaldo)
						visor.ShowDialog()
					End If


				End If
			End If
		Catch ex As System.Exception
			MsgBox(ex.Message)
		End Try
	End Sub

	Private Function Automatic_Printer_Dialog(ByVal PrinterToSelect As Byte) As String 'BISMARK
		Dim PrintDocument1 As New PrintDocument
		Dim DefaultPrinter As String = PrintDocument1.PrinterSettings.PrinterName
		Dim PrinterInstalled As String
		'BUSCA LA IMPRESORA PREDETERMINADA PARA EL SISTEMA
		For Each PrinterInstalled In PrinterSettings.InstalledPrinters
			Select Case Split(PrinterInstalled.ToUpper, "\").GetValue(Split(PrinterInstalled.ToUpper, "\").GetLength(0) - 1)
				Case "FACTURACION"
					If PrinterToSelect = 0 Then
						Return PrinterInstalled.ToString
						Exit Function
					End If
				Case "CONTADO"
					If PrinterToSelect = 1 Then
						Return PrinterInstalled.ToString
						Exit Function
					End If
				Case "CREDITO"
					If PrinterToSelect = 2 Then
						Return PrinterInstalled.ToString
						Exit Function
					End If
				Case "PUNTOVENTA"
					If PrinterToSelect = 3 Then
						Return PrinterInstalled.ToString
						Exit Function
					End If
				Case "FAX"
					If PrinterToSelect = 4 Then
						Return PrinterInstalled.ToString
						Exit Function
					End If
			End Select
		Next
		If MsgBox("No se ha encontrado las impresoras predeterminadas para el sistema..." & vbCrLf & "Desea proceder a selecionar una impresora....", MsgBoxStyle.YesNo + MsgBoxStyle.Critical, "Atenci�n...") = MsgBoxResult.Yes Then
			Dim PrinterDialog As New PrintDialog
			Dim DocPrint As New PrintDocument
			PrinterDialog.Document = DocPrint
			PrinterDialog.ShowDialog()
			If Windows.Forms.DialogResult.Yes Then
				Return PrinterDialog.PrinterSettings.PrinterName 'DEVUELVE LA IMPRESORA  SELECCIONADA
			Else
				Return DefaultPrinter 'NO SE SELECCIONO IMPRESORA ALGUNA
			End If
		End If
	End Function
#End Region

#Region "Toolbar"
	Private Sub ToolBar1_ButtonClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolBarButtonClickEventArgs) Handles ToolBar1.ButtonClick
		Select Case ToolBar1.Buttons.IndexOf(e.Button) + 1
			Case 1 : NuevaFactura()
			Case 2 : If PMU.Find Then Me.BuscarFactura() Else MsgBox("No tiene permiso para buscar informaci�n...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
			Case 3 : If PMU.Update Then Registrar() Else MsgBox("No tiene permiso para registrar o actualizar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
			Case 4 : If PMU.Delete Then Anular() Else MsgBox("No tiene permiso para anular datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
			Case 5 : If PMU.Print Then ImprimirFactura() Else MsgBox("No tiene permiso para imprimir datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
			Case 6 : BuscarCotizacion_importar()
			Case 7 : ReciboDinero()
			Case 8 : Recargas()
			Case 9 : spObtenerPeso()
			Case 10 : If MessageBox.Show("�Desea Cerrar el m�dulo " & Me.Text & "..?", "Atenci�n...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = DialogResult.Yes Then Me.Dispose(True) : Me.Close()
		End Select
	End Sub
#End Region

#Region "Escaner / Balanza"

	Dim WithEvents PuertoSerial As SerialPort = New System.IO.Ports.SerialPort("COM1", 9600, Parity.Odd, 7, StopBits.One) 'CHANGE PORT# to the port where your scanner is connected
	Dim EnvioSolicitudPeso As Boolean = False

	Private Sub spObtenerPeso()
		If Not EnvioSolicitudPeso Then
			If bwEnviarSolicitudPeso.IsBusy = False Then
				bwEnviarSolicitudPeso.RunWorkerAsync()
			End If
		End If
	End Sub

	Private Sub spDatoPuertoSerial(ByVal sender As System.Object, ByVal e As System.IO.Ports.SerialDataReceivedEventArgs) Handles PuertoSerial.DataReceived
		spAnalizaDatoSerial(PuertoSerial.ReadTo(Chr(13)))
	End Sub

	Public Sub spAnalizaDatoSerial(ByVal rec As String)
		'Retorna el peso S08XXXXXXXXXXX , las XXXXXXXXXXX son el c�digo de barras
		'Se elimina el S08 porque son datos por defecto enviados por el escaner
		'En los c�digos menores a 10 digitos se elimina el primero porque no coincide con los demas lectores instalados.
		Try
			Dim EsCodigo As String = ""

			If Mid(rec, 1, 4) = "S08F" Then
				EsCodigo = Mid(rec, 1, 3)
				rec = rec.Replace("S08F", "")
			Else
				EsCodigo = Mid(rec, 1, 3)
			End If

			If EsCodigo = "S08" Then
				rec = rec.Replace("S08", "")
				If rec.Length < 10 Then
					Me.TxtCodArticulo.Text = rec.Substring(1)
				Else
					Me.TxtCodArticulo.Text = rec.Substring(0)
				End If
				KeyPreview = True
				SendKeys.SendWait("{ENTER}")
			End If
		Catch ex As Exception
			MsgBox(ex.Message)
		End Try

		'Retorna el peso S11
		'Se elimina el S11 porque son datos que representan un cantidad en peso en GRAMOS.
		Try
			Dim EsPeso As String = Mid(rec, 1, 3)
			If EsPeso.Equals("S11") Then
				txtCantidad.Text = rec.Replace("S11", "") / 1000
			End If

		Catch ex As Exception

		End Try

		EnvioSolicitudPeso = False

	End Sub

	Private Sub bwEnviarSolicitudPeso_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bwEnviarSolicitudPeso.DoWork

		Do Until EnvioSolicitudPeso = True

			PuertoSerial.WriteLine("S11" & vbCr)

			EnvioSolicitudPeso = True

		Loop

	End Sub

	Private Sub spIniciarPuertoSerial()
		Try

			Dim Puerto As String = ""
			If Not GetSetting("Seesoft", "SeePos", "PuertoSerial") = "" Then
				Puerto = GetSetting("Seesoft", "SeePos", "PuertoSerial")
			Else
				SaveSetting("Seesoft", "SeePos", "PuertoSerial", "")
			End If

			Control.CheckForIllegalCrossThreadCalls = False
			PuertoSerial.ReadTimeout = 500
			PuertoSerial.Handshake = Handshake.RequestToSend
			PuertoSerial.PortName = Puerto

			If Not PuertoSerial.IsOpen Then
				PuertoSerial.Open()
				EscanerBalanza = True
			End If

		Catch ex As Exception
			MsgBox(ex.Message)
			EscanerBalanza = False
		End Try


	End Sub


#End Region

#Region "Recargar"
	Private Sub Recargas()
		If _Buscar_Apertura(Usua.Cedula) Then

			Dim dt As New DataTable
			Dim IdRecarga As New DataColumn("IdRecarga")
			IdRecarga.DataType = GetType(Integer)
			dt.Columns.Add(IdRecarga)
			Dim Numero As New DataColumn("Numero")
			IdRecarga.DataType = GetType(Integer)
			dt.Columns.Add(Numero)

			Dim cls As New ProcesaRecarga.clsProcesaRecarga(Usua.Cedula)
			cls.spProcesarLLamada(dt)

			If dt.Rows.Count > 0 Then
				For i As Integer = 0 To dt.Rows.Count - 1
					spAgregarDetalleRecarga(dt.Rows(i).Item("IdRecarga"), dt.Rows(i).Item("Numero"))
				Next
			End If

		End If
	End Sub

	Private Sub spAgregarDetalleRecarga(ByVal _IdRecarga As Integer, ByVal _Numero As Integer)

		Dim sql As New SqlClient.SqlCommand
		Dim dt As New DataTable

		sql.CommandText = "SELECT * FROM [Seepos].[dbo].[tb_S_Recarga] where IdRecargaCoope =  " & _IdRecarga & " AND NumeroCelular = " & _Numero

		clsEnlace.spCargarDatos(sql, dt)

		If dt.Rows.Count > 0 Then
			With Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle")
				.CancelCurrentEdit()
				.EndCurrentEdit()
				.AddNew()
				.Current("Codigo") = "0"
				.Current("Descripcion") = "Recarga " & dt.Rows(0).Item("Operador") & " " & dt.Rows(0).Item("NumeroCelular")
				.Current("Cantidad") = 1
				.Current("Precio_Costo") = dt.Rows(0).Item("Monto")
				.Current("Precio_Base") = dt.Rows(0).Item("Monto")
				.Current("Precio_Flete") = 0
				.Current("Precio_Otros") = 0
				.Current("Descuento") = 0
				.Current("Monto_Descuento") = 0
				.Current("Impuesto") = 0
				.Current("Monto_Impuesto") = 0
				.Current("SubtotalGravado") = 0
				.Current("Devoluciones") = 0
				.Current("Numero_Entrega") = 0
				.Current("Max_Descuento") = 0
				.Current("Tipo_Cambio_ValorCompra") = Me.StatusBarPanel8.Text
				.Current("Cod_MonedaVenta") = CB_Moneda.SelectedValue
				.Current("Comision") = 0
				.Current("MontoComision") = 0
				.Current("Id_bodega") = 0
				.Current("Max_Descuento") = 0
				.Current("Precio_Unit") = dt.Rows(0).Item("Monto")
				.Current("SubTotalExcento") = dt.Rows(0).Item("Monto")
				.Current("SubTotal") = dt.Rows(0).Item("Monto")
				.Current("Total_Ficticio") = dt.Rows(0).Item("Monto")
				.EndCurrentEdit()
				Calcular_totales()
				.AddNew()

			End With

		End If


	End Sub

#End Region

#Region "Importar Cotizaci�n"
	Private Sub BuscarCotizacion_importar()
		Try
			If Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Count > 0 Then
				If (MsgBox("Actualmente se est� realizando una venta, si contin�a se perderan los datos de la venta actual, �desea continuar?", MsgBoxStyle.YesNo)) = MsgBoxResult.No Then
					Exit Sub
				End If
			End If

			Me.DataSet_Facturaciones.Ventas_Detalle.Clear()
			Me.DataSet_Facturaciones.Ventas.Clear()
			Me.DataSet_Facturaciones.Cotizacion_Detalle.Clear()
			Me.DataSet_Facturaciones.Cotizacion.Clear()
			Me.opCredito.Checked = False
			Me.opContado.Checked = True
			Dim identificador As Double
			txtTotalArt.Text = 0
			Dim Fx As New cFunciones

			identificador = CDbl(Fx.Buscar_X_Descripcion_Fecha("Select TOP " & TopBuscador & " Cotizacion, Nombre_Cliente, Fecha from Cotizacion where Anulado = 0 and Venta = 0 Order by Fecha DESC", "Nombre_Cliente", "Fecha", "Buscar Cotizacion"))

			If identificador = 0.0 Then ' si se dio en el boton de cancelar
				'Me.buscando = False
				Exit Sub
			End If

			Importando = True
			Me.importar(identificador) 'se importa la cotizaci�n

			Dim valid As Integer = Me.BindingContext(Me.DataSet_Facturaciones, "Cotizacion").Current("Validez")
			Dim Fecha_Cotizacion As Date = Me.BindingContext(Me.DataSet_Facturaciones, "Cotizacion").Current("Fecha")

			If Fecha_Cotizacion.AddDays(valid) < Date.Today Then
				MsgBox("La Cotizaci�n ha vencido, los precios ser�n actualizados", MsgBoxStyle.Information)
				''llamar a la funcion para actualizar precios
				Cambiar_Precios()
			ElseIf ((MsgBox("�Desea actualizar los precios?", MsgBoxStyle.YesNo)) = MsgBoxResult.Yes) Then
				''llamar a la funcion para actualizar precios
				Cambiar_Precios()
			Else
				''llamar a la funcion para dejarlos igual
				Mantener_Precios()

			End If

			'Me.ToolBar1.Buttons(4).Enabled = True
			Me.coti = True
			Importando = False
		Catch ex As SystemException
			MsgBox(ex.Message)
		End Try
	End Sub

	Private Sub Mantener_Precios()
		Try
			Me.DataSet_Facturaciones.Ventas_Detalle.Clear()
			Me.DataSet_Facturaciones.Ventas.Clear()
			txtTotalArt.Text = 0
			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").EndCurrentEdit()
			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").AddNew()


			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("Cod_Cliente") = Me.BindingContext(Me.DataSet_Facturaciones, "Cotizacion").Current("Cod_Cliente")
			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("Nombre_Cliente") = Me.BindingContext(Me.DataSet_Facturaciones, "Cotizacion").Current("Nombre_Cliente")
			'Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("Cedula_Usuario") = Me.BindingContext(Me.DataSet_Facturaciones, "Cotizacion").Current("Cedula")
			'Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("Nombre_usuario") = 
			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("Cedula_Usuario") = Me.Cedula_usuario
			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("Nombre_Usuario") = Me.Nombre_usuario

			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("PagoImpuesto") = Me.BindingContext(Me.DataSet_Facturaciones, "Cotizacion").Current("PagoImpuesto")
			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("Cod_Moneda") = Me.BindingContext(Me.DataSet_Facturaciones, "Cotizacion").Current("CodMoneda")
			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("Moneda_Nombre") = Me.BindingContext(Me.DataSet_Facturaciones, "Cotizacion").Current("MonedaNombre")
			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("Transporte") = Me.BindingContext(Me.DataSet_Facturaciones, "Cotizacion").Current("Transporte")
			Me.impuesto_cliente = Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("PagoImpuesto")
			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").EndCurrentEdit()
			carga_datos_basicos_cliente()
			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").EndCurrentEdit()

			If Me.BindingContext(Me.DataSet_Facturaciones, "Cotizacion").Current("Contado") = True Then
				Me.TxtTipo.Text = "CON"
				Me.opContado.Checked = True
				Me.opCredito.Checked = False
			Else
				Me.TxtTipo.Text = "CRE"
				Me.opCredito.Checked = True
				Me.opContado.Checked = False
			End If


			'''''''''''Meter cotizacion detalle

			Dim i As Integer

			For i = 0 To Me.BindingContext(Me.DataSet_Facturaciones, "Cotizacion.CotizacionCotizacion_Detalle").Count - 1
				Me.BindingContext(Me.DataSet_Facturaciones, "Cotizacion.CotizacionCotizacion_Detalle").Position = i

				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").EndCurrentEdit()
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").AddNew()


				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Codigo") = Me.BindingContext(Me.DataSet_Facturaciones, "Cotizacion.CotizacionCotizacion_Detalle").Current("Codigo")
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Descripcion") = Me.BindingContext(Me.DataSet_Facturaciones, "Cotizacion.CotizacionCotizacion_Detalle").Current("Descripcion")
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Cantidad") = Me.BindingContext(Me.DataSet_Facturaciones, "Cotizacion.CotizacionCotizacion_Detalle").Current("Cantidad")
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Precio_Costo") = Me.BindingContext(Me.DataSet_Facturaciones, "Cotizacion.CotizacionCotizacion_Detalle").Current("Precio_Costo")
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Precio_Base") = Me.BindingContext(Me.DataSet_Facturaciones, "Cotizacion.CotizacionCotizacion_Detalle").Current("Precio_Base")
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Precio_Flete") = Me.BindingContext(Me.DataSet_Facturaciones, "Cotizacion.CotizacionCotizacion_Detalle").Current("Precio_Flete")
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Precio_Otros") = Me.BindingContext(Me.DataSet_Facturaciones, "Cotizacion.CotizacionCotizacion_Detalle").Current("Precio_Otros")
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Precio_Unit") = Me.BindingContext(Me.DataSet_Facturaciones, "Cotizacion.CotizacionCotizacion_Detalle").Current("Precio_Unit")
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Descuento") = Me.BindingContext(Me.DataSet_Facturaciones, "Cotizacion.CotizacionCotizacion_Detalle").Current("Descuento")
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Monto_Descuento") = Me.BindingContext(Me.DataSet_Facturaciones, "Cotizacion.CotizacionCotizacion_Detalle").Current("Monto_Descuento")
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Impuesto") = Me.BindingContext(Me.DataSet_Facturaciones, "Cotizacion.CotizacionCotizacion_Detalle").Current("Impuesto")
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Monto_Impuesto") = Me.BindingContext(Me.DataSet_Facturaciones, "Cotizacion.CotizacionCotizacion_Detalle").Current("Monto_Impuesto")
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("SubtotalGravado") = Me.BindingContext(Me.DataSet_Facturaciones, "Cotizacion.CotizacionCotizacion_Detalle").Current("SubtotalGravado")
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("SubTotalExcento") = Me.BindingContext(Me.DataSet_Facturaciones, "Cotizacion.CotizacionCotizacion_Detalle").Current("SubTotalExcento")
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("SubTotal") = Me.BindingContext(Me.DataSet_Facturaciones, "Cotizacion.CotizacionCotizacion_Detalle").Current("SubTotal")
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Max_Descuento") = Me.BindingContext(Me.DataSet_Facturaciones, "Cotizacion.CotizacionCotizacion_Detalle").Current("Max_Descuento")
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Tipo_Cambio_ValorCompra") = Me.BindingContext(Me.DataSet_Facturaciones, "Cotizacion.CotizacionCotizacion_Detalle").Current("Tipo_Cambio_ValorCompra")
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Cod_MonedaVenta") = Me.BindingContext(Me.DataSet_Facturaciones, "Cotizacion.CotizacionCotizacion_Detalle").Current("Cod_MonedaVenta")

				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").EndCurrentEdit()

			Next

			Me.GroupBox1.Enabled = True
			' Me.GroupBox2.Enabled = True

			Me.GroupBox3.Enabled = True
			If Me.txtDescuento.Properties.ReadOnly = True Then Me.txtDescuento.Properties.Enabled = False
			If Me.txtPrecioUnit.Properties.ReadOnly = True Then Me.txtPrecioUnit.Properties.Enabled = False


			Me.ToolBar1.Buttons(4).Enabled = False
			Me.ToolBar1.Buttons(2).Enabled = True
			Me.SimpleButton1.Enabled = True
			Me.SimpleButton2.Enabled = True

			Calcular_totales()
			Me.GridControl1.Enabled = True
		Catch ex As SystemException
			MsgBox(ex.Message)
		End Try
	End Sub

	Sub carga_datos_basicos_cliente()
		Try
			If Me.txtCodigo.Text <> "0" Then
				Cargar_Cliente(CDbl(Me.txtCodigo.Text))
				Dim rss() As System.Data.DataRow
				Dim rs As System.Data.DataRow

				rss = Me.DataSet_Facturaciones.Clientes.Select("Identificacion ='" & Me.txtCodigo.Text & "'")

				If rss.Length <> 0 Then ' si existe un cliente con ese c�digo

					rs = rss(0)
					Me.tipoprecio = rs("tipoprecio")
					Me.Txtdireccion.Text = rs("direccion")
					Me.txtTelefono.Text = rs("Telefono_01")

					descuento = rs("descuento")

				End If

			End If
		Catch ex As SystemException
			MsgBox(ex.Message)
		End Try

	End Sub
	Private Sub Cambiar_Precios()
		Try

			Me.DataSet_Facturaciones.Ventas_Detalle.Clear()
			Me.DataSet_Facturaciones.Ventas.Clear()

			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").EndCurrentEdit()
			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").AddNew()


			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("Cod_Cliente") = Me.BindingContext(Me.DataSet_Facturaciones, "Cotizacion").Current("Cod_Cliente")
			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("Nombre_Cliente") = Me.BindingContext(Me.DataSet_Facturaciones, "Cotizacion").Current("Nombre_Cliente")
			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("Cedula_Usuario") = Me.Cedula_usuario
			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("Nombre_Usuario") = Me.Nombre_usuario

			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("PagoImpuesto") = Me.BindingContext(Me.DataSet_Facturaciones, "Cotizacion").Current("PagoImpuesto")
			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("Cod_Moneda") = Me.BindingContext(Me.DataSet_Facturaciones, "Cotizacion").Current("CodMoneda")
			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("Moneda_Nombre") = Me.BindingContext(Me.DataSet_Facturaciones, "Cotizacion").Current("MonedaNombre")
			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("Transporte") = Me.BindingContext(Me.DataSet_Facturaciones, "Cotizacion").Current("Transporte")
			Me.impuesto_cliente = Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("PagoImpuesto")
			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").EndCurrentEdit()

			carga_datos_basicos_cliente()

			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").EndCurrentEdit()

			If Me.BindingContext(Me.DataSet_Facturaciones, "Cotizacion").Current("Contado") = True Then
				Me.TxtTipo.Text = "CON"
				Me.opContado.Checked = True
				Me.opCredito.Checked = False
			Else
				Me.TxtTipo.Text = "CRE"
				Me.opCredito.Checked = True
				Me.opContado.Checked = False
			End If


			'''''''''''Meter cotizacion detalle

			Dim i As Integer

			For i = 0 To Me.BindingContext(Me.DataSet_Facturaciones, "Cotizacion.CotizacionCotizacion_Detalle").Count - 1
				Me.BindingContext(Me.DataSet_Facturaciones, "Cotizacion.CotizacionCotizacion_Detalle").Position = i

				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").EndCurrentEdit()
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").AddNew()


				Dim cod As Double = Me.BindingContext(Me.DataSet_Facturaciones, "Cotizacion.CotizacionCotizacion_Detalle").Current("Codigo")
				Me.TxtCodArticulo.Text = cod
				Me.CargarInformacionArticulo(cod.ToString)
				Me.txtCantidad.Text = Me.BindingContext(Me.DataSet_Facturaciones, "Cotizacion.CotizacionCotizacion_Detalle").Current("Cantidad")
				Me.meter_al_detalle_cambiando()
			Next

			Me.GroupBox1.Enabled = True
			'Me.GroupBox2.Enabled = True
			Me.GroupBox3.Enabled = True
			If Me.txtDescuento.Properties.ReadOnly = True Then Me.txtDescuento.Properties.Enabled = False
			If Me.txtPrecioUnit.Properties.ReadOnly = True Then Me.txtPrecioUnit.Properties.Enabled = False

			Me.ToolBar1.Buttons(4).Enabled = False
			Me.ToolBar1.Buttons(2).Enabled = True
			Me.SimpleButton1.Enabled = True
			Me.SimpleButton2.Enabled = True
			Me.GridControl1.Enabled = True
			'Calcular_totales()

		Catch ex As SystemException
			MsgBox(ex.Message)
		End Try
	End Sub

	Private Sub recargar_Cliente()
		Dim rsm As SqlDataReader
		Dim cambio As Double
		Dim cod_mod As Integer
		sqlConexion = CConexion.Conectar



		Dim rss() As System.Data.DataRow
		Dim rs As System.Data.DataRow

		Dim codigo As Integer '= Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("Cod_Cliente")
		codigo = Me.txtCodigo.Text

		rss = Me.DataSet_Facturaciones.Clientes.Select("Identificacion ='" & codigo & "'")

		If rss.Length <> 0 Then ' si existe un cliente con ese c�digo

			Try
				rs = rss(0)
				Me.tipoprecio = rs("tipoprecio")

				cod_mod = rs("CodMonedaCredito")
				If rs("abierto") = "NO" Then

					Me.opCredito.Enabled = False
					Me.opCredito.Checked = False

				Else

					Me.opCredito.Enabled = True
					Me.opContado.Enabled = True
					Me.txtDiasPlazo.Enabled = True

				End If

				If rs("sinrestriccion") = "SI" Then
					sinrestriccion = True
				Else
					sinrestriccion = False
				End If


				rsm = CConexion.GetRecorset(sqlConexion, "Select ValorCompra from Moneda where CodMoneda = " & cod_mod)
				While rsm.Read
					Try
						cambio = rsm("ValorCompra")
					Catch ex As Exception
						MessageBox.Show(ex.Message)
						CConexion.DesConectar(CConexion.Conectar)
					End Try
				End While
				rsm.Close()
				CConexion.DesConectar(CConexion.Conectar)


				max_credito = rs("max_credito") * (cambio / (CDbl(Me.txtTipoCambio.Text)))
				plazo_credito = rs("plazo_credito")
				txtDiasPlazo.Text = plazo_credito
				descuento = rs("descuento")

			Catch ex As SystemException
				MsgBox(ex.Message)
			End Try
		Else
			Me.opCredito.Enabled = False
		End If

	End Sub


	Private Sub Ingresar_Importado()
		Try
			Dim resp As Integer
			If Me.txtCantidad.Text = "" Or Me.txtCantidad.Text = "0" Then
				MsgBox("La Cantidad de art�culos no es v�lida", MsgBoxStyle.Exclamation)
				Me.txtCantidad.Text = "1"
				Exit Sub
			End If
			resp = MessageBox.Show("�Desea agregar este art�culo a la factura?", "Seepos", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)

			If resp <> 6 Then
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
				Me.TxtCodArticulo.Focus()
				Exit Sub
			End If

			'If Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Count > 14 Then
			'    MsgBox("Ha alcanzado el l�mite de la factura", MsgBoxStyle.Information)
			'    Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
			'    Exit Function
			'End If


			If CDbl(Me.txtCantidad.Text) > Me.Existencia Then 'si la cantidad digitada es mayor que la existencia

				If Not Me.vende_existecias_negativas Then
					MsgBox("Usted no puede vender con existencias negativas", MsgBoxStyle.Critical)

					If Me.Existencia <= 0 Then  ' si en el inventario ese articulo tiene existencias negativas
						Me.txtCantidad.Text = 0 ' se vende solo lo que hay en el inventario
					Else
						Me.txtCantidad.Text = Me.Existencia '' se vende solo lo que hay en el inventario
					End If


					If Existencia = 0 Then ' si no hay articulos de ese tipo en el inventario
						Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
						Exit Sub
					End If

				End If

			ElseIf CDbl(Me.txtCantidad.Text) = Me.Existencia Then 'si con esta venta el inventario la existencia sera 0
				'MsgBox("Con esta Venta, la existencia de este art�culo ser� 0, se debe hacer un pedido", MsgBoxStyle.Information)

			End If

			Me.Calculos_Articulo()
			Validar_Punitario()


			If mensaje <> "" Then
				MsgBox(mensaje, MsgBoxStyle.Information, "Seepos")
				mensaje = ""
			End If

			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").EndCurrentEdit()


			Calcular_totales()

			Me.TxtCodArticulo.Focus()

		Catch ex As System.Exception
			MsgBox(ex.Message)
		End Try
	End Sub
#End Region

#Region "Buscar"
	Private Sub BuscarFactura()
		Try
			If Not fnValidarServicio() Then
				Exit Sub
			End If
			If Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Count > 0 Then
				If (MsgBox("Actualmente se est� realizando una venta, si contin�a se perderan los datos de la venta actual, �desea continuar?", MsgBoxStyle.YesNo)) = MsgBoxResult.No Then
					Exit Sub
				End If
			End If

			Me.ToolBar1.Buttons(0).Text = "Nuevo"
			Me.ToolBar1.Buttons(0).ImageIndex = 0
			Me.ToolBar1.Buttons(0).Enabled = True
			Me.ToolBarRecarga.Enabled = False

			Me.DataSet_Facturaciones.Ventas_Detalle.Clear()
			Me.DataSet_Facturaciones.Ventas.Clear()
			txtTotalArt.Text = 0
			CB_Exonerar.Enabled = False
			Me.DataSet_Facturaciones.Cotizacion_Detalle.Clear()
			Me.DataSet_Facturaciones.Cotizacion.Clear()

			Dim identificador As Double
			Dim Fx As New cFunciones
			identificador = CDbl(Fx.Buscar_X_Descripcion_Fecha("Select TOP " & TopBuscador & " Id, cast(cast(cast(num_factura as float) as int) as nvarchar(50)) + '-' + tipo As Factura, Nombre_Cliente as Cliente, Total, Fecha from Ventas where Num_Factura > 0 Order by Fecha DESC", "Cliente", "Fecha", "Buscar Factura de Venta"))
			buscando = True
			If identificador = 0.0 Then ' si se dio en el boton de cancelar
				Me.buscando = False
				Exit Sub
			End If

			Me.LlenarVentas(identificador)

			Me.GroupBox3.Enabled = False
			Me.txtorden.Enabled = False
			Me.ToolBar1.Buttons(2).Enabled = False
			If PMU.Delete And Me.CheckBox1.Checked = False Then Me.ToolBar1.Buttons(3).Enabled = True

			Dim i As Integer

			For i = 0 To Me.BindingContext(Me.DataSet_Facturaciones.Ventas_Detalle).Count - 1 ' busca si en el detalle de la factura existen devolucines
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Position = i
				Me.DataSet_Facturaciones.Ventas_Detalle.Rows(i).Item("Total_Ficticio") = Me.DataSet_Facturaciones.Ventas_Detalle.Rows(i).Item("SubTotal") + Me.DataSet_Facturaciones.Ventas_Detalle.Rows(i).Item("Monto_Impuesto") - Me.DataSet_Facturaciones.Ventas_Detalle.Rows(i).Item("Monto_Descuento")
				If (Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Devoluciones")) <> 0 Then
					MsgBox("Esta Factura no puede ser anulada, porque tiene devoluciones", MsgBoxStyle.Information)
					Me.ToolBar1.Buttons(3).Enabled = False
					Exit For
				End If
			Next i

			Me.ToolBar1.Buttons(4).Enabled = True
			Me.opContado.Enabled = False
			Me.opCredito.Enabled = False
			Me.txtorden.Enabled = False
			Me.SimpleButton1.Enabled = False
			Me.SimpleButton2.Enabled = False
			Me.txtCodigo.Enabled = False
			txtNombre.ReadOnly = True

		Catch ex As SystemException
			MsgBox(ex.Message)
		End Try
	End Sub
#End Region

#Region "Cliente"
	Private Sub CargarInformacionCliente(ByVal codigo As String)
		Dim rss() As System.Data.DataRow
		Dim rs As System.Data.DataRow
		Dim rsm As SqlDataReader
		Dim cod_mod As Integer
		Dim cambio As Double

		sqlConexion = CConexion.Conectar
		If codigo <> Nothing Then

			rss = Me.DataSet_Facturaciones.Clientes.Select("Identificacion ='" & codigo & "'")

			If rss.Length <> 0 Then ' si existe un cliente con ese c�digo

				Try
					rs = rss(0)
					txtCodigo.Text = rs("Identificacion")
					If Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Count > 0 Then
						Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("Cod_Cliente") = rs("Identificacion")
						Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("Nombre_Cliente") = rs("nombre")
					End If

					''''''''''''''''
					txtNombre.Text = rs("nombre")
					txtNombre.ReadOnly = True
					Me.Txtdireccion.Text = rs("direccion")
					Me.txtTelefono.Text = rs("Telefono_01")
					cod_mod = rs("CodMonedaCredito")

					rsm = CConexion.GetRecorset(sqlConexion, "Select ValorCompra from Moneda where CodMoneda = " & cod_mod)
					While rsm.Read
						Try
							cambio = rsm("ValorCompra")
						Catch ex As Exception
							MessageBox.Show(ex.Message)
							CConexion.DesConectar(CConexion.Conectar)
						End Try
					End While
					rsm.Close()



					If rs("abierto") = "NO" Then
						sinrestriccion = False
						Me.opCredito.Enabled = False
						Me.opCredito.Checked = False
						Me.opContado.Checked = True
						MsgBox("El Ciente no tiene Credito, la factura solo puede ser de contado", MsgBoxStyle.Information)
					Else
						If rs("sinrestriccion") = "SI" Then
							sinrestriccion = True
						Else
							sinrestriccion = False
						End If
						Me.opCredito.Enabled = True
						Me.opContado.Enabled = True
						If Not Me.Importando Then Me.opContado.Checked = False : opCredito.Checked = True
						Me.txtDiasPlazo.Enabled = True
					End If


					If Not sqlConexion.State = ConnectionState.Open Then
						sqlConexion = CConexion.Conectar
					End If

					rsm = CConexion.GetRecorset(sqlConexion, "SELECT porcentajeIVA, idTipoCliente FROM TipoCliente tc INNER JOIN Clientes c on id = idTipoCliente WHERE identificacion = " & codigo)
					While rsm.Read
						Try
							imp_iva_cliente = rsm("porcentajeIVA")
							tipoCliente = rsm("idTipoCliente")
						Catch ex As Exception
							MessageBox.Show(ex.Message)
							CConexion.DesConectar(CConexion.sQlconexion)
						End Try
					End While
					rsm.Close()
					CConexion.DesConectar(CConexion.sQlconexion)





					txtDeuda.EditValue = BuscaSaldoCliente(CDbl(txtCodigo.Text))
					txtDeuda.EditValue = Format(txtDeuda.EditValue, "#,#0.00")
					max_credito = rs("max_credito") * (cambio / (CDbl(Me.txtTipoCambio.Text)))
					plazo_credito = rs("plazo_credito")
					txtDiasPlazo.Text = plazo_credito
					descuento = rs("descuento")
					tipoprecio = rs("tipoprecio")
					impuesto_cliente = rs("impuesto")

					''''''''''''''''

					'si actualmente esta cotizacion tiene art�culos
					Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
					If Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Count > 0 Then
						Me.recalcular_cotizacion_cambio_cliente()
						'MsgBox("Factura actualizada de acuerdo al cliente", MsgBoxStyle.Information)
					End If

					Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").AddNew()
					txtExistencia.EditValue = 0
					CargoCliente = True
				Catch ex As Exception
					MessageBox.Show(ex.Message)
				End Try
			Else ' si no se encontro el cliente
				MsgBox("No existe un cliente con ese c�digo", MsgBoxStyle.Exclamation)
				Me.txtCodigo.Text = ""
				Me.txtTelefono.Text = ""
				Me.Txtdireccion.Text = ""
				Me.txtNombre.Text = "CLIENTE CONTADO"
				txtDeuda.EditValue = 0
				Me.txtCodigo.Focus()
				abierto = False
				sinrestriccion = False
				impuesto_cliente = 100
				max_credito = 0
				plazo_credito = 0
				descuento = 0
				tipoprecio = 1
				txtNombre.ReadOnly = False
				Me.opCredito.Enabled = False
				Me.opCredito.Checked = False
				Me.opContado.Checked = True
			End If

		Else 'se dio el boton de cancelar o no se selecciono ninguno
			Me.txtCodigo.Text = ""
			Me.txtTelefono.Text = ""
			Me.Txtdireccion.Text = ""
			Me.txtNombre.Text = "CLIENTE CONTADO"
			abierto = False
			sinrestriccion = False
			impuesto_cliente = 100
			max_credito = 0
			plazo_credito = 0
			descuento = 0
			tipoprecio = 1
			txtNombre.ReadOnly = False
		End If
	End Sub

	Private Sub enter_cod_cliente()
		Me.txtNombre.Text = ""
		Me.txtNombre.ReadOnly = False
		impuesto_cliente = 100

		If Me.txtCodigo.Text <> "" And Me.txtCodigo.Text <> "0" Then
			CargarInformacionCliente(txtCodigo.Text)
			If Me.GroupBox3.Enabled = False Then Me.iniciar_factura()

		Else
			Me.opCredito.Checked = False
			Me.opContado.Checked = True
			Me.opCredito.Enabled = False
			Me.impuesto_cliente = 100
			Me.txtNombre.Text = "CLIENTE CONTADO"
			txtDeuda.EditValue = 0
			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("Nombre_Cliente") = "CLIENTE CONTADO"
			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("Cod_Cliente") = 0
			Me.txtTelefono.Text = ""
			Me.Txtdireccion.Text = ""
			Me.txtCodigo.Text = "0"
			Me.opContado.Checked = True
		End If
		Me.TxtCodArticulo.Focus()
	End Sub

	Private Sub Cargar_Cliente(ByVal Id As Integer)
		Dim cnnv As SqlConnection = Nothing
		Dim dt As New DataTable
		'
		' Dentro de un Try/Catch por si se produce un error
		Try
			'''''''''Cotizacion''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
			' Obtenemos la cadena de conexi�n adecuada
			Dim sConn As String = GetSetting("SeeSOFT", "SeePOS", "CONEXION")
			cnnv = New SqlConnection(sConn)
			cnnv.Open()
			' Creamos el comando para la consulta
			Dim cmdv As SqlCommand = New SqlCommand
			Dim sel As String = "SELECT * FROM Clientes WHERE (identificacion = @Id_Factura) "

			cmdv.CommandText = sel
			cmdv.Connection = cnnv
			cmdv.CommandType = CommandType.Text
			cmdv.CommandTimeout = 90
			' Los par�metros usados en la cadena de la consulta 
			cmdv.Parameters.Add(New SqlParameter("@Id_Factura", SqlDbType.Int))

			cmdv.Parameters("@Id_Factura").Value = Id

			' Creamos el dataAdapter y asignamos el comando de selecci�n
			Dim dv As New SqlDataAdapter
			dv.SelectCommand = cmdv
			' Llenamos la tabla
			dv.Fill(Me.DataSet_Facturaciones, "Clientes")

		Catch ex As System.Exception
			' Si hay error, devolvemos un valor nulo.
			MsgBox(ex.ToString)
		Finally
			' Por si se produce un error,
			' comprobamos si en realidad el objeto Connection est� iniciado,
			' de ser as�, lo cerramos.
			If Not cnnv Is Nothing Then
				cnnv.Close()
			End If
		End Try
	End Sub
#End Region

#Region "Importar"
	Private Sub importar(ByVal Id As Double)

		Dim cnnv As SqlConnection = Nothing
		Dim dt As New DataTable
		'
		' Dentro de un Try/Catch por si se produce un error
		Try
			'''''''''Cotizacion''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
			' Obtenemos la cadena de conexi�n adecuada
			Dim sConn As String = GetSetting("SeeSOFT", "SeePOS", "CONEXION")
			cnnv = New SqlConnection(sConn)
			cnnv.Open()
			' Creamos el comando para la consulta
			Dim cmdv As SqlCommand = New SqlCommand
			Dim sel As String = "SELECT * FROM Cotizacion WHERE (Cotizacion = @Id_Factura) "

			cmdv.CommandText = sel
			cmdv.Connection = cnnv
			cmdv.CommandType = CommandType.Text
			cmdv.CommandTimeout = 90
			' Los par�metros usados en la cadena de la consulta 
			cmdv.Parameters.Add(New SqlParameter("@Id_Factura", SqlDbType.BigInt))

			cmdv.Parameters("@Id_Factura").Value = Id

			' Creamos el dataAdapter y asignamos el comando de selecci�n
			Dim dv As New SqlDataAdapter
			dv.SelectCommand = cmdv
			' Llenamos la tabla
			dv.Fill(Me.DataSet_Facturaciones, "Cotizacion")


			'If Me.BindingContext(Me.DataSet_Facturaciones, "Cotizacion").Current("Contado") = True Then
			'    Me.opContado.Checked = True
			'    Me.opCredito.Checked = False
			'Else
			'    Me.opCredito.Checked = True
			'    Me.opContado.Checked = False

			'End If

			'''''''''LLENAR COTIZACION DETALLES''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

			' Creamos el comando para la consulta
			Dim cmd As SqlCommand = New SqlCommand
			sel = "SELECT * FROM Cotizacion_Detalle WHERE (Cotizacion = @Id_Factura) "

			cmd.CommandText = sel
			cmd.Connection = cnnv
			cmd.CommandType = CommandType.Text
			cmd.CommandTimeout = 90
			' Los par�metros usados en la cadena de la consulta 
			cmd.Parameters.Add(New SqlParameter("@Id_Factura", SqlDbType.BigInt))

			cmd.Parameters("@Id_Factura").Value = Id

			' Creamos el dataAdapter y asignamos el comando de selecci�n
			Dim da As New SqlDataAdapter
			da.SelectCommand = cmd
			' Llenamos la tabla
			da.Fill(Me.DataSet_Facturaciones.Cotizacion_Detalle)


		Catch ex As System.Exception
			' Si hay error, devolvemos un valor nulo.
			MsgBox(ex.ToString)
		Finally
			' Por si se produce un error,
			' comprobamos si en realidad el objeto Connection est� iniciado,
			' de ser as�, lo cerramos.
			If Not cnnv Is Nothing Then
				cnnv.Close()
			End If
		End Try
	End Sub


	Private Sub LlenarVentas(ByVal Id As Double)

		Dim cnnv As SqlConnection = Nothing
		Dim dt As New DataTable
		'
		' Dentro de un Try/Catch por si se produce un error
		Try
			'''''''''LLENAR VENTAS''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
			' Obtenemos la cadena de conexi�n adecuada
			Dim sConn As String = GetSetting("SeeSOFT", "SeePOS", "CONEXION")
			cnnv = New SqlConnection(sConn)
			cnnv.Open()
			' Creamos el comando para la consulta
			Dim cmdv As SqlCommand = New SqlCommand
			Dim sel As String = "SELECT * FROM Ventas WHERE (Id = @Id_Factura) "

			cmdv.CommandText = sel
			cmdv.Connection = cnnv
			cmdv.CommandType = CommandType.Text
			cmdv.CommandTimeout = 90
			' Los par�metros usados en la cadena de la consulta 
			cmdv.Parameters.Add(New SqlParameter("@Id_Factura", SqlDbType.BigInt))

			cmdv.Parameters("@Id_Factura").Value = Id

			' Creamos el dataAdapter y asignamos el comando de selecci�n
			Dim dv As New SqlDataAdapter
			dv.SelectCommand = cmdv
			' Llenamos la tabla
			dv.Fill(Me.DataSet_Facturaciones, "Ventas")

			If Me.TxtTipo.Text = "CON" Then
				Me.opContado.Checked = True
				Me.opCredito.Checked = False
			Else
				Me.opCredito.Checked = True
				Me.opContado.Checked = False
			End If

			'''''''''LLENAR VENTAS DETALLES''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
			' Obtenemos la cadena de conexi�n adecuada
			'Dim sConn As String = GetSetting("SeeSOFT", "SeePOS", "CONEXION")
			'cnn = New SqlConnection(sConn)
			'cnn.Open()

			' Creamos el comando para la consulta
			Dim cmd As SqlCommand = New SqlCommand
			sel = "SELECT * FROM Ventas_Detalle WHERE (Id_Factura = @Id_Factura) "

			cmd.CommandText = sel
			cmd.Connection = cnnv
			cmd.CommandType = CommandType.Text
			cmd.CommandTimeout = 90
			' Los par�metros usados en la cadena de la consulta 
			cmd.Parameters.Add(New SqlParameter("@Id_Factura", SqlDbType.BigInt))

			cmd.Parameters("@Id_Factura").Value = Id

			' Creamos el dataAdapter y asignamos el comando de selecci�n
			Dim da As New SqlDataAdapter
			da.SelectCommand = cmd
			' Llenamos la tabla
			Me.DataSet_Facturaciones.Ventas_Detalle.Clear()
			da.Fill(Me.DataSet_Facturaciones.Ventas_Detalle)


		Catch ex As System.Exception
			' Si hay error, devolvemos un valor nulo.
			MsgBox(ex.ToString)
		Finally
			' Por si se produce un error,
			' comprobamos si en realidad el objeto Connection est� iniciado,
			' de ser as�, lo cerramos.
			If Not cnnv Is Nothing Then
				cnnv.Close()
			End If
		End Try
	End Sub
#End Region

#Region "Anular"
	Private Sub Anular()
		Try

			If PMU.Delete = False Then
				MsgBox("Usted no tiene permisos de anular ventas", MsgBoxStyle.Information)
				Exit Sub
			End If

			Dim resp As Integer

			If Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Count > 0 Then


				resp = MessageBox.Show("�Desea Anular esta Factura?", "Seepos", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)
				If resp = 6 Then
					CheckBox1.Checked = True
					Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").EndCurrentEdit()

					If Me.insertar_bitacora() And Registrar_Anulacion_Venta() Then
						spAnularAjusteInventario()

						'------------------------------------------------------------------ CE
						generarConsec.crearNumeroConsecutivo(BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("Cod_Cliente"), BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("Id"), BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("Num_Factura"), BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("Fecha"), True)

						'------------------------------------------------------------------ CE

						Me.DataSet_Facturaciones.AcceptChanges()
						MsgBox("La Factura ha sido anulada correctamente", MsgBoxStyle.Information)
						Me.DataSet_Facturaciones.Ventas_Detalle.Clear()
						Me.DataSet_Facturaciones.Ventas.Clear()
						txtTotalArt.Text = 0


						Me.ToolBar1.Buttons(4).Enabled = True
						Me.ToolBar1.Buttons(1).Enabled = True


						Me.SimpleButton1.Enabled = False
						Me.SimpleButton2.Enabled = False


						Me.ToolBar1.Buttons(0).Text = "Nuevo"
						Me.ToolBar1.Buttons(0).ImageIndex = 0
						Me.ToolBar1.Buttons(3).Enabled = False
						Me.ToolBar1.Buttons(2).Enabled = False
						Me.ToolBar1.Buttons(4).Enabled = False


						'Me.GroupBox6.Enabled = False
						Me.GroupBox3.Enabled = False
						Me.txtorden.Enabled = False
						'Me.Label10.Visible = False
						'Me.Label11.Visible = False
						'Me.Label_Costobase.Visible = False

						Me.logeado = False
						CB_Exonerar.Enabled = False
						Me.txtUsuario.Enabled = True
						Me.txtUsuario.Text = ""
						Me.txtNombreUsuario.Text = ""
						Me.txtUsuario.Focus()


						Me.ToolBar1.Buttons(3).Enabled = False
						Me.ToolBar1.Buttons(4).Enabled = False
						Me.Loggin_Usuario()
					End If

				Else : Exit Sub

				End If

			End If

		Catch ex As System.Exception
			MsgBox(ex.Message)
		End Try
	End Sub
	Private Sub spAnularAjusteInventario()
		Dim sql As New SqlClient.SqlCommand
		Dim cmd As New SqlClient.SqlCommand
		Dim dt As New DataTable
		Dim strConexion As String = GetSetting("SeeSOFT", "SeePos", "Conexion")
		Dim Aplicapromocion As Boolean
		Dim Fx As New cFunciones

		Try
			cmd.CommandText = "SELECT  count(*)  FROM [Seepos].[dbo].[tb_S_DetallePromocionAplicada] where [IdFactura] = @IdFactura"
			cmd.Parameters.AddWithValue("@IdFactura", Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("Id"))

			clsEnlace.spCargarDatos(cmd, dt)

			If dt.Rows(0).Item(0) > 0 Then
				Aplicapromocion = True
			End If

			sql.CommandText = "UPDATE AjusteInventario  SET [Anula] = 1 WHERE [Consecutivo] = (SELECT [ConsecutivoAjuste] " &
							  " FROM tb_S_DetallePromocionAplicada  where [IdFactura] = @IdFactura )       "
			If Aplicapromocion Then
				sql.CommandText += " UPDATE [Clientes] Set [AcumuladoVentas] = " &
							  " cast((SELECT isNull([AcumuladoVentas],0) FROM [Clientes] where [identificacion] = @Identificacion) as float) " &
							  " + cast((SELECT  isnull([AcumuladoAnterior],0)  FROM [Seepos].[dbo].[tb_S_DetallePromocionAplicada] where [IdFactura] = @IdFactura) as float) " &
							  " where identificacion = @Identificacion "
			Else
				sql.CommandText += " UPDATE [Clientes] Set [AcumuladoVentas] = (" &
						  " cast((SELECT isNull([AcumuladoVentas],0) FROM [Clientes] where [identificacion] = @Identificacion) as float) " &
						  " - @TotalFactura) " &
						  " where identificacion = @Identificacion "
			End If
			sql.Parameters.AddWithValue("@IdFactura", Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("Id"))
			sql.Parameters.AddWithValue("@Identificacion", Me.txtCodigo.Text)
			sql.Parameters.AddWithValue("@TotalFactura", TxtTotal.EditValue)

			Fx.fnEjecutar(sql, strConexion)
		Catch ex As Exception
			MsgBox(ex.Message)
		End Try
	End Sub

	Function insertar_bitacora() As Boolean
		Dim funciones As New Conexion
		Dim datos As String
		datos = "'VENTAS','" & Me.txtFactura.Text & "-" & TxtTipo.Text & "','" & Me.txtNombre.Text & "','FACTURA DE VENTA ANULADA','" & Usua.Nombre & "'," & 0 & "," & 0 & "," & 0 & "," & 0 & "," & 0
		If funciones.AddNewRecord("Bitacora", "Tabla,Campo_Clave,DescripcionCampo,Accion,Usuario,Costo,VentaA,VentaB,VentaC,VentaD", datos) <> "" And funciones.UpdateRecords("OpcionesDePago", "FormaPago = 'ANU'", "(Documento = " & txtFactura.Text & ") AND (TipoDocumento = 'FVC')") <> "" Then
			MsgBox("Problemas al Anular la venta", MsgBoxStyle.Critical)
			Return False
		Else
			Return True
		End If
	End Function

	Private Function Registrar_Anulacion_Venta() As Boolean

		If Me.SqlConnection1.State <> ConnectionState.Open Then Me.SqlConnection1.Open()
		Dim Trans As SqlTransaction = Me.SqlConnection1.BeginTransaction
		Try

			Me.SqlUpdateCommand4.Transaction = Trans

			Me.Adapter_Ventas.Update(Me.DataSet_Facturaciones, "Ventas")

			Trans.Commit()
			Return True


		Catch ex As Exception
			Trans.Rollback()
			MsgBox(ex.Message)
			Me.ToolBar1.Buttons(3).Enabled = True
			Return False
		End Try
	End Function
#End Region

#Region "Registrar"

	Function _Buscar_Apertura(ByVal usuario As String) As Boolean
		Try
			Dim conex As New Conexion
			Dim i As Integer
			i = conex.SQLExeScalar("SELECT COUNT(NApertura) FROM AperturaCaja WHERE (Anulado = 0) AND (Estado = 'A') AND (Cedula = '" & usuario & "')")
			Select Case i
				Case 1
					Return True
				Case 0
					MsgBox(Usua.Nombre & " " & "No tiene una apertura de caja abierta, digite la constrase�a de la caja", MsgBoxStyle.Exclamation)
					Return False
				Case Else
					MsgBox(Usua.Nombre & " " & "tiene mas de una abierta, digite la constrase�a de la caja", MsgBoxStyle.Exclamation)
					Return False
			End Select

		Catch ex As Exception
			MsgBox(ex.Message)
			Return False
		End Try
	End Function
	Private Sub spValidarCliente()
		Dim sql As New SqlClient.SqlCommand
		Dim dt As New DataTable
		Dim Cliente As String = BindingContext(DataSet_Facturaciones, "Ventas").Current("Cod_Cliente")
		Try
			sql.CommandText = "SELECT * FROM [Seepos].[dbo].[Clientes] where identificacion = " & Cliente

			clsEnlace.spCargarDatos(sql, dt)

			If dt.Rows.Count = 0 Then
				BindingContext(DataSet_Facturaciones, "Ventas").Current("Cod_Cliente") = 0
				BindingContext(DataSet_Facturaciones, "Ventas").EndCurrentEdit()
			End If
		Catch ex As Exception

		End Try

	End Sub


	Private Sub Registrar()
		spValidarCliente()
		Dim RegistroPago As Boolean
		RegistroPago = False
		Try
			If buscando = True Then
				MsgBox("No se puede modificar esta factura, solo reimprimirla", MsgBoxStyle.Information)
				Exit Sub
			End If

			Me.ToolBar1.Buttons(2).Enabled = False

			Me.BindingContext(DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()

			If Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Count = 0 Then 'Si la factura no tiene detalle
				MsgBox("No se Puede Registrar una venta si no contiene art�culos", MsgBoxStyle.Critical)
				Me.ToolBar1.Buttons(2).Enabled = False
				BindingContext(DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
				BindingContext(DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").EndCurrentEdit()
				BindingContext(DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").AddNew()
				txtExistencia.EditValue = 0
				Exit Sub
			End If

			If _Buscar_Apertura(Usua.Cedula) Then

				If MessageBox.Show("�Desea Registrar esta Factura?", "Atenci�n...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No Then  'si no desea guardar la facturacion
					If PMU.Update Then Else MsgBox("No tiene permiso para registrar o actualizar datos...", MsgBoxStyle.Information, "Atenci�n...") : Exit Sub
					Me.ToolBar1.Buttons(2).Enabled = True : TxtCodArticulo.SelectAll()
					BindingContext(DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
					BindingContext(DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").EndCurrentEdit()
					BindingContext(DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").AddNew()
					txtExistencia.EditValue = 0
					Exit Sub
				End If

				'--------------------------------------------------------------------------------------
				'VERIFICA EN LAS CONFIGURACIONES SI DESEA PREGUNTAR POR COMISIONISTA - ORA
				'If DataSet_Facturaciones.Config(0).Comisionista Then
				'    If txtComisionista.Text = "" Or txtComisionista.Text = "0" Then
				'        If MessageBox.Show("�Esta seguro que desea registrar la factura sin Comisionista?", "Atenci�n...", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No Then
				'            Me.ToolBar1.Buttons(2).Enabled = True : TxtCodArticulo.SelectAll()
				'            BindingContext(DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
				'            BindingContext(DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").EndCurrentEdit()
				'            BindingContext(DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").AddNew()
				'            txtExistencia.EditValue = 0
				'            Exit Sub
				'        End If
				'    End If
				'End If
				'--------------------------------------------------------------------------------------

				BindingContext(DataSet_Facturaciones, "Ventas").Current("pagoImpuesto") = impuesto_cliente
				BindingContext(DataSet_Facturaciones, "Ventas").Current("Num_Apertura") = Buscar_Apertura()
				BindingContext(DataSet_Facturaciones, "Ventas").Current("Cod_Moneda") = BindingContext(DataSet_Facturaciones, "Moneda").Current("CodMoneda")
				BindingContext(DataSet_Facturaciones, "Ventas").Current("Tipo_Cambio") = BindingContext(DataSet_Facturaciones, "Moneda").Current("ValorCompra")
				BindingContext(DataSet_Facturaciones, "Ventas").EndCurrentEdit()
				DataSet_Facturaciones.aperturacaja.Clear()
				Verificar_Consecutivos()
				BindingContext(DataSet_Facturaciones, "Ventas").EndCurrentEdit()
				If PremiosLealtad Then If CargoCliente Then spPremiosLealtad() ' Se muestran las promociones disponibles --PremiosLealtad--
				If opCredito.Checked = False Then
					Movimiento_Pago_Factura = New frmMovimientoCajaPagoAbono(Usua)
					Movimiento_Pago_Factura.Factura = CDbl(Me.txtFactura.Text)
					Movimiento_Pago_Factura.fecha = "" & dtFecha.Text
					Movimiento_Pago_Factura.Total = TxtTotal.EditValue
					Movimiento_Pago_Factura.Tipo = "FVC"
					Movimiento_Pago_Factura.codmod = Me.DataSet_Facturaciones.Ventas.Rows(0).Item("Cod_Moneda")
					Movimiento_Pago_Factura.ShowDialog()
					FrmVuelto.MontoVuelto = Movimiento_Pago_Factura.vuelto
					RegistroPago = Movimiento_Pago_Factura.Registra

				Else
					RegistroPago = True
					Dim conexc As New Conexion
					Dim dr As SqlDataReader
					dr = conexc.GetRecorset(conexc.Conectar, "SELECT direccion, Telefono_01  FROM Clientes  WHERE (identificacion = " & txtCodigo.Text & ")")
					If dr.Read Then
						Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("Direccion") = dr("direccion")
						Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("Telefono") = dr("Telefono_01")

					End If
				End If

				If RegistroPago = True Then
					Verificar_Consecutivos()
					'SE DEFINE COMO CANCELADA LA FACTURA.
					Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("FacturaCancelado") = True
					Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").EndCurrentEdit()
					If Registrar_SinRestricciones() And Me.RegistrarVenta() Then 'se registra en la base de datos then
						If PremiosLealtad Then spGuardarAcumuladoVentas() ' se registra el acumulado de ventas --PremiosLealtad--
						Me.DataSet_Facturaciones.AcceptChanges()
						If coti = True Then
							Me.BindingContext(Me.DataSet_Facturaciones, "Cotizacion").Current("Venta") = True
							Me.BindingContext(Me.DataSet_Facturaciones, "Cotizacion").EndCurrentEdit()
							Me.Adapter_Coti.Update(Me.DataSet_Facturaciones, "Cotizacion")
							coti = False
						End If
						Dim id As Long = Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("Id")


						'------------------------------------------------------------------ CE

						generarConsec.crearNumeroConsecutivo(BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("Cod_Cliente"), BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("Id"), BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("Num_Factura"), BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("Fecha"))

						'------------------------------------------------------------------ CE

						ToolBar1.Buttons(4).Enabled = True
						Me.ToolBar1.Buttons(1).Enabled = True
						Me.SimpleButton1.Enabled = False
						Me.SimpleButton2.Enabled = False
						Me.ToolBar1.Buttons(0).Text = "Nuevo"
						Me.ToolBar1.Buttons(0).ImageIndex = 0
						Me.ToolBar1.Buttons(3).Enabled = False
						Me.ToolBar1.Buttons(2).Enabled = False
						Me.ToolBar1.Buttons(4).Enabled = False
						Me.GroupBox3.Enabled = False
						Me.txtorden.Enabled = False
						Me.logeado = False
						Me.txtUsuario.Enabled = True
						Me.txtUsuario.Text = ""
						Me.txtNombreUsuario.Text = ""
						Me.txtUsuario.Focus()
						Me.SimpleButton3.Enabled = False
						Me.TxtUtilidad.Text = ""
						Me.Combo_Encargado.Visible = False
						'Me.CheckBoxPVE.Checked = True

						open_cashdrawer()
						'--------------------------------------------------------------------------------------
						'VERIFICA EN LAS CONFIGURACIONES SI DESEA PREGUNTAR POR IMPRIMIR - ORA
						If DataSet_Facturaciones.Config(0).PreguntaImprimir Then
							If MessageBox.Show("�Desea imprimir la Factura?", "Atenci�n...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
								ImprimirFactura()
							End If
						Else
							ImprimirFactura()
						End If
						'--------------------------------------------------------------------------------------
						'ImprimirFactura()
						If opContado.Checked = True Then Me.FrmVuelto.ShowDialog()

						If buscando = True Then buscando = False
						Me.DataSet_Facturaciones.Ventas_Detalle.Clear()
						Me.DataSet_Facturaciones.Ventas.Clear()
						Me.DataSet_Facturaciones.Clientes.Clear()
						txtTotalArt.Text = 0
						CB_Exonerar.Enabled = False
						CB_Moneda.Enabled = False
						Me.ToolBar1.Buttons(2).Enabled = True
						Me.ToolBar1.Buttons(5).Enabled = False
						Me.Loggin_Usuario()
					Else
						MsgBox("Error al Registrar", MsgBoxStyle.Critical)
						Me.ToolBar1.Buttons(2).Enabled = True
					End If
				Else
					Me.ToolBar1.Buttons(2).Enabled = True
					Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
					Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").EndCurrentEdit()
					Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").AddNew()
					txtExistencia.EditValue = 0
					'GridControl1.Enabled = False 
					TxtCodArticulo.Focus()

				End If

			Else
				Exit Sub
			End If

		Catch ex As System.Exception
			Me.ToolBar1.Buttons(2).Enabled = True
			MsgBox(ex.Message)
		End Try

	End Sub
	Dim TienePremioLealtad As Boolean
	Dim IdPromocion As Integer
	Dim AcumuladoVentas As Double
	Private Sub spPremiosLealtad()
		Dim sql As New SqlClient.SqlCommand
		Dim dt As New DataTable
		Dim dt1 As New DataTable
		Dim TotalAcumulado As Double

		Dim AcumuladoPromocion As Double
		Dim frm As New frmPromociones

		Try

			sql.CommandText = "SELECT [AcumuladoVentas] FROM [Seepos].[dbo].[Clientes] where [identificacion] = " & Me.BindingContext(Me.DataSet_Facturaciones, "Clientes").Current("identificacion")
			clsEnlace.spCargarDatos(sql, dt)

			sql.CommandText = "select isnull(min([MontoAcumulado]),0) as [AcumuladoPromocion] from [tb_S_Promociones] where Estado = 0"
			clsEnlace.spCargarDatos(sql, dt1)

			If dt.Rows.Count > 0 Then
				AcumuladoVentas = dt.Rows(0).Item(0)
			Else
				Exit Sub
			End If
			If dt1.Rows.Count > 0 Then
				If dt1.Rows(0).Item(0) > 0 Then AcumuladoPromocion = dt1.Rows(0).Item(0) Else Exit Sub
			End If

			TotalAcumulado = AcumuladoVentas + CDbl(TxtTotal.EditValue)
			If AcumuladoPromocion <= TotalAcumulado Then
				frm.pubAcumuladoVentas = TotalAcumulado
				If frm.ShowDialog() = Windows.Forms.DialogResult.Yes Then
					IdPromocion = frm.pubIdPromocion
					TienePremioLealtad = True
				End If
			End If

			CargoCliente = False

		Catch ex As Exception
			MsgBox(ex.Message)
		End Try

	End Sub
	Private Sub spCargarArticulosPromocion(ByVal _IdPromocion As Integer)
		Dim sql As New SqlClient.SqlCommand
		Dim dt As New DataTable
		Dim TotalSalida As Double

		Try
			sql.CommandText = " SELECT     dbo.tb_S_Paquete_Detalle.Codigo, dbo.tb_S_Paquete_Detalle.Cantidad, dbo.tb_S_Paquete_Detalle.Descripcion, dbo.Inventario.Barras, dbo.Inventario.Costo, " &
					  " dbo.Inventario.Existencia FROM dbo.tb_S_Promociones INNER JOIN dbo.tb_S_Paquetes ON dbo.tb_S_Promociones.IdPaquete = dbo.tb_S_Paquetes.Id INNER JOIN " &
					  " dbo.tb_S_Paquete_Detalle ON dbo.tb_S_Paquetes.Id = dbo.tb_S_Paquete_Detalle.IdPaquete INNER JOIN " &
					  " dbo.Inventario ON dbo.tb_S_Paquete_Detalle.Codigo = dbo.Inventario.Codigo " &
					  " WHERE     (dbo.tb_S_Promociones.Id = @IdPromocion) "
			sql.Parameters.AddWithValue("@IdPromocion", _IdPromocion)
			clsEnlace.spCargarDatos(sql, dt)

			If dt.Rows.Count > 0 Then
				For i As Integer = 0 To dt.Rows.Count - 1
					TotalSalida += dt.Rows(0).Item("Costo")
				Next

				spGuardarAjuste(TotalSalida)
				spGuardarAjusteDetalle(dt)
				spGuardarDetallePromocionAplicada()

			End If
		Catch ex As Exception
			MsgBox(ex.Message)
		End Try

	End Sub
	Private Sub spGuardarDetallePromocionAplicada()

		Dim sql As New SqlClient.SqlCommand
		Dim strConexion As String = GetSetting("SeeSOFT", "SeePos", "Conexion")
		Dim Fx As New cFunciones
		Try
			sql.CommandText = "INSERT INTO tb_S_DetallePromocionAplicada ([IdFactura],[IdPromocion],[ConsecutivoAjuste], [AcumuladoAnterior]) " &
							  " VALUES(@IdFactura,@IdPromocion,@ConsecutivoAjuste,@AcumuladoAnterior)"
			sql.Parameters.AddWithValue("@IdFactura", Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("Id"))
			sql.Parameters.AddWithValue("@IdPromocion", IdPromocion)
			sql.Parameters.AddWithValue("@ConsecutivoAjuste", Cons_Ajuste)
			sql.Parameters.AddWithValue("@AcumuladoAnterior", AcumuladoVentas)
			Fx.fnEjecutar(sql, strConexion)
		Catch ex As Exception
			MsgBox(ex.Message)
		End Try
	End Sub
	Private Sub spGuardarAjuste(ByVal _Total As Double)
		Dim sql As New SqlClient.SqlCommand
		Dim strConexion As String = GetSetting("SeeSOFT", "SeePos", "Conexion")
		Dim Fx As New cFunciones
		Try
			sql.CommandText = "INSERT INTO [AjusteInventario] ([Fecha], [Cedula], [TotalSalida] , [SaldoAjuste])  VALUES  (@Fecha,@Cedula,@TotalSalida,@SaldoAjuste)"
			sql.Parameters.AddWithValue("@Fecha", Now)
			sql.Parameters.AddWithValue("@Cedula", Usua.Cedula)
			sql.Parameters.AddWithValue("@TotalSalida", _Total)
			sql.Parameters.AddWithValue("@SaldoAjuste", (_Total * -1))

			Fx.fnEjecutar(sql, strConexion)
		Catch ex As Exception
			MsgBox(ex.Message)
		End Try

	End Sub
	Dim Cons_Ajuste As String = ""
	Private Sub spGuardarAjusteDetalle(ByVal _dt As DataTable)
		Dim rs As SqlDataReader
		Dim cmdConexion As New SqlClient.SqlConnection
		Dim sqlConexion As New Conexion

		Try

			cmdConexion.Close()
			cmdConexion = sqlConexion.Conectar
			rs = CConexion.GetRecorset(cmdConexion, "Select MAX(Consecutivo) as Consecutivo from AjusteInventario")
			While rs.Read
				Cons_Ajuste = rs("Consecutivo")
			End While

			Dim sql As String = ""

			Dim cmd As New SqlClient.SqlCommand
			Dim strConexion As String = GetSetting("SeeSOFT", "SeePos", "Conexion")
			Dim Fx As New cFunciones
			For i As Integer = 0 To _dt.Rows.Count - 1
				sql += "  INSERT INTO [AjusteInventario_Detalle] ([Cons_Ajuste],[Cod_Articulo],[Desc_Articulo],[Cantidad], " &
					  " [Salida],[observacion],[TotalSalida],[Existencia],[CostoUnit],[Barras] ) " &
					  "   VALUES( " &
					 Cons_Ajuste &
					 "," & _dt.Rows(i).Item("Codigo") &
					 ",'" & _dt.Rows(i).Item("Descripcion") &
					 "'," & _dt.Rows(i).Item("Cantidad") &
					 ",'" & True &
					 "', 'Salida de promocion' " &
					 "," & (_dt.Rows(i).Item("Costo") * _dt.Rows(i).Item("Cantidad")) &
					 "," & _dt.Rows(i).Item("Existencia") &
					 "," & _dt.Rows(i).Item("Costo") &
					 ",'" & _dt.Rows(i).Item("Barras") & "')  "
			Next

			cmd.CommandText = sql
			Fx.fnEjecutar(cmd, strConexion)
		Catch ex As Exception
			MsgBox(ex.Message)
		End Try

	End Sub
	Private Sub spGuardarAcumuladoVentas()
		Try
			Dim sql As New SqlClient.SqlCommand
			Dim dt As New DataTable

			Dim Fx As New cFunciones
			Dim strConexion As String = GetSetting("SeeSoft", "SeePos", "Conexion")

			If DataSet_Facturaciones.Clientes.Rows.Count > 0 Then
				If Me.BindingContext(Me.DataSet_Facturaciones, "Clientes").Current("identificacion") <> "0" Then
					If TienePremioLealtad Then
						sql.CommandText = "UPDATE [Clientes] Set [AcumuladoVentas] = 0,  CantidadPromociones =  cast((SELECT [CantidadPromociones] FROM [Clientes] where [identificacion] = @Identificacion) as int) + 1  where identificacion = @Identificacion"
					Else
						sql.CommandText = "UPDATE [Clientes] Set [AcumuladoVentas] = cast((SELECT [AcumuladoVentas] FROM [Clientes] where [identificacion] = @Identificacion) as float) + @Acumulado  where identificacion = @Identificacion"
					End If
					sql.Parameters.AddWithValue("@Identificacion", Me.BindingContext(Me.DataSet_Facturaciones, "Clientes").Current("identificacion"))
					sql.Parameters.AddWithValue("@Acumulado", TxtTotal.EditValue)
					Fx.fnEjecutar(sql, strConexion)
				End If
				If TienePremioLealtad Then
					spCargarArticulosPromocion(IdPromocion)
				End If
				TienePremioLealtad = False
			End If
		Catch ex As Exception
			TienePremioLealtad = False
			MsgBox(ex.Message)
		End Try

	End Sub

	Function Registrar_SinRestricciones() As Boolean
		Try
			If Me.sinrestriccion Then
				Dim funciones As New Conexion
				Dim mens As String
				mens = funciones.UpdateRecords("Clientes", "sinrestriccion = 'NO'", "identificacion =" & Me.txtCodigo.Text)

				If mens <> "" Then
					MsgBox(mens)
					Return False
				Else
					Return True
				End If

			Else
				Return True
			End If



		Catch ex As SystemException
			MsgBox(ex.Message)
			Return False
		End Try
	End Function
	Function RegistrarVenta() As Boolean

		If Me.SqlConnection1.State <> ConnectionState.Open Then Me.SqlConnection1.Open()

		Dim Trans As SqlTransaction = Me.SqlConnection1.BeginTransaction
		Dim strErrorNumber As String = ""
		Dim roll As String = ""
		Dim ms As String = ""
		Dim strQuery As String = ""
		Try

			Dim x As Integer
			Dim numeroFactura As String = Me.BindingContext(DataSet_Facturaciones.Ventas).Current("Num_Factura")


			'BuscaFactura()
			'pone las conexiones de opciones de pago con la conexion del formulario de ventas
			If Me.opCredito.Checked = False Then
				Movimiento_Pago_Factura.daopcionpago.InsertCommand.Connection = Me.SqlConnection1
				Movimiento_Pago_Factura.daopcionpago.DeleteCommand.Connection = Me.SqlConnection1
				Movimiento_Pago_Factura.daopcionpago.UpdateCommand.Connection = Me.SqlConnection1
				Movimiento_Pago_Factura.daopcionpago.SelectCommand.Connection = Me.SqlConnection1

				Movimiento_Pago_Factura.dadetalleopcionpago.InsertCommand.Connection = Me.SqlConnection1
				Movimiento_Pago_Factura.dadetalleopcionpago.DeleteCommand.Connection = Me.SqlConnection1
				Movimiento_Pago_Factura.dadetalleopcionpago.UpdateCommand.Connection = Me.SqlConnection1
				Movimiento_Pago_Factura.dadetalleopcionpago.SelectCommand.Connection = Me.SqlConnection1
			End If
			'Pone los comandos de transaccion de ventas

			SqlInsertCommand4.CommandText = "INSERT INTO Ventas (Num_Factura, Tipo, Cod_Cliente, Nombre_Cliente, Orden, Cedula_Usuario, Nombre_Usuario, Pago_Comision, SubTotal, Descuento, Imp_Venta, Total, Fecha, Vence, Cod_Encargado_Compra, Contabilizado, AsientoVenta, ContabilizadoCVenta, AsientoCosto, Anulado, PagoImpuesto, FacturaCancelado, Num_Apertura, Entregado, Cod_Moneda, Moneda_Nombre, Direccion, Telefono, SubTotalGravada, SubTotalExento, Transporte, Tipo_Cambio, Observaciones, Exonerar, CodComisionista, NombreComisionista, PagoComision) VALUES     (@Num_Factura,@Tipo,@Cod_Cliente,@Nombre_Cliente,@Orden,@Cedula_Usuario,@Nombre_Usuario,@Pago_Comision,@SubTotal,@Descuento,@Imp_Venta,@Total,@Fecha,@Vence,@Cod_Encargado_Compra,@Contabilizado,@AsientoVenta,@ContabilizadoCVenta,@AsientoCosto,@Anulado,@PagoImpuesto,@FacturaCancelado,@Num_Apertura,@Entregado,@Cod_Moneda,@Moneda_Nombre,@Direccion,@Telefono,@SubTotalGravada,@SubTotalExento,@Transporte,@Tipo_Cambio,@Observaciones,@Exonerar,@CodComisionista,@NombreComisionista,@PagoComision) " &
						"SELECT Id, Num_Factura, Tipo, Cod_Cliente, Nombre_Cliente, Orden, Cedula_Usuario, Nombre_Usuario, Pago_Comision, SubTotal, Descuento, Imp_Venta, Total, Fecha, Vence, Cod_Encargado_Compra, Contabilizado, AsientoVenta, ContabilizadoCVenta, AsientoCosto, Anulado, PagoImpuesto, FacturaCancelado, Num_Apertura, Entregado, Cod_Moneda, Moneda_Nombre, Direccion, Telefono, SubTotalGravada, SubTotalExento, Transporte, Tipo_Cambio, Observaciones, Exonerar, CodComisionista, NombreComisionista, PagoComision FROM Ventas WHERE (Id = SCOPE_IDENTITY())"

			Me.Adapter_Ventas.InsertCommand.Transaction = Trans
			Me.Adapter_Ventas.UpdateCommand.Transaction = Trans

			'SqlInsertCommand.CommandText = " INSERT INTO Ventas_Detalle (Id_Factura, Codigo, Descripcion, Cantidad, Precio_Costo, Precio_Base, Precio_Flete, Precio_Otros, Precio_Unit, Descuento, Monto_Descuento, Impuesto, Monto_Impuesto, SubtotalGravado, SubTotalExcento, SubTotal, Devoluciones, Numero_Entrega, Max_Descuento,  " &
			'		 " Tipo_Cambio_ValorCompra, Cod_MonedaVenta, Comision, MontoComision, Id_bodega) VALUES     (@Id_Factura,@Codigo,@Descripcion, @Cantidad,@Precio_Costo,@Precio_Base,@Precio_Flete,@Precio_Otros,@Precio_Unit,@Descuento,@Monto_Descuento,@Impuesto,@Monto_Impuesto,@SubtotalGravado,@SubTotalExcento,@SubTotal,@Devoluciones,@Numero_Entrega,@Max_Descuento,@Tipo_Cambio_ValorCompra,@Cod_MonedaVenta,@Comision,@MontoComision,@Id_bodega)"

			Me.Adapter_Ventas_Detalles.InsertCommand.Transaction = Trans
			Me.Adapter_Ventas_Detalles.UpdateCommand.Transaction = Trans

			'pone los comandos de opciones de pago en la misma transaccion
			If Me.opCredito.Checked = False Then
				Movimiento_Pago_Factura.daopcionpago.InsertCommand.Transaction = Trans
				Movimiento_Pago_Factura.daopcionpago.UpdateCommand.Transaction = Trans
				Movimiento_Pago_Factura.daopcionpago.DeleteCommand.Transaction = Trans
				Movimiento_Pago_Factura.daopcionpago.SelectCommand.Transaction = Trans

				Movimiento_Pago_Factura.dadetalleopcionpago.InsertCommand.Transaction = Trans
				Movimiento_Pago_Factura.dadetalleopcionpago.UpdateCommand.Transaction = Trans
				Movimiento_Pago_Factura.dadetalleopcionpago.DeleteCommand.Transaction = Trans
				Movimiento_Pago_Factura.dadetalleopcionpago.SelectCommand.Transaction = Trans
			End If
			'Se registra la Venta
			Me.Adapter_Ventas.Update(Me.DataSet_Facturaciones.Ventas)
			Me.Adapter_Ventas_Detalles.Update(Me.DataSet_Facturaciones.Ventas_Detalle)

			If Me.opCredito.Checked = False Then
				For x = 0 To Me.Movimiento_Pago_Factura.DataSet_Opciones_Pago1.OpcionesDePago.Count - 1
					Me.Movimiento_Pago_Factura.DataSet_Opciones_Pago1.OpcionesDePago.Rows(x).Item("Documento") = numeroFactura
					Me.Movimiento_Pago_Factura.BindingContext(Me.Movimiento_Pago_Factura.DataSet_Opciones_Pago1, "OpcionesDePago").EndCurrentEdit()
				Next

				Me.Movimiento_Pago_Factura.BindingContext(Me.Movimiento_Pago_Factura.DataSet_Opciones_Pago1, "OpcionesDePago.OpcionesDePagoDetalle_pago_caja").EndCurrentEdit()
				Me.Movimiento_Pago_Factura.BindingContext(Me.Movimiento_Pago_Factura.DataSet_Opciones_Pago1, "OpcionesDePago.OpcionesDePagoDetalle_pago_caja").AddNew()
				Me.Movimiento_Pago_Factura.BindingContext(Me.Movimiento_Pago_Factura.DataSet_Opciones_Pago1, "OpcionesDePago.OpcionesDePagoDetalle_pago_caja").CancelCurrentEdit()

				For x = 0 To Me.Movimiento_Pago_Factura.DataSet_Opciones_Pago1.Detalle_pago_caja.Count - 1
					Me.Movimiento_Pago_Factura.DataSet_Opciones_Pago1.Detalle_pago_caja.Rows(x).Item("NumeroFactura") = Me.txtFactura.Text
					Me.Movimiento_Pago_Factura.BindingContext(Me.Movimiento_Pago_Factura.DataSet_Opciones_Pago1, "OpcionesDePago.OpcionesDePagoDetalle_pago_caja").EndCurrentEdit()
				Next
				Me.Movimiento_Pago_Factura.daopcionpago.Update(Movimiento_Pago_Factura.DataSet_Opciones_Pago1, "OpcionesDePago")
				Me.Movimiento_Pago_Factura.dadetalleopcionpago.Update(Movimiento_Pago_Factura.DataSet_Opciones_Pago1, "Detalle_pago_caja")
			End If

			Trans.Commit()
			Return True

		Catch ex As Exception
			Trans.Rollback()
			roll = "1"
			strErrorNumber = Err.Number
			MsgBox(ex.Message)
			Me.ToolBar1.Buttons(2).Enabled = True
			Return False
		End Try
	End Function
#End Region

#Region "Nueva Factura"
	Private Sub NuevaFactura()
		Try
			If Me.ToolBar1.Buttons(0).Text = "Nuevo" Then 'n si ya hay un registropendiente por agregar
				Me.ToolBar1.Buttons(0).Text = "Cancelar"
				Me.ToolBar1.Buttons(0).ImageIndex = 8
				Me.ToolBar1.Buttons(3).Enabled = False
				Me.GroupBox3.Enabled = False
				Me.dtFecha.Enabled = False
				Me.txtorden.Enabled = False
				Me.txtUsuario.Enabled = True
				Me.txtUsuario.Text = ""
				Me.txtNombreUsuario.Text = ""
				Me.txtUsuario.Focus()
				Me.logeado = False
				Me.ToolBarRecarga.Enabled = True

				Me.SimpleButton3.Enabled = False
				Me.DataSet_Facturaciones.Ventas_Detalle.Clear()
				Me.DataSet_Facturaciones.Ventas.Clear()
				Me.DataSet_Facturaciones.Cotizacion_Detalle.Clear()
				Me.DataSet_Facturaciones.Cotizacion.Clear()
				Me.Combo_Encargado.Items.Clear()
				Me.Combo_Encargado.Visible = False
				txtTotalArt.Text = 0
				Me.txtDiasPlazo.Visible = False
				Me.Label7.Visible = False
				Me.opContado.Checked = True
				Loggin_Usuario()

			Else
				If Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Count = 0 Then 'Si la factura no tiene detalle
					Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").CancelCurrentEdit()
					Me.SimpleButton1.Enabled = False
					Me.SimpleButton2.Enabled = False
					Me.DataSet_Facturaciones.Ventas_Detalle.Clear()
					Me.DataSet_Facturaciones.Ventas.Clear()
					Me.ToolBar1.Buttons(0).Text = "Nuevo"
					Me.ToolBar1.Buttons(0).ImageIndex = 0
					Me.GroupBox3.Enabled = False
					Me.txtorden.Enabled = False
					Me.dtFecha.Enabled = False
					Me.logeado = False
					Me.txtUsuario.Enabled = True
					Me.txtUsuario.Text = ""
					Me.txtNombreUsuario.Text = ""
					Me.SimpleButton3.Enabled = False
					Me.txtUsuario.Focus()
					Me.Loggin_Usuario()
					Exit Sub
				End If

				If Not fnValidarServicio() Then
					Exit Sub
				End If

				If MessageBox.Show("Desea Guardar esta Factura", "Atenci�n...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
					Me.Registrar() ' se guarda en la base de datos
				Else
					Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").CancelCurrentEdit()
					Me.SimpleButton1.Enabled = False
					Me.SimpleButton2.Enabled = False
					Me.DataSet_Facturaciones.Ventas_Detalle.Clear()
					Me.DataSet_Facturaciones.Ventas.Clear()
					Me.ToolBar1.Buttons(0).Text = "Nuevo"
					Me.ToolBar1.Buttons(0).ImageIndex = 0
					Me.ToolBar1.Buttons(3).Enabled = True
					Me.GroupBox3.Enabled = False
					Me.dtFecha.Enabled = False
					Me.logeado = False
					Me.SimpleButton3.Enabled = False
					Me.txtUsuario.Enabled = True
					Me.txtUsuario.Text = ""
					Me.txtNombreUsuario.Text = ""
					Me.txtUsuario.Focus()
					Me.Loggin_Usuario()

				End If
			End If

		Catch ex As SystemException
			MsgBox(ex.Message)
		End Try
	End Sub
	Function fnValidarServicio() As Boolean
		Dim PuedeSalir As Boolean = True

		If Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Count > 0 Then
			For i As Integer = 0 To Me.DataSet_Facturaciones.Ventas_Detalle.Count - 1
				If Not Me.DataSet_Facturaciones.Ventas_Detalle.Rows(i).RowState = DataRowState.Deleted Then
					If Me.DataSet_Facturaciones.Ventas_Detalle.Rows(i).Item("Codigo") = 0 Then
						PuedeSalir = False
					End If
				End If
			Next
		End If

		If Not PuedeSalir Then
			MessageBox.Show("No puede cancelar la factura porque tiene servicios.", "Atenci�n...", MessageBoxButtons.OK)
			Return False
		End If
		Return True
	End Function

	Private Sub inicializar()
		Try
			If Me.ComboBox1.SelectedIndex = -1 Then
				MsgBox("Debe Seleccionar la Moneda en la que se va a cotizar", MsgBoxStyle.Exclamation)
				Exit Sub
			End If
			Me.GroupBox1.Enabled = True
			Me.dtFecha.Enabled = True
			Me.DtVence.Enabled = True
			Me.ComboBox1.Enabled = False
			Me.SimpleButton3.Enabled = True
			Me.txtCodigo.Enabled = True
			Me.txtNombre.ReadOnly = False
			CB_Exonerar.Enabled = PMU.Others
			Me.iniciar_factura()
			Me.opContado.Enabled = True
			'-----------------------------------------------------------------------
			Dim mi_moneda As Integer = 1
			Try
				mi_moneda = CInt(GetSetting("SeeSOFT", "SeePos", "Moneda"))
			Catch ex As Exception
				SaveSetting("SeeSOFT", "SeePos", "Moneda", "1")
			Finally
				CB_Moneda.SelectedValue = mi_moneda
			End Try
			'-----------------------------------------------------------------------

		Catch ex As System.Exception
			MsgBox(ex.Message)
		End Try
	End Sub

	Private Sub iniciar_factura()
		Try
			Dim nom As String = Me.txtNombre.Text

			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("Cod_Moneda") = Me.BindingContext(Me.DataSet_Facturaciones, "Moneda").Current("CodMoneda")
			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("Cedula_Usuario") = Me.Cedula_usuario
			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("Nombre_Usuario") = Me.Nombre_usuario

			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("pagoImpuesto") = impuesto_cliente
			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("Tipo_Cambio") = CDbl(txtTipoCambio.Text)
			Me.txtNombre.Text = nom

			If Me.txtCodigo.Text = "" Then
				Me.txtCodigo.Text = "0"
			End If

			If Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Count = 1 Then
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").EndCurrentEdit()
				dias_credito()
			End If

			Me.GridControl1.Enabled = True
			Me.SimpleButton1.Enabled = True

			Me.ToolBar1.Buttons(2).Enabled = True 'se activa el botond e guardar

			Me.GroupBox3.Enabled = True
			If Me.txtDescuento.Properties.ReadOnly = True Then Me.txtDescuento.Properties.Enabled = False
			If Me.txtPrecioUnit.Properties.ReadOnly = True Then Me.txtPrecioUnit.Properties.Enabled = False

			Me.txtCantidad.Text = 1

			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").EndCurrentEdit()
			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").AddNew()
			txtExistencia.EditValue = 0
			Me.TxtCodArticulo.Focus()

		Catch ex As System.Exception
			MsgBox(ex.Message)
		End Try
	End Sub
#End Region

#Region "Articulo"
	Private Function InformacionArticulo(ByVal codigo As String) As SqlDataReader
		Dim ConexionX As New Conexion
		Dim RS As SqlDataReader

		Try
			sqlConexion.Close()
			sqlConexion = CConexion.Conectar

			If EsCodigoBarras Then
				Return CConexion.GetRecorset(sqlConexion, "SELECT Inventario.Max_Descuento, Inventario.Precio_Promo, Inventario.Promo_Activa, Inventario.Codigo, Inventario.Barras, Inventario.Descripcion, Inventario.SubFamilia, ArticulosXBodega.Existencia, Inventario.PrecioBase, Inventario.Fletes, Inventario.OtrosCargos, Inventario.Costo, Inventario.MonedaCosto, Inventario.MonedaVenta, Inventario.Precio_A, Inventario.Precio_B, Inventario.Precio_C, Inventario.Precio_D, Inventario.IVenta, Inventario.PreguntaPrecio, Inventario.Servicio, Familia.Comision, Familia.SobrePrecio FROM Inventario INNER JOIN  Presentaciones ON Presentaciones.CodPres = Inventario.CodPresentacion INNER JOIN SubFamilias ON Inventario.SubFamilia = SubFamilias.Codigo INNER JOIN  Familia ON SubFamilias.CodigoFamilia = Familia.Codigo INNER JOIN  ArticulosXBodega ON Inventario.Codigo = ArticulosXBodega.Codigo WHERE (Inhabilitado = 0) and (ArticulosXBodega.Idbodega=" & iddebodega & ") and Barras = '" & codigo & "'")
			Else
				If Not IsNumeric(codigo) Then
					Return CConexion.GetRecorset(sqlConexion, "SELECT Inventario.Max_Descuento, Inventario.Precio_Promo, Inventario.Promo_Activa, Inventario.Codigo, Inventario.Barras, Inventario.Descripcion, Inventario.SubFamilia, ArticulosXBodega.Existencia, Inventario.PrecioBase, Inventario.Fletes, Inventario.OtrosCargos, Inventario.Costo, Inventario.MonedaCosto, Inventario.MonedaVenta, Inventario.Precio_A, Inventario.Precio_B, Inventario.Precio_C, Inventario.Precio_D, Inventario.IVenta, Inventario.PreguntaPrecio, Inventario.Servicio, Familia.Comision, Familia.SobrePrecio FROM Inventario INNER JOIN  Presentaciones ON Presentaciones.CodPres = Inventario.CodPresentacion INNER JOIN SubFamilias ON Inventario.SubFamilia = SubFamilias.Codigo INNER JOIN  Familia ON SubFamilias.CodigoFamilia = Familia.Codigo INNER JOIN  ArticulosXBodega ON Inventario.Codigo = ArticulosXBodega.Codigo WHERE (Inhabilitado = 0) and (ArticulosXBodega.Idbodega=" & iddebodega & ") and Barras = '" & codigo & "'")
				Else
					Return CConexion.GetRecorset(sqlConexion, "SELECT Inventario.Max_Descuento, Inventario.Precio_Promo, Inventario.Promo_Activa, Inventario.Codigo, Inventario.Barras, Inventario.Descripcion, Inventario.SubFamilia, ArticulosXBodega.Existencia, Inventario.PrecioBase, Inventario.Fletes, Inventario.OtrosCargos, Inventario.Costo, Inventario.MonedaCosto, Inventario.MonedaVenta, Inventario.Precio_A, Inventario.Precio_B, Inventario.Precio_C, Inventario.Precio_D, Inventario.IVenta, Inventario.PreguntaPrecio, Inventario.Servicio, Familia.Comision, Familia.SobrePrecio FROM Inventario INNER JOIN  Presentaciones ON Presentaciones.CodPres = Inventario.CodPresentacion INNER JOIN SubFamilias ON Inventario.SubFamilia = SubFamilias.Codigo INNER JOIN  Familia ON SubFamilias.CodigoFamilia = Familia.Codigo INNER JOIN  ArticulosXBodega ON Inventario.Codigo = ArticulosXBodega.Codigo WHERE (Inhabilitado = 0) and (ArticulosXBodega.Idbodega=" & iddebodega & ") and (Inventario.Codigo =" & codigo & " )")
				End If
			End If
			Return RS

		Catch ex As Exception
			MsgBox(ex.Message)
		Finally
			'RS.Close()
		End Try
	End Function

	Private Function CargarInformacionArticulo(ByVal codigo As String, Optional ByVal recargar As Boolean = False) As Boolean
		Dim rs As SqlDataReader
		Dim Encontrado As Boolean
		If codigo <> Nothing Then
			sqlConexion = CConexion.Conectar
			'rs = InformacionArticulo(codigo)
			rs = CConexion.GetRecorset(sqlConexion, "SELECT top 1  Max_Descuento, Precio_Promo, Promo_Activa, Inventario.Codigo, Barras, Alterno, dbo.Inventario.Descripcion AS Descripcion , SubFamilia, Inventario.Existencia, PrecioBase, Fletes, OtrosCargos, Costo, MonedaCosto, MonedaVenta, Precio_A, Precio_B, Precio_C, Precio_D, cb.porcentajeIVA as 'IVenta', PreguntaPrecio, Servicio, A.Costo_Promedio as Costo_Promedio_Bodega " &
														" from Inventario INNER JOIN Presentaciones ON Presentaciones.CodPres = Inventario.CodPresentacion inner join ArticulosXBodega as A on Inventario.Codigo = A.Codigo INNER JOIN SubFamilias as sf on sf.Codigo = Inventario.SubFamilia INNER JOIN Cabys as cb on cb.id = sf.idCabys WHERE (Inhabilitado = 0) And Inventario.Codigo = '" & codigo & "'")
			Encontrado = False
			While rs.Read
				Try
					Encontrado = True
					'BindingContext(DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Comision") = rs("Comision")
					'BindingContext(DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Impuesto") = rs("IVenta")
					TxtCodArticulo.Text = rs("Codigo")
					codigo = rs("Codigo")
					AgregandoNuevoItem = True
					If Excento(codigo) = True Then
						txtNombreArt.Text = rs("Descripcion")
					Else
						txtNombreArt.Text = rs("Descripcion")
					End If

					'IMPUESTO CABYS E IMPUESTO TIPO CLIENTE
					imp_producto = Format(rs("IVenta"), "#,#0.00")
					cargarImpuestoIVA()
					'-------------------------------------------------

					BindingContext(DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Barras") = rs("Barras")
					txtNombreArt.Text = rs("Descripcion")
					txtSubFamilia.Text = rs("SubFamilia")
					PrecioBase = rs("PrecioBase")
					txtCostoBase.Text = PrecioBase
					Max_Descuento_Articulo = rs("Max_Descuento")
					TxtMaxdescuento.Text = Me.Max_Descuento_Articulo
					Max_Descuento_Articulo = rs("Max_Descuento")
					PrecioCosto = rs("Costo")
					TxtprecioCosto.Text = rs("Costo")
					Flete = rs("Fletes")
					txtFlete.Text = Flete
					OtrosMontos = rs("OtrosCargos")
					txtOtros.Text = OtrosMontos
					If CB_Exonerar.Checked Then
						txtImpVenta.Text = Format(0, "#,#0.00")
					Else
						txtImpVenta.Text = Format(rs("IVenta"), "#,#0.00")
						imp_producto = Format(rs("IVenta"), "#,#0.00")
						cargarImpuestoIVA()
					End If
					If rs("Servicio") Then
						Existencia = 1000
					Else
						Existencia = rs("Existencia")
					End If
					txtExistencia.EditValue = rs("Existencia")
					If rs("PreguntaPrecio") Then
						If recargar = False Then
							Dim Precio As New Monto_Transporte_Ventas
							Precio.Text = "Establecer Precio"
							Precio.GroupBox1.Text = "Digite el precio del articulo"
							If tipoprecio = 1 Then
								Precio.TextNumero.Text = rs("Precio_A") * (1 + (CDbl(txtImpVenta.Text) / 100))
							ElseIf tipoprecio = 2 Then
								Precio.TextNumero.Text = rs("Precio_B") * (1 + (CDbl(txtImpVenta.Text) / 100))
							ElseIf tipoprecio = 3 Then
								Precio.TextNumero.Text = rs("Precio_C") * (1 + (CDbl(txtImpVenta.Text) / 100))
							ElseIf tipoprecio = 4 Then
								Precio.TextNumero.Text = rs("Precio_D") * (1 + (CDbl(txtImpVenta.Text) / 100))
							End If
							Precio.ShowDialog()
							If Precio.DialogResult = Windows.Forms.DialogResult.OK Then
								PrecioA = (Precio.Monto / (1 + (CDbl(txtImpVenta.Text) / 100)))
								PrecioB = (Precio.Monto / (1 + (CDbl(txtImpVenta.Text) / 100)))
								PrecioC = (Precio.Monto / (1 + (CDbl(txtImpVenta.Text) / 100)))
								PrecioD = (Precio.Monto / (1 + (CDbl(txtImpVenta.Text) / 100)))
							Else
								Precio.Dispose()
								rs.Close()
								'GridControl1.Enabled = False
								TxtCodArticulo.Focus()
								CConexion.DesConectar(CConexion.Conectar)
								Cambio_Cantidad = False
								Return False
							End If
							Precio.Dispose()
						Else
							PrecioA = Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Precio_Unit")
							PrecioB = Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Precio_Unit")
							PrecioC = Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Precio_Unit")
							PrecioD = Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Precio_Unit")
						End If
					Else
						PrecioA = rs("Precio_A")
						PrecioB = rs("Precio_B")
						PrecioC = rs("Precio_C")
						PrecioD = rs("Precio_D")
					End If
					precio_promo_valor = rs("Precio_Promo")
					MonedaCosto = rs("MonedaVenta") 'rs("MonedaCosto")
					Txtcodmoneda_Venta.Text = rs("MonedaVenta")
					MonedaBase = rs("MonedaCosto")
					MonedaVenta = BindingContext(DataSet_Facturaciones, "Moneda").Current("CodMoneda")
					If rs("Promo_Activa") = True Then ' si el articulo esta en promocion
						promo_activa_valor = True
						txtDescuento.Enabled = False
					Else
						promo_activa_valor = False ' se habilita el text
						If txtDescuento.Properties.ReadOnly = False Then Me.txtDescuento.Enabled = True
					End If
				Catch ex As Exception
					MessageBox.Show(ex.Message)
					CConexion.DesConectar(CConexion.Conectar)
				End Try
			End While
			rs.Close()
			If Not Encontrado Then
				'MsgBox("No existe o est� inhabilitado un art�culo con este c�digo", MsgBoxStyle.Critical)
n:
				If MessageBox.Show("No existe o est� inhabilitado un art�culo con este c�digo" & vbCrLf & "Desea Continuar?", "Atenci�n...", MessageBoxButtons.YesNo, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
					'GridControl1.Enabled = False
					Me.TxtCodArticulo.Focus()
					CConexion.DesConectar(CConexion.Conectar)
					Cambio_Cantidad = False
					txtExistencia.EditValue = 0
					Return False
				Else
					GoTo n
				End If
			End If
			CConexion.DesConectar(sqlConexion)
			sqlConexion = CConexion.Conectar
			rs = CConexion.GetRecorset(sqlConexion, "Select ValorCompra from Moneda where CodMoneda = " & MonedaCosto)
			While rs.Read
				Try
					ValorCosto = rs("ValorCompra")
				Catch ex As Exception
					MessageBox.Show(ex.Message)
					CConexion.DesConectar(CConexion.Conectar)
				End Try
			End While
			rs.Close()
			'Consulta el Tipo de Cambio de la Moneda usada para la Venta
			CConexion.DesConectar(sqlConexion)
			sqlConexion = CConexion.Conectar
			rs = CConexion.GetRecorset(sqlConexion, "Select ValorCompra from Moneda where CodMoneda = " & CInt(Me.Txtcodmoneda_Venta.Text))
			While rs.Read
				Try
					Txt_TipoCambio_Valor_Compra.Text = rs("ValorCompra")
				Catch ex As Exception
					MessageBox.Show(ex.Message)
					CConexion.DesConectar(CConexion.Conectar)
				End Try
			End While
			rs.Close()
			ValorVenta = CDbl(Me.txtTipoCambio.Text)
			CConexion.DesConectar(sqlConexion)
			sqlConexion = CConexion.Conectar
			rs = CConexion.GetRecorset(sqlConexion, "Select ValorCompra from Moneda where CodMoneda = " & MonedaBase)
			While rs.Read
				Try
					ValorBase = rs("ValorCompra")
				Catch ex As Exception
					MessageBox.Show(ex.Message)
					CConexion.DesConectar(CConexion.Conectar)
				End Try
			End While
			rs.Close()
			Calculo_precio_unitario()
			If Me.txtNombreArt.Text <> "" Then 'si se recuperaron los datos de un articulo
				If Me.txtPrecioUnit.Enabled = True Then 'si el usuario puede disminuir o aumentar el costo del articulo
					Me.txtPrecioUnit.Focus()
				Else
					If Me.txtDescuento.Enabled = True Then
						txtDescuento.Focus()
					Else
						Me.txtCantidad.Focus()
					End If
				End If
				'Me.GridControl1.Enabled = False
				Cambio_Cantidad = False
				Return True
			End If
		Else ' si no se recupero ningun articulo, se termina la edicion
			Try
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
				Me.TxtCodArticulo.Focus()
			Catch ex As SystemException
				MsgBox(ex.Message)
				CConexion.DesConectar(CConexion.Conectar)
			End Try
			CConexion.DesConectar(CConexion.Conectar)
		End If
		Me.TxtUtilidad.Text = Utilidad(Me.txtCostoBase.Text, (Me.txtPrecioUnit.Text - txtFlete.Text - txtOtros.Text))
	End Function

	Private Sub Calculo_precio_unitario()
		Try
			If Me.promo_activa_valor Then ' si el articulo esta actualmente en promoci�n 
				txtPrecioUnit.Text = Math.Round((Me.precio_promo_valor * (ValorCosto / ValorVenta)), 2)

				'Calculo de Costo
				txtCostoBase.Text = (CDbl(txtCostoBase.Text) * (ValorBase / ValorVenta))
				PrecioBase = txtCostoBase.Text
				txtFlete.Text = (CDbl(txtFlete.Text) * (ValorBase / ValorVenta))
				txtOtros.Text = (CDbl(txtOtros.Text) * (ValorBase / ValorVenta))
				Me.precio_unitario = txtPrecioUnit.Text


				'Me.TxtUtilidad.Text = Utilidad(Me.txtCostoBase.Text, (Me.txtPrecioUnit.Text - txtFlete.Text - txtOtros.Text))
				Exit Sub
			End If

			If Me.tipoprecio = 0 Then
				Me.tipoprecio = 1
			End If

			'Calculos para el Precio Unitario
			Select Case tipoprecio
				Case 1 : txtPrecioUnit.Text = Math.Round((PrecioA * (ValorCosto / ValorVenta)), 2)
				Case 2 : txtPrecioUnit.Text = Math.Round((PrecioB * (ValorCosto / ValorVenta)), 2)
				Case 3 : txtPrecioUnit.Text = Math.Round((PrecioC * (ValorCosto / ValorVenta)), 2)
				Case 4 : txtPrecioUnit.Text = Math.Round((PrecioD * (ValorCosto / ValorVenta)), 2)
			End Select

			If txtNomComisionista.Text <> "" Then
				txtPrecioUnit.Text += (txtPrecioUnit.Text * (BindingContext(DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("sobrePrecio") / 100))
			End If

			'Calculo de Costo
			txtCostoBase.Text = (CDbl(txtCostoBase.Text) * (ValorBase / ValorVenta))
			PrecioBase = txtCostoBase.Text
			txtFlete.Text = (CDbl(txtFlete.Text) * (ValorBase / ValorVenta))
			txtOtros.Text = (CDbl(txtOtros.Text) * (ValorBase / ValorVenta))
			TxtprecioCosto.Text = (CDbl(Me.TxtprecioCosto.Text) * (ValorBase / ValorVenta))
			precio_unitario = txtPrecioUnit.Text
			txtPrecioUnit.Text = precio_unitario
			'Me.TxtUtilidad.Text = Utilidad(Me.txtCostoBase.Text, (Me.txtPrecioUnit.Text - txtFlete.Text - txtOtros.Text))
		Catch ex As SystemException
			MsgBox(ex.Message)
		End Try
	End Sub

	'5 primeros es el codigo(menos los 2 primeros)
	'5 segundos es el peso(menos el ultimo)
	Dim EsCodigoBarras As Boolean
	Private Function GetTipoArticulo(ByRef _id As String, ByRef cant As Decimal) As Boolean
		Try
			Dim c, _codigo, _peso, _decimal, _precio, precioIVA As String

			If Len(_id) = 13 Then
				c = _id.Substring(0, 2)
				If c = 20 Then
					'_codigo = _id.Substring(2, 6)
					_codigo = _id.Substring(0, 6)
					_peso = _id.Substring(7, 2)
					_precio = _id.Substring(6, 5)
					'_decimal = _id.Substring(9, 3)
					_decimal = _id.Substring(12, 1)

					Dim SQL As New GestioDatos
					Dim dts As New DataTable
					dts = SQL.Ejecuta("select * from inventario where Inhabilitado = 0 and BARRAS = '" & _codigo & "'")
					If dts.Rows.Count > 0 Then
						_id = _codigo
						Me.txtCantidad.Text = CDec(_peso & "." & _decimal)
						cant = CDec(_peso & "." & _decimal)
						'---------------------------------------------
						precioIVA = CDbl(dts.Rows(0).Item("Precio_A")) * (CDbl(dts.Rows(0).Item("IVenta")) / 100)
						precioIVA = CDbl(dts.Rows(0).Item("Precio_A")) + precioIVA
						_peso = CDbl(_precio) / (precioIVA)
						Me.txtCantidad.Text = CDec(_peso)
						cant = CDbl(_precio) / (precioIVA)
						Return True
					Else
						Return False
					End If
				Else
					Return False
				End If
			Else
				Return False
			End If
		Catch ex As Exception
			Return False
		End Try
	End Function

	Private Function fnObtenerProducto(ByRef _Codigo As String, ByRef _Peso As Decimal) As Boolean
		Try
			Dim Codigo As String
			Dim Peso As String
			Dim Precio As String
			Dim precioIVA As String
			'Codigo = _Codigo.Substring(1, 5)
			Codigo = _Codigo.Substring(0, 6)
			'Peso = _Codigo.Substring(6, 5)
			Peso = _Codigo.Substring(6, 5)
			Precio = _Codigo.Substring(6, 5)
			Dim SQL As New GestioDatos
			Dim dts As New DataTable
			dts = SQL.Ejecuta("select * from inventario where Barras = '" & Codigo & "'")
			If dts.Rows.Count > 0 Then
				If dts.Rows(0).Item("EsPesado") Then
					_Codigo = Codigo
					cod_art = CDbl(dts.Rows(0).Item("Codigo"))
					precioIVA = CDbl(dts.Rows(0).Item("Precio_A")) * (CDbl(dts.Rows(0).Item("IVenta")) / 100)
					precioIVA = CDbl(dts.Rows(0).Item("Precio_A")) + precioIVA
					_Peso = CDbl(Precio) / (precioIVA)
					Peso = CDbl(Precio) / (precioIVA)
					Return True
				Else
					Return False
				End If
			Else
				Return False
			End If
			Return False
		Catch ex As Exception
			Return False
		End Try
	End Function

	Private Sub meter_al_detalle()
		Try
			If Me.txtCantidad.Text = "" Or Me.txtCantidad.Text = "0" Then
				MsgBox("La Cantidad de art�culos no es v�lida", MsgBoxStyle.Exclamation)
				Me.txtCantidad.Text = "1"
				Exit Sub
			End If
			If CDbl(Me.txtCantidad.Text) > Me.Existencia Then 'si la cantidad digitada es mayor que la existencia
				If Not Me.vende_existecias_negativas Then
					MsgBox("Usted no puede vender con existencias negativas", MsgBoxStyle.Critical)
					If Me.Existencia <= 0 Then  ' si en el inventario ese articulo tiene existencias negativas
						Me.txtCantidad.Text = 0 ' se vende solo lo que hay en el inventario
					Else
						Me.txtCantidad.Text = Me.Existencia '' se vende solo lo que hay en el inventario
					End If

					If Existencia = 0 Then ' si no hay articulos de ese tipo en el inventario
						Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
						Exit Sub
					End If

				End If

			ElseIf CDbl(Me.txtCantidad.Text) = Me.Existencia Then 'si con esta venta el inventario la existencia sera 0
				MsgBox("Con esta Venta, la existencia de este art�culo ser� 0, se debe hacer un pedido", MsgBoxStyle.Information)
			End If
			Calculos_Articulo()
			Validar_Punitario()

			If mensaje <> "" Then
				MsgBox(mensaje, MsgBoxStyle.Information, "Seepos")
				mensaje = ""
			End If

			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").EndCurrentEdit()


			Calcular_totales()
			Me.TxtCodArticulo.Focus()
			Me.GridControl1.Enabled = True

			'Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Position = Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Count - 1
		Catch ex As System.Exception
			MsgBox(ex.Message)
		End Try
	End Sub


	Private Sub meter_al_detalle_cambiando()
		Try
			If CDbl(Me.txtCantidad.Text) > Me.Existencia Then 'si la cantidad digitada es mayor que la existencia
				'MsgBox("No Existen " + txtCantidad.Text + " art�culos, la Existencia en el inventario es de " + Me.Existencia.ToString + " ,debe hacer un pedido", MsgBoxStyle.Exclamation)

				If Not Me.vende_existecias_negativas Then
					MsgBox("Usted no puede vender con existencias negativas", MsgBoxStyle.Critical)

					If Me.Existencia <= 0 Then  ' si en el inventario ese articulo tiene existencias negativas
						Me.txtCantidad.Text = 0 ' se vende solo lo que hay en el inventario
					Else
						Me.txtCantidad.Text = Me.Existencia '' se vende solo lo que hay en el inventario
					End If


					If Existencia = 0 Then ' si no hay articulos de ese tipo en el inventario
						Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
						Exit Sub
					End If

				End If

			ElseIf CDbl(Me.txtCantidad.Text) = Me.Existencia Then 'si con esta venta el inventario la existencia sera 0
				'MsgBox("Con esta Venta, la existencia de este art�culo ser� 0, se debe hacer un pedido", MsgBoxStyle.Information)
			End If

			Me.Calculos_Articulo()
			Validar_Punitario()


			If mensaje <> "" Then
				MsgBox(mensaje, MsgBoxStyle.Information, "Seepos")
				mensaje = ""
			End If

			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").EndCurrentEdit()

			Calcular_totales()
			Me.TxtCodArticulo.Focus()

		Catch ex As System.Exception
			MsgBox(ex.Message)
		End Try
	End Sub

	Private Sub calculo_descuento_articulo()
		Try
			If CDbl(Me.txtDescuento.Text) > 0 Then 'si el articulo tiene un descuento

				'''''''''''''''''''''''''''''PROMO'''''''''''''''''''''' ACTIVA''''''''''''''''''''''''''''''''''''''''''
				If Me.promo_activa_valor Then 'si el articulo esta en promocion 
					Me.txtDescuento.Text = 0.0
					DescuentoCalc = 0
					Me.txtmontodescuento.Text = DescuentoCalc ' p one el monto de descuento
					mensaje = mensaje + "Cod: " + Me.TxtCodArticulo.Text + " Se Aplic� un: (" + Me.txtDescuento.Text + ") El art�culo est� en promoci�n" + vbCrLf
					Exit Sub
				End If
				''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

				'SI EL ARTICULO NO PERMITE DESCUENTO
				If CDbl(TxtMaxdescuento.Text) = 0 Then
					'Si el articulo  no permite que se le realize un descuento
					Me.txtDescuento.Text = 0.0
					DescuentoCalc = 0
					Me.txtmontodescuento.Text = DescuentoCalc ' p one el monto de descuento
					mensaje = mensaje + "Cod: " + Me.TxtCodArticulo.Text + " Se Aplic� un: (" + Me.txtDescuento.Text + ") El art�culo no permite descuento" + vbCrLf
					Exit Sub
				End If


				'SI EL USUARIO NO PUEDE DAR DESCUENTO

				If Me.porcentaje_descuento = 0 Then
					MsgBox("Usted no puede realizar descuentos", MsgBoxStyle.Critical)
					Me.txtDescuento.Text = 0.0
					DescuentoCalc = 0
					Me.txtmontodescuento.Text = DescuentoCalc ' p one el monto de descuento
					Exit Sub
				End If


				' validar si el descuento se puede aplicar o no el descuento

				''''''''''''''''''''''''''''''''''''''''''''''''''''
				' max_aplicar = CDbl(Me.TxtMaxdescuento.Text) * Me.porcentaje_descuento / 100

				'If CDbl(Me.TxtMaxdescuento.Text) < CDbl(Me.txtDescuento.Text) Then
				'    MsgBox("El porcentaje descuento supera el monto m�ximo permitido.", MsgBoxStyle.Critical)
				'    Exit Sub
				'Else
				max_aplicar = CDbl(Me.TxtMaxdescuento.Text)
				'End If



				'SI EL DESCUENTO DESEADO ES TOTALMENTE APLICABLE AL ARTICULO
				If CDbl(Me.txtDescuento.Text) <= max_aplicar Then
					'si el descuento que se desea aplicar, el usuario lo puede aplicar
					'es aplicable al cliente
					'se asigna el maximo porcentaje que el usuaio puede dar
					If perdiendo() Then
						Exit Sub
					End If

					If Me.descuento = 0 Then ' si el cliente no tiene predefinido un descuento
						DescuentoCalc = (CDbl(txtPrecioUnit.Text) * CDbl(txtCantidad.Text)) * (CDbl(txtDescuento.Text) / 100)
						Me.txtmontodescuento.Text = DescuentoCalc  ' pone el monto de descuento
						Exit Sub
					End If

					If Me.descuento <> 0 And Me.txtDescuento.Text <= descuento Then
						'si el Cliente tiene un descuento predefinido, y lo que se desea aplicar es <= que lo permitido por el cliente
						DescuentoCalc = (CDbl(txtPrecioUnit.Text) * CDbl(txtCantidad.Text)) * (CDbl(txtDescuento.Text) / 100)
						Me.txtmontodescuento.Text = DescuentoCalc  ' pone el monto de descuento
						Exit Sub
					End If

				End If


				'SI NO LO PUEDE APLICAR EL USUARIO, PERO SI EL CLIENTE
				If CDbl(Me.txtDescuento.Text) > max_aplicar Then
					'si el descuento que se desea aplicar
					'es mayor que el que el usuario puede aplicar

					If perdiendo() Then
						Exit Sub
					End If

					If descuento = 0 Then ' si el cliente no tiene predefinido un descuento
						Me.txtDescuento.Text = max_aplicar
						DescuentoCalc = (CDbl(txtPrecioUnit.Text) * CDbl(txtCantidad.Text)) * (CDbl(txtDescuento.Text) / 100)
						Me.txtmontodescuento.Text = DescuentoCalc  ' pone el monto de descuento
						mensaje = mensaje + "Cod: " + Me.TxtCodArticulo.Text + " Se Aplic� un: (" + Me.txtDescuento.Text + " %) M�ximo permitido por el usuario" + vbCrLf
						Exit Sub
					End If

					If Me.descuento <> 0 And Me.txtDescuento.Text <= descuento Then
						Me.txtDescuento.Text = descuento 'aplico el descuento del cliente
						DescuentoCalc = (CDbl(txtPrecioUnit.Text) * CDbl(txtCantidad.Text)) * (CDbl(txtDescuento.Text) / 100)
						Me.txtmontodescuento.Text = DescuentoCalc  ' pone el monto de descuento
						mensaje = mensaje + "Cod: " + Me.TxtCodArticulo.Text + " Se Aplic� un: (" + Me.txtDescuento.Text + " %) M�ximo permitido para el cliente" + vbCrLf
						Exit Sub
					End If

				End If

			Else 'si el campo de descuento esta en cero
				DescuentoCalc = (CDbl(txtPrecioUnit.Text) * CDbl(txtCantidad.Text)) * (CDbl(txtDescuento.Text) / 100)
				Me.txtmontodescuento.Text = DescuentoCalc  ' pone el monto de descuento
			End If

		Catch ex As System.Exception
			MsgBox(ex.Message)
		End Try
	End Sub

	Function perdiendo() As Boolean
		Try
			Dim pre_unit As Double
			pre_unit = CDbl(Me.txtPrecioUnit.Text) - CDbl(Me.txtPrecioUnit.Text) * CDbl(Me.txtDescuento.Text) / 100
			If pre_unit < CDbl(Me.TxtprecioCosto.Text) Then
				Me.monto_Perdido = CDbl(Me.TxtprecioCosto.Text) - pre_unit
				'Me.perfil_administrador
				If (PMU.Others) Then  'si es un administrador, quien est� haciedo la facturacion
					DescuentoCalc = (CDbl(txtPrecioUnit.Text) * CDbl(txtCantidad.Text)) * (CDbl(txtDescuento.Text) / 100)
					Me.txtmontodescuento.Text = DescuentoCalc  ' pone el monto de descuento
					mensaje = mensaje + "Cod: " + Me.TxtCodArticulo.Text + " Se Aplic� un: (" + Me.txtDescuento.Text + " %) Con esta venta se est� perdiendo " + Me.Label31.Text + Me.monto_Perdido.ToString + vbCrLf
				Else
					Me.txtDescuento.Text = 0.0
					DescuentoCalc = 0
					Me.txtmontodescuento.Text = DescuentoCalc ' p one el monto de descuento
					mensaje = mensaje + "Cod: " + Me.TxtCodArticulo.Text + " Se Aplic� un: (" + Me.txtDescuento.Text + " %) Usted no puede vender perdiendo, +con esta venta se estaria perdiendo " + Me.Label31.Text + Me.monto_Perdido.ToString + vbCrLf
				End If
				Return True
			Else
				Return False

			End If
		Catch ex As SystemException
			MsgBox(ex.Message)
		End Try

	End Function


	Function perdiendo_PUnit() As Boolean
		Try
			If (CDbl(Me.txtPrecioUnit.Text) < CDbl(Me.TxtprecioCosto.Text)) Then
				Me.monto_Perdido = CDbl(Me.TxtprecioCosto.Text) - CDbl(Me.txtPrecioUnit.Text)
				'Me.perfil_administrador
				If (PMU.Others) Then  'si es un administrador, quien est� haciedo la facturacion

					mensaje = mensaje + "Cod: " + Me.TxtCodArticulo.Text + " Se disminuy� el P.unit. en " + Label31.Text + (CDbl(Me.precio_unitario - Me.txtPrecioUnit.Text)).ToString + " ,Con esta venta se est� perdiendo " + Me.Label31.Text + Me.monto_Perdido.ToString + vbCrLf
				Else
					mensaje = mensaje + "Cod: " + Me.TxtCodArticulo.Text + " Usted no puede vender perdiendo, la disminuci�n del P.unit. en " + Label31.Text + (CDbl(Me.precio_unitario - Me.txtPrecioUnit.Text)) + " provocaria una perdida de " + Me.Label31.Text + Me.monto_Perdido.ToString + vbCrLf
				End If
				Return True
			Else
				Return False
			End If

		Catch ex As SystemException
			MsgBox(ex.Message)
		End Try

	End Function


	Private Sub Calculos_Articulo()
		'Dim precio As Double
		Try
			calculo_descuento_articulo()
			'If CB_Exonerar.Checked = True Then
			'	txtImpVenta.Text = Math.Round(Me.impuesto_cliente * CDbl(0) / 100, 2)
			'Else
			'	txtImpVenta.Text = Math.Round(Me.impuesto_cliente * CDbl(Me.txtImpVenta.Text) / 100, 2)
			'End If
			cargarImpuestoIVA()
			If txtImpVenta.Text <> 0 Then 'si tiene impuesto de venta
				If (CDbl(txtFlete.Text) + CDbl(txtOtros.Text)) > CDbl(txtPrecioUnit.Text) Then
					txtFlete.Text = 0
					txtOtros.Text = 0
				End If
				'Gravado = ((CDbl(txtPrecioUnit.Text) - CDbl(txtFlete.Text) - CDbl(txtOtros.Text)) * CDbl(txtCantidad.Text)) - CDbl(Me.txtmontodescuento.Text)
				Gravado = Math.Round(((CDbl(txtPrecioUnit.Text) - CDbl(txtFlete.Text) - CDbl(txtOtros.Text)) * CDbl(txtCantidad.Text)), 2)
				'txtMontoImpuesto.Text = Gravado * (CDbl(txtImpVenta.Text) / 100)
				txtMontoImpuesto.Text = Math.Round((Gravado - CDbl(Me.txtmontodescuento.Text)) * (CDbl(txtImpVenta.Text) / 100), 2)
				txtSGravado.Text = Gravado
				Exento = 0

			Else 'si no tiene impuesto de venta
				'Exento = ((CDbl(txtPrecioUnit.Text) - CDbl(txtFlete.Text) - CDbl(txtOtros.Text)) * CDbl(txtCantidad.Text)) - CDbl(Me.txtmontodescuento.Text)
				Exento = Math.Round(((CDbl(txtPrecioUnit.Text) - CDbl(txtFlete.Text) - CDbl(txtOtros.Text)) * CDbl(txtCantidad.Text)), 2)
				Gravado = 0
				txtMontoImpuesto.Text = 0
				txtSGravado.Text = 0
				txtSExcento.Text = Exento
			End If

			Exento = Math.Round(Exento + ((CDbl(txtFlete.Text) + CDbl(txtOtros.Text)) * CDbl(txtCantidad.Text)), 2)
			txtSExcento.Text = Exento
			txtSubtotal.Text = Math.Round(CDbl(txtSGravado.Text) + CDbl(txtSExcento.Text), 2)
			Text_Ficticio.Text = Math.Round(CDbl(txtSubtotal.Text) + CDbl(txtMontoImpuesto.Text) - CDbl(Me.txtmontodescuento.Text), 2)

			'CALCULAR EL MONTO DE COMISI�N - ORA
			'precio = txtPrecioUnit.EditValue
			txtMontoComision.Text = Math.Round((CDbl(Text_Ficticio.Text) * (BindingContext(DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Comision") / 100)), 2)
			'txtPrecioUnit.EditValue = precio

		Catch ex As System.Exception
			MsgBox(ex.Message)
		End Try
	End Sub

	Private Sub Calcular_totales()
		Try
			BindingContext(DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").EndCurrentEdit()
			Calcular_Totales_Cotizacion()
			txtTotalArt.Text = BindingContext(DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Count

			BindingContext(DataSet_Facturaciones, "Ventas").EndCurrentEdit()
			BindingContext(DataSet_Facturaciones, "Ventas").AddNew()
			BindingContext(DataSet_Facturaciones, "Ventas").CancelCurrentEdit()
			BindingContext(DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Position = BindingContext(DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Count
			If opCredito.Checked = True Then
				busca_Facturas_Pendientes()
			End If

		Catch ex As System.Exception
			MsgBox(ex.Message)
		End Try
	End Sub


	Private Sub Calcular_Totales_Cotizacion() ' calcula el monto Total de la cotizaci�n
		Try
			txtSubtotalT.Properties.DisplayFormat.FormatString = Label31.Text & " #,#0.00"
			Lb_Subgravado.Properties.DisplayFormat.FormatString = Label31.Text & " #,#0.00"
			Lb_SubExento.Properties.DisplayFormat.FormatString = Label31.Text & " #,#0.00"
			txtDescuentoT.Properties.DisplayFormat.FormatString = Label31.Text & " #,#0.00"
			txtImpVentaT.Properties.DisplayFormat.FormatString = Label31.Text & " #,#0.00"
			TxtTotal.Properties.DisplayFormat.FormatString = Label31.Text & " #,#0.00"

			txtSubtotalT.Text = Math.Round(colSubTotal.SummaryItem.SummaryValue, RedondeoTotal)
			Lb_Subgravado.Text = Math.Round(colSubtotalGravado.SummaryItem.SummaryValue, RedondeoTotal)
			Lb_SubExento.Text = Math.Round(colSubTotalExcento.SummaryItem.SummaryValue, RedondeoTotal)
			txtDescuentoT.Text = Math.Round(colMonto_Descuento.SummaryItem.SummaryValue, RedondeoTotal)
			txtImpVentaT.Text = Math.Round(colMonto_Impuesto.SummaryItem.SummaryValue, RedondeoTotal)
			TxtTotal.Text = Math.Round(CDbl(txtSubtotalT.Text) - CDbl(txtDescuentoT.Text) + CDbl(txtImpVentaT.Text) + CDbl(Label46.Text), RedondeoTotal)

		Catch ex As System.Exception
			MsgBox(ex.Message)
		End Try
	End Sub

	Private Sub Eliminar_Ariculo_Detalle()
		Dim resp As Integer
		Try 'se intenta hacer
			If Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Count > 0 Then  ' si hay ubicaciones
				If Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Codigo") = "0" Then
					MessageBox.Show("No puede eliminar un servicio de la factura.", "Seepos", MessageBoxButtons.OK)
					Exit Sub
				End If
				resp = MessageBox.Show("�Desea eliminar este art�culo de la Factura?", "Seepos", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)

				If resp = 6 Then
					Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").RemoveAt(Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Position)
					Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").EndCurrentEdit()
					Me.Calcular_totales()
					Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").EndCurrentEdit()

					Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
					Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").EndCurrentEdit()
					Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").AddNew()
					txtExistencia.EditValue = 0
					TxtCodArticulo.Focus()
				Else
					Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
				End If
			Else
				MsgBox("No Existen Art�culos para eliminar en la Factura", MsgBoxStyle.Information)
			End If
		Catch ex As System.Exception
			MsgBox(ex.Message)
		End Try
	End Sub
#End Region

#Region "Controles Funciones"

#Region "KeyPress"
	Private Sub SoloNumericos_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtDescuento.KeyPress, txtPrecioUnit.KeyPress
		If (Not Char.IsDigit(e.KeyChar)) Then ' valida que en este campo solo se digiten numeros y/o "-"
			'If Not (e.KeyChar = Convert.ToChar(Keys.Back)) And Not (e.KeyChar = "."c) Then PERMITE DECIMALES
			If Not (e.KeyChar = Convert.ToChar(Keys.Back)) Then 'NO PERMITE DECIMALES
				e.Handled = True  ' esto invalida la tecla pulsada
			End If
		End If
	End Sub

	Private Sub txtTelefono_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtTelefono.KeyPress
		If (Not Char.IsDigit(e.KeyChar)) Then ' valida que en este campo solo se digiten numeros y/o "-"
			If Not (e.KeyChar = Convert.ToChar(Keys.Back)) And Not (e.KeyChar = "-"c) Then
				e.Handled = True  ' esto invalida la tecla pulsada
			End If
		End If
	End Sub

	Private Sub txtComisionista_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtComisionista.GotFocus
		txtComisionista.SelectAll()
	End Sub

	Private Sub txtCodigo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCodigo.KeyPress, txtComisionista.KeyPress
		If (Not Char.IsDigit(e.KeyChar)) Then ' valida que en este campo solo se digiten numeros y/o "-"
			If Not (e.KeyChar = Convert.ToChar(Keys.Back)) Then
				e.Handled = True  ' esto invalida la tecla pulsada
			End If
		End If
	End Sub

	Private Sub txtorden_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtorden.KeyPress
		If (Not Char.IsDigit(e.KeyChar)) Then ' valida que en este campo solo se digiten numeros y/o "-"
			If Not (e.KeyChar = Convert.ToChar(Keys.Back)) Then
				e.Handled = True  ' esto invalida la tecla pulsada
			End If
		End If
	End Sub

	Private Sub TxtUtilidad_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxtUtilidad.KeyPress
		If Not IsNumeric(sender.text & e.KeyChar) Then
			If Not (e.KeyChar = Convert.ToChar(Keys.Back)) Then
				e.Handled = True  ' esto invalida la tecla pulsada
			End If
		End If
	End Sub
#End Region

	Private Sub opCredito_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opCredito.CheckedChanged
		Try
			If Me.opCredito.Checked = True And Me.txtCodigo.Text = "0" And Not Me.Importando Then
				MsgBox("Este es un Cliente de Contado, no se le puede dar cr�dito", MsgBoxStyle.Information)
				Me.opCredito.Checked = False
				Me.opContado.Checked = True
				Exit Sub
			End If

			If Me.opCredito.Checked = True And Me.buscando = False Then ' si esta marcada la opcion de credito
				Me.Cargar_Cliente(CInt(txtCodigo.Text))
				recargar_Cliente()
				Me.busca_Facturas_Pendientes()
			End If
			dias_credito()
			TxtCodArticulo.Focus()

		Catch ex As SystemException
			MsgBox(ex.Message)
		End Try
	End Sub

	Private Sub opContado_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles opContado.CheckedChanged
		dias_credito()
		TxtCodArticulo.Focus()
	End Sub

	Private Sub SimpleButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SimpleButton2.Click
		Try
			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()

			If Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Count = 0 Then
				MsgBox("No se puede aplicar descuento, a�n no hay art�culos", MsgBoxStyle.Critical)
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").EndCurrentEdit()
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").AddNew()
				TxtCodArticulo.Focus()
				Exit Sub
			End If

			Dim maximo_descuento As Double = Me.porcentaje_descuento
			Dim ajustar_descuento_general_objero As New Ajuste_Descuento_Factura(maximo_descuento, Me.descuento)
			ajustar_descuento_general_objero.ShowDialog()

			If ajustar_descuento_general_objero.DialogResult = Windows.Forms.DialogResult.OK Then
				Dim nuevo_descuento As Double = ajustar_descuento_general_objero.nuevo_porc_descuento
				If nuevo_descuento = 0 Then TxtCodArticulo.Focus() : Exit Sub ' si se digito 0 en el descuento 

				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
				Me.GridControl1.Enabled = True
				GridControl1.Focus()

				recalcular_cotizacion(nuevo_descuento)
				Calcular_totales()

				If mensaje <> "" Then
					MsgBox(mensaje, MsgBoxStyle.Information, "Seepos")
					mensaje = ""
				End If
			End If

			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").EndCurrentEdit()
			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").AddNew()
			TxtCodArticulo.Focus()

		Catch ex As System.Exception
			MsgBox(ex.Message)
		End Try
	End Sub

	Private Sub SimpleButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SimpleButton1.Click
		Dim trans As Double
		Dim mont_trans As New Monto_Transporte_Ventas
		Try
			mont_trans.ShowDialog()
			trans = mont_trans.Monto

			Label46.Text = trans
			Me.Calcular_Totales_Cotizacion()
			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").EndCurrentEdit()

		Catch ex As System.Exception
			MsgBox(ex.Message)
		End Try
	End Sub

	Private Sub txtCodigo_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCodigo.GotFocus
		txtCodigo.SelectAll()
	End Sub

	Private Sub txtCantidad_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCantidad.KeyPress
		Try
			If GetSetting("seesoft", "seepos", "decimales") = 0 Then
				SoloNumericos_KeyPress(sender, e)
			End If
		Catch ex As Exception
			SaveSetting("seesoft", "seepos", "decimales", "0")
		End Try
	End Sub

	Private Sub txtCantidad_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtCantidad.Validating
		Try

			If Me.txtCantidad.Text = "" Then 'si el campo est� vac�o
				ErrorProvider1.SetError(sender, "Debes cotizar almenos un art�culo")
				txtCantidad.Text = 1

			Else
				ErrorProvider1.SetError(sender, "")
			End If


			If CDbl(Me.txtCantidad.Text) = 0 Then 'si el campo est� vac�o
				ErrorProvider1.SetError(sender, "Debes cotizar almenos un art�culo")

				txtCantidad.Text = 1
			Else
				ErrorProvider1.SetError(sender, "")
			End If

		Catch ex As SystemException
			txtCantidad.Text = 1
			ErrorProvider1.SetError(sender, "")
		End Try

	End Sub

	Private Sub txtDescuento_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtDescuento.Validating
		Try

			If Me.txtDescuento.Text = "" Then 'si el campo est� vac�o
				ErrorProvider1.SetError(sender, "Digite el descuento")
				txtCantidad.Text = 0

			Else
				ErrorProvider1.SetError(sender, "")
			End If


		Catch ex As SystemException
			txtDescuento.Text = 0
			ErrorProvider1.SetError(sender, "")
		End Try
	End Sub

	Private Sub txtPrecioUnit_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtPrecioUnit.Validating
		Try
			If Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Count = 0 Then
				Exit Sub
			End If
			If Me.txtPrecioUnit.Text = "" Then 'si el campo est� vac�o
				'ErrorProvider1.SetError(sender, "Debes digitar el precio unitario")
				'txtPrecioUnit.Text = Me.precio_unitario

				Exit Sub
			Else
				ErrorProvider1.SetError(sender, "")
				Exit Sub
			End If


			If CDbl(Me.txtPrecioUnit.Text) = 0 Then  'si el campo est� vac�o
				'ErrorProvider1.SetError(sender, "Debes digitar el precio unitario")
				'txtPrecioUnit.Text = Me.precio_unitario

				Exit Sub
			Else
				ErrorProvider1.SetError(sender, "")
				Exit Sub
			End If

		Catch ex As SystemException
			'txtPrecioUnit.Text = Me.precio_unitario
			txtPrecioUnit.Text = 0
		End Try
	End Sub

	Private Sub SimpleButton3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SimpleButton3.Click
		Me.ComboBox1.Enabled = True
		Me.ComboBox1.Focus()
		SendKeys.Send("{F4}")
	End Sub

	Private Sub txtNombre_LostFocus(ByVal sender As System.Object, ByVal e As System.EventArgs)
		If Me.opContado.Checked = True Then
			Me.iniciar_factura()
		End If
	End Sub

	Private Sub GridControl1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridControl1.DoubleClick
		BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
	End Sub

	Private Sub txtCantidad_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCantidad.Leave
		If Me.txtCantidad.Text = "" Then
			Exit Sub
		End If

		If Not IsDBNull(Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Descripcion")) Then
			BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
		End If

	End Sub

	Private Sub txtPrecioUnit_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPrecioUnit.Leave
		Try
			If Me.txtPrecioUnit.Text = "" Then  'si el campo est� vac�o
				'txtPrecioUnit.Text = Me.precio_unitario
				Exit Sub
			End If


			If Not IsDBNull(Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Descripcion")) Then
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
			End If


		Catch ex As SystemException
			MsgBox(ex.Message)
		End Try
	End Sub

	Private Sub txtDescuento_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDescuento.Leave
		Try
			If Me.txtDescuento.Text = "" Then
				'MsgBox("No puede estar vac�o", MsgBoxStyle.Exclamation)
				' Me.txtDescuento.Text = "0"
				Exit Sub
			Else
				If (CDbl(Me.txtDescuento.Text) > 100) Then Me.txtDescuento.Text = 100

				If Not IsDBNull(Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Descripcion")) Then
					Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
				End If
			End If
		Catch EX As SystemException
			MsgBox(EX.Message)
		End Try
	End Sub

	Private Sub txtCodArticulo_GotFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TxtCodArticulo.GotFocus
		Try

			TxtCodArticulo.SelectAll()
		Catch ex As Exception

		End Try

	End Sub

	Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
		If Me.CheckBox1.Checked = True Then
			Me.Label_Anulada.Visible = True
			Me.StatusBarPanel4.Text = "Anulada"
		Else
			Me.StatusBarPanel4.Text = "Activa"
			Me.Label_Anulada.Visible = False
			Me.StatusBarPanel4.Icon = Nothing
		End If
	End Sub

	Private Sub txtTipoCambio_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTipoCambio.TextChanged
		Me.StatusBarPanel8.Text = txtTipoCambio.Text
	End Sub

	Private Sub Label31_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Label31.TextChanged
		'Me.Label4.Text = Me.Label31.Text
		'Me.Label32.Text = Me.Label31.Text
		'Me.Label33.Text = Me.Label31.Text
		'Me.Label34.Text = Me.Label31.Text
		'Me.Label35.Text = Me.Label31.Text
		Me.Label29.Text = Me.Label31.Text
		'Me.Label44.Text = Me.Label31.Text
		'Me.Label11.Text = Me.Label31.Text
		Label43.Text = Me.Label31.Text
	End Sub


	Private Sub GridControl1_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles GridControl1.MouseClick
		BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
	End Sub


	Private Sub GridControl1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridControl1.GotFocus
		BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
		If BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Count = 0 And TxtCodArticulo.Enabled Then
			BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
			BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").EndCurrentEdit()
			BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").AddNew()
			TxtCodArticulo.Focus()
		End If

	End Sub


#End Region

#Region "Abrir Caja"
	Public Sub open_cashdrawer()
		Dim Puerto As String
		Dim Ubicacion As String = ""

		If GetSetting("SeeSoft", "SeePos", "UbicacionEscapes") = "" Then
			SaveSetting("SeeSoft", "SeePos", "UbicacionEscapes", "c:\escapes.txt")
		Else
			Ubicacion = GetSetting("SeeSoft", "SeePos", "UbicacionEscapes")
		End If
		'------------------------------------------------------------------------------
		'VALIDA SI DESEA ABRIR CAJA O NO - ORA
		If GetSetting("SeeSoft", "SeePos", "PuertoImp") <> "NO" Then
			Dim intFileNo As Integer = FreeFile()
			FileOpen(1, Ubicacion, OpenMode.Output)
			PrintLine(1, Chr(27) & "p" & Chr(0) & Chr(25) & Chr(250))
			FileClose(1)

			'------------------------------------------------------------------------------
			'VALIDA EL PUERTO DE LA IMPRESORA - ORA
			Try
				Puerto = GetSetting("SeeSoft", "SeePos", "PuertoImp")
				If Puerto = "" Then
					SaveSetting("SeeSoft", "SeePos", "PuertoImp", "COM3")
					Puerto = "COM3"
				End If
			Catch ex As Exception
				SaveSetting("SeeSoft", "SeePos", "PuertoImp", "COM3")
				Puerto = "COM3"
			End Try
			'------------------------------------------------------------------------------

			Shell("print /d:" & Puerto & " c:\escapes.txt", AppWinStyle.NormalFocus)
		End If
		'------------------------------------------------------------------------------
	End Sub
#End Region

#Region "Recibo Dinero"
	Private Sub ReciboDinero()
		If _Buscar_Apertura(Usua.Cedula) Then
			Dim Recibo As New ReciboMontoDinero(Usua)
			Try
				Recibo.Cod_Cliente = CDbl(txtCodigo.Text)
				Recibo.Cod_Moneda = BindingContext(DataSet_Facturaciones, "Moneda").Current("CodMoneda")
				Recibo.ShowDialog()

				If BindingContext(DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Count < 2 Then
					Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").CancelCurrentEdit()
					Me.SimpleButton1.Enabled = False
					Me.SimpleButton2.Enabled = False
					Me.DataSet_Facturaciones.Ventas_Detalle.Clear()
					Me.DataSet_Facturaciones.Ventas.Clear()
					Me.ToolBar1.Buttons(0).Text = "Nuevo"
					Me.ToolBar1.Buttons(0).ImageIndex = 0
					Me.ToolBar1.Buttons(3).Enabled = True
					Me.GroupBox3.Enabled = False
					Me.dtFecha.Enabled = False
					Me.logeado = False
					Me.SimpleButton3.Enabled = False
					Me.txtUsuario.Enabled = True
					Me.txtUsuario.Text = ""
					Me.txtNombreUsuario.Text = ""
					Me.txtUsuario.Focus()
					Me.Loggin_Usuario()
				End If

				TxtCodArticulo.Focus()

			Catch ex As Exception
				MsgBox(ex.ToString)
			Finally
				Recibo.Dispose()
			End Try
		Else
			Exit Sub
		End If

	End Sub
#End Region

#Region "Movimiento de Caja"
	Private Sub MovimientoCaja()
		If _Buscar_Apertura(Usua.Cedula) Then
			Dim Movimiento As New MovimientoCaja(Usua)
			Try
				Movimiento.ShowDialog()

				If BindingContext(DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Count < 2 Then
					Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").CancelCurrentEdit()
					Me.SimpleButton1.Enabled = False
					Me.SimpleButton2.Enabled = False
					Me.DataSet_Facturaciones.Ventas_Detalle.Clear()
					Me.DataSet_Facturaciones.Ventas.Clear()
					Me.ToolBar1.Buttons(0).Text = "Nuevo"
					Me.ToolBar1.Buttons(0).ImageIndex = 0
					Me.ToolBar1.Buttons(3).Enabled = True
					Me.GroupBox3.Enabled = False
					Me.dtFecha.Enabled = False
					Me.logeado = False
					Me.SimpleButton3.Enabled = False
					Me.txtUsuario.Enabled = True
					Me.txtUsuario.Text = ""
					Me.txtNombreUsuario.Text = ""
					Me.txtUsuario.Focus()
					Me.Loggin_Usuario()
				End If

				TxtCodArticulo.Focus()

			Catch ex As Exception
				MsgBox(ex.ToString)
			Finally
				Movimiento.Dispose()
			End Try
		Else
			Exit Sub
		End If

	End Sub
#End Region

#Region "Devoluciones de Venta"
	Private Sub Devoluciones()
		Dim Devolucion As New FrmDevolucionesVentas()
		Try
			Devolucion.ShowDialog()

			If BindingContext(DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Count < 2 Then
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").CancelCurrentEdit()
				Me.SimpleButton1.Enabled = False
				Me.SimpleButton2.Enabled = False
				Me.DataSet_Facturaciones.Ventas_Detalle.Clear()
				Me.DataSet_Facturaciones.Ventas.Clear()
				Me.ToolBar1.Buttons(0).Text = "Nuevo"
				Me.ToolBar1.Buttons(0).ImageIndex = 0
				Me.ToolBar1.Buttons(3).Enabled = True
				Me.GroupBox3.Enabled = False
				Me.dtFecha.Enabled = False
				Me.logeado = False
				Me.SimpleButton3.Enabled = False
				Me.txtUsuario.Enabled = True
				Me.txtUsuario.Text = ""
				Me.txtNombreUsuario.Text = ""
				Me.txtUsuario.Focus()
				Me.Loggin_Usuario()
			End If

			TxtCodArticulo.Focus()

		Catch ex As Exception
			MsgBox(ex.ToString)
		Finally
			Devolucion.Dispose()
		End Try
	End Sub
#End Region

#Region "Funciones"
	Sub Verificar_Consecutivos()
		Try
			If Me.buscando Then Exit Sub
			Dim func As New Conexion
			Dim Configuracion As System.Data.DataRow
			Dim numfact As String = ""
			Configuracion = Me.DataSet_Facturaciones.configuraciones.Rows(0)

			If Configuracion("UnicoConsecutivo") = True Then
				numfact = func.SQLExeScalar("SELECT ISNULL(MAX(Num_Factura), 0) + 1 AS Num_Nueva_Factura FROM Ventas")
				If numfact < 1 Then
					numfact = 1
				End If
				Me.BindingContext(DataSet_Facturaciones.Ventas).Current("Num_Factura") = numfact
				Exit Sub
			End If

			If Configuracion("ConsContado") = True And Me.opContado.Checked = True Then
				Me.txtFactura.Text = func.SQLExeScalar("SELECT ISNULL(MAX(Num_Factura), 0) + 1 AS Num_Nueva_Factura FROM Ventas WHERE (Tipo = 'CON')")
				Me.TxtTipo.Text = "CON"
				Exit Sub
			End If

			If Configuracion("ConsCredito") = True And Me.opCredito.Checked = True Then
				numfact = func.SQLExeScalar("SELECT ISNULL(MAX(Num_Factura), 0) + 1 AS Num_Nueva_Factura FROM Ventas WHERE (Tipo = 'CRE')")
				Me.BindingContext(DataSet_Facturaciones.Ventas).Current("Num_Factura") = numfact
				Me.TxtTipo.Text = "CRE"
				Exit Sub
			End If

			If Configuracion("ConsPuntoVenta") = True Then
				numfact = func.SQLExeScalar("SELECT ISNULL(MAX(Num_Factura), 0) + 1 AS Num_Nueva_Factura FROM Ventas WHERE (Tipo = 'PVE')")
				Me.BindingContext(DataSet_Facturaciones.Ventas).Current("Num_Factura") = numfact
				Me.TxtTipo.Text = "PVE"
				Exit Sub
			End If

		Catch ex As SystemException
			Dim str As String = Err.Number
			If Err.Number <> 9 Then
				MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Mensaje")
			End If
		End Try

	End Sub

	Private Sub recalcular_cotizacion_cambio_cliente()
		Try

			For i As Integer = 0 To Me.BindingContext(Me.DataSet_Facturaciones, "Ventas_Detalle").Count
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Position = i

				Me.CargarInformacionArticulo(Me.TxtCodArticulo.Text, True)

				Me.Calculos_Articulo() ' se calcula  de nuevo los datos del articulo cotizado

				If mensaje <> "" Then
					MsgBox(mensaje, MsgBoxStyle.Information, "Seepos")
					mensaje = ""
				End If
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").EndCurrentEdit()
			Next

			Calcular_Totales_Cotizacion()
			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").EndCurrentEdit()
			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").AddNew()
			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").CancelCurrentEdit()
			MsgBox("Factura actualizada de acuerdo al cliente", MsgBoxStyle.Information)

		Catch ex As System.Exception
			MsgBox(ex.Message)
		End Try
	End Sub


	Private Sub Cargar_Encargados_Compra(ByVal codigo As String)
		Dim rss() As System.Data.DataRow
		Dim rs As System.Data.DataRow
		Dim i As Integer
		Me.Combo_Encargado.Items.Clear() ' limpia el combo
		Try
			If codigo <> Nothing Then

				rss = Me.DataSet_Facturaciones.encargadocompras.Select("Cliente ='" & codigo & "'")

				If rss.Length <> 0 Then ' si existe un cliente con ese c�digo


					For i = 0 To rss.Length - 1 'mientras encargados de compra
						rs = rss(i)
						Me.Combo_Encargado.Items.Add(rs("Nombre"))

					Next i

					If Me.Combo_Encargado.SelectedIndex = -1 Then
						Me.Combo_Encargado.SelectedIndex = 0
					End If


					'Me.Label41.Visible = True
					Me.Combo_Encargado.Visible = True

				Else
					'Me.Label41.Visible = False
					Me.Combo_Encargado.Visible = False

				End If


			Else 'si ese cliente no tiene clientes encargados
				'Me.Combo_Encargado.Text = Me.txtNombre.Text
				'Me.txtCodigo.Focus()
				'Me.Label41.Visible = False
				Me.Combo_Encargado.Visible = False

			End If
		Catch ex As System.Exception
			MsgBox(ex.Message)
		End Try
	End Sub

	Private Sub recalcular_cotizacion(ByVal nuev_des As Double)
		Dim i As Integer
		Try
			With Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle")
				For i = 0 To .Count - 1
					.Position = i
					Me.txtDescuento.Text = nuev_des

					Me.Calculos_Articulo() ' se calcula de nuevo los datos del articulo cotizado
					.EndCurrentEdit()
				Next

				Calcular_Totales_Cotizacion()
			End With

			MsgBox("Descuentos sobre art�culos han sido Actualizados", MsgBoxStyle.Information)

		Catch ex As System.Exception
			MsgBox(ex.Message)
		End Try
	End Sub

	Private Sub dias_credito()
		Try
			If Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Count > 0 Then ' si hay ventas
				If Me.opContado.Checked = False Then 'Si esta marcada la opcion de credito
					txtDiasPlazo.Visible = True
					Label7.Visible = True
					txtDiasPlazo.Text = Me.plazo_credito

					If Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Count > 0 Then Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("Tipo") = "CRE"

					Me.DateTimePicker1.Value = Me.dtFecha.Text
					Me.fecha_vence.Value = DateTimePicker1.Value.AddDays(Me.txtDiasPlazo.Text)
					Me.DtVence.Text = Me.fecha_vence.Value
					Me.Cargar_Encargados_Compra(Me.txtCodigo.Text)
					Me.DtVence.Visible = True
					Me.Label26.Visible = True
					If Not buscando Then Me.txtorden.Enabled = True


				Else 'la opcion de contado esta marcada
					txtDiasPlazo.Text = 0
					txtDiasPlazo.Visible = False
					Label7.Visible = False


					If Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Count > 0 Then Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("Tipo") = "CON"
					Me.Combo_Encargado.Items.Clear()
					Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("Cod_Encargado_Compra") = "NINGUNO"

					'Me.Label41.Visible = False
					Me.Combo_Encargado.Visible = False
					Me.txtorden.Text = 0
					Me.txtorden.Enabled = True
					Me.DtVence.Text = Me.dtFecha.Text
					Me.DtVence.Visible = False
					Me.Label26.Visible = False
				End If

				Verificar_Consecutivos()

			End If

			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").EndCurrentEdit()
		Catch ex As SystemException
			MsgBox(ex.Message)
		End Try
	End Sub

	Private Sub Validar_Punitario()
		Try
			Dim max_precio_unit As Double

			max_precio_unit = ((Me.variacion_Punit / 100) * Me.precio_unitario)    ' se calcula la variacioon maxima que se puede hacer sobre ese articulo

			If CDbl(Me.txtPrecioUnit.Text) < Me.precio_unitario Then
				If perdiendo_PUnit() Then
					Exit Sub
				End If

				If (Me.precio_unitario - CDbl(Me.txtPrecioUnit.Text)) > (max_precio_unit) Then ' si se bajo el precio mas de lo posible
					MsgBox("Precio unitario inv�lido, solo puede variar el precio en un " + variacion_Punit.ToString + "% = " + Me.Label31.Text.ToString + " " + max_precio_unit.ToString, MsgBoxStyle.Exclamation)
					txtPrecioUnit.Text = Me.precio_unitario
					Exit Sub
				End If
			End If

		Catch ex As SystemException
			txtPrecioUnit.Text = Me.precio_unitario
			'MsgBox(ex.Message)
		End Try
	End Sub

	Private Function Utilidad(ByVal Costo As Double, ByVal Precio As Double) As Double
		Utilidad = ((Precio / Costo) - 1) * 100
		Return Utilidad
	End Function

	Private Sub valid_cambios_Punit_Desc_Cant()
		'If Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Descuento") <> CDbl(Me.txtDescuento.Text) Then

		'    ''''''Descuento

		'    If Me.txtDescuento.Text = "" Then
		'        MsgBox("No puede estar vac�o", MsgBoxStyle.Exclamation)
		'        Me.txtDescuento.Text = Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Descuento")
		'        Exit Function
		'    Else

		'        If (CDbl(Me.txtDescuento.Text) > 100) Then Me.txtDescuento.Text = 100

		'    End If

		'    Me.Calculos_Articulo()
		'    If mensaje <> "" Then
		'        MsgBox(mensaje, MsgBoxStyle.Information, "Seepos")
		'        mensaje = ""
		'    End If

		'    If Not IsDBNull(Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Descripcion")) Then
		'        Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").EndCurrentEdit()
		'        Calcular_totales()
		'    End If

		'    'Me.txtCantidad.Focus()

		'End If


		'If Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Precio_Unit") <> CDbl(Me.txtPrecioUnit.Text) Then

		'    '''''Precio Unitario
		'    Me.Calculos_Articulo()
		'    Validar_Punitario()

		'    If mensaje <> "" Then
		'        MsgBox(mensaje, MsgBoxStyle.Information, "Seepos")
		'        mensaje = ""
		'    End If

		'    If Not IsDBNull(Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Descripcion")) Then
		'        Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").EndCurrentEdit()
		'        Calcular_totales()
		'    End If

		'End If


		'If Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Cantidad") <> CDbl(Me.txtCantidad.Text) Then
		'    '''''''''''Cantidad
		'    Me.meter_al_detalle()
		'End If
	End Sub

	Private Sub busca_Facturas_Pendientes()
		Try
			Dim cConexion As New Conexion
			Dim sqlConexion As New SqlConnection
			Dim ConexionLocal As New Conexion
			Dim rs As SqlDataReader
			Dim id_Factura As Long
			Dim num_Fac As Double
			Dim monto_Fact As Double
			Dim TCambio As Double
			Dim Ven As DateTime
			sqlConexion = cConexion.Conectar
			rs = ConexionLocal.GetRecorset(ConexionLocal.Conectar, "SELECT id, Num_Factura, Total, Tipo_Cambio,Vence from Ventas where Tipo = 'CRE' and FacturaCancelado = 0 and Anulado = 0 and Cod_Cliente =" & txtCodigo.Text)


			While rs.Read
				id_Factura = rs("Id")
				num_Fac = rs("Num_Factura")
				monto_Fact = rs("Total")
				TCambio = rs("Tipo_Cambio")
				Ven = rs("Vence")
				If Today > Ven.Date And Not Me.sinrestriccion And plazo_credito > 0 Then
					MsgBox("El Cliente tiene una(s) Factura(s) pendiente(s), la factura solo puede ser de contado", MsgBoxStyle.Critical)
					Me.opContado.Checked = True
					Exit Sub
				End If

				Me.Monto_Adeudado += Me.Saldo_Facturas(num_Fac, monto_Fact, TCambio, id_Factura)

			End While

			If (Monto_Adeudado + Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").Current("Total")) > Me.max_credito Then

				If (Me.sinrestriccion) Then ' si el cliente es sin restricciones
					MsgBox("Con esta Factura el Cliente sobrepasar� el l�mite de cr�dito, permitido ", MsgBoxStyle.Information)

				Else
					'usua.Perfil = "Administrador"
					If PMU.Others Then MsgBox("Con esta Factura el Cliente sobrepasar� el l�mite de cr�dito ( " + Label31.Text + Me.max_credito.ToString + "), Cliente tiene un monto Adeudado de " + Label31.Text + Me.Monto_Adeudado.ToString + ", la factura solo puede ser de contado", MsgBoxStyle.Critical)
					MsgBox("Este Cliente Sobrepasa el l�mite de cr�dito con esta factura, la factura solo puede ser de contado", MsgBoxStyle.Critical)
					Me.opContado.Checked = True
				End If

			End If

			cConexion.DesConectar(sqlConexion)
			Me.Monto_Adeudado = 0

		Catch ex As SystemException
			MsgBox(ex.Message)
		End Try
	End Sub


	Public Function Saldo_Facturas(ByVal FacturaNo As Long, ByVal MontoFactura As Double, ByVal TipoCambFact As Double, ByVal id As Long) As Double
		Dim cConexion As New Conexion
		Dim sqlConexion As New SqlConnection
		Dim MontoDevoluciones As Double
		Dim MontoAbonos As Double
		Dim MontoNCredito As Double
		Dim MontoNDebito As Double
		Dim Saldo_de_Factura As Double

		Try
			sqlConexion = cConexion.Conectar

			'If FacturaNo = 0 Then Exit Function

			MontoDevoluciones = 0

			'C�lcula Devoluciones
			MontoDevoluciones = cConexion.SlqExecuteScalar(sqlConexion, "SELECT ISNULL(SUM(Monto),0) as TotalMonto FROM Devoluciones_Ventas WHERE Id_Factura =" & id & " AND Anulado = 0")
			'C�lcula los Abonos
			MontoAbonos = cConexion.SlqExecuteScalar(sqlConexion, "select  ISNULL(SUM(detalle_abonoccobrar.Abono_SuMoneda),0) AS TotalAbono FROM detalle_abonoccobrar INNER JOIN  abonoccobrar ON detalle_abonoccobrar.Id_Recibo = abonoccobrar.Id_Recibo WHERE (detalle_abonoccobrar.Tipo = 'CRE') AND Factura =" & FacturaNo & " AND abonoccobrar.Anula = 0")

			'NOTAS DE CREDITO
			MontoNCredito = cConexion.SlqExecuteScalar(sqlConexion, "SELECT  ISNULL(SUM(detalle_ajustesccobrar.Ajuste),0) AS TotalAjuste FROM detalle_ajustesccobrar INNER JOIN ajustesccobrar ON detalle_ajustesccobrar.Id_AjustecCobrar = ajustesccobrar.ID_Ajuste WHERE (detalle_ajustesccobrar.Tipo = 'CRE') AND Factura =" & FacturaNo & " AND (detalle_ajustesccobrar.Tipo = 'CRE')")

			'NOTAS DE DEBITO
			MontoNDebito = cConexion.SlqExecuteScalar(sqlConexion, "SELECT  ISNULL(SUM(detalle_ajustesccobrar.Ajuste),0) AS TotalAjuste FROM detalle_ajustesccobrar INNER JOIN ajustesccobrar ON detalle_ajustesccobrar.Id_AjustecCobrar = ajustesccobrar.ID_Ajuste WHERE (detalle_ajustesccobrar.Tipo = 'CRE') AND Factura =" & FacturaNo & " AND (detalle_ajustesccobrar.Tipo = 'DEB')")
			'Obtener el saldo final de la factura
			Saldo_de_Factura = MontoFactura + ((MontoNDebito - MontoNCredito - MontoAbonos - MontoDevoluciones) * TipoCambFact)
			'Saldo_de_Factura = ((MontoFactura - MontoDevoluciones) + InteresCob + MontoNDebito) - (MontoNCredito + MontoAbonos)
			cConexion.DesConectar(sqlConexion)

			Return Saldo_de_Factura

		Catch ex As SystemException
			MsgBox(ex.Message)
		End Try
	End Function

	Public Function Datos(ByVal Codigo As String, ByRef data As SqlDataReader) As Boolean
		Dim X As Integer
		Dim ConexionX As New Conexion
		Dim DatoX As SqlDataReader
		For X = Len(Codigo) To 1 Step -1
			DatoX = ConexionX.GetRecorset(ConexionX.Conectar, "SELECT Precio_Promo, Promo_Activa, Existencia,MonedaCosto, MonedaVenta, Precio_A, Precio_B, Precio_C, Precio_D, IVenta from Inventario where Barras = '" & Codigo.Substring(0, X) & "'")
			If Not DatoX.Read Then
				data = DatoX
				Exit For
			End If
			DatoX.Close()
		Next
		ConexionX.DesConectar(ConexionX.sQlconexion)
	End Function

	Function Buscar_Apertura() As Double
		Try
			Dim func As New cFunciones
			Dim i As Integer

			Me.DataSet_Facturaciones.aperturacaja.Clear()
			cFunciones.Cargar_Tabla_Generico(Me.Adapter_Apertura, "SELECT * FROM AperturaCaja WHERE (Anulado = 0) AND (Estado = 'A') AND (Cedula = '" & Usua.Cedula & "')")
			i = Me.Adapter_Apertura.Fill(Me.DataSet_Facturaciones.aperturacaja)

			Select Case i
				Case 1
					Buscar_Apertura = Me.DataSet_Facturaciones.aperturacaja.Rows(0).Item("NApertura")
					Return Buscar_Apertura
				Case 0
					'MsgBox(Usua.Nombre & " " & "No tiene una apertura de caja abierta, digite la constrase�a de la caja", MsgBoxStyle.Exclamation)
					Return 0
				Case Else
					'MsgBox(Usua.Nombre & " " & "tiene mas de una abierta, digite la constrase�a de la caja", MsgBoxStyle.Exclamation)
					Return 0
			End Select

		Catch ex As Exception
			MsgBox(ex.Message)
			Return 0
		End Try
	End Function


	Function BuscaSaldoCliente(ByVal Codigo As Double) As Double
		Dim ConexionLocal As New Conexion
		Dim rs As SqlDataReader
		Dim id_Factura As Long
		Dim num_Fac, monto_Fact, TCambio As Double
		Dim Monto As Double = 0
		Try
			rs = ConexionLocal.GetRecorset(ConexionLocal.Conectar, "SELECT id, Num_Factura, Total, Tipo_Cambio from Ventas where Tipo = 'CRE' and FacturaCancelado = 0 and Anulado = 0 and Cod_Cliente =" & Codigo)

			While rs.Read
				id_Factura = rs("Id")
				num_Fac = rs("Num_Factura")
				monto_Fact = rs("Total")
				TCambio = rs("Tipo_Cambio")
				Monto += Me.Saldo_Facturas(num_Fac, monto_Fact, TCambio, id_Factura)
			End While

			Return Monto

		Catch ex As Exception
			MsgBox(ex.Message)
			Return 0
		Finally
			If rs.IsClosed = False Then rs.Close()
			ConexionLocal.DesConectar(ConexionLocal.sQlconexion)
		End Try
	End Function

	Private Sub PrecioComision()
		Try
			BindingContext(DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()

			For i As Integer = 0 To BindingContext(DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Count - 1
				BindingContext(DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Position = i
				Calculo_precio_unitario()
				Calculos_Articulo()
				BindingContext(DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").EndCurrentEdit()
			Next

			Calcular_totales()

			BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
			BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").EndCurrentEdit()
			BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").AddNew()
			TxtCodArticulo.Focus()

		Catch ex As Exception
			MsgBox(ex.Message)
		End Try
	End Sub
#End Region



#Region "KeyDown"
	Dim cod_art As String
	Private Sub txtCodArticulo_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TxtCodArticulo.KeyDown
		Select Case e.KeyCode
			Case Keys.F1
				Dim BuscarArt As New FrmBuscarArticulo
				Dim cant As Double
				Try
					If TxtCodArticulo.Text = "0" Then
						Exit Sub
					End If
					If IsNumeric(txtCantidad.Text) Then
						cant = CDbl(txtCantidad.Text)
					Else
						MsgBox("Digite una cantidad v�lida.", MsgBoxStyle.Exclamation, "Facturaci�n")
						Exit Sub
					End If

					'GridControl1.Enabled = False
					If txtNomComisionista.Text <> "" Then
						BuscarArt.SobrePrecio = True
					Else
						BuscarArt.SobrePrecio = False
					End If
					BuscarArt.Bodega = iddebodega
					BuscarArt.StartPosition = FormStartPosition.CenterParent
					BuscarArt.ShowDialog()

					If Cambio_Cantidad Then Me.txtCantidad.Text = cant
					If BuscarArt.Cancelado Or BuscarArt.Codigo = 0 Then
						Exit Sub
					End If

					TxtCodArticulo.Text = BuscarArt.Codigo
					TxtBarras.Text = BuscarArt.Barras
					BuscarArt.Dispose()
					If CargarInformacionArticulo(TxtCodArticulo.Text) Then
						If Me.TxtCodArticulo.Text = "" Then Exit Sub
						If Me.txtCantidad.Text = "" Then Exit Sub
						meter_al_detalle()
						If IsNumeric(Me.txtImpVenta.Text) = True Then Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Impuesto") = Me.txtIVA.Text
						Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
						Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").EndCurrentEdit()
						Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").AddNew()
						txtExistencia.EditValue = 0
					Else
						txtExistencia.EditValue = 0
						If Me.txtCantidad.Text = "" Then Me.txtCantidad.Text = cant
						Me.TxtCodArticulo.Focus()
					End If
				Catch ex As Exception
					MsgBox(ex.Message)
				End Try

			Case Keys.Enter
				Try
					Dim EsPesado As Boolean = False
					If Me.TxtCodArticulo.Text = "" Then
						Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
						Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").EndCurrentEdit()
						Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").AddNew()
						txtExistencia.EditValue = 0
						'GridControl1.Enabled = False
						Me.Cambio_Cantidad = True
						Me.txtCantidad.Focus()
						Exit Sub
					End If


					Dim cant As Double
					cod_art = Me.TxtCodArticulo.Text
					cant = CDbl(Me.txtCantidad.Text)
					Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
					Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").EndCurrentEdit()
					Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").AddNew()
					Dim PesaKG As String = GetSetting("SeeSoft", "SeePOS", "PesaKG")
					If GetSetting("SeeSoft", "SeePOS", "PesaKG").Equals("1") Then
						EsCodigoBarras = Me.GetTipoArticulo(cod_art, cant)
					Else
						If GetSetting("SeeSoft", "SeePOS", "PesaKG").Equals("2") Then
							If cod_art.Length > 10 Then
								If fnObtenerProducto(cod_art, cant) Then
									EsPesado = True
								Else
									EsPesado = False
								End If

							End If
						Else
							SaveSetting("SeeSOFT", "SeePos", "PesaKG", "0")
						End If
					End If

					txtExistencia.EditValue = 0
					'GridControl1.Enabled = False
					' If Cambio_Cantidad Then Me.txtCantidad.Text = cant
					Me.txtCantidad.Text = cant
					If Not EsPesado Then
						Dim sB As String = GetSetting("SeeSoft", "SeePOS", "SoloBarras")
						If sB.Equals("1") Then
							If cod_art.Length > 5 Then
								Dim dt As New DataTable
								If EscanerBalanza Then
									cFunciones.Llenar_Tabla_Generico("Select Codigo From Inventario Where Barras like '%" & cod_art & "%'", dt)
									If dt.Rows.Count > 0 Then
										If dt.Rows.Count > 1 Then
											Dim dt2 As New DataTable

											cFunciones.Llenar_Tabla_Generico("Select Codigo,Barras From Inventario Where Barras like '%" & cod_art & "%'", dt2)

											Dim Cant_Caracter As Integer = dt2.Rows(0).Item(1).ToString.Length

											cod_art = dt2.Rows(0).Item(0)
											For i As Integer = 0 To dt2.Rows.Count - 1
												If dt2.Rows(i).Item(1).ToString.Length < Cant_Caracter Then
													cod_art = dt2.Rows(i).Item(0)
													Cant_Caracter = dt2.Rows(i).Item(1).ToString.Length
												End If
											Next
										Else
											cod_art = dt.Rows(0).Item(0)
										End If
									End If
								Else
									cFunciones.Llenar_Tabla_Generico("Select Codigo From Inventario Where Barras = '" & cod_art & "'", dt)
									If dt.Rows.Count > 0 Then
										cod_art = dt.Rows(0).Item(0)
									End If

								End If

							End If
						End If
					End If

					If CargarInformacionArticulo(cod_art) Then
						If Me.TxtCodArticulo.Text = "" Then Exit Sub
						If Me.txtCantidad.Text = "" Then Exit Sub

						If Me.txtPrecioUnit.EditValue = 0 Then
							Me.txtPrecioUnit.Enabled = True
							Me.txtPrecioUnit.Visible = True
							Me.txtPrecioUnit.Focus()
							Exit Sub
						End If

						meter_al_detalle()
						If IsNumeric(Me.txtImpVenta.Text) = True Then Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Impuesto") = Me.txtImpVenta.Text
						Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
						Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").EndCurrentEdit()
						Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").AddNew()
						'GridControl1.Enabled = False
						txtExistencia.EditValue = 0
					Else
						txtExistencia.EditValue = 0
						If Me.txtCantidad.Text = "" Then Me.txtCantidad.Text = "1"
						Me.TxtCodArticulo.Focus()
					End If

				Catch ex As SystemException
					MsgBox(ex.Message)
				End Try

			Case Keys.F2
				' Me.CheckBoxPVE.Checked = True
				Me.Registrar()

			Case Keys.F3
				BuscarFactura()

			Case Keys.F4
				apartados()

			Case Keys.F5
				If MessageBox.Show("�Desea cambiar de Usuario?", "Atenci�n...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
					Me.password_antiguo = Me.txtUsuario.Text
					Me.txtUsuario.Text = ""
					Me.txtUsuario.Enabled = True
					Me.txtNombreUsuario.Text = ""
					Me.GroupBox3.Enabled = False
					txtorden.Enabled = False
					Me.txtUsuario.Focus()
				End If

			Case Keys.F6
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
				'Me.GridControl1.Enabled = True
				Me.txtPrecioUnit.Focus()
			Case Keys.F7
				txtComisionista.Focus()

			Case Keys.F8
				Devoluciones()

			Case Keys.F9
				txtCodigo.Focus()

			Case Keys.F10
				opCredito.Checked = Not (opCredito.Checked)
				opContado.Checked = Not (opCredito.Checked)
				TxtCodArticulo.Focus()

			Case Keys.F11
				ReciboDinero()

			Case Keys.F12
				MovimientoCaja()

			Case Keys.Escape
				If MessageBox.Show("�Desea cancelar esta factura?", "Atenci�n...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
					If MessageBox.Show("�Desea cancelar esta factura?", "Atenci�n...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
						Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").CancelCurrentEdit()
						Me.SimpleButton1.Enabled = False
						Me.SimpleButton2.Enabled = False
						Me.DataSet_Facturaciones.Ventas_Detalle.Clear()
						Me.DataSet_Facturaciones.Ventas.Clear()
						Me.ToolBar1.Buttons(0).Text = "Nuevo"
						Me.ToolBar1.Buttons(0).ImageIndex = 0
						Me.ToolBar1.Buttons(3).Enabled = True
						Me.GroupBox3.Enabled = False
						Me.dtFecha.Enabled = False
						Me.logeado = False
						Me.SimpleButton3.Enabled = False
						Me.txtUsuario.Enabled = True
						Me.txtUsuario.Text = ""
						Me.txtNombreUsuario.Text = ""
						Me.txtUsuario.Focus()
						Me.Loggin_Usuario()
					End If
				End If

			Case Keys.Insert
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").EndCurrentEdit()
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").AddNew()
				'GridControl1.Enabled = False
				txtExistencia.EditValue = 0

			Case Keys.Multiply
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").EndCurrentEdit()
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").AddNew()
				txtExistencia.EditValue = 0
				'GridControl1.Enabled = False
				Me.Cambio_Cantidad = True
				TxtCodArticulo.EditValue = ""
				TxtCodArticulo.Text = ""
				Me.txtCantidad.Focus()
		End Select
	End Sub

	Private Sub txtCodigo_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCodigo.KeyDown
		Try
			Select Case e.KeyCode
				Case Keys.F1
					Dim Cliente As New frmBuscarCliente
					Cliente.ShowDialog()

					If Cliente.Cancelado = False And Cliente.Codigo <> "" And Cliente.Codigo <> 0 Then
						Me.txtCodigo.Text = Cliente.Codigo
					Else
						Exit Sub
					End If

					If Me.txtCodigo.Text <> "" And Me.txtCodigo.Text <> "0" Then
						sqlConexion = CConexion.Conectar
						ayuda = True
						Me.Cargar_Cliente(CInt(txtCodigo.Text))
						CargarInformacionCliente(txtCodigo.Text)
					Else
						Exit Sub
					End If
					If Me.GroupBox3.Enabled = False Then Me.iniciar_factura()
					Me.TxtCodArticulo.Focus()

				Case Keys.Enter
					Me.Cargar_Cliente(CInt(txtCodigo.Text))
					enter_cod_cliente()
					txtCantidad.Focus()

				Case Keys.F2
					' Me.CheckBoxPVE.Checked = True
					Me.Registrar()

				Case Keys.F3
					BuscarFactura()

				Case Keys.F4
					apartados()

				Case Keys.F5
					If MessageBox.Show("�Desea cambiar de Usuario?", "Atenci�n...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
						password_antiguo = txtUsuario.Text : txtUsuario.Text = ""
						txtUsuario.Enabled = True : txtNombreUsuario.Text = ""
						GroupBox3.Enabled = False : txtorden.Enabled = False
						txtUsuario.Focus()
					End If

				Case Keys.F6
					BindingContext(DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
					GridControl1.Enabled = True
					GridControl1.Focus()

				Case Keys.F7
					txtComisionista.Focus()

				Case Keys.F8
					Devoluciones()

				Case Keys.F9
					'----------------------------------- CE
					Me.ValidarCliente()
					'----------------------------------- CE
					If Me.txtCodigo.Text = "" And Me.txtCodigo.Text = "0" Then
						Dim cFunciones As New cFunciones
						Dim c As String
						c = cFunciones.BuscarDatos("select identificacion as Identificaci�n,nombre as Nombre from Clientes", "Nombre")
						If c <> "" Then
							Me.txtCodigo.Text = c
						Else
							Exit Sub
						End If

						If Me.txtCodigo.Text <> "" And Me.txtCodigo.Text <> "0" Then
							sqlConexion = CConexion.Conectar
							ayuda = True
							Me.Cargar_Cliente(CInt(txtCodigo.Text))
							CargarInformacionCliente(txtCodigo.Text)
						Else
							Exit Sub
						End If
						If Me.GroupBox3.Enabled = False Then Me.iniciar_factura()
					End If
					Me.TxtCodArticulo.Focus()

				Case Keys.F10
					opCredito.Checked = Not (opCredito.Checked)
					opContado.Checked = Not (opCredito.Checked)
					TxtCodArticulo.Focus()

				Case Keys.F11
					ReciboDinero()

				Case Keys.F12
					MovimientoCaja()

				Case Keys.Escape
					If MessageBox.Show("�Desea cancelar esta factura?", "Atenci�n...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
						If MessageBox.Show("�Desea cancelar esta factura?", "Atenci�n...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
							Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").CancelCurrentEdit()
							Me.SimpleButton1.Enabled = False
							Me.SimpleButton2.Enabled = False
							Me.DataSet_Facturaciones.Ventas_Detalle.Clear()
							Me.DataSet_Facturaciones.Ventas.Clear()
							Me.ToolBar1.Buttons(0).Text = "Nuevo"
							Me.ToolBar1.Buttons(0).ImageIndex = 0
							Me.ToolBar1.Buttons(3).Enabled = True
							Me.GroupBox3.Enabled = False
							Me.dtFecha.Enabled = False
							Me.logeado = False
							Me.SimpleButton3.Enabled = False
							Me.txtUsuario.Enabled = True
							Me.txtUsuario.Text = ""
							Me.txtNombreUsuario.Text = ""
							Me.txtUsuario.Focus()
							Me.Loggin_Usuario()
						End If
					End If

				Case Keys.Insert
					Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
					Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").EndCurrentEdit()
					Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").AddNew()
					txtExistencia.EditValue = 0
					'GridControl1.Enabled = False
					TxtCodArticulo.Focus()
			End Select
		Catch ex As System.Exception
			MsgBox(ex.Message)
		End Try
	End Sub

#Region "Apartado"
	Sub apartados()
		Dim formapartados As New Apartados
		formapartados.Cedula_usuario = Me.Cedula_usuario
		formapartados.Usua = Me.Usua
		formapartados.ShowDialog()


	End Sub
#End Region


	Private Sub txtCantidad_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCantidad.KeyDown
		Select Case e.KeyCode
			Case Keys.Enter
				If TxtCodArticulo.Text = "0" Then
					Exit Sub
				End If
				If Me.txtNombreArt.Text = "" Then ' si aun no se ha agregado el articulo
					Me.TxtCodArticulo.Text = ""
					Me.TxtCodArticulo.Focus()
				Else
					If Not buscando And Me.txtNombreArt.Text <> "" Then
						If Not Validar_articulo(Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Codigo")) Then
							Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").EndCurrentEdit()
							Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").AddNew()
							Exit Sub
						End If
					End If
					meter_al_detalle()
					If IsNumeric(Me.txtImpVenta.Text) = True Then Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Impuesto") = Me.txtImpVenta.Text
					Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
					Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").EndCurrentEdit()
					Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").AddNew()
					'GridControl1.Enabled = False
					txtExistencia.EditValue = 0
					TxtCodArticulo.Focus()
				End If

			Case Keys.F2
				' Me.CheckBoxPVE.Checked = True
				Me.Registrar()

			Case Keys.F3
				BuscarFactura()

			Case Keys.F4
				apartados()

			Case Keys.F5
				If MessageBox.Show("�Desea cambiar de Usuario?", "Atenci�n...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
					Me.password_antiguo = Me.txtUsuario.Text
					Me.txtUsuario.Text = ""
					Me.txtUsuario.Enabled = True
					Me.txtNombreUsuario.Text = ""
					Me.GroupBox3.Enabled = False
					txtorden.Enabled = False
					Me.txtUsuario.Focus()
				End If

			Case Keys.F6
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
				Me.GridControl1.Enabled = True
				GridControl1.Focus()

			Case Keys.F7
				txtComisionista.Focus()

			Case Keys.F8
				Devoluciones()

			Case Keys.F9
				txtCodigo.Focus()

			Case Keys.F10
				opCredito.Checked = Not (opCredito.Checked)
				opContado.Checked = Not (opCredito.Checked)
				TxtCodArticulo.Focus()

			Case Keys.F11
				ReciboDinero()

			Case Keys.F12
				MovimientoCaja()

			Case Keys.Escape
				If MessageBox.Show("�Desea cancelar esta factura?", "Atenci�n...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
					If MessageBox.Show("�Desea cancelar esta factura?", "Atenci�n...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
						Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").CancelCurrentEdit()
						Me.SimpleButton1.Enabled = False
						Me.SimpleButton2.Enabled = False
						Me.DataSet_Facturaciones.Ventas_Detalle.Clear()
						Me.DataSet_Facturaciones.Ventas.Clear()
						Me.ToolBar1.Buttons(0).Text = "Nuevo"
						Me.ToolBar1.Buttons(0).ImageIndex = 0
						Me.ToolBar1.Buttons(3).Enabled = True
						Me.GroupBox3.Enabled = False
						Me.dtFecha.Enabled = False
						Me.logeado = False
						Me.SimpleButton3.Enabled = False
						Me.txtUsuario.Enabled = True
						Me.txtUsuario.Text = ""
						Me.txtNombreUsuario.Text = ""
						Me.txtUsuario.Focus()
						Me.Loggin_Usuario()
					End If
				End If

			Case Keys.Insert
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").EndCurrentEdit()
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").AddNew()
				txtExistencia.EditValue = 0
				'GridControl1.Enabled = False
				TxtCodArticulo.Focus()
		End Select
	End Sub

	Private Sub txtDescuento_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtDescuento.KeyDown
		Try
			Select Case e.KeyCode
				Case Keys.Enter
					If TxtCodArticulo.Text = "0" Then
						Exit Sub
					End If
					If Not buscando And Me.txtNombreArt.Text <> "" Then
						If Not Validar_articulo(Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Codigo")) Then
							Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").EndCurrentEdit()
							Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").AddNew()
							Exit Sub
						End If
					End If
					If Me.txtDescuento.Text = "" Then
						MsgBox("No puede estar vac�o", MsgBoxStyle.Exclamation)
						Me.txtDescuento.Text = "0"
						Exit Sub
					Else
						If (CDbl(Me.txtDescuento.Text) > 100) Then Me.txtDescuento.Text = 100
					End If

					Me.Calculos_Articulo()
					If mensaje <> "" Then
						MsgBox(mensaje, MsgBoxStyle.Information, "Seepos")
						mensaje = ""
					End If

					If Not IsDBNull(Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Descripcion")) Then
						Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").EndCurrentEdit()
						Calcular_totales()
					End If
					Me.txtCantidad.Focus()

				Case Keys.F2
					'  Me.CheckBoxPVE.Checked = True
					Me.Registrar()

				Case Keys.F3
					BuscarFactura()

				Case Keys.F4
					apartados()

				Case Keys.F5
					If MessageBox.Show("�Desea cambiar de Usuario?", "Atenci�n...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
						Me.password_antiguo = Me.txtUsuario.Text
						Me.txtUsuario.Text = ""
						Me.txtUsuario.Enabled = True
						Me.txtNombreUsuario.Text = ""
						Me.GroupBox3.Enabled = False
						txtorden.Enabled = False
						Me.txtUsuario.Focus()
					End If

				Case Keys.F6
					Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
					Me.GridControl1.Enabled = True
					GridControl1.Focus()

				Case Keys.F7
					txtComisionista.Focus()

				Case Keys.F8
					Devoluciones()

				Case Keys.F9
					txtCodigo.Focus()

				Case Keys.F10
					opCredito.Checked = Not (opCredito.Checked)
					opContado.Checked = Not (opCredito.Checked)
					TxtCodArticulo.Focus()

				Case Keys.F11
					ReciboDinero()

				Case Keys.F12
					MovimientoCaja()

				Case Keys.Escape
					If MessageBox.Show("�Desea cancelar esta factura?", "Atenci�n...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
						Me.BindingContext(Me.DataSet_Facturaciones, "Ventas").CancelCurrentEdit()
						Me.SimpleButton1.Enabled = False
						Me.SimpleButton2.Enabled = False
						Me.DataSet_Facturaciones.Ventas_Detalle.Clear()
						Me.DataSet_Facturaciones.Ventas.Clear()
						Me.ToolBar1.Buttons(0).Text = "Nuevo"
						Me.ToolBar1.Buttons(0).ImageIndex = 0
						Me.ToolBar1.Buttons(3).Enabled = True
						Me.GroupBox3.Enabled = False
						Me.dtFecha.Enabled = False
						Me.logeado = False
						Me.SimpleButton3.Enabled = False
						Me.txtUsuario.Enabled = True
						Me.txtUsuario.Text = ""
						Me.txtNombreUsuario.Text = ""
						Me.txtUsuario.Focus()
						Me.Loggin_Usuario()
					End If

				Case Keys.Insert
					Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
					Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").EndCurrentEdit()
					Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").AddNew()
					txtExistencia.EditValue = 0
					'GridControl1.Enabled = False
					TxtCodArticulo.Focus()
			End Select

		Catch ex As SystemException
			MsgBox(ex.Message)
		End Try
	End Sub

	Private Sub GridControl1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles GridControl1.KeyDown
		If e.KeyCode = Keys.Delete Then
			Eliminar_Ariculo_Detalle()
		End If

		If e.KeyCode = Keys.F2 Then
			If buscando = True Then Exit Sub
			Me.Registrar()
		End If
		If CInt(txtCodigo.Text) <> 0 Then
			If e.Control = True And e.KeyCode = Keys.E Then
				Exoneracion_por_Registro()
			End If
		End If
		If e.KeyCode = Keys.Escape Then
			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").EndCurrentEdit()
			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").AddNew()
			'Me.GridControl1.Enabled = False
			txtExistencia.EditValue = 0
			TxtCodArticulo.Focus()
		End If

		If e.KeyCode = Keys.F6 Then
			txtCantidad.Focus()
		End If

		If e.KeyCode = Keys.Insert Then
			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").EndCurrentEdit()
			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").AddNew()
			txtExistencia.EditValue = 0
			'GridControl1.Enabled = False
			TxtCodArticulo.Focus()
		End If
	End Sub

	Private Sub txtNombreArt_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNombreArt.KeyDown
		If e.KeyCode = Keys.Enter Then
			meter_al_detalle()
			If IsNumeric(Me.txtImpVenta.Text) = True Then Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Impuesto") = Me.txtImpVenta.Text
			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").EndCurrentEdit()
			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").AddNew()
			txtExistencia.EditValue = 0
			'GridControl1.Enabled = False
			TxtCodArticulo.Focus()
		End If
	End Sub

	Private Sub TxtUtilidad_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TxtUtilidad.KeyDown
		If e.KeyCode = Keys.Enter Then
			Try
				If Me.TxtUtilidad.Text = "" Then Exit Sub
				Me.txtPrecioUnit.Text = CDbl(Me.txtCostoBase.Text) * (Me.TxtUtilidad.Text / 100) + Me.txtFlete.Text + Me.txtOtros.Text + CDbl(txtCostoBase.Text)

				Me.Calculos_Articulo()
				Validar_Punitario()

				If mensaje <> "" Then
					MsgBox(mensaje, MsgBoxStyle.Information, "Seepos")
					mensaje = ""
				End If


				If Not IsDBNull(Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Descripcion")) Then
					Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").EndCurrentEdit()
					Calcular_totales()
				End If

			Catch ex As SystemException
				MsgBox(ex.Message)
			End Try
		End If
	End Sub

	Private Sub txtPrecioUnit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtPrecioUnit.KeyDown

		Select Case e.KeyCode
			Case Keys.Enter
				Try
					If TxtCodArticulo.Text = "0" Then
						Exit Sub
					End If
					If Not buscando And Me.txtNombreArt.Text <> "" Then
						If Not Validar_articulo(Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Codigo")) Then
							Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").EndCurrentEdit()
							Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").AddNew()
							Exit Sub
						End If
					End If
					If Me.txtPrecioUnit.Text = "" Then Exit Sub
					Me.TxtUtilidad.Text = Utilidad(Me.txtCostoBase.Text, (Me.txtPrecioUnit.Text - txtFlete.Text - txtOtros.Text))

					Me.Calculos_Articulo()
					Validar_Punitario()

					If mensaje <> "" Then
						MsgBox(mensaje, MsgBoxStyle.Information, "Seepos")
						mensaje = ""
					End If

					If Not IsDBNull(Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Descripcion")) Then
						Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").EndCurrentEdit()
						Calcular_totales()
					End If


					If Me.txtDescuento.Enabled = True Then
						Me.txtDescuento.Focus()
					Else
						Me.txtCantidad.Focus()
					End If

				Catch ex As System.Exception
					MsgBox(ex.Message)
				End Try
			Case Keys.F2 : Me.Registrar()
			Case Keys.F5
				If MessageBox.Show("�Desea cambiar de Usuario?", "Atenci�n...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
					Me.password_antiguo = Me.txtUsuario.Text
					Me.txtUsuario.Text = ""
					Me.txtUsuario.Enabled = True
					Me.txtNombreUsuario.Text = ""
					Me.GroupBox3.Enabled = False
					txtorden.Enabled = False
					Me.txtUsuario.Focus()
				End If
			Case Keys.Escape
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
				'Me.GridControl1.Enabled = True
			Case Keys.Insert
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").EndCurrentEdit()
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").AddNew()
				txtExistencia.EditValue = 0
				'GridControl1.Enabled = False
				TxtCodArticulo.Focus()
		End Select

	End Sub

	Private Sub txtSubtotal_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSubtotal.KeyDown
		Select Case e.KeyCode
			Case Keys.F5
				If MessageBox.Show("�Desea cambiar de Usuario?", "Atenci�n...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
					Me.password_antiguo = Me.txtUsuario.Text
					Me.txtUsuario.Text = ""
					Me.txtUsuario.Enabled = True
					Me.txtNombreUsuario.Text = ""
					Me.GroupBox3.Enabled = False
					txtorden.Enabled = False
					Me.txtUsuario.Focus()
				End If
			Case Keys.Escape
				BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
				Me.GridControl1.Enabled = True
			Case Keys.Insert
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").EndCurrentEdit()
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").AddNew()
				txtExistencia.EditValue = 0
				'GridControl1.Enabled = False

			Case Keys.F2 : Me.Registrar()
		End Select
	End Sub

	Private Sub txtNombre_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNombre.KeyDown
		Select Case e.KeyCode
			Case Keys.Enter
				'----------------------------------- CE
				Me.ValidarCliente()
				'----------------------------------- CE
				Me.TxtCodArticulo.Focus()
			Case Keys.F2 : Me.Registrar()
			Case Keys.F5
				If MessageBox.Show("�Desea cambiar de Usuario?", "Atenci�n...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
					Me.password_antiguo = Me.txtUsuario.Text
					Me.txtUsuario.Text = ""
					Me.txtUsuario.Enabled = True
					Me.txtNombreUsuario.Text = ""
					Me.GroupBox3.Enabled = False
					txtorden.Enabled = False
					Me.txtUsuario.Focus()
				End If
			Case Keys.Escape
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
				Me.GridControl1.Enabled = True

			Case Keys.Insert
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").EndCurrentEdit()
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").AddNew()
				txtExistencia.EditValue = 0
				'GridControl1.Enabled = False
		End Select
	End Sub
	Private Sub ValidarCliente()
		If Me.txtCodigo.Text = "0" Then 'AndAlso Me.txtNombre.Text <> ""
			If MessageBox.Show("�Desea Registrar un Cliente?", "Atenci�n...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = DialogResult.No Then 'si no desea guardar la facturacion
				Exit Sub
			Else
				Dim CargarFrmCliente As New Frmcliente(Usua, txtNombre.Text, True, Usua.Clave_Interna) 'Si el cliente no existe, entonces que lo registre
				CargarFrmCliente.ShowDialog()
				txtCodigo.Text = Frmcliente.CodigoClienteGenerado
				sqlConexion = CConexion.Conectar
				ayuda = True
				Me.Cargar_Cliente(CInt(txtCodigo.Text))
				CargarInformacionCliente(txtCodigo.Text)
			End If
		End If
	End Sub '-- CE
	Private Sub Txtdireccion_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Txtdireccion.KeyDown
		If e.KeyCode = Keys.Enter Then
			If txtorden.Enabled = False Then txtorden.Enabled = True
			txtorden.Focus()
		End If

		If e.KeyCode = Keys.F2 Then
			Registrar()
		End If
	End Sub

	Private Sub FacturacionPVE_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		If e.KeyCode = Keys.F5 Then
			txtUsuario.Enabled = True
		End If
	End Sub

	Private Sub txtTelefono_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtTelefono.KeyDown
		If e.KeyCode = Keys.F2 Then
			Registrar()
		End If
	End Sub

	Private Sub Combo_Encargado_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Combo_Encargado.KeyDown
		If e.KeyCode = Keys.F2 Then
			Registrar()
		End If
	End Sub

	Private Sub txtorden_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtorden.KeyDown
		Select Case e.KeyCode
			Case Keys.F2 : Me.Registrar()
			Case Keys.Escape
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
				Me.GridControl1.Enabled = True
		End Select
	End Sub

	Private Sub ComboBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ComboBox1.KeyDown
		If e.KeyCode = Keys.Enter Then
			inicializar()
		End If
		If e.KeyCode = Keys.F5 Then
			password_antiguo = Me.txtUsuario.Text
			txtUsuario.Text = ""
			txtUsuario.Enabled = True
			txtNombreUsuario.Text = ""
			GroupBox3.Enabled = False
			txtorden.Enabled = False

			txtUsuario.Focus()
		End If
	End Sub

	Private Sub txtComisionista_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtComisionista.KeyDown
		If e.KeyCode = Keys.F1 Then
			Dim funcion As New cFunciones
			Dim Id As String
			Dim con As String = GetSetting("SeeSoft", "SeePos", "Conexion")
			Try
				Id = funcion.BuscarDatos("Select Codigo, Nombre from Comisionista", "Nombre", "Buscar por nombre del comisionista ...", con)
				If Id = Nothing Then ' si se dio en el boton de cancelar
					buscando = False
					Exit Sub
				End If

				If Id <> "" And Id <> "0" Then
					txtComisionista.Text = Id
				Else
					Exit Sub
				End If

				If txtComisionista.Text <> "" And Me.txtComisionista.Text <> "0" Then
					sqlConexion = CConexion.Conectar("SeePOS")
					txtNomComisionista.Text = CConexion.SlqExecuteScalar(sqlConexion, "Select Nombre from Comisionista where Codigo =" & txtComisionista.Text)
					CConexion.DesConectar(sqlConexion)
				Else
					txtComisionista.Text = "0"
					txtNomComisionista.Text = ""
					Exit Sub
				End If

				TxtCodArticulo.Focus()
			Catch ex As SystemException
				MsgBox(ex.Message)
			Finally
				PrecioComision()
			End Try
		End If

		If e.KeyCode = Keys.Enter Then
			Try
				If txtComisionista.Text <> "" And Me.txtComisionista.Text <> "0" Then
					sqlConexion = CConexion.Conectar("SeePOS")
					txtNomComisionista.Text = CConexion.SlqExecuteScalar(sqlConexion, "Select Nombre from Comisionista where Codigo =" & txtComisionista.Text)
					CConexion.DesConectar(sqlConexion)
				Else
					txtComisionista.Text = "0"
					txtNomComisionista.Text = ""
					Exit Sub
				End If

			Catch ex As Exception
				MsgBox(ex.Message)
			Finally
				PrecioComision()
				txtCantidad.Focus()
			End Try
		End If
	End Sub
#End Region


	Private Sub CB_Exonerar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CB_Exonerar.Click
		Try
			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()

			If Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Count = 0 Then
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").EndCurrentEdit()
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").AddNew()
				TxtCodArticulo.Focus()
				Exit Sub
			End If

			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
			Me.GridControl1.Enabled = True
			GridControl1.Focus()

			If CB_Exonerar.Checked Then
				txtImpVenta.Text = Format(0, "#,#0.00")
			Else
				txtImpVenta.Text = Format(13, "#,#0.00")
			End If

			_recalcular_exoneracion()
			Calcular_totales()

			If mensaje <> "" Then
				MsgBox(mensaje, MsgBoxStyle.Information, "Seepos")
				mensaje = ""
			End If

			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").CancelCurrentEdit()
			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").EndCurrentEdit()
			Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").AddNew()
			TxtCodArticulo.Focus()

		Catch ex As System.Exception
			MsgBox(ex.Message)
		End Try
	End Sub
	Private Sub _recalcular_exoneracion()
		Dim i As Integer
		Try
			For i = 0 To Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Count - 1
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Position = i
				Me.Calculos_Articulo() ' se calcula de nuevo los datos del articulo cotizado
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").EndCurrentEdit()
			Next
			Calcular_Totales_Cotizacion()
			MsgBox("Descuentos sobre art�culos han sido Actualizados", MsgBoxStyle.Information)
		Catch ex As System.Exception
			MsgBox(ex.Message)
		End Try
	End Sub





	Private Sub CB_Moneda_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles CB_Moneda.KeyDown
		Select Case e.KeyCode
			Case Keys.Enter
				Me.TxtCodArticulo.Focus()
		End Select
	End Sub

	Private Sub Label14_Click(sender As Object, e As EventArgs) Handles Label14.Click
		Dim ver As New clsVersion(My.Application.Deployment)
		ver.EsPosible = My.Application.IsNetworkDeployed
		Dim fr As New frmActualiza(ver)
		If fr.ShowDialog = Windows.Forms.DialogResult.OK Then
			Close()
		End If
	End Sub

	Private Sub SimpleButton4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SimpleButton4.Click
		Me.CB_Moneda.Enabled = True
		Me.CB_Moneda.Focus()
		SendKeys.Send("{F4}")
	End Sub

	Private Sub Label25_Click(sender As Object, e As EventArgs) Handles Label25.Click

	End Sub
	Public Function Excento(ByVal cod As String) As Boolean
		Dim cnnv As SqlConnection = Nothing
		Dim dt As SqlDataReader
		Dim dime As Boolean
		Dim ex As Integer = 0

		Dim sConn As String = GetSetting("SeeSOFT", "SeePos", "CONEXION")
		cnnv = New SqlConnection(sConn)
		cnnv.Open()
		' Creamos el comando para la consulta
		Dim cmdv As SqlCommand = New SqlCommand("SELECT IVenta FROM Inventario WHERE (Codigo='" & cod & "') ", cnnv)
		dt = cmdv.ExecuteReader()
		While (dt.Read)
			If CB_Exonerar.Checked Then
				dime = True
			Else
				If (dt.Item("IVenta") = 0) Then
					dime = True
				End If
			End If

		End While
		cnnv.Close()
		Return dime
	End Function
	Private Sub cargarImpuestoIVA()
		txtIVA.Text = Format(ImpuestoProducto.Obtener(txtCodigo.Text, TxtCodArticulo.Text), "#,#0.00")
		txtImpVenta.Text = txtIVA.Text
	End Sub
	Function UsaLectora(ByVal recargar As Boolean) As Boolean
		Try
			If recargar Then Return False
			UsaLector = GetSetting("SeeSOFT", "SeePOS", "UsaLector")
			If UsaLector.Equals("") Or UsaLector.Equals("0") Then
				SaveSetting("SeeSOFT", "SeePOS", "UsaLector", "0")
				Me.txtPrecioUnit.Focus()
				Return False
			Else
				txtPrecioEnter()
				Dim PidePrecio As String = GetSetting("SeeSOFT", "SeePOS", "PedirPrecio")
				If PidePrecio.Equals("") Or PidePrecio.Equals("0") Then
					SaveSetting("SeeSOFT", "SeePOS", "PedirPrecio", "0")
					txtDescuentoEnter()
				Else
					PedirPrecio()
					txtDescuentoEnter()
				End If
				Return True
			End If

		Catch ex As Exception
			Return False
		End Try
	End Function
	Sub txtPrecioEnter()
		Try
			If Me.txtPrecioUnit.Text = "" Then Exit Sub
			If TxtCodArticulo.Text = ArticuloGenerico And ArticuloGenerico <> "" Then
				Me.txtPrecioUnit.Text = QuitarIVI(Me.txtPrecioUnit.Text)
			End If
			Me.TxtUtilidad.Text = Utilidad(Me.txtCostoBase.Text, (Me.txtPrecioUnit.Text - txtFlete.Text - txtOtros.Text))
			BanderaImpuesto = False
			Me.Calculos_Articulo()
			Validar_Punitario()

			If mensaje <> "" Then
				MsgBox(mensaje, MsgBoxStyle.Information, "Seepos")
				mensaje = ""
			End If

			If Not IsDBNull(Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Descripcion")) Then
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").EndCurrentEdit()
				Calcular_totales()
			End If


			If Me.txtDescuento.Enabled = True Then
				Me.txtDescuento.Focus()
			Else
				Me.txtCantidad.Focus()
			End If

		Catch ex As System.Exception
			MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
		End Try
	End Sub
	Private Function QuitarIVI(ByVal precio As Double) As Double
		If Me.txtImpVenta.Text <> 0 Then
			precio = precio / ((Me.txtImpVenta.Text + 100) / 100)
		End If
		Return precio
	End Function
	Sub txtDescuentoEnter()
		Try
			If Me.txtDescuento.Text = "" Then
				MsgBox("No puede estar vac�o", MsgBoxStyle.Exclamation)
				Me.txtDescuento.Text = "0"
				Exit Sub
			Else
				If (CDbl(Me.txtDescuento.Text) > 100) Then Me.txtDescuento.Text = 100
			End If
			BanderaImpuesto = True
			Me.Calculos_Articulo()
			If mensaje <> "" Then
				MsgBox(mensaje, MsgBoxStyle.Information, "Seepos")
				mensaje = ""
			End If

			If Not IsDBNull(Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").Current("Descripcion")) Then
				Me.BindingContext(Me.DataSet_Facturaciones, "Ventas.VentasVentas_Detalle").EndCurrentEdit()
				Calcular_totales()
			End If
			Me.txtCantidad.Focus()
		Catch ex As Exception

		End Try
	End Sub
	Sub PedirPrecio()
		Try
			Dim frmSolicitarPrecio As New frmPedirPrecio
			AddOwnedForm(frmSolicitarPrecio)
			frmSolicitarPrecio.PrecioBase = Convert.ToDouble(txtPrecioUnit.Text)
			frmSolicitarPrecio.ShowDialog()
			txtFlete.Text = 0
			txtOtros.Text = 0

		Catch ex As Exception
			MsgBox("Error al pedir precio. " & ex.Message, MsgBoxStyle.Critical)
		End Try
	End Sub
	Private Sub Exoneracion_por_Registro()

		Dim Exoneracion As Double
		Dim MontoImpuesto As Double
		Dim FrmExonera As New FrmExoneracion

		Try

			FrmExonera.ShowDialog()
			Exoneracion = FrmExonera.Monto
			ImpuestoIV = 13
			ImpuestoIV = (ImpuestoIV - (ImpuestoIV * Exoneracion) / 100)
			MontoImpuesto = ((CDbl(txtSubtotal.Text) - CDbl(Me.txtmontodescuento.Text)) * CDbl(ImpuestoIV / 100))
			If Exoneracion > 100 Then
				MsgBox("El monto digitado no puede ser mayor a un: 100%", MsgBoxStyle.Information, "Porcentaje Exoneraci�n Impuesto")
				Exit Sub
			End If
			If FrmExonera.DialogResult = DialogResult.OK Then
				For I As Integer = 0 To Me.DataSet_Facturaciones.Ventas_Detalle.Count - 1

					If Me.DataSet_Facturaciones.Ventas_Detalle.Rows(I).Item("CODIGO") = Me.TxtCodArticulo.Text Then

						Me.DataSet_Facturaciones.Ventas_Detalle.Rows(I).Item("IMPUESTO") = ImpuestoIV
						Me.DataSet_Facturaciones.Ventas_Detalle.Rows(I).Item("MONTO_IMPUESTO") = MontoImpuesto
					End If
				Next
				MsgBox("Se exonero un " & Exoneracion & "%, del Impuesto venta " & ImpuestoIV & ", monto impuesto " & MontoImpuesto & ", del Articulo " & Me.TxtBarras.Text, MsgBoxStyle.Information)
				Calcular_Totales_Cotizacion()
			Else
				Exit Sub
			End If
			FrmExonera.Dispose()

		Catch ex As System.Exception
			MsgBox(ex.Message, MsgBoxStyle.Information, "Atenci�n...")
		End Try

	End Sub
End Class

